//REQUIRE

	var appPersistence			= KnuddelsServer.getPersistence();

var App = (new function() {
 KnuddelsServer.getUserByNickname = function(nick) {
	 try {var userid = KnuddelsServer.getUserId(nick);
		var user = KnuddelsServer.getUser(userid);return user;
		} catch (e) {return null;
		}
	};

	var channel					= KnuddelsServer.getChannel().getChannelName();
	var channl					= KnuddelsServer.getChannel();
	var channelConfiguration	= channl.getChannelConfiguration();
	var channelRights			= channelConfiguration.getChannelRights();
	var channelOwners			= channelRights.getChannelOwners();
	var owners					= channl.getChannelConfiguration().getChannelRights().getChannelOwners();
	var owner					= owners[0];
	var chanown 				= channelOwners.toString();
	var chown					= chanown.split("[");
	var cown					= chown[1].replace("]", "");
	var Bot						= KnuddelsServer.getDefaultBotUser();
	var isAdmin					= user.isInTeam("Admin");
	var isHzmApp				= appPersistence.getObject('isHzmApp', ['']);
	var AcceptedNicks			= appPersistence.getObject('CMs', ['Holgi']);
	var ChannelClosed			= appPersistence.getObject('ChanKey', ['opened']);
	var StressUser				= appPersistence.getObject('stressuserliste', ['']);
	var VCMs					= appPersistence.getObject('vcms', ['']);
	var SonderUser				= appPersistence.getObject('sonderuser', ['']);
	var HZAs					= appPersistence.getObject('hzae', ['']);
	var DevUser					= appPersistence.getObject('devuser', ['Pega16']);
	var whiteyellowcard			= appPersistence.getObject('whiteyellow', ['']);
	var yellowcard				= appPersistence.getObject('yellow', ['']);
	var yellowredcard			= appPersistence.getObject('yellowred', ['']);
	var wineredcard				= appPersistence.getObject('winered', ['']);
	var redcard					= appPersistence.getObject('red', ['']);
	var cancme					= appPersistence.getObject('cancme', ['']);
	var isbll					= appPersistence.getObject('blacklisted');
	var restriction				= appPersistence.getObject('restrictiontext');
	var closedtext				= appPersistence.getObject('closedtext');
	var iswarned				= appPersistence.getObject('iswarned');
	var greetings				= appPersistence.getObject('greetings', ['on']);
	var devgreeting				= appPersistence.getObject('devgreeting', ['on']);
	var schriftart				= appPersistence.getObject('changefont');
	var discoconfetti			= appPersistence.getObject('discoconfetti', 'off');
	var blloo					= appPersistence.getObject('blacklistonoff', ['off']);
	var matchicofunc			= appPersistence.getObject('matchicobeta');

	var DEVCHANNEL		= "/Knuddel-Bank";
	var DEMOCHANNEL		= "/Sprechstunde";
	var BETACHANNEL		= "/CMG";

	/**
	 * Liefert eine Map aller registrierten Chatbefehle in diesem Channel
	 * @property {function}
	 * @name Channel#_getRegisteredChatCommandNames
	 * @param {Boolean} includeSelf
	 * @return {Object}
	 */
	Object.defineProperty(Channel.prototype,'_getRegisteredChatCommandNames', {

	    value: function _getRegisteredChatCommandNames(includeSelf) {
	        includeSelf = typeof includeSelf === 'undefined' ? false : includeSelf;
	        var instances = KnuddelsServer.getAppAccess().getAllRunningAppsInChannel(includeSelf);

	        var commands = {};

	        instances.forEach(function(instance) {
	            var cmds = instance.getRegisteredChatCommandNames();
	            cmds.forEach(function(cmd) {
	                commands[cmd.toLowerCase()] = instance;
	            })
	        });
	        return commands;
	    }
	});
	
	require('framework/KFramework.min.js');
	require('Commands.js');
	require('User.js');
	require('versionCompare.js');
	require('extensions/greetings.js');
	require('extensions/sysmsg.js');
	require('extensions/ESA.js');
	require('ver.js');

};