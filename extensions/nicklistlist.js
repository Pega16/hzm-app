					if(pspl[0] != undefined && pspl[1] != undefined && pspl[1].toLowerCase() == "list"){
						
						if(pspl[0].toLowerCase() == "secki") {
							if(!user.isAppDeveloper()) {
								user.sendPrivateMessage('Secki ist ein amtierender Admin. Aus diesem Grunde werden hier keinerlei Nicklisten von diesem Nutzer angezeigt.');
								return;
							}
						}
						
						if(pspl[0].toLowerCase() == "administrativ"){
							if(!user.isAppDeveloper() && user.getNick() != "Seelenverbrannt"){
								user.sendPrivateMessage('Diese Liste ist rein administrativ zu Testzwecken erstellt. Sie hat keinerlei Inhalt, sondern testet nur die Funktionalität und sammelt die Fehlermeldungen, damit der Programmierer der App diese dann beheben kann.°##°_°BB°Sollten Fehler auftauchen, so melde diese bitte trotz Protokollierung innerhalb dieser Funktion per °>/m Pega16|/m Pega16<°, damit eine schnelle Bearbeitung erfolgen kann. Vielen Dank!_°r°');
								return;
							}
						}
						
						var nicklist = appPersistence.getObject('nl-'+pspl[0], []);
						var chknl = appPersistence.getObject('chknl-'+pspl[0], []);
						var showchknl = appPersistence.getObject('chknl-'+pspl[0]);
						var existendnicklists = appPersistence.getObject('extnl', []);
						
						
//Sammel die Nicks für die Nickliste
						if(user.isAppDeveloper()){
							var msg = '_°BB°Übersicht der Nicks von '+pspl[0]+': °r°_°#°';
							for(i=0;i<nicklist.length;i++){
								msg=msg + i+' | '+ nicklist[i] +' | °R°_°>Entfernen|/nicklist '+pspl[0]+':del:'+nicklist[i]+'<°_°r° °#°'
							}
							msg = msg+ '°#° °[000,175,225]°_°>Nick zu '+pspl[0]+'s Nicks hinzufügen|/tf-overridesb /nicklist '+pspl[0]+':add:[Nick1,Nick2,Nick3,Nick4,...]<°_°r°';
						}
						
						if(!user.isAppDeveloper()){
							var msg = '_°BB°Übersicht der Nicks von '+pspl[0]+': °r°_°#°';
							for(i=0;i<nicklist.length;i++){
								msg=msg + i+' | '+ nicklist[i] +' °#°'
							}
							msg = msg+ '°#° °[000,175,225]°_°>Nick zu '+pspl[0]+'s Nicks hinzufügen|/tf-overridesb /nicklist '+pspl[0]+':add:[Nick1,Nick2,Nick3,Nick4,...]<°_°r°';
						}

//Nun sammel die Check-Nickliste zu Nick.
						if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) > -1){
							var chkmsg = '_°BB°Liste der noch zu prüfenden Nicks von '+pspl[0]+':°r°_°##°"';
							for(i=0;i<chknl.length;i++){
								chkmsg=chkmsg + i+' | '+ chknl[i] +' | °R°_°>Trifft zu|/nicklist '+pspl[0]+':add:'+chknl[i]+'<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
							}
						} else 	{
							var chkmsg = '_°BB°Liste der noch zu prüfenden Nicks von '+pspl[0]+':°r°_°##°"';
							for(i=0;i<chknl.length;i++){
								chkmsg=chkmsg + chknl[i] +', '
							}
						}
						
//Nicklist leer?
						if(nicklist.length < 0){
							user.sendPrivateMessage(pspl[0]+' hat noch keine eingetragenen Nicks, daher kann ich Dir auch keine anzeigen! °R°_°>Jetzt hinzufügen|/tf-overridesb /nicklist '+pspl[0]+':add:[James]<°_°r°');

//Wenn Nicklist leer lösche den Nick aus der Liste der existenten Nicklists (sofern vorhanden)
							if(existendnicklists.indexOf(pspl[0]) > -1){
								existendnicklists.splice(existendnicklists.indexOf(pspl[0]), 1);
								appPersistence.setObject('extnl', existendnicklists);
//								return;
							}
						}
//ENDE dieser Abfrage.
					
//Zeige mir nun alle existenten Nicklisten											
						if(user.isAppDeveloper() || appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0 || appPersistence.getObject('vcms', []).indexOf(user.getNick()) >= 0){
							var existendnicklists = appPersistence.getObject('extnl', []);
							var messg = '_°BB°Existierende Nicklists:_°r°°##°';
						
							for(i=0;i<existendnicklists.length;i++){
							
								messg=messg + i+' | °>'+ existendnicklists[i] +'|/nicklist '+ existendnicklists[i] +':list<° °r° °#°';
							}
							user.sendPrivateMessage(messg);
						
						}
					
						return;
					}