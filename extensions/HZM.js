function HZM() {
this.super();
}
Object.extends(HZM, Module);

HZM.prototype.mayJoinChannel = function (user) {
    if(user._isChannelOwner()) {
        return ChannelJoinPermission.accepted();
    }
    
	var closed = this.getPersistence().hasNumber("Closed");
	
	if(closed) {
		if(!user._isAppManager()) {
			if(this.getPersistence().getString("Closed", "") == "") {
				return ChannelJoinPermission.denied('_Der Channel ist aktuell °R°GESCHLOSSEN!°r°°#°°#°bitte versuche es später erneut._');
			} else {
				var text = this.getPersistence().getString("Closed", "");
				return ChannelJoinPermission.denied('_'+ text +'_');
			}
		}
	} 
	
	var usual = this.getPersistence().hasNumber("Usual");
	
	if(usual) {
		if(!user._isAppManager()) {
			// Prüfung auf indexOf
			var HZMarr = this.getPersistence().getObject("HZM_ARR", []);
			var CMarr = this.getPersistence().getObject("CM_ARR", []);
			var VCMarr = this.getPersistence().getObject("VCM_ARR", []);
			var Gastarr = this.getPersistence().getObject("Gast_ARR", []);
			var role = this.getPersistence().getUserNumber(user, "Role", 0);
			
			if(HZMarr.indexOf(user.getUserId()) != -1){
				return ChannelJoinPermission.accepted();
			} else if(CMarr.indexOf(user.getUserId()) != -1){
				return ChannelJoinPermission.accepted();
			} else if(VCMarr.indexOf(user.getUserId()) != -1){
				return ChannelJoinPermission.accepted();
			} else if(Gastarr.indexOf(user.getUserId()) != -1){
				return ChannelJoinPermission.accepted();
			} else if(role == 1 && role == 2 && role == 3 && role == 4){
				return ChannelJoinPermission.accepted();
			} else {
				return ChannelJoinPermission.denied('Hallo '+user.getNick()+'°#°°#°°R°_Dieser Channel wurde durch den Channeleigentümer so eingestellt, dass nur eine begrenzte Nutzergruppe den Channel betreten kann.°#°°#°Diese Einstellung wird häufig von Hauptzuständigen Admins und Ehrenmitgliedern verwendet, um die internen HZA/E-CM - Räumlichkeiten abzusichern.°r°°#°°#°Solltest Du der Meinung sein, diese Meldung irrtümlich zu bekommen, so schreibe dem Channel-Eigentümer eine /m.°#°°#°Vielen Dank für Dein Verständnis.')
			}
			}
		}
	};
	
	// Funktion zur auflistung aller User die betreten dürfen
	
//};

HZM.prototype.onUserJoined = function (user){
	var obj = this.getPersistence().getObject("HZM_ARR", []);
	if(obj.indexOf(user.getUserId()) != -1) {
		var ind = obj.indexOf(user.getUserId());
		var tmp_arr = Array.remove(obj, ind);
		this.getPersistence().setObject("HZM_ARR", tmp_arr);
		this.getPersistence().setUserNumber(user, "HZM", 1);
		this.getPersistence().setUserNumber(user, "Role", 4);
	}
	var role = this.getPersistence().getUserNumber(user, "Role", 0);
	var icon = "";
	var length = 35;
	if(role == 1){
		// User ist CM
		icon = KnuddelsServer.getFullImagePath('CM.png');
	} else if(role == 2) {
		// VCM
		icon = KnuddelsServer.getFullImagePath('VCM.png');
	} else if(role == 3) {
		// GAST
		icon = KnuddelsServer.getFullImagePath('GAST.png');
	} else if(role == 4) {
		// HZM
		icon = KnuddelsServer.getFullImagePath('HZM.png');
	} else if(role == 5) {
		//ChannelOwner
		length = 25;
		icon = KnuddelsServer.getFullImagePath('CO.png');
	} else if(role == 6) {
		//Dev
		icon = KnuddelsServer.getFullImagePath('DEV.png');
	}
	user._addNicklistIconWithId(icon, length, "Role")
};

HZM.prototype.cmdHzm = function(user,params, func){
	if(!this.isHZM(user)) {
		user._sendPrivateMessage("Du darfst diese Funktion nicht nutzen.");
		return;
	}
	
	if(params.trim() == "") {
		var msg = "Bitte nutze den Befehl wie folgt:°#r°";
		if(user._isChannelOwner()) {
			msg += "_°>/"+func+" close|/"+func+" close<°_ -> Schließt den Channel.°#r°";
			msg += "_°>/"+func+" usual|/"+func+" usual<°_ -> Öffnet den Channel für CMs, HZA/E und Gäste.°#r°";
			msg += "_°>/"+func+" open|/"+func+" open<°_ -> Öffnet den Channel für Alle.°#r°";
			msg += "_°>/"+func+" chanstate|/"+func+" chanstate<°_ -> Zeigt den Aktuellen Status des Channels an.°#r°";
			msg += "_/"+func+" cmt:TEXT_ -> Setzt TEXT als Channelsperrtext für alle die den Channel im CM-Modus (/hzm usual) nicht betreten dürfen.°#r°";
			msg += "_/"+func+" channelclosed:TEXT_ -> Setzt TEXT als Channelsperrtext, der angezeigt wird, wenn der Channel aufgrund eines Gespräches geschlossen ist.°#r°";
		}
		if(user._isAppManager()) {
			msg += "_/"+func + " +hzm:NICK_ -> Fügt Nick als HZM hinzu.°#r°";
			msg += "_/"+func + " -hzm:NICK_ -> Entfernt Nick als HZM.°#r°";
			msg += "_°>/"+func + " listhzm|/"+func +" listhzm<°_ -> Zeigt eine Übersicht der gespeicherten HZM.°#r°";
		}
		if(this.isHZM(user)) {
			msg += "_/"+func + " +cm:NICK_ -> Fügt Nick als CM hinzu.°#r°";
			msg += "_/"+func + " -cm:NICK_ -> Entfernt Nick als CM.°#r°";
			msg += "_°>/"+func + " listcm|/"+func +" listcm<°_ -> Zeigt eine Übersicht der CMs.°#r°";
			msg += "_/"+func + " +vcm:NICK_ -> Fügt Nick als VCM hinzu.°#r°";
			msg += "_/"+func + " -vcm:NICK_ -> Entfernt Nick als VCM.°#r°";
			msg += "_°>/"+func + " listvcm|/"+func +" listvcm<°_ -> Zeigt eine Übersicht der VCMs.°#r°";
			msg += "_°>/"+func + " disco|/"+func +" disco<°_ -> Discokugel zum Ausklang einer CMV.°#r°";
			msg += "_°>/"+func + " confetti|/"+func +" confetti<°_ -> Confetti zum Ausklang einer CMV.°#r°";
		}
		
		user._sendPrivateMessage(msg);
		return;
	}
	
	var ind = params.indexOf(":");
    if (ind == -1) {
        var action = params.substring(0).trim().toLowerCase();
        var command = "";
    } else {
        var action = params.substring(0, ind).trim().toLowerCase();
        var command = params.substr(ind+1).trim();
    }
    if(action == "+hzm") {
    	this.addHZM(user, command);
    	return;
    } else if(action == "-hzm") {
    	this.removeHZM(user, command);
    	return;
    } else if(action == "listhzm") {
    	this.showHZM(user);
    	return;
    } else if(action == "+cm") {
    	this.addCM(user, command);
    	return;
    } else if(action == "-cm") {
    	this.removeCM(user, command);
    	return;
    } else if(action == "listcm") {
    	this.showCM(user);
    	return;
    } else if(action == "+vcm") {
    	this.addVCM(user, command);
    	return;
    } else if(action == "-vcm") {
    	this.removeVCM(user, command);
    	return;
    } else if(action == "listvcm") {
    	this.showVCM(user);
    	return;
    } else if(action == "close") {
    	this.CloseChannel(user);
    	return;
    } else if(action == "usual") {
    	this.UsualChannel(user);
    	return;
    } else if(action == "open") {
    	this.OpenChannel(user);
    	return;
    } else if(action == "chanstate") {
    	this.ChanState(user);
    	return;
    } else if(action == "disco") {
    	this.ChanDisco(user);
    	return;
    } else if(action == "confetti") {
    	this.Confetti(user);
    	return;
    } else if(action == "cmt") {
    	this.cmt(user, command);
    	return;
    } else if(action == "closedtext") {
    	this.closedtext(user, command);
    	return;
    }
};

HZM.prototype.ChanState = function(user) {
	if(!user._isChannelOwner()) {
		user._sendPrivateMessage("Du darfst die funktion nicht ausführen.");
		return;
	}
	if(this.getPersistence().hasNumber("Closed")) {
		user._sendPrivateMessage("Channel geschlossen");
		return;
	}
	if(this.getPersistence().hasNumber("Usual")) {
		user._sendPrivateMessage("Channel im CM-Modus");
		return;
	}
	user._sendPrivateMessage("Channel ist offen");
};


HZM.prototype.CloseChannel = function(user) {
	if(!user._isChannelOwner()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	this.getPersistence().setNumber("Closed", 1);
	this.getPersistence().deleteNumber("Usual");
	user._sendPrivateMessage('Ich habe den Channel geschlossen °R°_°>Channel CM-Einstellung|/hzm usual<°_ oder °[255,49,49]°_°>Channel für jedermann öffnen|/hzm open<°_°r°');
	App.bot.sendPublicMessage('°R°_*INFO*_§ Ich habe den Channel geschlossen. _Ab sofort kann °R°NIEMAND_§ den _°R°Channel betreten._°r°');

};


HZM.prototype.UsualChannel = function(user) {
	if(!user._isChannelOwner()) {
		user._sendPrivateMessage("Du darfst die funktion nicht ausführen.");
		return;
	}
	this.getPersistence().deleteNumber("Closed");
	this.getPersistence().setNumber("Usual", 1);
	user._sendPrivateMessage('Ich habe den Channel wieder geöffnet (CM-Einstellung!) °R°_°>Channel SCHLIEßEN|/hzm close<°_ oder °[255,49,49]°_°>Channel für jedermann öffnen|/hzm open<°_°r°');
	App.bot.sendPublicMessage('_°R°*INFO*_§ Der _Channel_ befindet sich nun im °R°_CM-Modus._°r°');
	
};


HZM.prototype.OpenChannel = function(user) {
	if(!user._isChannelOwner()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	this.getPersistence().deleteNumber("Closed");
	this.getPersistence().deleteNumber("Usual");
	user._sendPrivateMessage('Ich habe den Channel nun für _jedermann_ geöffnet. °R°_°>Channel CM-Einstellung|/hzm usual<°_ oder °[255,49,49]°_°>Channel SCHLIEßEN|/hzm close<°_°r°');
	App.bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
};

HZM.prototype.showHZM = function(user) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	var users = [];
	
	var key = this.getPersistence().getkey("HZM");
	var managed = UserPersistenceNumbers.getSortedEntried(key);
	for(var k in managed) {
		var mnguser = managed[k];
		if(mnguser.getValue() == 1 && users.indexOf(mnguser.getUserId()) == -1) {
			users.push(mnguser.getUserId()); 
		}
	}
	var unmanaged = this.getPersistence().getObject("HZM_ARR", []);
	for(var k in unmanaged) {
		users.push(unmanaged[k]);
	}
	
	var msg = "°RR°_Folgende User sind derzeit als HZM gespeichert:°#r°";
	msg += users.join(", ");
	
	user._sendPrivateMessage(msg);
};

HZM.prototype.addHZM = function(user, params) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	
	var exists = KnuddelsServer.getUserAccess().exists(params);
	
	if(!exists) {
		user._sendPrivateMessage("Den von dir angegebenen User gibt es nicht.");
		return;
	}
	var tUserId = KnuddelsServer.getUserAccess().getUserId(params);
	var check = KnuddelsServer.getUserAccess().mayAccess(tUserId);
	if(check) {
		var tUser = KnuddelsServer.getUserAccess().getUserById(tUserId);
		this.getPersistence().setUserNumber(tUser, "HZM", 1);
		this.getPersistence().setUserNumber(tUser, "Role", 4);
	} else {
		var obj = this.getPersistence().getObject("HZM_ARR", []);
		obj.push(tUserId);
		this.getPersistence().setObject("HZM_ARR", obj);
	}
	user._sendPrivateMessage("Du hast soeben $NICK als HZM hinzugefügt.".formater({
		NICK: KnuddelsServer.getUserAccess().getNick(tUserId)}));
	};
	

HZM.prototype.removeHZM = function(user, params) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	
	var exists = KnuddelsServer.getUserAccess().exists(params);
	
	if(!exists) {
		user._sendPrivateMessage("Den von dir angegebenen User gibt es nicht.");
		return;
	}
	var tUserId = KnuddelsServer.getUserAccess().getUserId(params);
	var check = KnuddelsServer.getUserAccess().mayAccess(tUserId);
	if(check) {
		var tUser = KnuddelsServer.getUserAccess().getUserById(tUserId);
		this.getPersistence().setUserNumber(tUser, "HZM", 0);
		this.getPersistence().deleteNumber(tUser, "Role");
	} else {
		var obj = this.getPersistence().getObject("HZM_ARR", []);
		var ind = obj.indexOf(tUserId);
		if(ind != -1) {
			var tmp_obj = Array.remove(obj, ind);
			this.getPersistence().setObject("HZM_ARR", tmp_obj);
		}
	}
	user._sendPrivateMessage("Du hast soeben $NICK als HZM entfernt.".formater({
		NICK: KnuddelsServer.getUserAccess().getNick(tUserId)}));
	};

HZM.prototype.showCM = function(user) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	var users = [];
	
	var key = this.getPersistence().getkey("CM");
	var managed = UserPersistenceNumbers.getSortedEntried(key);
	for(var k in managed) {
		var mnguser = managed[k];
		if(mnguser.getValue() == 1 && users.indexOf(mnguser.getUserId()) == -1) {
			users.push(mnguser.getUserId()); 
		}
	}
	var unmanaged = this.getPersistence().getObject("CM_ARR", []);
	for(var k in unmanaged) {
		users.push(unmanaged[k]);
	}
	
	var msg = "°RR°_Folgende User sind derzeit als CM gespeichert:°#r°";
	msg += users.join(", ");
	
	user._sendPrivateMessage(msg);
};

HZM.prototype.addCM = function(user, params) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	
	var exists = KnuddelsServer.getUserAccess().exists(params);
	
	if(!exists) {
		user._sendPrivateMessage("Den von dir angegebenen User gibt es nicht.");
		return;
	}
	var tUserId = KnuddelsServer.getUserAccess().getUserId(params);
	var check = KnuddelsServer.getUserAccess().mayAccess(tUserId);
	if(check) {
		var tUser = KnuddelsServer.getUserAccess().getUserById(tUserId);
		this.getPersistence().setUserNumber(tUser, "CM", 1);
		this.getPersistence().setUserNumber(tUser, "Role", 1);
	} else {
		var obj = this.getPersistence().getObject("CM_ARR", []);
		obj.push(tUserId);
		this.getPersistence().setObject("CM_ARR", obj);
	}
	user._sendPrivateMessage("Du hast soeben $NICK als CM hinzugefügt.".formater({
		NICK: KnuddelsServer.getUserAccess().getNick(tUserId)}));
	};
	

HZM.prototype.removeCM = function(user, params) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	
	var exists = KnuddelsServer.getUserAccess().exists(params);
	
	if(!exists) {
		user._sendPrivateMessage("Den von dir angegebenen User gibt es nicht.");
		return;
	}
	var tUserId = KnuddelsServer.getUserAccess().getUserId(params);
	var check = KnuddelsServer.getUserAccess().mayAccess(tUserId);
	if(check) {
		var tUser = KnuddelsServer.getUserAccess().getUserById(tUserId);
		this.getPersistence().setUserNumber(tUser, "CM", 0);
		this.getPersistence().deleteNumber(tUser, "Role");
	} else {
		var obj = this.getPersistence().getObject("CM_ARR", []);
		var ind = obj.indexOf(tUserId);
		if(ind != -1) {
			var tmp_obj = Array.remove(obj, ind);
			this.getPersistence().setObject("CM_ARR", tmp_obj);
		}
	}
	user._sendPrivateMessage("Du hast soeben $NICK als CM entfernt.".formater({
		NICK: KnuddelsServer.getUserAccess().getNick(tUserId)}));
	};
	

HZM.prototype.showVCM = function(user) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	var users = [];
	
	var key = this.getPersistence().getkey("VCM");
	var managed = UserPersistenceNumbers.getSortedEntried(key);
	for(var k in managed) {
		var mnguser = managed[k];
		if(mnguser.getValue() == 1 && users.indexOf(mnguser.getUserId()) == -1) {
			users.push(mnguser.getUserId()); 
		}
	}
	var unmanaged = this.getPersistence().getObject("VCM_ARR", []);
	for(var k in unmanaged) {
		users.push(unmanaged[k]);
	}
	
	var msg = "°RR°_Folgende User sind derzeit als CM gespeichert:°#r°";
	msg += users.join(", ");
	
	user._sendPrivateMessage(msg);
};

HZM.prototype.addVCM = function(user, params) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	
	var exists = KnuddelsServer.getUserAccess().exists(params);
	
	if(!exists) {
		user._sendPrivateMessage("Den von dir angegebenen User gibt es nicht.");
		return;
	}
	var tUserId = KnuddelsServer.getUserAccess().getUserId(params);
	var check = KnuddelsServer.getUserAccess().mayAccess(tUserId);
	if(check) {
		var tUser = KnuddelsServer.getUserAccess().getUserById(tUserId);
		this.getPersistence().setUserNumber(tUser, "VCM", 1);
		this.getPersistence().setUserNumber(tUser, "Role", 2);
	} else {
		var obj = this.getPersistence().getObject("VCM_ARR", []);
		obj.push(tUserId);
		this.getPersistence().setObject("VCM_ARR", obj);
	}
	user._sendPrivateMessage("Du hast soeben $NICK als VCM hinzugefügt.".formater({
		NICK: KnuddelsServer.getUserAccess().getNick(tUserId)}));
	};

HZM.prototype.removeVCM = function(user, params) {
	if(!user._isAppManager()) {
		user._sendPrivateMessage("Du darfst die Funktion nicht ausführen.");
		return;
	}
	
	var exists = KnuddelsServer.getUserAccess().exists(params);
	
	if(!exists) {
		user._sendPrivateMessage("Den von dir angegebenen User gibt es nicht.");
		return;
	}
	var tUserId = KnuddelsServer.getUserAccess().getUserId(params);
	var check = KnuddelsServer.getUserAccess().mayAccess(tUserId);
	if(check) {
		var tUser = KnuddelsServer.getUserAccess().getUserById(tUserId);
		this.getPersistence().setUserNumber(tUser, "VCM", 0);
		this.getPersistence().deleteNumber(tUser, "Role");
	} else {
		var obj = this.getPersistence().getObject("VCM_ARR", []);
		var ind = obj.indexOf(tUserId);
		if(ind != -1) {
			var tmp_obj = Array.remove(obj, ind);
			this.getPersistence().setObject("VCM_ARR", tmp_obj);
		}
	}
	user._sendPrivateMessage("Du hast soeben $NICK als VCM entfernt.".formater({
		NICK: KnuddelsServer.getUserAccess().getNick(tUserId)}));
	};

HZM.prototype.isHZM = function(user) {
	if(user._isChannelOwner())
		return true;
	if(user._isAppDeveloper())
		return true;
	if(this.getPersistence().getUserNumber(user, "HZM", 0) == 1)
		return true;
	if(this.getPersistence().getObject("HZM_ARR", []).indexOf(user.getUserId()) != -1)
		return true;
	
	
	return false;
};

HZM.prototype.ChanDisco = function(user,params,command) {
		if(!this.isHZM){
				user._sendPrivateMessage('Tut mir Leid, dies können hier nur die _HZM_.');
				return;
				}
				App.bot.sendPublicMessage('°>{sprite}type:disco<°');
			};
	  
HZM.prototype.Confetti = function(user,params,command) {
				if(!this.isHZM){
				user.sendPrivateMessage('Tut mir Leid, dies können hier nur die _HZM_.');
				return;
				}
				App.bot.sendPublicMessage('°>{sprite}type:confetti<°');
			};


		
HZM.prototype.cmt = function(user,command){
//				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user._isAppManager()){
				user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}
			
					var restriction = this.getPersistence().getObject('cmmode', []);
//					restriction.push('restrictiontext');
					this.getPersistence().setObject('cmmode', command);
					user._sendPrivateMessage('Der angezeigte Sperrtext für nicht zugelassene User (CM-Modus) lautet ab sofort: _' + command +'_');
					return;
			};

		
HZM.prototype.closedtext = function(user,command){
//				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user._isAppManager()){
				user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}
			
					var closedtext = this.getPersistence().getObject('Closed', []);
					this.getPersistence().setObject('Closed', command);
					user._sendPrivateMessage('Der angezeigte Sperrtext für den _geschlossenen Channel_ lautet ab sofort: _' + command +'_');
					return;
			};

		
// HZM.prototype.CMcard	wird noch hinzugefügt.
	/* Kartenquerys
		var whiteyellowcard = appPersistence.getObject('whiteyellow', ['']);
		var yellowcard = appPersistence.getObject('yellow', ['']);
		var yellowredcard = appPersistence.getObject('yellowred', ['']);
		var wineredcard = appPersistence.getObject('winered', ['']);
		var redcard = appPersistence.getObject('red', ['']);
*/


/*HZM.prototype.cmdSul = function(user,params,func){

			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 && this.getPersistence().getObject('CMs', []).indexOf(user.getNick()) < 0 && !user._isAppManager()){
					user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] == "help"){
					user._sendPrivateMessage('Die Syntax für die Stress-User-Listen-Funktion lautet:°#°_/sul set:NICK_ zum setzen von NICK auf die Stress-User-Liste.°#°Mit _/sul_ zeigst Du Alle Nicks auf der Stress-User-Liste an.°#°Mit _/sul delete:NICK_ entfernst Du NICK von der Stress-User-Liste.°#°°#°_Bitte beachten:_°#°das Anzeigen der Stress-User-Liste ist den CMs (und höher) gestattet. Das Verändern jedoch ausschließlich den HZA/E.');
					return;
				}
				
				if(pspl[0] == "set" && pspl[1] != undefined && pspl[1] != " "){
					if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 ){
						user._sendPrivateMessage('Für diese Funktion musst Du die HZA/E - Rolle haben.');
						return;
						}
					
					var userAccess = KnuddelsServer.getUserAccess();
					
					if(userAccess.exists(pspl[1])){
						var users	= this.getPersistence().getObject('stressuserliste', []);
							users.push(pspl[1]);
							this.getPersistence().setObject('stressuserliste', users);
							user._sendPrivateMessage('Ich habe '+ pspl[1] +' auf die Stress-User-Liste gesetzt. °R°_°>Rückgängig|/sul delete:'+ pspl[1] +'<°_°r°');
							return;
					} else {
						user._sendPrivateMessage('Dieser User existiert nicht! -- Hast Du Dich vielleicht vertippt?');
					}
				}	
/*					else {						
					user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
					return;
					}
*/
/*				if(pspl[0] == "delete" && pspl[1] != undefined){
					if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 ){
						user._sendPrivateMessage('Für diese Funktion musst Du die HZA/E - Rolle haben.');
						return;
						}
						
					var lsul = this.getPersistence().getObject('stressuserliste', []);
				
					if(lsul.indexOf(pspl[1]) == -1){
						user._sendPrivateMessage('Der Nutzer '+ pspl[1] +' ist kein StressUser!');
						return;
					}
					
					lsul.splice(lsul.indexOf(pspl[1]), 1);
				
					this.getPersistence().setObject('stressuserliste', lsul);
				
					user._sendPrivateMessage('Ich habe '+ pspl[1] +' von der Stress-User-Liste entfernt. °R°_°>Rückgängig|/sul set:'+ pspl[1] +'<°_°r°');
					return;
				}
				
					if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 && this.getPersistence().getObject('CMs', []).indexOf(user.getNick()) < 0){
						user._sendPrivateMessage('Leider hast Du nicht die Berechtigung für diese Funktion!');
						return;
					}
					
					var lsul = this.getPersistence().getObject('stressuserliste', []);
				
					if(lsul.length <= 0){
						user._sendPrivateMessage('Du hast keine StressUser auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /sul set:[James]<°_°r°');
						return;
					}
					
					var msg = 'Übersicht der StressUser: °#°';
				
					for(i=0;i<lsul.length;i++){
						msg=msg + i+' | '+ lsul[i] +' | °R°_°>Entfernen|/sul delete:'+ lsul[i] +'<°_°r° °#°'
					}
					msg = msg+ '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
						user._sendPrivateMessage(msg);
						return;
			}
		};
*/		
		
//################################################################################
		
		// /cme self:Nummer:Text:Text
		/*
		 /cme self:12:TEXT
		 var ind = params.indexOf(":");
		if (ind == -1) {
	        user._sendPrivateMessage("Bitte nutze die funktion wie folgt /cme self:NUMMER:TEXT");
	        return;
        }
        
        var action = params.substring(0, ind).trim().toLowerCase();
        var command = params.substr(ind+1).trim();
		 self
		 if(action == "self") {
		 var ind2 = command.indexOf(":");
		 if(ind2 == -1) {
	        // Ausgabe /cme self:12
	        var number = parseInt(command);
    	} else {
    		//Speichern
    		// /cme self:10:text
	    	var number = parseInt(command.substring(0, ind).trim());
	        var text = command.substr(ind+1).trim();
	        var pers_cme = this.getPersistence().getUserObject(user, "CME", []);
	        var length = pers_cme.length;
	        if(number > length+1) {
	        user.sendPrivateMessage("Du hast eine Zahl übersprungen");
	        }
	        pers_cme.splice(number-1, 0, text);
	        
		 }
		 */
/*		
HZM.prototype.cmdCme = function(user,params,func){

			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();
			
			if (userAccess.exists(user)){
					var pspl = params.split('^');
					var msg	 = "";
								if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 ){
									user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion. Um diese Funktion nutzen zu können, musst Du als HZA-angelegt sein. Hilfe hierzu findest Du unter _°>/role help|/role help<°_');
									return;
									}				
						if(pspl[0] == "help"){
							user._sendPrivateMessage('Die Syntax für die CME-Funktion lautet:°#°_/cme self^[ZAHL]_°#°zum Ändern Deiner Texte nutze:°#°_/cme self^edit^ZAHL^TEXT_');
							return;
						}
						if(pspl[0] == "self" && pspl[1] != undefined && pspl[1] != "" && pspl[1] > 0 ){
								if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 ){
									user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
									return;
									}
							var cme	= [];
								cme[pspl[1]] = this.getPersistence().getObject('cme'+pspl[1]);
									if(cme[pspl[1]] == null){
										user._sendPrivateMessage('Es wurde noch kein Text für den Absatz '+ pspl[1] +' hinterlegt. Bitte hinterlege hierfür mit _/cme self^edit^'+ pspl[1] +'^TEXT_ einen enstprechenden Absatz.');
									return;
								} else {						
							App.bot.sendPublicMessage(cme[pspl[1]]);
							return;
								}
						}
						
							if(pspl[0] == "self" && pspl[1] == "edit" && pspl[2] != undefined && pspl[2] != "" && pspl[2] > 0 ){
								if(this.getPersistence().getObject('hzae', []).indexOf(user.getNick()) < 0 ){
									user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
									return;
									}
								
								var setcme = [];
								setcme[pspl[2]] = this.getPersistence().setObject('cme'+pspl[2], pspl[3]);
														
							user._sendPrivateMessage('Der Text für den CME Absatz '+ pspl[2] +' wurde nun auf den folgenden gesetzt:');
							user._sendPrivateMessage(pspl[3]);
							return;
							}
							
//							user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!°#°°#°Die Syntax des Befehles bekommst Du angezeigt, wenn Du _°>/cme help|/cme help<°_ eingibst.');
//							return;
						
							if(pspl[0] == "sys" && pspl[1] == "edit" && pspl[2] == "grant" && pspl[3] != undefined && pspl[3] != ""){
								if(user.getNick() != "Pega16"){
									user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
									return;
									}
								
								var userAccess = KnuddelsServer.getUserAccess();
								
								if(userAccess.exists(pspl[3])){
									var users = this.getPersistence().getObject('cancme', []);
									users.push(pspl[3]);
									this.getPersistence().setObject('cancme', users);
									user._sendPrivateMessage('Ich habe '+ pspl[3] +' erlaubt, die System-CME zu nutzen. °R°_°>Rückgängig|/cme sys^edit^remark^'+pspl[3]+'<°_°r°');
									return;
								} else {
									user._sendPrivateMessage('Der Nutzer '+pspl[3]+' existiert nicht. Hast Du Dich vielleicht vertippt?');
								}
							}
							
							if(pspl[0] == "sys" && pspl[1] == "list" && pspl[2] == "granted"){
								if(user.getNick() != "Pega16"){
									user._sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
									return;
								}
								
								var canusecme = this.getPersistence().getObject('cancme', []);
								
								if(canusecme.length <= 0){
									user._sendPrivateMessage('Du hast keine System-CME-berechtigten auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /cme sys^edit^grant^[Pega16]<°_°r°');
									return;
								}
			
								var msg = 'Übersicht der System-CME-berechtigten: °#°';
			
								for(i=0;i<canusecme.length;i++){
								msg=msg + i+' | '+ canusecme[i] + ' | °R°_°>Entfernen|/cme sys^edit^remark^'+canusecme[i]+'<°_°r° °#°'
								}
				
								msg = msg+ '°#° °[000,175,225]°_°>Weiteren Nutzer hinzufügen|/tf-overridesb /cme sys^edit^grant^[Pega16]<°_°r°';
			
								user._sendPrivateMessage(msg);
								return;
							}
							
							if(pspl[0] == "sys" && pspl[1] == "edit" && pspl[2] == "remark" && pspl[3] != undefined && pspl[3] != " "){
								if(user.getNick() != "Pega16"){
									user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
									return;
								}
								
								var canusecme = this.getPersistence().getObject('cancme', []);
								
								if(canusecme.indexOf(pspl[3]) == -1){
									user._sendPrivateMessage('Der Nutzer  ('+pspl[3]+') existiert nicht. Hast Du Dich vielleicht vertippt?');
									return;
								}
								canusecme.splice(canusecme.indexOf(pspl[3]), 1);
								this.getPersistence().setObject('cancme', canusecme);
								user._sendPrivateMessage('Ich habe soeben '+ pspl[3] +' untersagt, die System-CME weiterhin zu nutzen. °R°_°>Rückgängig|/cme sys^edit^grant^'+pspl[3]+'<°_°r°');
								return;
							}
						
							if(pspl[0] == "sys" && pspl[1] != undefined && pspl[1] != "" && pspl[1] > 0 && pspl[1] < 35){
								if(this.getPersistence().getObject('cancme', []).indexOf(user.getNick()) < 0){
								user._sendPrivateMessage('Falls Du die von _°>Pega16|/w Pega16|/m Pega16<°_ geschriebene CME nutzen möchtest, so schreibe bitte eine kurze /m an ihn.');
								return;
								}
								scme = [];
									scme[0] = "";
									scme[1] = "°18°_Herzlich Willkommen zur CM-Einführung. Nocheinmal herzlichen Glückwunsch zur überstandenen Wahl. Für die nächsten zwei Monate sorgt ihr in euren Channeln für Recht und Ordnung. Damit ihr wisst, wie das genau geht, werdet ihr heute kurz in euer Amt eingeführt. Wir werden die CM-Funktionen besprechen und auf bestimmte Problemsituationen eingehen. Zum Schluss können Fragen gestellt werden.#Diese Einführung wird gemacht, damit ihr euer CM-Amt besser ausführt und somit die Fehler, die ihr machen könnt, reduziert werden. Außerdem erhoffen wir uns dadurch eine bessere Zusammenarbeit. Jetzt bitten wir euch noch, aus eurem LC oder sonstigen Channel zu gehen und °[255,49,49]°nur hier §°18°_anwesend zu sein, Danke._";
									scme[2] = "°18°_Ihr seid ein Team. Bitte handelt auch als Team und seid keine Einzelkämpfer. Grade die CMs, die länger im Amt sind, sollten den Neuen helfen. Auch eure Freunde müsst ihr wie normale Chatter behandeln. Auch diese müssen bestraft werden, wenn sie was machen. Falls ihr damit nicht klar kommt, sagt dann einem Kollegen Bescheid, der sich dann um die Sache kümmern soll. Bei Fragen sind wir gerne eure Ansprechpartner. Falls ihr euch nicht 100%ig sicher seid, fragt lieber nach, wir helfen euch gerne. Screens und Beschwerden über Kollegen immer zuerst an uns HZAs. Sollten wir nicht online sein, hilft euch auch jeder andere Admin. Jedoch kümmern wir uns als HZA nicht um alles. Wir haben viele Sachen zu machen und können nicht ständig im Channel hocken und euch über die Schulter schauen. Mit Fragen, Problemen ect.pp. solltet ihr deswegen immer zu uns kommen. Auch können wir nicht riechen, „wann“ ihr eine CMV wollt, deswegen auch hier bitte auf uns zukommen!_";
									scme[3] = "°18°_Fragen können direkt gestellt werden, jedoch klären wir auch am Schluss noch einige auf, die nicht beantwortet wurden. Meistens klärt sich nämlich das meiste während der CMV *gg*. (gegebenenfalls Fragen AUFSCHREIBEN!)_";
									scme[4] = "°18°_Zum Thema Passwortweitergabe & Passwortklau °#°°#° °R°PW- Weitergabe: Strengstens verboten -> sofortiger Amtsentzug! °#°°#°§°18°> Passwortklau oder sonstiges abhanden kommen des PW’s: Sofort einem Admin melden um den Nick zur Sicherheit sperren lassen und dann mit 2. Nick bei uns melden, dass wir mit einem Teamleiter des PW Team reden können für die PW Neusetzung, als CM °[255,49,49]°NIE §°18°einen PW- Antrag machen!_";
									scme[5] = "°18°_Als CM hast du eine gewisse Vorbildfunktion. Deshalb solltest du dich mehr als alle anderen an die Regeln halten. Darüber hinaus darfst du niemanden dafür bestrafen, was du nicht selbst auch machst. Weiterhin darfst Du gegen die AGB und Spielregeln nicht verstoßen. Outen bei Mafia, Cheaten, Chatbotbenutzung und anderes würden zu einem Amtsentzug führen._";
									scme[6] = "°18°_Korrekte Notrufe! °#°°#°Immer wieder bekommen wir Notrufe die absolut unkorrekt sind -grummel-°#°Weiterleiten solltet ihr Notrufe die:°#° >eure Fähigkeiten übersteigen wie HP-Sperren, Nickklau etc. pp. Und die deutlich und freundlich formuliert sind.°#°°#°Nicht Weiterleiten solltet ihr Notrufe wie: °#°   > wann ist mein Foto frei °#°  > Ich hab mal en Problem helf mir °#°   > hier wird Werbung gemacht (ohne Namen und ohne Angabe was für Werbung)°#°  > sonstigen Schwachsinn wie total undeutlich und unfreundlich beleidigende Notrufe °#° > Notrufe die keinen genauen Grund angeben oder bei denen Namen oder Internetseite fehlen. °#°°#° In einen Notruf sollte, was ist wann, wo, wie und mit wem geschehen! Jedoch: Nicht immer gleich zum Admin rennen, bei Fragen die ihr selbst beantworten könnt, wie HP-„Problemchen“ (Verweis auf www.ramnip.net) Fotodinge (z.B. Verweis aufs Forum) Mentorenfragen ect.pp., sondern versuchen einfache Dinge wie diese den Chattern selber zu erklären_";
									scme[7] = "°18°_korrekte Notrufbeispiele:_°#°°#°_Jamesfaking (/afk Passwort) (bitte SOFORT /mute!):_ /notruf -> Beschwerde über andere Chat-Teilnehmer -> Botnutzung / Faking von Nachrichten -> NICK -> OK -> Nachricht(en) markieren -> HOAX-Versuch ChannelZ /afk Passwort öffentlich -> OK -> Notruf absenden°#°°#°_Jamesfaking (/knuddel NickXY) (bitte SOFORT /mute!):_ /notruf -> Beschwerde über andere Chat-Teilnehmer -> Botnutzung / Faking von Nachrichten -> NICK -> OK -> Nachricht(en) markieren -> HOAX-Versuch ChannelZ öffentlich /knuddel Passwort -> OK -> Notruf absenden°#°";
									scme[8] = "°18°_Denkt dran keine CM Infos weiterzugeben, denn das dürft ihr nicht.°#°°#°CM-Infos gehen niemanden außer CMs und Admins etwas an. Gebt diese nicht weiter. Auch dies kann und wird mit Amtsentzug bestraft werden. D.h., es geht auch keinen Chatter etwas an, wie viele mutes oder /cls er schon hat. Oder von welchem CM er gemutet wurde. Wurde er von einem Kollegen gemutet, nennt den Nick nicht dem Chatter, sondern sprecht den Kollegen darauf an dass XY entmutet werden möchte und er sich mit ihm in Verbindung setzen soll. Jeder Chatter sieht selbst in der /mute - Meldung, wer ihn gemutet hat!_";
									scme[9] = "°18°_Ach ja Stresser°#°°#°_Immer wieder & immer öfters tauchen sie auf, die heiß begehrten Stresser xD Am besten nicht aufregen lassen sondern verfahren wie bei jedem anderen die meisten die euch Schläge androhen haben eh nur eine große klappe im Chat °>sm_01.gif<°°#°°#°>Bei Bedrohungen aller Art /admin -> Beschwerde über Chatteilnehmer -> Bedrohung -> NICK -> Drohungen auswählen -> Kurz die Situation schildern, wie es dazu kam -> OK -> Notruf absenden -> OK °#° >Bei Stresser 2 mal Verwarnen ~> /mute°#° >Bei weiterem privat stressen als CM _NIEMALS_ /ig oder /block, sondern /pp NICK   dann Fenster klein machen später einfach schließen. Sollte er mehrere Leute ansprechen, geht /pp nicht mehr. Wenn’s nicht mehr einzudämmen ist, dann an einen Admin wenden._";
									scme[10] = "°18°_Extremistische Äußerungen_°#°°#°Bei solchen Sachen nicht lang rummachen sofort /cl NICK:GRUND + /notruf + _Screen_ wenn ihr euch nicht sicher seid ob der Grund zum cl reicht einfach ein cm kollege oder Admin fragen!  Am besten _/fa aet_._";
									scme[11] = "°18°_Funktion /mute:°#°_ Die /mute-Funktion wird eingesetzt um nervige Leute öffentlich mundtot zu machen. Mit der Funktion /mute !Nick wird dies wieder rückgängig gemacht. Hier einige kleine Einsetzungsmöglichkeiten der /mute-Funktion: °#°1.) Spammen von sinnlosen Texten°#°2.) Verfassungswidrige Äußerungen (siehe auch /cl)°#°3.) Beleidigungen eines Chatter gegenüber anderen (Genaueres nachher)°#°4.) Werbung (Genaueres nachher)°#°°#°Merke: In jedem Fall (bis auf Werbung) ist vorher eine Verwarnung angebracht. Außerdem ist es jedem CM selbst überlassen, ob er erst 2-3 verwarnen möchte oder direkt zum Mute greift. (beachte hierzu Allgemeines, Punkt 3!)_";
									scme[12] = "°18°_Funktion /cmute:_°#°°#° Mit ihr kann man das Farbig-, Groß- und Fettschreiben einer Person verhindert. Rückgängig zu machen mit der Funktion /cmute !Nick. Auch hier einige Einsetzungsmöglichkeiten: °#°1.) Dauerhaftes Farbig-, Groß- oder Fettschreiben°#°2.) Immer wieder großgeschriebene Texte einer Person.°#°3.) Wenn ein Text in Farbe als störend empfunden wird.°#° Merke: Hier sollte eine Verwarnung angebracht sein, wenn jedoch keine Zeit dafür bleibt, darf man auch direkt zum /cmute greifen. (Genaueres ist auch im CM-Forum zu finden)_";
									scme[13] = "°18°_Funktion /cl:_°#°°#° Mit dieser Funktion kann man jemanden des Channels verweisen. Anschließend kann der Gekickte (egal, mit welchem Nick) den Channel für einen Tag nicht mehr betreten. Deshalb sollte mit dieser Funktion am Vorsichtigsten agiert werden. Nun auch hier (lol) jetzt einige Einsetzungsmöglichkeiten:°#°1.) Verfassungswidrige Äußerungen (siehe auch AGBs)°#°2.) James-Faking (bitte erst NACH dem Absetzen des Notrufes!!!)°#°3.) Werbung für sexistische Internet-Seiten.°#°4.) Private Beleidigungen anderer Chatter (sollte überprüft werden)°#°°#° Merke: Bei Punkt 3.) sollte man sofort zum /cl greifen. Bei Punkt 1.) und 4.) sollte vorerst verwarnen._";
									scme[14] = "°18°_Funktion /fa:_°#°°#° Mit dieser Funktion könnt ihr die Liste der Admins/Teamler einsehen, die gerade online sind. Habt ihr Fragen, dann sucht euch von dieser Liste zuerst eure Hauptzuständigen Admins (HZA) heraus, wenn diese nicht on sind, dann hilft jeder andere Admin gerne weiter. Bitte habt auch Geduld, wenn es mal wieder länger dauert bis man einen Admin erwischt, aber man kennt das ja, viele Leute wollen was von wenigen Leuten die etwas höher gestellt sind obwohl sie auch nur 10 Finger haben und nicht springen können ;)_";
									scme[15] = "°18°_Funktion /admin:_°#°°#° Mit dieser Funktion könnt ihr nicht nur einen Adminruf absetzen, sondern auch die letzten sieben Notrufe einsehen, die in eurem Chanel abgesetzt worden sind. (Auch bekannt als ALTES Notrufsystem. Wurde allerdings aufs Neue weitergeleitet!)_";
									scme[16] = "°18°_Funktion /cm text:_°#° Mit dieser Funktion können neue Channeleigene Regeln festgelegt werden. Bevor ihr sie anwendet solltet ihr euren neuen Entwurf auf jeden Fall erst einem eurer HZAs zeigen._";
									scme[17] = "°18°_CM-Info:_°#°°#° Bestimmt ist euch beim Aufruf eines Profils schon die CM-Info aufgefallen. Dort steht drin, wer in welchem Channel von wem gechannellockt, gemutet oder gecolormutet ist, wer einen Gamelock hat oder die Anzahl von /cls und /mutes.°#° Ganz wichtig: Diese Informationen dürft ihr an niemanden rausgeben. Nichteinmal an den Chatter, den sie betreffen. Dasselbe gilt für die /cl, /mute und /fa-Listen. Diese Listen heißen CM-Infos, weil sie nur für CMs bestimmt sind und nicht für Chatter. Wer gegen diese einfache Regel verstößt, kann mit einem Entzug des CM-Amtes rechnen._";
									scme[18] = "°18°_Problemgebiet: Werbung:_°#°°#° MyChannel-Werbung ist das größte momentane Problemgebiet. Bisher haben wir uns darauf geeinigt, bei jeder Werbung sofort zu muten. Die Werber müssten nämlich alle wissen, dass Werbung verboten ist. Man kann es in den AGBs, der /h knigge und sogar in den MyChannel-Infos lesen. Außerdem solltet ihr die Werber per Adminruf (/admin grund) melden. Bitte nennt in diesem Adminruf folgende Faktoren:°#° 1.) Den Nick des Werbungmachers°#°2.) Für welchen Raum Werbung gemacht wurde.°#° Bei Werbung für Sexseiten wie z.B. www.nadine.eu.tp, reicht ein normaler Adminruf. °#° Bei Werbung für Fake-Seiten, Cheater-Seiten und Hacker-Seiten macht ihr dies bitte genauso. Erst /mute, dann /notruf._";
									scme[19] = "°18°_Problemgebiet: verbale Beleidigungen:_°#° Solche Beleidigungen sind ein weiteres Problemgebiet. Hier einige Formen dieses Problemgebietes:°#°1.) Werden wir CMs/Admins öffentlich beleidigt, sollte man damit routiniert umgehen können. Diese Personen sehen es meistens nur darauf ab, Aufmerksamkeit zu erhalten oder euch euer Amt möglichst schwer zu machen. Reagieren sollte man vorerst gar nicht, erst wenn er keine Anzeichen macht aufzuhören, sollte man (geht auch ohne Verwarnung) zum Mute greifen.°#°2.) Private Beleidigungen solltet ihr euch erst gar nicht gefallen lassen, einfach /pp Nick - das Fenster minimieren und das wäre auch geklärt.°#°3.) Beleidigungen gegenüber anderen Chattern, öffentlich oder privat, sollten so schnell wie möglich aus dem Weg geschafft werden. Entweder durch das Anwenden von /ig und/oder /block, oder schlimmstenfalls mit /mute bzw. /cl._";
									scme[20] = "°18°_Jamesfaking ist so ziemlich das schlimmste, was ein Chatter hier Verbrechen kann. Ein Jamesfaker muss sofort gemutet und ein Admin mittels /admin sofort alarmiert werden. Bitte diese User nicht kicken, da wir sonst nicht mehr sehen, ob sie noch mit anderen Nicks  online sind!_";
									scme[21] = "°18°_Wie mache ich einen Screenshot?:_°#°Hier eine kurze Anleitung:°#°1.) Zu der gewünschten Stelle scrollen, die fotografiert werden soll°#°2.) Drücke die Taste Druck (oder auch Print, S-Abf, ...) rechts oben auf der Tasstatur, neben den F-Tasten.°#°3.) Öffne das Programm Paint (Start -> Programme -> Zubehör -> Paint)°#°4.) Drücke die Tasten STRG + V gleichzeitig, wenn nötig lasse Paint das Bild automatisch vergrößern (Abfrage bestätigen).°#°5.) Speichere die Datei als jpeg oder besser noch, als .PNG (Datei -> Speichern unter -> Dateityp: jpeg/jpg oder eben Dateityp PNG).°#°6.) Schicke die Datei immer als Anhang (Das Büroklammernsymbol), niemals direkt in die eMail einfügen!_";
									scme[22] = "°18°_Verhalten gegenüber anderen:_°#° CM-Kollegen: Sicherlich, und das wird nicht zu vermeiden sein, wird es auch einige Uneinstimmigkeiten zwischen euch CMs geben. Ich möchte euch bitten, diese Sachen privat zu klären, denn nach außen müsst ihr als ein Team dastehen, schon mal damit euch niemand gegenseitig ausspielen kann. Des Weiteren darf kein CM von den anderen ausgeschlossen oder heruntergemacht werden. Es heißt nicht umsonst CM-Team. Und bitte helft euch bei Problemen gegenseitig oder fragt im Zweifel einfach uns! Auch wenn ich zum Beispiel nicht da bin, so antworte ich doch meist ziemlich zeitnah auf °>/m<°s, da ich diese auf dem Handy lese… Es ist nämlich nicht schlimm mit einer Situation nicht allein fertig zu werden, im Gegenteil, so wird der Teamgeist gestärkt. °>sm_01.gif<°_";
									scme[23] = "°18°_°#°Chatter/Freunde:_ Es ist klar, dass jeder Chatter gleich zu behandeln ist, egal ob Freund oder Feind. °#°_Newbies:_ Newbies müsst ihr freundlich entgegenkommen, auf ihre Fragen müsst ihr antworten können, dazu seid ihr schließlich da. °>sm_01.gif<°°#°_Admins:_ Hoheiten, behandelt uns wie Gott, auch wenn manche von uns nicht mal halb so gut sind wie ihr. :P Nene, es sollte euch schon bewusst sein, wie ihr euch Admins gegenüber verhält, oder? °>sm_10.gif<°_";
									scme[24] = "°18°_CM-Forum:_°#°°#° Das CM-Forum ist mit der Funktion /forum, alternativ F12, abrufbar. Dort loggt ihr euch unter Login mit dem Chatnick + Passwort ein, klickt auf Übersicht der Foren, auf Administration und dort auf Channelmoderatoren. Dort könnt ihr euch mit anderen CMs über die alltäglichen Problemstellungen austauschen und euch auch wichtige Informationen usw. einholen. Schaut auf jeden Fall mal vorbei. ;) (Forum is einfach toll °>fullheart.png<° *lieb* °>sm_10.gif<°)_";
									scme[25] = "°18°_Knuddelsphilosophie…_°#°°#°Ja, ich weiß, der ein oder andere sagt klar, er könne die Philosophie nicht in der Form unterschreiben… Hier stellt sich aber wiederum die Frage, WARUM dies so ist… Wir werden jetzt kurz die vier Säulen der °>/philosophie<° durchgehen und einige Interpretationen dazu besprechen…°#°°#°°#°_1. Gemeinsam Spaß haben_°#°Was versteht IHR darunter? °#°°R°_Antwort bitte öffentlich im Channel!_°r°_";
									scme[26] = "°18°_Richtig!°#° Knuddels lebt durch die Menschen, die hier ihre Freizeit miteinander verbringen. Wir alle haben unsere eigenen, individuellen Wünsche und Erwartungen, aber auch eine große Gemeinsamkeit: wir wollen zusammen Spaß haben!_";
									scme[27] = "°18°_2.) einander FREUNDLICH begegnen!_°#°Was versteht ihr darunter? °#°°R°_Antwort bitte öffentlich im Channel! °r°_";
									scme[28] = "°18°_RICHTIG! Um dies möglich zu machen, hat das Knuddelsteam im Laufe der Jahre einen Ort mit unzähligen Räumen und Möglichkeiten geschaffen. Diesen Ort gestalten wir mit Begegnungen, Gesprächen, Kreativität und unserer Individualität. Dabei achten wir aufeinander, nehmen Rücksicht und unterstützen uns gegenseitig._";
									scme[29] = "°18°_3.) Sich gegenseitig unterstützen_°#°Was versteht ihr darunter?°#°°R°_Antwort öffentlich im Channel!°r°_";
									scme[30] = "°18°_Richtig!_°#°Knuddels.de lebt durch ihre Mitglieder. Bei Knuddels haben wir die Möglichkeit, jeden Tag neue, tolle Menschen kennenzulernen. Wir lehnen niemanden prinzipiell ab, sondern begegnen uns vorurteilsfrei und freundlich. Unsere Unterstützung gilt insbesondere den Personen, die sich ehrenamtlich mit ganzem Herz für die Community einsetzen._";
									scme[31] = "°18°_Aufeinander Rücksicht nehmen…_°#°Ja, das ist wohl der Punkt, an dem die meisten sich an den Kopf packen und sagen „ihr könnt mich mal“… ABER was steckt tatsächlich dahinter?°#°°R°_Antwort wie immer öffentlich im Channel.°r°_";
									scme[32] = "°18°_Richtig…_°#°Wir verstehen, dass sich jeder von uns frei in seiner eigenen Art präsentiert, ausdrückt und auslebt. Kommt es dennoch zu Konflikten mit anderen Mitgliedern, so versuchen wir immer zuerst sie selber zu lösen, indem wir über leichte/minimale Fehler (Kleinstvergehen) hinwegsehen oder die Personen ignorieren, bzw. sie im /p auf ihren Fehler freundlich hinweisen.°#°Auf diese Weise kann Knuddels das sein, was wir uns alle wünschen: _ein Ort an dem wir uns wohl fühlen, so unterschiedlich wir auch sind._°#°°#°Das ist das Primäre Ziel… Das heißt aber keinesfalls, dass nun jeder alles tun und lassen kann/darf, was er/sie möchte!_";
									scme[33] = "°18°_Und nun wünsche ich euch  noch viel Spass in eurem Amt! Wenn ihr Fragen habt, fragt ruhig, es reißt euch keiner den Kopf ab! Achja.. wenn ihr denkt – wir sind nie online, falsch gedacht °>sm_01.gif<° wir sind oft zwischendurch und abends online, da wir meist viel zu tun haben, solltet ihr aber ein Gespräch wünschen o.ä. werden wir uns natürlich Zeit nehmen °>sm_00.gif<°_";
									scme[34] = "°18°_Kommen wir nun vom trockenen Theorieteil zum CMV-Teil dieser Veranstaltung… Nun seid ihr gefragt… was geht aktuell im Channel ab (ja, einiges weiß ich, bzw. wissen wir schon!)…? Gehen wir mal die wichtigsten Punkte GEMEINSAM durch, damit wir auch gemeinsam eine für alle passende Lösung finden können… °>sm_10.gif<°_";

								user._sendPrivateMessage('_Absatz '+ pspl[1] +' von 34_:');
							
								msg = scme[pspl[1]];
							
								App.bot.sendPublicMessage(msg);
								return;
							}
								else {
							user._sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!°#°°#°Die Syntax des Befehles bekommst Du angezeigt, wenn Du _°>/cme help|/cme help<°_ eingibst.');
							}
				
					}
			};

//######################################################################################		
		
			
HZM.prototype.cmdSofu = function (user, params, func) {
  // Hier definieren wir die Funktion /sofu
    user._sendPrivateMessage("Du hast /sofu eingegeben.");
};

*/





HZM.self = new HZM();