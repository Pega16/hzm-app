var SERVERPATH = "http://www.knuddeliger-tunichtgut.de/KnLoGs/HZMVersion/";
function checkVersion() {
    KnuddelsServer.getExternalServerAccess().getURL(SERVERPATH + 'version.txt', {
        onSuccess: function(responseData, externalServerResponse) {
            if(versionCompare(responseData, extver)) {
                var message = ' Version ' + responseData + ' steht bereit, App wird neugestartet. °>sm_00.gif<°';
                KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateApp(message);
            }
        },
        onFailure: function(responseData, externalServerResponse) { KnuddelsServer.defaultLogger.error('Unable to get Version Information'); }
    });
}