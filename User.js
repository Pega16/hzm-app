User.prototype.addBlacklist = function(gr) {
 var persis = this.getPersistence();
 this.private('Du wurdest gerade auf die Blacklist gesetzt. Grund: '+gr);
 return persis.setNumber("_muted", 1);
};

User.prototype.remBlacklist = function(gr) {
 var persis = this.getPersistence();
 this.private('Du wurdest von der Blacklist runtergesetzt. Grund: '+gr);
 return persis.deleteNumber("_muted");
};

User.prototype.getBlacklist = function() {
 var persis = this.getPersistence();
 return persis.getNumber("_muted",0);
};