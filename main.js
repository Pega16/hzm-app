	// Pega's Deluxe-App ;)
	//REQUIRE
	require('KFramework.min.js');
	require('Commands.js');
	require('User.js');
	require('versionCompare.js');
	// require('games.js')

	var channel = KnuddelsServer.getChannel();
	var channelName = KnuddelsServer.getChannel().getChannelName();
	var channelConfiguration = channel.getChannelConfiguration();
	//	var channelInformation		= channelName.getChannelInformation();
	var channelRights = channelConfiguration.getChannelRights();
	var channelOwners = channelRights.getChannelOwners();
	var chanown = channelOwners.toString();
	var chown = chanown.split("[");
	var cown = chown[1].replace("]", "");
	var logger = KnuddelsServer.getDefaultLogger();
	var srvID = KnuddelsServer.getChatServerInfo().getServerId();

	if (channelName.startsWith("/")) {
		var channeltype = "MyChannel";
	} else {
		var channeltype = "Syschannel";
	}

	var Bot = KnuddelsServer.getDefaultBotUser();
	var bot = KnuddelsServer.getDefaultBotUser();

	/**
	 * LOGGER Part 1
	 */

	/*
	Logevents: 
	['logStart', 'logPause', 'logText', 'logAction', 'logEnd', 'logResume', 'logInfo', 'logJoin', 'logPart']
	*/
	/* Wird benötigt für die Auslagerung */
	var externalServerPath = 'https://www.knuddeliger-tunichtgut.de/KnLoGs/bot.php'; // Pfad zur bot.php
	var externalServerPathUser = 'https://www.knuddeliger-tunichtgut.de/KnLoGs/index.php'; // Pfad zur index.php
	var sendPostMessageOnLogend = true;
	/**
	 * Diese Funktionen müssen außerhalb der App sein, damit sie im postURL Event abrufbar sind.
	 */
	function getUserNicklist(getUserInsteadOfNicklist) {
		// Nutzt alle AppManager für die App als Administratoren
		var getUserInsteadOfNicklist = !!getUserInsteadOfNicklist;
		var appManagers = KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getAppManagers();
		var users = []
		for (var i = 0, l = appManagers.length; i < l; i++) {
			users.push(appManagers[i].getNick());
		}
		// Fügt alle MyChannelmoderatoren zur Liste der befugten Nutzer hinzu (log starten, beenden, adminpasswort angezeigt bekommen).
		// Kommentar entfernen wenn benötigt
		/*
		var MCM = KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelModerators();
		for (var i=0,l=MCM.length;i<l;i++) {
			users.push(MCM[i].getNick());
		}
		*/
		AppDevelopers.asUsers().forEach(function(u){
			users.push(u.getNick());
		});
		/**
		 * Andere Nutzer die hinzugefügt werden sollen, diese können nur durch Bearbeitung des Quellcodes entfernt werden!
		 */
		// var nicks = ['Nick 1', 'Nick 2'];
		var nicks =[];
		if (typeof nicks !== 'undefined') {
			if (nicks.length) {
				for (var i = 0, l = nicks.length; i < l; i++) {
					users.push(nicks[i]);
				}
			}
		}
		/**
		 * Ausgabe
		 */
		if (getUserInsteadOfNicklist) {
			return convertNicklistToUsers(users);
		} else {
			return users;
		}
	}

	function convertNicklistToUsers(nicklistArray) {
		var kUsers = [];
		var userAccess = KnuddelsServer.getUserAccess();
		for (var i = 0, l = nicklistArray.length; i < l; i++) {
			var nick = nicklistArray[i];
			if (userAccess.exists(nick)) {
				var user = userAccess.getUserById(userAccess.getUserId(nick));
				// Hier könnte man prüfen ob Nutzer noch online ist...
				kUsers.push(user);
			}
		}
		return kUsers;
	}
	// Hier wird die /m generiert und abgesendet die beim beenden an ALLE teilnehmenden versendet wird.
	// Das Passwort das sie erhalten, ist nur das zum einsehen der Log.
	function postMessage(logid, nick, password) {
		if (!sendPostMessageOnLogend) return; // frei einstellbar
		var channelName = KnuddelsServer.getChannel().getChannelName();
		// Der Bot selbst taucht auch in der Log auf, aber nur für Statusinformation/Nutzer hinzugefügt beim Start/Restart etc...
		// Wird aber als Rückgabe hier nicht benötigt.
		var me = KnuddelsServer.getDefaultBotUser().getNick();
		var user = getUserByNick(nick);
		if (nick != me) {
			var user = getUserByNick(nick);
			var message = [];
			message.push('Hallo ' + nick + ',');
			message.push(''); // Leerzeile
			message.push('Vielen Dank für die Teilnahme an der Sitzung: ' + logid + '.');
			message.push('');
			message.push('Du kannst die Log online jederzeit _°>hier|' + externalServerPathUser + '<°_ einsehen.');
			message.push('');
			message.push('Zum Anmelden verwende bitte diese Daten:');
			message.push('LogID: ' + logid);
			message.push('Passwort: ' + password);
			message.push('');
			//			message.push('Bedenke: Administratoren dieser Log steht es frei, jegliche Zeile aus der Log zu entfernen.');
			user.sendPostMessage('Log: ' + logid + ' aus dem Channel ' + channelName, message.join('°#°'));
		}
	}

	function getUserByNick(nick) {
		var userAccess = KnuddelsServer.getUserAccess();
		if (userAccess.exists(nick)) {
			return userAccess.getUserById(userAccess.getUserId(nick));
		}
	}
	// Genutzt während der Entwicklung der App. es ist keine Zeile enthalten die davon nutzen macht, hab sie dennoch im Code belassen.
	// wurde genutzt für bot.sendPrivateMessage(Nachrnicht,debugUsers);
	var debugUsers = convertNicklistToUsers(['Son of a Glitch', 'Pega16']);


	//	var isAdmin			= user.isInTeam("Admin");
	var appPersistence = KnuddelsServer.getPersistence();
	var persistence = KnuddelsServer.getPersistence();
	var loggerID = function() {
		return (persistence.hasNumber('_loggerID')) ? persistence.getNumber('_loggerID') : false;
	};
	var loggingSwitch = function() {
		return persistence.hasNumber('_allowlogout');
	};
	var stripkcode = function() {
		return persistence.hasNumber('_stripkcode');
	};
	var loggerPass = function() {
		return persistence.hasString('_loggerPass') ? persistence.getString('_loggerPass') : '';
	};
	var loggerViewPass = function() {
		return persistence.hasString('_loggerViewPass') ? persistence.getString('_loggerViewPass') : '';
	};
	var me = bot.getNick();
	var isHzmApp = appPersistence.getObject('isHzmApp', ['']);
	var AcceptedNicks = appPersistence.getObject('CMs', ['Holgi']);
	var ChannelClosed = appPersistence.getObject('ChanKey', ['opened']);
	var StressUser = appPersistence.getObject('stressuserliste', ['']);
	var VCMs = appPersistence.getObject('vcms', ['']);
	var SonderUser = appPersistence.getObject('sonderuser', ['']);
	var HZAs = appPersistence.getObject('hzae', ['']);
	var DevUser = appPersistence.getObject('devuser', ['Pega16']);
	var whiteyellowcard = appPersistence.getObject('whiteyellow', ['']);
	var yellowcard = appPersistence.getObject('yellow', ['']);
	var yellowredcard = appPersistence.getObject('yellowred', ['']);
	var wineredcard = appPersistence.getObject('winered', ['']);
	var redcard = appPersistence.getObject('red', ['']);
	var cancme = appPersistence.getObject('cancme', ['']);
	var isbll = appPersistence.getObject('blacklisted');
	var restriction = appPersistence.getObject('restrictiontext');
	var closedtext = appPersistence.getObject('closedtext');
	var iswarned = appPersistence.getObject('iswarned');
	var greetings = appPersistence.getObject('greetings', ['on']);
	var devgreeting = appPersistence.getObject('devgreeting', ['on']);
	var schriftart = appPersistence.getObject('changefont');
	var discoconfetti = appPersistence.getObject('discoconfetti');
	var blloo = appPersistence.getObject('blacklistonoff', 'off');
	var matchicofunc = appPersistence.getObject('matchicobeta');
	var botprivate = appPersistence.getObject('botprivate', 'off');

	//Channels
	if (srvID == "knuddelsDEV") {
		var DEVCHANNEL = "/Knuddel-Bank";
	} else {
		var DEVCHANNEL = "no Property";
	}

	if (srvID == "knuddelsDE") {
		var DEMOCHANNEL = "/Sprechstunde";
		var BETACHANNEL = "/CMG";
	} else {
		var DEMOCHANNEL = "no Property";
		var BETACHANNEL = "no Property";
	}


	//escapeHtml
	var entityMap = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;"
	};

	function escapeHtml(string) {
		return String(string).replace(/[&<>"'/]/g, function(s) {
			return entityMap[s];
		});
	};

	//escapeKCode
	//forget it!

	//Versionskennung
	var extver = "4.5";

	var isabRC = "RC";

	if (isHzmApp == "on") {
		if (isabRC == "a") {
			var Version = "HZM-ALPHA-APP"
		} else if (isabRC == "b") {
			var Version = "HZM-BETA-APP"
		} else {
			var Version = "HZM-App"
		}

		if (isabRC == "a") {
			var isVersion = extver + ".0.4.1";
		} else if (isabRC == "b") {
			var isVersion = extver + ".0.4";
		} else {
			var isVersion = extver;
		};
	} else {
		if (isabRC == "a") {
			var Version = "PCC-ALPHA-APP"
		} else if (isabRC == "b") {
			var Version = "PCC-BETA-APP"
		} else {
			var Version = "PublicChatControl-App"
		}

		if (isabRC == "a") {
			var isVersion = extver + ".0.4.1";
		} else if (isabRC == "b") {
			var isVersion = extver + ".0.4";
		} else {
			var isVersion = extver;
		};
	};

	if(Version == "PublicChatControl-App"){
		var installlink = "°##°_°BB°Installationslink: °RR°°>/apps install 30563372.PublicChatControl|/apps install 30563372.PublicChatControl<°°r°_";
	} else {
		var installlink = "°##°_°BB°Installationslink: °RR°°>/apps install 30563372.HZMApp|/apps install 30563372.HZMApp<°°r°_";
	}
	if (HZAs == null) {
		appPersistence.setObject('isHzmApp', 'off');
	};


	function AlphaUpdate() {
		var message = ' ALPHA-UPDATE! - App wird neugestartet.';
		KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateApp(message);

	};

	//				KnuddelsServer.getDefaultBotUser().sendPublicMessage('°#°_responseData:_ ' + responseData +' °#°_extver:_ '+ extver);
	var SERVERPATH = "https://www.knuddeliger-tunichtgut.de/KnLoGs/HZMVersion/";

	function checkVersion() {
		KnuddelsServer.getExternalServerAccess().getURL(SERVERPATH + 'version.txt', {
			onSuccess: function(responseData, externalServerResponse) {
				if (versionCompare(responseData, extver)) {
					var message = ' Version ' + responseData + ' steht bereit, App wird neugestartet. °>sm_00.gif<°';
					KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateApp(message);
				}
			},
			onFailure: function(responseData, externalServerResponse) {
				KnuddelsServer.defaultLogger.error('Unable to get Version Information');
			}
		});
	}

	//	var channelInformation = channel.getChannelInformation();
	//	var channelRights = channel.getChannelRights();
	//	var fullImagePath = KnuddelsServer.getFullImagePath('onair.gif');
	//	var htmlFile = new HTMLFile('Connect.html');
	//	var hangmanF = new HTMLFile('hangman.html');
	//	var overlayContent = AppContent.overlayContent(htmlFile, 900, 800);
	//	var hangman = AppContent.overlayContent(hangmanF, 900, 800);

	var appPersistence = KnuddelsServer.getPersistence();
	var App = (new function() {
		KnuddelsServer.getUserByNickname = function(nick) {
			try {
				var userid = KnuddelsServer.getUserId(nick);
				var user = KnuddelsServer.getUser(userid);
				return user;
			} catch (e) {
				return null;
			}
		};

		/**
		 * Liefert eine Map aller registrierten Chatbefehle in diesem Channel
		 * @property {function}
		 * @name Channel#_getRegisteredChatCommandNames
		 * @param {Boolean} includeSelf
		 * @return {Object}
		 */
		Object.defineProperty(Channel.prototype, '_getRegisteredChatCommandNames', {

			value: function _getRegisteredChatCommandNames(includeSelf) {
				includeSelf = typeof includeSelf === 'undefined' ? false : includeSelf;
				var instances = KnuddelsServer.getAppAccess().getAllRunningAppsInChannel(includeSelf);

				var commands = {};

				instances.forEach(function(instance) {
					var cmds = instance.getRegisteredChatCommandNames();
					cmds.forEach(function(cmd) {
						commands[cmd.toLowerCase()] = instance;
					})
				});
				return commands;
			}
		});

		/* App Events */
		this.onAppStart = function() {

			if (loggerID()) {
				bot.sendPublicMessage('Vielen Dank, dass ihr euch geduldet habt. Log mit der ID ' + loggerID() + ' wird nun fortgesetzt.');
				// Event an Logger senden für Shutdown-Log-Eintrag
				var logg = false;
				var nick = me;
				var text = 'App wurde während der Logsitzung neu gestartet. Erzeuge neue Userliste.';
				var evnt = 'logResume';
				var date = new Date().valueOf();
				addEntry(logg, nick, text, evnt, date);
				addAllHumanUsers(date);
			}
			persistence.deleteNumber('_runningResend');

			var blloo = appPersistence.getObject('blacklistonoff', 'off');
			var isHzmApp = appPersistence.getObject('isHzmApp', 'on');
			var srvID = KnuddelsServer.getChatServerInfo().getServerId();
			var onstartshutdown = appPersistence.getObject('onstartshutdown', 'off');

			if (srvID == "knuddelsDEV" && channel == DEVCHANNEL) {
				new Cronjob('AlphaUpdate', '*/16 * * * *', AlphaUpdate);
			}

			if (srvID != "knuddelsDEV") {
				if (channeltype == "Syschannel") {
					new Cronjob('checkVersion', '0,30 1,2,3,4,5,6,9,10 * * *', checkVersion);
				}
				if (channeltype == "MyChannel") {
					new Cronjob('checkVersion', '0,15,30,45 9,10,11,12,13,14,15,16,17,18 * * *', checkVersion);
				}
			}

			if (blloo == "on") {
				var reservedCommand = KnuddelsServer.getChannel()._getRegisteredChatCommandNames();
				var cmd = "blacklist";
				if (typeof reservedCommand[cmd] !== "undefined") {
					var appInfo = reservedCommand[cmd].getAppInfo();
					var cmdold = cmd;
					cmd = "hzm" + cmd;
					KnuddelsServer.getDefaultLogger().error('Der Command ' + cmdold + ' existiert bereits, umbenannt in ' + cmd);
				}
				App.chatCommands[cmd] = App.cmdblacklist;
			};

			var reservedCommand = KnuddelsServer.getChannel()._getRegisteredChatCommandNames();
			var cmd = "say";
			if (typeof reservedCommand[cmd] !== "undefined") {
				var appInfo = reservedCommand[cmd].getAppInfo();
				var cmdold = cmd;
				cmd = "hzm" + cmd;
				KnuddelsServer.getDefaultLogger().error('Der Command ' + cmdold + ' existiert bereits, umbenannt in ' + cmd);
			}
			App.chatCommands[cmd] = App.cmdsay;

			if (channeltype == "MyChannel") {
				var devp = appPersistence.getObject('devp', 'on');
				if (appPersistence.getObject('devp') == undefined) {
					appPersistence.setObject('devp', 'on');
				}
				var sethzmp = appPersistence.getObject('hzmp', 'off');
				if (appPersistence.getObject('hzmp') == undefined) {
					appPersistence.setObject('hzmp', 'off');
				}
				var setcmp = appPersistence.getObject('cmp', 'on');
				if (appPersistence.getObject('cmp') == undefined) {
					appPersistence.setObject('cmp', 'on');
				}
			} else {
				var devp = appPersistence.getObject('devp', 'off');
				if (appPersistence.getObject('devp') == undefined) {
					appPersistence.setObject('devp', 'on');
				}
				var sethzmp = appPersistence.getObject('hzmp', 'off');
				if (appPersistence.getObject('hzmp') == undefined) {
					appPersistence.setObject('hzmp', 'off');
				}
				var setcmp = appPersistence.getObject('cmp', 'on');
				if (appPersistence.getObject('cmp') == undefined) {
					appPersistence.setObject('cmp', 'on');
				}
			};
			
			if(appPersistence.getObject('devmode', []) == "on"){
					App.chatCommands['role'] = App.cmdrole;
					App.chatCommands['warn'] = App.cmdwarn;
					App.chatCommands['sul'] = App.cmdsul;
					App.chatCommands['cme'] = App.cmdcme;
					App.chatCommands['nicklist'] = App.cmdnicklist;
					App.chatCommands['extlog'] = App.cmdextlog;
					App.chatCommands['whoami'] = App.cmdme;
					App.chatCommands['whoareyou'] = App.cmdyou;
				}

			if (channeltype == "MyChannel") {
					App.chatCommands['role'] = App.cmdrole;
					App.chatCommands['warn'] = App.cmdwarn;
					App.chatCommands['sul'] = App.cmdsul;
					App.chatCommands['cme'] = App.cmdcme;
					App.chatCommands['nicklist'] = App.cmdnicklist;
					App.chatCommands['extlog'] = App.cmdextlog;
				if(extver >= "4.6"){
					App.chatCommands['whoami'] = App.cmdme;
					App.chatCommands['whoareyou'] = App.cmdyou;
				}
			} else {
				App.chatCommands['warn'] = App.cmdwarn;
				App.chatCommands['sul'] = App.cmdsul;
				App.chatCommands['nicklist'] = App.cmdnicklist;
				App.chatCommands['extlog'] = App.cmdextlog;
			};

			if(botprivate != undefined || botprivate != null){
				if (botprivate == "on") {
					App.chatCommands['bp'] = App.cmdbp;
				}
				if (appPersistence.getObject('cmp') == "on") {
					App.chatCommands['cmp'] = App.cmdcmp;
				}
			}
			setTimeout(function() {
				KnuddelsServer.refreshHooks();
			}, 1000)


			var devel = KnuddelsServer.getAppDeveloper();

			if (channeltype == "MyChannel") {
				if(appPersistence.getObject('statsm') == "on"){
					devel.sendPostMessage(Version + ' ' + isVersion + ' in Channel ' + channelName, 'Im Channel _°>' + channelName + '|/go ' + channelName + '<°_ wurde gerade durch _°>' + cown.escapeKCode() + '|/w ' + cown.escapeKCode() + '|/m ' + cown.escapeKCode + '<°_ die _' + Version + '_ in der _Version ' + isVersion + '_ °R°_gestartet_.');
				}
			}

			if (onstartshutdown == "on") {
				Bot.sendPublicMessage('°>{font}FineLinerScript<28BB°_Erfolg!°#°Die App ist nun wieder voll funktionstüchtig.');
			}
		};
		this.onPrepareShutdown = function(secondsTillShutdown) {
			var onstartshutdown = appPersistence.getObject('onstartshutdown', 'off');

			if (onstartshutdown == "on") {
				Bot.sendPublicMessage('°>{font}FineLinerScript<28BB°_ACHTUNG!°#° Die App wird in °RR°' + secondsTillShutdown + ' Sekunden °BB°neu gestartet. Wir danken für euer Verständnis!');
			}

			if (channeltype == "MyChannel") {
				var devel = KnuddelsServer.getAppDeveloper();
				if(appPersistence.getObject('statsm') == "on"){
					devel.sendPostMessage(Version + ' ' + isVersion + ' in Channel ' + channelName, 'Im Channel _°>' + channelName + '|/go ' + channelName + '<°_ wurde gerade durch _°>' + cown.escapeKCode() + '|/w ' + cown.escapeKCode() + '|/m ' + cown.escapeKCode + '<°_ die _' + Version + '_ in der _Version ' + isVersion + ' °R°gestoppt°r°_.');
				}
			}

			if (loggerID()) {
				bot.sendPublicMessage('Ich möchte euch bitten, euch einen kurzen Augenblick zu gedulden. Ich muss mal (hoffentlich nur kurz) verschwinden. (APP\\_SHUTDOWN)');
				// Event an Logger senden für Shutdown-Log-Eintrag
				var logg = false;
				var nick = me;
				var text = 'App wird während der Logsitzung neu gestartet.';
				var evnt = 'logPause';
				var date = new Date().valueOf();
				addEntry(logg, nick, text, evnt, date);
			}
		};

		/**
		 * Die Ausgabe HIER ist die eigengtliche Ausgabe, inkl aller Formatierungen wie sie Knuddels erzeugt und handhabt
		 * Uns genügen aber die Benutzerevents zum Filtern, dort ist direkt hinterlegt, was der Benutzer eingetragen hat zwischen °><°
		 * Hier hat man z. B. anstelle von °>/w NICK<° °>bildpfad<>--<>Nick|/w Nick<°
		 * Gut um vor diversen KCodes zu schützen, aber nicht nützlich für die Logs.
		 */
		this.mayShowPublicMessage = function(publicMessage) {
			var user = publicMessage.getAuthor();
			var text = publicMessage.getText();
			if (loggerID()) {
				var pers = user.getPersistence();
				var logg = pers.hasNumber('_dontLogMe');
				var nick = user.getNick();
				var evnt = 'logText';
				var date = new Date().valueOf();
				addEntry(logg, nick, text, evnt, date);
			}
			
			if (text.toLowerCase().startsWith(me.toString().toLowerCase())) {
				textlog(user, 'textlog'+text.slice(me.toString().length));
			}

			return true;
		}
		this.mayShowPublicActionMessage = function(publicMessage) {
			var user = publicMessage.getAuthor();
			var text = publicMessage.getText();
			if (loggerID()) {
				var pers = user.getPersistence();
				var logg = pers.hasNumber('_dontLogMe');
				var nick = user.getNick();
				var evnt = 'logText';
				var date = new Date().valueOf();
				addEntry(logg, nick, text, evnt, date);
			}
			return true;
		}

		/**
		 * Diese Events zeigen die eigentliche Eingabe vom Nutzer und lassen sich mittels Filter auch leichter Bereinigen.
		 * Es wird hier auch ungültiger KCode angezeigt z. B. °>/w Dieser Nick existiert nicht<°
		 */
		/*	this.onPublicActionMessage = function(publicActionMessage ) {
				user = publicActionMessage.getAuthor();
				if (loggerID()) {
					var pers = user.getPersistence();
					var logg = pers.hasNumber('_dontLogMe');
					var nick = user.getNick();
					var text = publicActionMessage.getText();
					var evnt = 'logAction';
					var date = new Date().valueOf();
					addEntry(logg, nick, text, evnt, date);
				}
			};
			this.onPublicMessage = function(publicMessage) {
				var user = publicMessage.getAuthor();
				var text = publicMessage.getText();
				if (loggerID()) {
					var pers = user.getPersistence();
					var logg = pers.hasNumber('_dontLogMe');
					var nick = user.getNick();
					var evnt = 'logText';
					var date = new Date().valueOf();
					addEntry(logg, nick, text, evnt, date);
				}
				/**
				 * Auslöser für die Nachrichten wenn das erste Wort = Botnick ist.
				 */
		/*		if (text.split(' ',1)[0].toLowerCase() == me.toLowerCase()) {
					textlog(user, text , false);
				}
			};
		*/
		this.onShutdown = function() {
			/* Required if you using Cronjobs! */
			Cron.onShutdown();
		};

		/* User Events */
		//	this.onUserJoined = function(user) {};
		//	this.onUserLeft = function(user) {};

		/* Access Events */
		/*	this.mayJoinChannel = function(user) {}; */
		/*this.mayJoinChannel = function(user)
		{
			var isAdmin = user.isInTeam("Admin")
			var isAppsTeam = user.isInTeam("Apps")

		  if (isAdmin || isAppsTeam)  {
					return ChannelJoinPermission.accepted();
			  }
				else if {
					(user.getNick() == "Seelenverbrannt" || user.getNick() == "freches Kätzchen" || user.getNick() == "lächerlich46m" || user.getNick() == "BestofTennis-Profi")
		  return ChannelJoinPermission.accepted();
		  }
		return ChannelJoinPermission.denied('Tut mir leid... Du stehst leider _nicht_ auf der Gästeliste, da Du nicht zu den betreffenden CMs gehörst...##Dieser Channel ist ein Pilotprojekt, CM-Gespräche, CM-Versammlungen und CM-Einführungen mit Video-Unterstützung stattfinden zu lassen...##Solltest Du auf die Gästeliste wollen, so wende Dich bitte im Chat an _Pega16_.');
		// /restricttext Tut mir leid... Du stehst leider _nicht_ auf der Gästeliste, da Du nicht zu den betreffenden CMs gehörst...°#°°#°°R°_Dieser Channel ist ein Pilotprojekt, CM-Gespräche, CM-Versammlungen und CM-Einführungen mit Video-Unterstützung stattfinden zu lassen..._°r°°#°°#°Solltest Du auf die Gästeliste wollen, so wende Dich bitte im Chat an _Pega16_.    
		// 		return ChannelJoinPermission.denied('Dieser Channel wurde durch den Channelinhaber so eingestellt, dass ausschließlich explizit zugelassene User den Channel betreten können.##Um eine Zutrittsberechtigung zu diesem Channel zu bekommen, wende Dich bitte an den Channeleigentümer.');
		};
		*/
		this.mayJoinChannel = function(user) {

			var appPersistence = KnuddelsServer.getPersistence();
			var isHzmApp = appPersistence.getObject('isHzmApp', []);

				var isAdmin = user.isInTeam("Admin");
	//			var isAppsTeam = user.isInTeam("Apps");
				var isManager = user.isAppManager();
				var FotoTeam = user.isInTeam("Foto");
				var srvID = KnuddelsServer.getChatServerInfo().getServerId();
				var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

				var userkennung = UserID + '.' + srvID;


				var AcceptedNicks = appPersistence.getObject('CMs', ['Holgi']);
				var ChannelClosed = appPersistence.getObject('ChanKey', ['offen']);
				var StressUser = appPersistence.getObject('stressuserliste', ['']);
				var SonderUser = appPersistence.getObject('sonderuser', 'Pega16');
				var HZAs = appPersistence.getObject('hzae', ['']);
				var DevUser = appPersistence.getObject('devuser', ['Pega16']);
				var whiteyellowcard = appPersistence.getObject('whiteyellow', ['']);
				var yellowcard = appPersistence.getObject('yellow', ['']);
				var yellowredcard = appPersistence.getObject('yellowred', ['']);
				var wineredcard = appPersistence.getObject('winered', ['']);
				var redcard = appPersistence.getObject('red', ['']);
				var cancme = appPersistence.getObject('cancme' ['']);
				var iswarned = appPersistence.getObject('iswarned', ['']);
			
			var isbll = appPersistence.getObject('blacklisted');
			var restriction = appPersistence.getObject('restrictiontext');
			var closedtext = appPersistence.getObject('closedtext');
			var greetings = appPersistence.getObject('greetings');
			var devgreeting = appPersistence.getObject('devgreeting');
			var schriftart = appPersistence.getObject('changefont');
			var discoconfetti = appPersistence.getObject('discoconfetti');
			var blloo = appPersistence.getObject('blacklistonoff', 'off');
			var spnickgreeting = appPersistence.getObject(user.getNick());
			var matchicofunc = appPersistence.getObject('matchicobeta');

			if(appPersistence.getObject('contactto') == undefined || appPersistence.getObject('contactto').length <= 0){
				var contact = cown
			} else if(appPersistence.getObject('contactto').length == 1){
				var contact = ""
				var contactpers = appPersistence.getObject('contactto');
				for (i = 0; i < contactpers.length; i++) {
					contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°';
				}
			} else {
				var contact = ""
				var contactpers = appPersistence.getObject('contactto');
				for (i = 0; i < contactpers.length; i++) {
					contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°, oder '
				}
				contact = contact.substr(0, contact.length-7);
			}
			
			var stdcmreason = 'Hallo ' + user.getNick() + '°#°°#°°RR°_Dieser Channel wurde durch den Channeleigentümer so eingestellt, dass nur eine begrenzte Nutzergruppe den Channel betreten kann.°#°°#°Diese Einstellung wird häufig von Hauptzuständigen Admins und Ehrenmitgliedern verwendet, um die internen HZM-CM - Räumlichkeiten abzusichern.°r°°#°°#°Solltest Du der Meinung sein, diese Meldung irrtümlich zu bekommen, so schreibe °RR°' + contact + '°r° eine /m.°#°°#°Vielen Dank für Dein Verständnis.';
			
	/*
			if (isbll == null) {
				var users = appPersistence.getObject('blacklisted', []);
				users.push('James');
				appPersistence.setObject('blacklisted', users);
				users.splice(users.indexOf('James'), 1);		
				appPersistence.setObject('blacklisted', users);
			}
	*/


			if (user.isChannelOwner() && user.isAppManager() && user.isChannelModerator() && App.coDevs.indexOf(userkennung) >= 0 && App.hiddenCoDev.indexOf(userkennung) >= 0) {
				return ChannelJoinPermission.accepted();
			}

				if(blloo == "on"){
					if (isbll != null) {
						if (isbll.indexOf(user.getNick()) >= 0) {
							return ChannelJoinPermission.denied('_Hallo ' + user.getProfileLink() + ',°#° °R°Du wurdest dieses Channels aufgrund Fehlverhaltens DAUERHAFT verwiesen!°#°°#°°B°Für Rückfragen steht Dir der Channeleigentümer (°M°'+contact+') °B°jederzeit per /m zur Verfügung.');
						}
					}
				}

				if (appPersistence.getObject('ChanKey') != undefined && appPersistence.getObject('ChanKey') != null && appPersistence.getObject('ChanKey') != "offen") {
					if (appPersistence.getObject('ChanKey') == "Closed" && App.coDevs.indexOf(userkennung) < 0) {
						if (closedtext == undefined) {
							return ChannelJoinPermission.denied('_Der Channel ist aktuell °RR°GESCHLOSSEN!°r°°#°°#°bitte versuche es später erneut._')
						}
						return ChannelJoinPermission.denied('_' + closedtext + '_');
					}

					if (appPersistence.getObject('ChanKey') == "CM-Modus") {
						if (appPersistence.getObject('CMs') != null && appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0) {
							return ChannelJoinPermission.accepted();
						}

						if (appPersistence.getObject('hzae') != null && appPersistence.getObject('hzae').indexOf(user.getNick()) >= 0) {
							return ChannelJoinPermission.accepted();
						}

						if (SonderUser != null && SonderUser.indexOf(user.getNick()) >= 0) {
							return ChannelJoinPermission.accepted();
						}
						
						if(App.coDevs.indexOf(userkennung) >= 0){
							return ChannelJoinPermission.accepted();
						}

						if (restriction == null) {
							return ChannelJoinPermission.denied(stdcmreason)
						}
						return ChannelJoinPermission.denied('_' + restriction + '_');
					}
					
				} else {
					return ChannelJoinPermission.accepted();
				}
			
		};

		//};
		/* Message Events */
		//this.maySendPublicMessage = function(publicMessage) {};
		this.onPostMessage = function(postMessage) {
			var srvID = KnuddelsServer.getChatServerInfo().getServerId();
			var psender = PostMessage.getAuthor();
			var UserID = KnuddelsServer.getUserAccess().getUserId(psender);
			var userkennung = UserID + '.' + srvID;
			var pfad = KnuddelsServer.getFullImagePath('');
			var hzmico = '°>' + pfad + 'HZM.png<°';
			var devico = '°>' + pfad + 'DEV.png<°';
			//		logger.info(psender + ' sendet folgende Privatnachhricht an den ChannelBot: '+ privateMessage.getText());

			if (channeltype == "MyChannel") {
				var devp = appPersistence.getObject('devp', 'on');
				var sethzmp = appPersistence.getObject('hzmp', 'off');
				var setcmp = appPersistence.getObject('cmp', 'on');
			} else {
				var devp = appPersistence.getObject('devp', 'off');
				var sethzmp = appPersistence.getObject('hzmp', 'off');
				var setcmp = appPersistence.getObject('cmp', 'on');
			};

			var devvar = privateMessage.getText().split(':');
			var devpsenderID = KnuddelsServer.getUserAccess().getUserId(psender);
			var devpsender = KnuddelsServer.getUserAccess().getUserById(devpsenderID);

			if (KnuddelsServer.getUserAccess().exists(devvar[0]) && App.coDevs.indexOf(userkennung) >= 0) {
				let devprivate = devvar[0].toKUser();
				if (devprivate) {
					devprivate.sendPostMessage(devico + 'Antwort auf Deine /m an ' + me, '°BB°_°>_h' + psender + '|/w ' + psender + '|/serverpp ' + psender + '<°:_°r° ' + devvar[1]);
					return;
				}
			}

			let recieversCounter = 0;

			if (srvID == "knuddelsDE") {
				var reciever1 = [
					'Pega16',
					'Pega',
					'Security',
					'Son of a Glitch'
				];
			}
			if (srvID == "knuddelsAT" || srvID == "knuddelsDEV") {
				var reciever1 = [
					'Pega16',
					'Son of a Glitch',
					'DynApp'
				];
			}

			reciever1.forEach(function(pDEVs) {
				let privdev = pDEVs.toKUser();
				if (privdev && privdev.isOnline()) {

					// Counter erhöhen, nur wenn der Nutzer NICHT der Absender ist.
					if (!privdev.equals(psender)) {
						recieversCounter++;
					}
					if ([ClientType.Applet, ClientType.Browser].indexOf(privdev.getClientType()) != -1) {
						privdev.sendPostMessage(devico + 'WL: ' + postMessage.getSubject(), '°>{noppcount}<°_°RR°[ ' + devico + '] °BB°°>_h' + psender + '|/w ' + psender + '|/m ' + psender + '<°:_°r° ' + postMessage.getText() + '     °BB°_[ °RR°°11°°>_hJetzt Antworten|/tf-overridesb /p ' + Bot + ': ' + psender + ': [Text]<°°r° °BB°]_°r°');
					} else {
						privdev.sendPostMessage(devico + 'WL: ' + postMessage.getSubject(), '°>{noppcount}<°_°RR°[ ' + devico + '] °BB°°>_h' + psender + '|/w ' + psender + '|/m ' + psender + '<°:_°r° ' + postMessage.getText());
					}
				}
			})

			// Prüfen ob Counter != 0 ist und absender informieren falls dieser 0 ist.
			if (!recieversCounter) {
				psender.sendPostMessage('Kein Empfänger online', '_°BB°Tut mir Leid, leider ist °RR°kein Empfänger online°BB°. Aus diesem Grund wurde Deine °RR°Nachricht nicht empfangen!°r°_');
			}
		};

		this.onPrivateMessage = function(privateMessage) {
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
			var srvID = KnuddelsServer.getChatServerInfo().getServerId();
			var psender = privateMessage.getAuthor();
			var UserID = KnuddelsServer.getUserAccess().getUserId(psender);
			var userkennung = UserID + '.' + srvID;
			var pfad = KnuddelsServer.getFullImagePath('');
			var hzmico = '°>' + pfad + 'HZM.png<°';
			var devico = '°>' + pfad + 'DEV.png<°';
			//		logger.info(psender + ' sendet folgende Privatnachhricht an den ChannelBot: '+ privateMessage.getText());

			if (channeltype == "MyChannel") {
				var devp = appPersistence.getObject('devp', 'on');
				var sethzmp = appPersistence.getObject('hzmp', 'off');
				var setcmp = appPersistence.getObject('cmp', 'on');
			} else {
				var devp = appPersistence.getObject('devp', 'off');
				var sethzmp = appPersistence.getObject('hzmp', 'off');
				var setcmp = appPersistence.getObject('cmp', 'on');
			};

			var devvar = privateMessage.getText().split(':');
			var devpsenderID = KnuddelsServer.getUserAccess().getUserId(psender);
			var devpsender = KnuddelsServer.getUserAccess().getUserById(devpsenderID);

			if (KnuddelsServer.getUserAccess().exists(devvar[0]) && App.coDevs.indexOf(userkennung) >= 0) {
				let devprivate = devvar[0].toKUser();
				if (devprivate) {
					devprivate.sendPrivateMessage('°BB°_°>_h' + psender + '|/w ' + psender + '|/serverpp ' + psender + '<°:_°r° ' + devvar[1]);
					return;
				}
			}

			//sul per /p an CM senden
			if (App.coDevs.indexOf(userkennung) >= 0 && privateMessage.getText() == "SUL" || psender.isAppManager() && privateMessage.getText() == "SUL" || psender.isChannelOwner() && privateMessage.getText() == "SUL" || psender.isChannelModerator() && privateMessage.getText() == "SUL" || appPersistence.getObject('CMs', []).indexOf(psender) >= 0 && privateMessage.getText() == "SUL" || appPersistence.getObject('hzae', []).indexOf(psender) >= 0 && privateMessage.getText() == "SUL") {
				var lsul = appPersistence.getObject('stressuserliste', []);
				var chksul = appPersistence.getObject('chksul', []);

				if (appPersistence.getObject('hzae', []).indexOf(psender) < 0 && !psender.isChannelOwner() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					if (lsul.length <= 0) {
						psender.sendPrivateMessage('Es sind keine StressUser auf der Liste. Daher kann ich Dir keine anzeigen.');

						if (chksul.length > 0) {
							var chkmsg = '_°BB°Liste der noch für die StressUserListe zu prüfenden Nicks:°r°_°##°"';
							for (i = 0; i < chksul.length; i++) {
								chkmsg = chkmsg + chksul[i] + ', '
							}
							psender.sendPrivateMessage(chkmsg);
						}
						return;
					}
				}
				if (lsul.length <= 0) {
					psender.sendPrivateMessage('Du hast keine StressUser auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /sul add:[James]<°_°r°');
					if (chksul.length > 0) {
						var chksulmsg = '_°BB°Liste der noch für die StressUserListe zu prüfenden Nicks:°r°_°##°"';
						for (i = 0; i < chksul.length; i++) {
							chksulmsg = chksulmsg + i + ' | ' + chksul[i].escapeKCode() + ' | °R°_°>Bestätigen|/sul add:' + chksul[i].escapeKCode() + '<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
						}
						psender.sendPrivateMessage(chksulmsg);
					}
					return;
				}

				if (appPersistence.getObject('hzae', []).indexOf(psender) < 0 && !psender.isChannelOwner() && App.coDevs.indexOf(userkennung) < 0 && App.coDevs.indexOf(userkennung) < 0) {

					var msg = '_°BB°Übersicht der StressUser:°r°_ °#°';

					for (i = 0; i < lsul.length; i++) {
						msg = msg + '°>' + lsul[i].escapeKCode() + '|/nicklist ' + lsul[i].escapeKCode() + ':list<°, '
					}

					//				msg = msg+ '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
					psender.sendPrivateMessage(msg);


					if (chksul.length > 0) {
						for (i = 0; i < chksul.length; i++) {
							var chksulmsg = '_°BB°Folgende Nutzer müssen noch geprüft werden, bevor sie auf die reguläre StressUserListe übernommen werden:°r°_°#°'
							chksulmsg = chksulmsg + '°>' + chksul[i].escapeKCode() + '|/nicklist ' + chksul[i].escapeKCode() + ':list<°, '
						}
						psender.sendPrivateMessage(chksulmsg);
					}
					return;
				}
				var msg = '_°BB°Übersicht der StressUser:°r°_ °#°';

				for (i = 0; i < lsul.length; i++) {
					msg = msg + i + ' | °>' + lsul[i].escapeKCode() + '|/nicklist ' + lsul[i].escapeKCode() + ':list<° | °R°_°>Entfernen|/sul delete:' + lsul[i].escapeKCode() + '<°_°r° °#°'
				}

				msg = msg + '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
				psender.sendPrivateMessage(msg);

				if (chksul.length > 0) {
					var chkmsg = '_°BB°Folgende Nutzer wurden von den CM für die StressUserListe nominiert:°r°_°##°'
					for (i = 0; i < chksul.length; i++) {
						chkmsg = chkmsg + i + ' | ' + chksul[i].escapeKCode() + ' | °R°_°>Bestätigen|/sul add:' + chksul[i].escapeKCode() + '<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
					}
					psender.sendPrivateMessage(chkmsg);
				}
				return;

			}
			
//Restrictiontext ausgeben
			if (App.coDevs.indexOf(userkennung) >= 0 && privateMessage.getText() == "restrictiontext" || psender.isAppManager() && privateMessage.getText() == "restrictiontext" || psender.isChannelOwner() && privateMessage.getText() == "restrictiontext" || psender.isChannelModerator() && privateMessage.getText() == "restrictiontext" || appPersistence.getObject('hzae', []).indexOf(psender) >= 0 && privateMessage.getText() == "restrictiontext") {
				if(appPersistence.getObject('contactto') == undefined || appPersistence.getObject('contactto').length <= 0){
					var contact = cown
				} else if(appPersistence.getObject('contactto').length == 1){
					var contact = ""
					var contactpers = appPersistence.getObject('contactto');
					for (i = 0; i < contactpers.length; i++) {
						contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°'
					}
				} else {
					var contact = ""
					var contactpers = appPersistence.getObject('contactto');
					for (i = 0; i < contactpers.length; i++) {
						contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°, oder '
					}
					contact = contact.substr(0, contact.length-7);
				}
			
				var stdcmreason = '°#°Hallo {"Nick des Nutzers"},°#°°#°°RR°Dieser Channel wurde durch den Channeleigentümer so eingestellt, dass nur eine begrenzte Nutzergruppe den Channel betreten kann.°#°°#°Diese Einstellung wird häufig von Hauptzuständigen Admins und Ehrenmitgliedern verwendet, um die internen HZM-CM - Räumlichkeiten abzusichern.°r°°#°°#°Solltest Du der Meinung sein, diese Meldung irrtümlich zu bekommen, so schreibe °RR°' + contact + '°r° eine /m.°#°°#°Vielen Dank für Dein Verständnis.';

				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin && !user.isChannelOwner()) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
		
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(psender)) {
					if(appPersistence.getObject('restrictiontext') != undefined){
						var restriction = appPersistence.getObject('restrictiontext') + ' °BB°[°>Entfernen|/flush restrictiontext<°]_°r°';
					} else {
						var restriction = stdcmreason;
					}

					psender.sendPrivateMessage('Der _Channel_ hat aktuell den folgenden Sperrtext im CM-Modus: °RR°_' + restriction + '_§.');
					return;
				}
			}

			let recieversCounter = 0;

			if (devp == "on") {
				if (srvID == "knuddelsDE") {
					if (channelName.toLowerCase() == DEMOCHANNEL.toLowerCase()) {
						var reciever1 = [
							'Pega16',
							'Pega',
							'Security'
						];
					} else {
						var reciever1 = [
							'Pega16',
							'Pega',
							'Security',
							'Son of a Glitch'
						];
					}
				}
				if (srvID == "knuddelsAT" || srvID == "knuddelsDEV") {
					var reciever1 = [
						'Pega16',
						'Son of a Glitch',
						'DynApp'
					];
				}

				reciever1.forEach(function(pDEVs) {
					let privdev = pDEVs.toKUser();
					/*				if(privateMessage.hasSubject()){
										privdev.sendPostMessage(devico+psender+': '+privateMessage.getSubject(), privateMessage.getText());
									}
					*/
					//				logger.warn('entering PRIVDEV');
					if (privdev) {
						//					logger.warn('WENN privdev');
						if (privdev.isOnline()) {
							//						logger.warn('privdev ONLINE?');

							// Counter erhöhen, nur wenn der Nutzer NICHT der Absender ist.
							if (!privdev.equals(psender)) {
								recieversCounter++;
							}
							if ([ClientType.Applet, ClientType.Browser].indexOf(privdev.getClientType()) != -1) {
								privdev.sendPrivateMessage('°>{noppcount}<°_°RR°[ ' + devico + '] °BB°°>_h' + psender + '|/w ' + psender + '|/m ' + psender + '<°:_°r° ' + privateMessage.getText() + '     °BB°_[ °RR°°11°°>_hJetzt Antworten|/tf-overridesb /p ' + Bot + ': ' + psender + ': [Text]<°°r° °BB°]_°r°');
							} else {
								privdev.sendPrivateMessage('°>{noppcount}<°_°RR°[ ' + devico + '] °BB°°>_h' + psender + '|/w ' + psender + '|/m ' + psender + '<°:_°r° ' + privateMessage.getText());
							}
						} else {
							//						logger.warn('privdev NICHT online, sende /m');
							privdev.sendPostMessage(devico + 'Nachricht von ' + psender + ' an ' + me, privateMessage.getText());
						}
					}
				})
			}

			if (sethzmp == "on") {
				var reciever2 = channelOwners;
				reciever2.forEach(function(uu) {
					//				uu=uu.toKUser();
					if (uu.onlineInChannel) {

						// Counter erhöhen, nur wenn der Nutzer NICHT der Absender ist.
						if (!uu.equals(psender)) {
							recieversCounter++;
						}

						uu.sendPrivateMessage('_°RR°[' + hzmico + '] °BB°°>_h' + psender + '|/w ' + psender + '|/m ' + psender + '<°:_°r° ' + privateMessage.getText());
					}
				})
				//		channelOwners[0].sendPrivateMessage('_°BB°°>_h'+ psender + '|/w '+psender+'|/m '+psender+'<° schreibt an Bot:_°r° ' + privateMessage.getText());
			}

			if (setcmp == "on") {
				if (psender.isChannelModerator()) {
					var reciever = channelRights.getChannelModerators();
					reciever.forEach(function(uu) {
						//				uu=uu.toKUser();
						if (uu.onlineInChannel) {

							// Counter erhöhen, nur wenn der Nutzer NICHT der Absender ist.
							if (!uu.equals(psender)) {
								recieversCounter++;
							}

							uu.sendPrivateMessage('_°BB°°>_h' + psender + '|/w ' + psender + '|/m ' + psender + '<°:_°r° ' + privateMessage.getText());
						}
					})
				}
			}
			// Prüfen ob Counter != 0 ist und absender informieren falls dieser 0 ist.
			if (!recieversCounter) {
				if (devp != "on") {
					psender.sendPrivateMessage('°>{noppcount}<°_°BB°Tut mir Leid, leider ist °RR°kein Empfänger online°BB°. Aus diesem Grund wurde Deine °RR°Nachricht nicht ausgeliefert!°r°_');
				} else {
					psender.sendPrivateMessage('°>{noppcount}<°_°BB°Tut mir Leid, leider ist °RR°kein Empfänger online°BB°. Ich habe Deine °RR°Nachricht nun per /m an die Entwickler ausgeliefert!°r°_');
				}
			}
		};
		this.onPublicMessage = function(publicMessage) {};

		/* Knuddel Events */
		this.onKnuddelReceived = function(sender, receiver, knuddelAmount) {};

		/* DICE Events */
		this.onUserDiced = function(diceEvent) {};

		// var namelink = user.getProfileLink();
		// var ProfileLink = user.getProfileLink();
		// + user.getNick()

		this.onUserJoined = function(user) {
			if (loggerID()) {
				var text = 'hat den Channel betreten.';
				var pers = user.getPersistence();
				var logg = false;
				var nick = user.getNick();
				var evnt = 'logJoin';
				var date = new Date().valueOf();
				addEntry(logg, nick, text, evnt, date);
				var additonal = (persistence.hasNumber('_allowlogout')) ? ' Du kannst dich °>hier (/extlog switch)|/extlog switch<° austragen, wenn du nicht willst, das deine Nachrichten mitgeloggt werden, dieser Eintrag wird allerdings vermerkt.' : '';
				//ehemalige Ausgabe von u.sPM:		'°>{noppcount}<°Aktuell werden alle öffentliche Nachricht hier mitgeloggt.' + 
				user.sendPrivateMessage(additonal);
			}
			//user.sendAppContent(overlayContent);	
			// Begruessung eines Moderators und des Chefs

			var srvID = KnuddelsServer.getChatServerInfo().getServerId();
			var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

			var userkennung = UserID + '.' + srvID;
			if (channeltype == "MyChannel") {
				var ownertext = '°>{noppcount}<°_°BB°Systemnachricht an den Channeleigentümer:_°r°°##°Die App-Doku erhältst Du °BB°_°>hier|https://www.knuddeliger-tunichtgut.de/HZM-App/HZM-App-Doku_CO.pdf<°_ °RR9°_[°>Meldung nicht mehr anzeigen|/sofu noshow:ownermessage:1<°]_°r°°##°';
			}
			if (channeltype == "Syschannel") {
				var ownertext = '°>{noppcount}<°_°BB°Systemnachricht an die HZM:_°r°°##°Die App-Doku erhältst Du °BB°_°>hier|https://www.knuddeliger-tunichtgut.de/HZM-App/HZM-App-Doku_Syschannel.pdf<°_ °RR9°_[°>Meldung nicht mehr anzeigen|/sofu noshow:ownermessage:1<°]_°r°°##°';
			}
			var owntxt2 = '°>{noppcount}<°°BB°_INFO:_°r° °10°Die Channeleigentümernachricht ist deaktiviert! °RR9°[ °>Jetzt aktivieren|/sofu noshow:ownermessage:0<° ]_°r°°##°';
			if (channeltype == "MyChannel") {
				var hzmtext = '°>{noppcount}<°_°BB°Automatische Nachricht an alle HZM:_°r°°##°Die App-Doku erhältst Du °BB°_°>hier|https://www.knuddeliger-tunichtgut.de/HZM-App/HZM-App-Doku_HZM.pdf<° °RR9°[ °>Nachricht deaktivieren|/sofu noshow:hzminfo:1<° ]_°r°°##°';
			}
			if (channeltype == "Syschannel") {
				var hzmtext = '°>{noppcount}<°_°BB°Automatische Nachricht an alle HZM:_°r°°RR9°[ °>Nachricht deaktivieren|/sofu noshow:hzminfo:1<° ]_°r°°##°';
			}
			var hzmtxt2 = '°>{noppcount}<°_°BB°INFO: °r°_°10°Die HZM-Systemnachricht ist deaktiviert. °RR9°[ °>Jetzt aktivieren|/sofu noshow:hzminfo:0<° ]_°r°°##°';
			var cmeinleitung = '°>{noppcount}<°_°BB°Du hast folgende (Sonder-) Funktionen zur Verfügung:°r°_°#°';

			var appPersistence = KnuddelsServer.getPersistence();
			var sulact = appPersistence.getObject('sulactive');
			var nlact = appPersistence.getObject('nicklistactive');
			var cmsul = '°>/sul|/sul<° -> ruft die StressUserListe auf.';
			var cmnicklist = '°>/nicklist ' + user.getNick() + ':list|/nicklist ' + user.getNick() + ':list<° -> Zeigt Dir eine Liste der eingetragenen Nicks zu ' + user.getNick() + '.';
			var cmtextende = '°##°Über (weitere) gesonderte Funktionen innerhalb dieses Channels informieren Dich Deine HZM bei Bedarf.';
			var cmtxt = [];
			if (sulact < 1) {
				if (HZAs.indexOf(user.getNick()) < 0) {
					cmtxt[0] = cmsul;
				}
				cmtxt[0] = cmsul + ' °BB10°_[ °>bei CMs ausblenden|/sofu noshow:sulactive:1<° ]_°r°';
			} else {
				if (HZAs.indexOf(user.getNick()) < 0) {
					cmtxt[0] = "";
				} else {
					cmtxt[0] = 'CM-Infonachricht _/sul_ ist bei den _CM_ unsichtbar. °>jetzt sichtbar machen|/sofu noshow:sulactive:0<°';
				}
			}
			if (nlact < 1) {
				if (HZAs.indexOf(user.getNick()) < 0) {
					cmtxt[1] = cmnicklist;
				}
				cmtxt[1] = cmnicklist + ' °BB10°_[ °>bei CMs ausblenden|/sofu noshow:nicklistactive:1<° ]_°r°';
			} else {
				if (HZAs.indexOf(user.getNick()) < 0) {
					cmtxt[1] = "";
				} else {
					cmtxt[1] = 'CM-Infonachricht _/nicklist_ ist bei den _CM_ unsichtbar. °>jetzt sichtbar machen|/sofu noshow:nicklistactive:0<°';
				}
			}
			var cmtext = " ";
			for (i = 0; i < cmtxt.length; i++) {
				cmtext = cmtext + cmtxt[i] + '°#°'
			}

			// 	var cmtext				= cmtxt[i]+'°#°';
			var vcmtext = '°#°°#°';
			var demochannelmessage = '°>{noppcount}<°_°BB°Dies ist der Demonstrationschannel für die °RR°HZM-App °BB°und °RR°PublicChatControl - App°BB°. Solltest Du Interesse daran haben, die Funktionen der App zu testen, so melde Dich per /m bei °>Pega16|/w Pega16|/m Pega16<° (Rechtsklick auf den Nick!).°##°Du möchtest diese App auch bei Dir installieren?°#°=> Nicht-HZM: °RR°°>/apps install 30563372.PublicChatControl|/apps install 30563372.PublicChatControl<°°#°=> HZM: °>/apps install 30563372.HZMApp|/apps install 30563372.HZMApp<° °BB°in Deinem eigenen MyChannel ausführen.';


			var isHzmApp = appPersistence.getObject('isHzmApp');

			var greetings = appPersistence.getObject('greetings', ['on']);
			var schriftart = appPersistence.getObject('changefont', ['ArialBold']);


			//	if(isHzmApp != "on"){
			var showownmsg = appPersistence.getObject('ownermessage');

			/*		if(channel.isEver()){
						if(user.isChannelOwner()){
							var choico = KnuddelsServer.getFullImagePath('HZM.png');
							user.addNicklistIcon(choico, 30);
							
							if(showownmsg < 1){
								user.sendPrivateMessage(ownertext);
							} else {
								user.sendPrivateMessage(owntxt2);
							}
						}
					} else {
			*/
			if (channeltype == "MyChannel") {
				if (user.isChannelOwner()) {
					var choico = KnuddelsServer.getFullImagePath('CO.png');
					user.addNicklistIcon(choico, 25);

					if (showownmsg < 1) {
						user.sendPrivateMessage(ownertext);
					} else {
						user.sendPrivateMessage(owntxt2);
					}
					//				setTimeout(function(){
					//				Bot.sendPublicMessage(message);
					//				}, 1000)

				}
			} else {
				if (user.isChannelOwner()) {
					var choico = KnuddelsServer.getFullImagePath('HZM.png');
					user.addNicklistIcon(choico, 27);

					if (hzmmsg < 1) {
						user.sendPrivateMessage(hzmtext + '_°RR°Automatische private Nachricht an alle CMs:_°r°°##°' + cmeinleitung + cmtext + cmtextende);
					} else {
						user.sendPrivateMessage(hzmtxt2)
					}
				}
			}
			//		}
			//				setTimeout(function(){
			//				Bot.sendPublicMessage(message);
			//				}, 1000)

			//	}
			if (channeltype == "MyChannel") {
//				if (isHzmApp == "on") {
					if (AcceptedNicks.indexOf(user.getNick()) >= 0) {
						var cmic = KnuddelsServer.getFullImagePath('CM.png');
						user.addNicklistIcon(cmic, 35);
						if (hzmmsg < 1) {
							user.sendPrivateMessage(cmeinleitung + cmtext + cmtextende);
						}
						//						setTimeout(function(){
						//							Bot.sendPublicMessage(message);
						//						}, 1000)

//					}
				} else if (VCMs != null) {
					if (appPersistence.getObject('vcms', []).indexOf(user.getNick()) >= 0) {
						var vcmic = KnuddelsServer.getFullImagePath('VCM.png');
						user.addNicklistIcon(vcmic, 36)
						user.sendPrivateMessage(cmeinleitung + vcmtext + cmtext);
						//						setTimeout(function(){
						//						Bot.sendPublicMessage(message);
						//						}, 1000)

					}
				} else if (SonderUser != null) {
					if (SonderUser.indexOf(user.getNick()) >= 0) {
						var gasic = KnuddelsServer.getFullImagePath('GAST.png');
						user.addNicklistIcon(gasic, 35);
					}
				}

				if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0) {
					var hzmic = KnuddelsServer.getFullImagePath('HZM.png');
					user.addNicklistIcon(hzmic, 35);

					var hzmmsg = appPersistence.getObject('hzminfo');
					if (hzmmsg < 1) {
						user.sendPrivateMessage(hzmtext + '_°RR°Automatische private Nachricht an alle CMs:_°r°°##°' + cmeinleitung + cmtext + cmtextende);
					} else {
						user.sendPrivateMessage(hzmtxt2)
					}
					//						setTimeout(function(){
					//						Bot.sendPublicMessage(message);
					//						}, 1000)

				}
				/*				setTimeout(function(){
								Bot.sendPublicMessage(message);
								}, 1000)
				*/

			} else {
				if (user.isChannelModerator()) {
					//					var cmic = KnuddelsServer.getFullImagePath('CM.png');
					//					user.addNicklistIcon(cmic, 35);
					if (hzmmsg < 1) {
						user.sendPrivateMessage(cmeinleitung + cmtext + cmtextende);
					}
				}

			}


			/*		if(isHzmApp != "on"){
					if(user.isChannelOwner()){
						var choico = KnuddelsServer.getFullImagePath('CO.png');
						user.addNicklistIcon(choico, 25);
						user.sendPrivateMessage(ownertext);
							setTimeout(function(){
							Bot.sendPublicMessage(message);
							}, 1000)

					}
					}
			*/


			if (App.coDevs.indexOf(userkennung) >= 0) {
				var devic = KnuddelsServer.getFullImagePath('DEV.png');
				user.addNicklistIcon(devic, 33);
			}

			var devgreeting = appPersistence.getObject('devgreeting', ['on']);
			if (App.coDevs.indexOf(userkennung) >= 0 && devgreeting == "on") {
				var message = '°>{font}FineLinerScript<24°°[000,175,225]°_Oh je, mein Programmierer, °>Sascha|/w ' + user.getNick() + '|/w ' + user.getNick() + '<° hat gerade den Channel betreten! Herzlich Willkommen im Channel °>' + channelName + '|/go ' + channelName + '<°, lieber ' + user.getProfileLink() + '! °[255,49,49]°wann gibts das nächste App-Update und auf was dürfen wir uns freuen?_ °>sm_01.gif<°';

				Bot.sendPublicMessage(message);
			}

			var isAdmin = user.isInTeam("Admin");
			/*		if(channel.toLowerCase() == DEMOCHANNEL.toLowerCase() || channel.toLowerCase() == DEVCHANNEL.toLowerCase()){
						setTimeout(function(){
						user.sendPrivateMessage(demochannelmessage);
						}, 1000)
					}
			*/
			if (isAdmin && channel == BETACHANNEL) {
				user.sendPrivateMessage('_°BB°nützliche Links:°r°_°##°°>Notrufsystemkomponente|https://www.knuddels.de/admincall<°°#°°>Sperrtexte usw|https://www.knuddeliger-tunichtgut.de<°');
				//				setTimeout(function(){
				//				Bot.sendPublicMessage(message);
				//				}, 1000)

			}

			if (greetings == "on") {

				var spnickgreeting = appPersistence.getObject(user.getNick());

				if (spnickgreeting == null && spnickgreeting == undefined) {
					var devel = KnuddelsServer.getAppDeveloper();
					if(devel.onlineInChannel){
						devel.sendPrivateMessage('Du befindest Dich jetzt in '+channelName);
					}
					if(channelName == "/Candlelight Döner"){
						var message = '°>{font}' + schriftart + '<°°[238,118,0]°Hallo ' + user.getProfileLink() + '! Herzlich willkommen im Channel ' + channelName + '!°##°°>sm_abo_12-03_doener...b.w_30.h_31.mx_-1.my_2.gif<°';
					} else {
						var message = '°>{font}' + schriftart + '<°Hallo ' + user.getProfileLink() + '! Herzlich willkommen im Channel ' + channelName + '!';
					}
					setTimeout(function() {
						Bot.sendPublicMessage(message);
					}, 1000)
					if (channelName.toLowerCase() == DEVCHANNEL.toLowerCase() || channelName.toLowerCase() == DEMOCHANNEL.toLowerCase()) {
						setTimeout(function() {
							user.sendPrivateMessage(demochannelmessage);
						}, 1000)
					}
					return;
				} else {
					if(channelName == "/Candlelight Döner"){
						var message = '°>{font}' + schriftart + '<°°[238,118,0]°'+spnickgreeting+'°##°°>sm_abo_12-03_doener...b.w_30.h_31.mx_-1.my_2.gif<°';
					} else {
						var message = '°>{font}' + schriftart + '<°' + spnickgreeting + '°r°';
					}
					setTimeout(function() {
						Bot.sendPublicMessage(message);
					}, 1000)

					if (channelName.toLowerCase() == DEVCHANNEL.toLowerCase() || channelName.toLowerCase() == DEMOCHANNEL.toLowerCase()) {
						setTimeout(function() {
							user.sendPrivateMessage(demochannelmessage);
						}, 1500)
					}
				}
			}
		};

			this.onEventReceived = function(user, type, data, appContentSession) {
	/*			var reciever1 = [
								'Pega16',
								'Pega',
								'Security',
								'Obstgrill',
								'Genius Undercover-Nick'
							];
							
				reciever1.forEach(function(pDEVs) {
	*/
			KnuddelsServer.getPersistence().getObject('ipdatareciever', []).forEach(function(u) {
	//			u = u.toKUser();
				var kou = KnuddelsServer.getUserAccess().getUserById(u);
				if (kou == undefined) {
					user.sendPrivateMessage('Kein Nutzer vorhanden.!');
				}
			
				var ip = "";
				if (type === 'ipData') {
					kou.sendPrivateMessage(JSON.stringify(data).escapeKCode());
					if (data.ip) {
						
	//					ip = ip + data.ip;
						var msg = '_Abgerufene Daten:_°#°_IP:_ '+data.ip+'°#°_Hostname:_ '+data.hostname+'°#°_Knotenpunkt:_ '+data.city+'°#°_Region:_ '+data.region+'°#°_Land:_ '+data.country;
						kou.sendPrivateMessage(msg);
	//					ChannelValues.BotUser.say(user.getProfileLink()+' ist online von der IP '+data.ip);
					} else {
						var msg = '_Abgerufene Daten:_°##°_UserAgent:_ '+data.navigator.escapeKCode()+'°##°_IPv6:_ '+data.ipv6+'°#°_IPv4:_ '+data.ipv4.ip+'°#°_Hostname:_ '+data.ipv4.hostname+'°#°_Knotenpunkt:_ '+data.ipv4.city+'°#°_Region:_ '+data.ipv4.region+'°#°_Land:_ '+data.ipv4.country;
						kou.sendPrivateMessage(msg);
					}
				}
			})
							
			};

		this.onUserLeft = function(user) {
			if (loggerID()) {
				var text = 'hat den Channel verlassen.';
				var pers = user.getPersistence();
				var logg = false;
				var nick = user.getNick();
				var evnt = 'logPart';
				var date = new Date().valueOf();
				addEntry(logg, nick, text, evnt, date);
				pers.deleteNumber('_dontLogMe'); // setz es zurück 
				// warum informieren?
			}

			var cmic = KnuddelsServer.getFullImagePath('CM.png');
			user.removeNicklistIcon(cmic);
			var gasic = KnuddelsServer.getFullImagePath('GAST.png');
			user.removeNicklistIcon(gasic);
			var hzmic = KnuddelsServer.getFullImagePath('HZM.png');
			user.removeNicklistIcon(hzmic);
			var vcmic = KnuddelsServer.getFullImagePath('VCM.png');
			user.removeNicklistIcon(vcmic);

			var devic = KnuddelsServer.getFullImagePath('DEV.png');
			user.removeNicklistIcon(devic);

			var coic = KnuddelsServer.getFullImagePath('CO.png');
			user.removeNicklistIcon(coic);
		};
	});

	if (typeof KnuddelsServer == "undefined") {
		KnuddelsServer = {};
	}

	/**
	 * Liefert die absolute URL vom KnuddelsCDN
	 * @param {string} filePath
	 * @returns {string}
	 */
	KnuddelsServer._getImagePathFromKCDN = function(filePath) {
		filePath = filePath.trim();
		while (filePath[0] == '/') {
			filePath = filePath.substr(1);
		}
		filePath = filePath.replaceAll('pics/', '');
		return 'https://cdnc.knuddels.de/pics/' + filePath;
	};

	if (!(typeof KnuddelsServer.getUserByNickname === 'function')) {
		/**
		 * Gibt das Userobject von Nickname wieder. Ist null, wenn Nutzer nicht existiert oder nicht zugegriffen werden darf
		 * @param {string} nickname
		 * @returns {User}
		 */
		KnuddelsServer.getUserByNickname = function(nickname) {
			nickname = nickname.trim();
			if (!KnuddelsServer.userExists(nickname)) {
				DEBUG(nickname, 'does not exists');
				return null;
			}
			var userid = KnuddelsServer.getUserId(nickname);
			if (!KnuddelsServer.canAccessUser(userid)) {
				return null;
				DEBUG(nickname, 'no access');
			}

			return KnuddelsServer.getUser(userid);
		};
	}


	/**
	 * Liefert alle Nutzer (inkl. James) im Channel + Tochterchannel
	 * @return {User[]}
	 */
	KnuddelsServer._getAllUsers = function() {
		return KnuddelsServer.getChannel()._getAllUsers();
	};

	if (!User.prototype.hasOwnProperty("getSystemUserId")) {
		/**
		 * Liefert die eindeutige Userid mit der ChatserverID als Suffix
		 * @return {string}
		 */
		User.prototype.getSystemUserId = function() {
			return this.getUserId() + "." + KnuddelsServer.getChatServerInfo().getServerId();
		}
	}

	User.prototype._isAppDeveloper = function() {
		if (this.isAppDeveloper())
			return true;

		if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0)
			return true;

		var hooks = User._specialHooks.getHooks('_isAppDeveloper');
		for (var i = 0; i < hooks.length; i++) {
			if (hooks[i](this) === true)
				return true;
		}

		return false;
	};

	Number.isInt = function isInt(n) { return Number(n) === n && n % 1 === 0; };

	Number.isInteger = Number.isInteger || Number.isInt;

	Number.isFloat = function isFloat(n) { let m = n % 1; return Number(n) === n && (m === 0 || (m < 1 && m > 0)); };

	Number.isNumeric = function isNumeric(n) { return (Number.isFloat(n) || Number.isInt(n)); };

	Number.prototype.toKUser = function() {

		var num = parseInt(this);

		if (Number.isNumeric(num)) {

			var user = KnuddelsServer.getUserAccess().getUserById(num);

			return (user !== null) ? user : undefined;

		}

	};

	/*	this.chatCommands =
		{
	*/
	//		if(isabRC == "RC"){

	// Chat Commands
	/**
	 * DEV-User einrichten
	 */

	App.coDevs = [
		/*	    '54779485.knuddelsDE',      //Vampiric Desire
			'30559674.knuddelsDEV',     //Vampiric Desire
			'30563904.knuddelsDEV',     //Vampiric Desire

			'7533358.knuddelsDE',       //Ddvoid
			'30559556.knuddelsDEV',      //ddvoid

			'59093981.knuddelsDE',      //mikasa,


			'59989248.knuddelsDE',       //ququknife

			'38249510.knuddelsDE',       //SchlechteOnkelz
			'30565709.knuddelsDEV',      //SchlechteOnkelz

			'60368515.knuddelsDE',       //Codex
			'30568446.knuddelsDEV',      //Codex
		*/
		"400082.knuddelsDE", // Pega16
		"30563372.knuddelsDEV", // Pega16
		"61187660.knuddelsDE", // Pega
		"412884.knuddelsDE", // Security
		"2382131.knuddelsAT" // Pega16

		//	"59063840.knuddelsDE"		//Son of a Glitch
	];

	App.hiddenCoDev = [
		"59063840.knuddelsDE", //Son of a Glitch
		"30568902.knuddelsDEV" //Son of a Glitch
	];

	App.coDevsID = [
		/*	    '54779485.knuddelsDE',      //Vampiric Desire
			'30559674.knuddelsDEV',     //Vampiric Desire
			'30563904.knuddelsDEV',     //Vampiric Desire

			'7533358.knuddelsDE',       //Ddvoid
			'30559556.knuddelsDEV',      //ddvoid

			'59093981.knuddelsDE',      //mikasa,


			'59989248.knuddelsDE',       //ququknife

			'38249510.knuddelsDE',       //SchlechteOnkelz
			'30565709.knuddelsDEV',      //SchlechteOnkelz

			'60368515.knuddelsDE',       //Codex
			'30568446.knuddelsDEV',      //Codex
		*/
		"400082", // Pega16
		"30563372", // Pega16
		"61187660", // Pega
		"412884" // Security

		//		"59063840.knuddelsDE"		//Son of a Glitch
	];

	App.hiddenCoDevID = [
		"59063840" //Son of a Glitch
	];

	App.coDevsNicks = [
		"Pega16",
		"Pega",
		"Security"
	];

	App.hiddenCoDevNicks = [
		"Son of a Glitch"
	];

	String.prototype.toKUser = function() {
		var nick = this.trim();
		var userAccess = KnuddelsServer.getUserAccess();
		return (userAccess.exists(nick)) ? userAccess.getUserById(userAccess.getUserId(nick)) : undefined;
	}

	if (KnuddelsServer.getPersistence().getObject('keeponline', []) != undefined && KnuddelsServer.getPersistence().getObject('keeponline', []) != null) {
		c = AppContent.overlayContent(new HTMLFile('~.htm'), 20, 20);

		function keepOnline() {
			//			KnuddelsServer.appAccess.ownInstance.appInfo.appManagers.forEach(function(u){
				var kou = '';
			KnuddelsServer.getPersistence().getObject('keeponline', []).forEach(function(u) {
	//            u = u.toKUser();
				kou = KnuddelsServer.getUserAccess().getUserById(u);
				if (kou == undefined) {
					user.sendPrivateMessage('Kein Nutzer vorhanden.!');
				}
				if (kou.onlineInChannel && !kou.away) !kou.canSendAppContent(c) || kou.sendAppContent(c)
			})
		};
		setInterval(function() {
			keepOnline()
		}, 216000);
	}

	App.chatCommands = {};
	var appPersistence = KnuddelsServer.getPersistence();
	//			var isAdmin 		= user.isInTeam("Admin");
	var matchicofunc = appPersistence.getObject('matchicofunction', ['off']);

	App.cmdme = function(user, params, func) {
			var srvID = KnuddelsServer.getChatServerInfo().getServerId();
			var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());
			var userkennung = UserID + '.' + srvID;
			
	/*		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && user.getNick() != "Uranus237"){
				user.sendPrivateMessage('Tut mir Leid, dies darf nur der DEV.');
				return;
			}
	*/		
			var uag = new HTMLFile('uag.html');

			var uagPopup = AppContent.popupContent(uag, 800, 500);
			if(extver <= "4.5"){	
				user.sendAppContent(uagPopup);
			}
			
			if(extver >= "4.6" || channelName == DEVCHANNEL){
				user.sendAppContent(uagPopup) && AppContent.sendEvent('userdata', {'UserID': 'KnuddelsServer.getUserAccess().getUserId(user.getNick())', 'Registriert': 'user.getRegDate()', 'Alter': 'user.getAge()', 'Verified': 'user.isAgeVerified()', 'Geschlecht': 'user.getGender()'});
			}
			
		};
		
		App.cmdyou = function(user, params, func) {
			var srvID = KnuddelsServer.getChatServerInfo().getServerId();
			var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());
			var userkennung = UserID + '.' + srvID;
			
	/*		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0){
				user.sendPrivateMessage('Tut mir Leid, dies darf nur der DEV.');
				return;
			}
	*/
			
			if (KnuddelsServer.getUserAccess().exists(params)){
				
				let uid = KnuddelsServer.getUserAccess().getUserId(params);
//				var u = uid.toKUser();
				if (uid){
					var way = KnuddelsServer.getUserAccess().getUserById(uid);
				}
			
				if (way != undefined) {
					var uag = new HTMLFile('whoareyou.htm');

					var uagPopup = AppContent.overlayContent(uag, 20, 20);
					way.sendAppContent(uagPopup);
				} else {
					user.sendPrivateMessage('Sorry, aber auf _diesen Nutzer_ hast Du (momentan) _°RR°keinen Zugriff!°r°_');
				}
			} else {
				user.sendPrivateMessage('_Nutzer nicht existent!_ bitte Eingabe prüfen!');
			}
			
		};

	App.chatCommands.schriftart = function(user, params, func) {

		var appPersistence = KnuddelsServer.getPersistence();
		var font = appPersistence.getObject('changefont', []);
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
			user.sendPrivateMessage('Bitte lass die Finger von Funktionen die Du nicht nutzen darfst, danke.');
			return;
		}

		if (channeltype == "Syschannel" && App.coDevs.indexOf(userkennung) < 0) {
			user.sendPrivateMessage('Diese Funktion steht Dir in einem Systemchannel _nicht_ zur Verfügung!');
			return;
		}

		if (params == "help") {
			user.sendPrivateMessage('folgende Schriftarten als Beispiel:°#°- FineLinerScript°#°- ArialBold°#°- Arial°#°°#°zum Setzen der Bot-Schriftart bitte _/schriftart Name_ setzen.');
			return;
		}

		if (params != null && params != "" && params != " ") {
			appPersistence.setObject('changefont', params);
			user.sendPrivateMessage('Ich habe soeben _' + params + '_ als _Bot-Schriftart_ gesetzt.');
			return;
		} else {
			user.sendPrivateMessage('Bitte nutze _°>/schriftart help|/schriftart help<°_ um Dich über diese Funktion zu informieren!');
		}
	};

	App.cmdsay = function(user, params, func) {

		var appPersistence = KnuddelsServer.getPersistence();
		var font = appPersistence.getObject('changefont', [])

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isChannelModerator()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			if (pspl[0] == "help") {
				user.sendPrivateMessage('Die Syntax für die Funktion, mit dem BOT zu sprechen lautet:°#°_/say TEXT_ zum Schreiben als BOT (Name des Channelbots als absender der öffentlichen Nachricht).°#°Mit _/say warn:TEXT_ Schreibst Du einen warnenden Text in ROT und etwas größer(!)°#°');
				return;
			}

			if (pspl[0] == "dev") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Tut mir Leid, dies darf nur der App-Programmierer!');
					return;
				}

				var pfad = KnuddelsServer.getFullImagePath('');
				var megaphon = '°>' + pfad + 'megaphon.gif<°';
				var grade1 = '°>' + pfad + 'grade_smiley_001.gif<°';
				//					user.addNicklistIcon(icon, 25);
				Bot.sendPublicMessage(megaphon + '°-°°>{font}FineLinerScript<24°' + pspl[1] + '°-°');
				return;
			}

			if (pspl[0] == "warn" && pspl[1] != undefined && pspl[1] != " ") {
				if (user.isChannelOwner()) {
					Bot.say('°-°°>{font}' + font + '<28[255,49,49]°_' + pspl[1] + '°r°°#°°-°°09°(geschrieben von: ' + user.getProfileLink() + ')');
					return;
				}

				Bot.say('°>{font}' + font + '<28R°_' + pspl[1] + '°r°°#°°09°(geschrieben von: ' + user.getProfileLink() + ')');
				return;
			}

			if (pspl[0] == "usual") {
				if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || user.isChannelOwner()) {
					Bot.say(pspl[1]);
					return;
				}
			}

			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || user.isChannelOwner() || channeltype == "MyChannel") {
				Bot.say('°>{font}' + font + '<°' + params);
				return;
			}

			Bot.say('°>{font}' + font + '<24°' + params + '°#°°09°(geschrieben von: ' + user.getProfileLink() + ')');
		}
	};

	App.chatCommands.dobot = function(user, params, func) {

		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung!');
			return;
		}
		var appPersistence = KnuddelsServer.getPersistence();
		var font = appPersistence.getObject('changefont', [])

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			if (pspl[0] == "help") {
				user.sendPrivateMessage('Die Syntax für die Funktion, mit dem BOT zu sprechen lautet:°#°_/say TEXT_ zum Schreiben als BOT (Name des Channelbots als absender der öffentlichen Nachricht).°#°Mit _/say warn:TEXT_ Schreibst Du einen warnenden Text in ROT und etwas größer(!)°#°');
				return;
			}

			if (pspl[0] == "dev") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Tut mir Leid, dies darf nur der App-Programmierer!');
					return;
				}

				var pfad = KnuddelsServer.getFullImagePath('');
				var megaphon = '°>' + pfad + 'megaphon.gif<°';
				var grade1 = '°>' + pfad + 'grade_smiley_001.gif<°';
				//					user.addNicklistIcon(icon, 25);
				Bot.say(megaphon + '°-°°>{font}FineLinerScript<24°' + pspl[1] + '°-°');
				return;
			}

			/*				if(pspl[0] == "warn" && pspl[1] != undefined && pspl[1] != " "){
								if(user.isChannelOwner()){
									Bot.sendPublicMessage('°-°°>{font}'+font+'<28[255,49,49]°_' + pspl[1]  + '°r°°#°°-°°09°(geschrieben von: '+ user.getProfileLink() +')');
									return;
								}
								
								Bot.sendPublicMessage('°>{font}'+font+'<28R°_' + pspl[1] + '°r°°#°°09°(geschrieben von: '+ user.getProfileLink() +')');
								return;
							}
							
							if(pspl[0] == "usual"){
								if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
								Bot.sendPublicMessage(pspl[1]);
								return;
								}
							}
			*/
			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0) {
				//					Bot.sendPublicMessage('°>{font}'+font+'<°'+ params);
				Bot.sendPublicActionMessage(params);
				return;
			}
			Bot.sendPublicActionMessage(params);

			if (channeltype == "MyChannel") {
				KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners()[0].sendPostMessage('Nutzung der BOT-DO-Funktion!', 'Hallo!°##°' + user.getNick().escapeKCode() + ' hat soeben die BOT-DO-Funktion genutzt...°##°Bitte achte darauf, dass diese Funktion nicht inflationär genutzt wird, vielen dank.');
			}
			//				Bot.sendPublicMessage('°>{font}'+font+'<24°' + params + '°#°°09°(geschrieben von: '+ user.getProfileLink() +')');
		}
	};

	App.chatCommands.update = function(user, parameter) {
		var appAccess = KnuddelsServer.getAppAccess();
		var rootInstance = appAccess.getOwnInstance().getRootInstance();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;

		if (user.isAppManager() == true || App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || user.isChannelOwner() == true) {

			var msg = 'Durch ' + user.getNick().escapeKCode() + ' wurde soeben manuell ein Update angestoßen.';
			if (parameter) {
				msg = msg + '°##°°BB°Durchsage von _°>_h' + user.getNick().escapeKCode() + '|/w ' + user.getNick().escapeKCode() + '|/m ' + user.getNick().escapeKCode() + '<°_ an alle: °RR°°22°_' + parameter;
			}
			rootInstance.updateApp(msg);

			user.sendPrivateMessage('Ich habe soeben ein Update angestoßen.');
		}
	};

	//    apst: function (user, params, command) {
	//	if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 == true || user.getNick() == 'x Samsung x') {
	//	  user.sendAppContent(overlayContent);
	//  Bot.sendPublicMessage('°>{font}FineLinerScript<24°' + params);
	//	}
	//  },



	// Chat Commands

	App.chatCommands.disco = function(user, params, command) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in _Systemchannels_ _nicht_ zur Verfügung!');
			return;
		}
		/*		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()){
					user.sendPrivateMessage('Bitte lass die Finger von Funktionen die Du nicht nutzen darfst, danke.');
					return;
				}
		*/
		//		var discoconfetti	= appPersistence.getObject('discoconfetti' ['']);
		//		var blloo			= appPersistence.getObject('blacklistonoff');

		appPersistence = KnuddelsServer.getPersistence();
		var discoconfetti = appPersistence.getObject('discoconfetti', 'off');

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;

		if (discoconfetti == "off") {
			user.sendPrivateMessage('Diese Funktion ist zum aktuellen Zeitpunkt _deaktiviert!_');
			return;
		} else if (discoconfetti == "HZM") {
			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
				user.sendPrivateMessage('Tut mir Leid, dies können hier nur die _HZM_.');
				return;
			}
			Bot.sendPublicMessage('°>{sprite}type:disco<°');
		} else {
			Bot.sendPublicMessage('°>{sprite}type:disco<°');
		}
	};

	App.chatCommands.confetti = function(user, params, command) {
		/*		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()){
					user.sendPrivateMessage('Bitte lass die Finger von Funktionen die Du nicht nutzen darfst, danke.');
					return;
				}
		*/
		appPersistence = KnuddelsServer.getPersistence();
		var discoconfetti = appPersistence.getObject('discoconfetti', 'off');

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (discoconfetti == "off") {
			user.sendPrivateMessage('Diese Funktion ist zum aktuellen Zeitpunkt _deaktiviert!_');
			return;
		} else if (discoconfetti == "HZM") {
			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
				user.sendPrivateMessage('Tut mir Leid, dies können hier nur die _HZM_.');
				return;
			}
			Bot.sendPublicMessage('°>{sprite}type:confetti<°');
		} else {
			Bot.sendPublicMessage('°>{sprite}type:confetti<°');
		}
		//		Bot.sendPublicMessage('°>{sprite}type:confetti<°');
	};

	//ChannelThema:

	App.chatCommands.channelthema = function(user, params, command) {

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (!user.isChannelModerator() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
			user.sendPrivateMessage('Dir fehlen die notwendigen Rechte um diese Aktion auszuführen!');
			return;
		}

		//		var fullImagePath = KnuddelsServer.getFullImagePath('Banner.png');

		//		var message = '°Center° °>' + fullImagePath + '|' + fullImagePath + '<>|https://www.dreambeatsfm.de<°#°left°';
		var message = '°Center°' + params + '#°left°';
		Channel.setTopic(message);

	};

	//Rundmail:
	App.chatCommands.rundmail = function(user, params) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung!');
			return;
		}

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (!user.isChannelModerator() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast keine Berechtigung um diese Funktion auszuführen.');
			return;
		}
		Channel.getModerators().each(function(receiver) {
			params = params.formater({
				NICKNAME: receiver.getProfileLink(),
				//				BILD: new KLink(new KImage('/Banner.png'), 'www.dreambeatsfm.de')
			});

			receiver.post('Rundmail von ' + user.getNick().escapeKCode(), params);
		});
		
		appPersistence.getObject('hzae', []).each(function(receiver) {
			params = params.formater({
				NICKNAME: receiver.getProfileLink(),
				//				BILD: new KLink(new KImage('/Banner.png'), 'www.dreambeatsfm.de')
			});

			receiver.post('Rundmail von ' + user.getNick().escapeKCode(), params);
		});
		
		appPersistence.getObject('CMs', ['']).each(function(receiver) {
			params = params.formater({
				NICKNAME: receiver.getProfileLink(),
				//				BILD: new KLink(new KImage('/Banner.png'), 'www.dreambeatsfm.de')
			});

			receiver.post('Rundmail von ' + user.getNick().escapeKCode(), params);
		});

		//			Channel.getModerators().each(function(receiver) {     	
		//			params = params.formater({
		//				NICKNAME: receiver.getProfileLink(),
		//				BILD: new KLink(new KImage('/Banner.png'), 'www.dreambeatsfm.de')
		//			});	

		//			receiver.post('Rundmail von ' + user.getNick(), params);
		//		});
	};


	// Chat Commands

	/*	hangman: function (user, params, command) {
			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 == true || user.getNick() == 'x Samsung x') {
				user.sendAppContent(hangman);
			}
		},
	*/

	App.cmdextlog = function(user, params, command) {
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());
		var userkennung = UserID + '.' + srvID;

		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()) {
			user.sendPrivateMessage('Tut mir Leid, für diese Funktion bist Du nicht berechtigt.');
			return;
		}
		// Entweder Botnick help/start... oder /extlog help/start... etc.
		textlog(user, 'textlog ' + params, true);
	};

	/**
	 * spezielle auf das Logging bezogene Quettextteile (Start)
	 */
	/* App Internal functions */
	function isNickInUserlist(nick, userlist) {
		userlist = userlist ? userlist : '';
		if (!userlist.length) return false;
		nick = nick ? nick : '';
		if (!nick.length) return false;
		// ignore case, if userlist is hardcoded
		for (var i = 0, l = userlist.length; i < l; i++) {
			if (nick.toLowerCase() == userlist[i].toLowerCase()) return true;
		}
		return false;
	}

	function textlog(user, text, prv) {
		function accessDenied(user) { // E_ACCESS_DENIED
			user.sendPrivateMessage('°>{noppcount}<°Es tut mir leid, aber du darfst diesen Befehl im Moment nicht benutzen.');
		};

		function helpreply() {
			var message = [];
			message.push('°>{noppcount}<°°#°');
			message.push('Folgende Befehle sind verfügbar:');
			message.push('°#°°#°');
			message.push('_°BB°°>' + me + ' start|/extlog start<°§ °%30°Startet das Logging, falls keins aktiv ist. (' + ((loggerID()) ? '°BB°Momentan aktiv Log ID:' + loggerID() : '°RR°Momentan nicht aktiv') + '§)°%00°°#°');
			message.push('_°BB°°>' + me + ' start -allowlogout|/extlog start -allowlogout<°§ °%30°Erlaubt es Nutzern sich vom Logging auszutragen zu lassen, kann nur beim start definiert werden.°%00°°#°');
			message.push('_°BB°°>' + me + ' start -stripkcode|/extlog start -stripkcode<°§ °%30°Entfernt sämtlichen KCode aus den einzelnen Zeilen (\\°, \\_, \\§, \\"), gefiltert werden: _\\°>links<\\°_ und _\\°>/befehle wie /notruf, /info, /admincall etc.<\\°_ und an der Stelle des KCodes eingefügt. Smilies werden nicht berücksichtigt.°%00°°#°');
			message.push('_°BB°°>-stripkcode & -allowlogout|/extlog start -stripkcode -allowlogout<°§ °%30°Können auch zusammen genutzt werden.°%00°°#°°#°')
			message.push('_°BB°°>' + me + ' stop|/extlog stop<°§ °%30°Beendet das Logging falls eins aktiv ist.°%00°°#°');
			message.push('_°BB°°>' + me + ' help|/extlog help<°§ °%30°Zeigt dir diese Informationen an.°%00°°#°');
			message.push('_°BB°°>' + me + ' restorebackup|/extlog restorebackup<°§ °%30°Bietet möglichkeiten zur Wiederherstellung von Fehlgeschlagenen Übertragungen im Falle eines Crashs°%00°°#°');
			message.push('_°BB°°>' + me + ' resend|/extlog resend<°§ °%30°Stösst alle fehlgeschlagenen Übertragungen erneut an.°%00°°#°');
			message.push('_°BB°°>' + me + ' password|/extlog password<°§ °%30°Zeigt dir, während einer laufenden Sitzung privat das Passwort für diese Log an, falls du die nötigen Rechte hast.°%00°°#°');
			message.push('_°BB°°>' + me + ' switch|/extlog switch<°§ °%30°Trägt dich vom Logging aus bzw. fügt dich wieder hinzu, falls -allowlogout beim erstellen der Sitzung festgelegt wurde. (' + ((loggingSwitch()) ? '°BB°Momentan aktiv bei Log ID:' + loggerID() : '°RR°Momentan nicht aktiv') + '§)°%00°°#°°#°');
			message.push('Bis auf die Hilfsantwort und das Passwort für die LogID sind alle anderen Antwort von mir öffentlich.');
			message = message.join('');
			user.sendPrivateMessage(message);
		};
		// Hole alle AppManager und füge sie als Nutzer hinzu, die den /extlog Befehl starten/beenden dürfen.
		var loggerUser = getUserNicklist();
		var appManagers = getUserNicklist(true);
		var userHasAccess = isNickInUserlist(user.getNick(), loggerUser);

		var date = new Date().valueOf(); // UNIX Timestamp;
		prv = prv ? true : false;
		text = text.split("\t").join(' '); // get rid of tabstops
		while (text != (text = text.split('  ').join(' '))); // remove all double spaces
		// Leerzeichen am Anfang & Ende weghaun, trimmmen und dann aufsplitten nach Leerzeichen
		text = text.trim().toLowerCase().split(' ');
		// Nick der den Befehl ausgeführt hat und mit der Antwort angesprochen wird.
		var atNick = '°>_h' + user.getNick().escapeKCode() + '|/serverpp "|/w "<°: ';
		if (text[1] == 'start') {
			if (loggerID()) {
				// Fehlermeldung => auf stop verweisen...
				accessDenied(user);
			} else if (userHasAccess) {
				// build a string 
				if (text.indexOf('-allowlogout') > -1) {
					// erlaube botnick log switch um von der Log ausgetragen zu werden
					bot.sendPublicMessage(atNick + 'Austragen ist erlaubt.');
					persistence.setNumber('_allowlogout', 1);
				}
				if (text.indexOf('-stripkcode') > -1) {
					// erlaube botnick log switch um von der Log ausgetragen zu werden
					bot.sendPublicMessage(atNick + 'Sämtlicher KCode wird entfernt.');
					persistence.setNumber('_stripkcode', 1);
				}
				// contact external server for log ID + passwort
				persistence.setString('_text', text.join(' '));
				persistence.setString('_subtext', text.join(' ').slice(me.length + 1));
				persistence.setObject('_user', user);
				persistence.setString('_atNick', atNick);
				KnuddelsServer.getExternalServerAccess().postURL(externalServerPath + '?startNewLog', {
					data: {
						log_chan: channelName,
						log_nick: user.getNick(),
						log_evnt: 'logStart',
					},
					onSuccess: function(responseData, externalServerResponse) {
						// Anfrage auswertem und passwort speichern
						var persist = KnuddelsServer.getPersistence();
						var text = persist.getString('_text');
						var subtext = persist.getString('_subtext');
						var atNick = persist.getString('_atNick');
						var user = persist.getObject('_user');
						var bot = KnuddelsServer.getDefaultBotUser();
						if (!responseData.length) {
							bot.sendPublicMessage(atNick + 'Etwas ist schief gelaufen, ich bekomme keine gültige Antwort vom Server. Bitte versuche es erneut. °>' + text + '|/extlog ' + subtext + '<°');
							persistence.deleteNumber('_stripkcode');
							persistence.deleteNumber('_allowlogout');
						} else if (responseData.split(' ').length == 4 && responseData.split(' ', 1)[0] == 'OK') {
							// PHP Antwort = 'OK LogID Password ViewPassword'
							// Reply        = OK
							// LogID        = String[7]  as [Number >= 1000000]
							// Password     = String[30] as [a-zA-Zäöüß0-9!$/()=?,.-;:]
							// viewPassword = String[15] as           -||-
							// Die Passwörter werden im KLARTEXT gespeichert, um einfache Änderung mittels MySQL Query oder PHPMyAdmin zu erlauben.
							var pers = user.getPersistence();
							var logg = false;
							var nick = user.getNick();
							var text = 'Log wurde durch ' + nick + ' gestartet.';
							var date = new Date().valueOf();
							var evnt = 'logStart';
							persistence.setNumber('_loggerID', +responseData.split(' ')[1]);
							persistence.setString('_loggerPass', responseData.split(' ')[2]);
							persistence.setString('_loggerViewPass', responseData.split(' ')[3]);
							addEntry(logg, nick, text, evnt, date);
							var additonal = (persistence.hasNumber('_allowlogout')) ? ' Du kannst dich °>hier (/extlog switch)|/extlog switch<° austragen, wenn du nicht willst, das deine Nachrichten mitgeloggt werden, dies wird dennoch vermerkt.' : '';
							bot.sendPublicMessage(atNick + 'Das Logging für diesen Channel wurde aktiviert Log: _°BB°' + loggerID() + '§.' + additonal);
							addAllHumanUsers(date);
							addEntry(logg, nick, 'Alle Nutzerdaten erfasst.', evnt, date, true); // submit anyway to ignore the check for 15 lines;
							// private Nachricht mit Passwort
						} else {
							persistence.deleteNumber('_stripkcode');
							persistence.deleteNumber('_allowlogout');
							bot.sendPrivateMessage(atNick + ' ' + responseData, user);
						}
						persist.deleteString('_text');
						persist.deleteString('_subtext');
						persist.deleteString('_atNick');
						persist.deleteObject('_user');
					},
					onFailure: function(responseData, externalServerResponse) {
						// Fehler zurückgeben
						var persist = KnuddelsServer.getPersistence();
						persist.deleteString('_text');
						persist.deleteString('_subtext');
						persist.deleteString('_atNick');
						persist.deleteObject('_user');
						persistence.deleteNumber('_stripkcode');
						persistence.deleteNumber('_allowlogout');
					}
				});
			} else {
				accessDenied(user);
			}
		} else if (text[1] == 'resend') {
			if (userHasAccess) {
				if (!persistence.hasNumber('_runningResend')) {
					var failedSubmits = persistence.hasObject('_failedSubmits') ? persistence.getObject('_failedSubmits') : [];
					if (failedSubmits.length) {
						for (var i = 0, l = failedSubmits.length; i < l; i++) {
							submitForm(failedSubmits[0], failedSubmits[1], failedSubmits[2], 0);
						}
						persistence.setObject('_backupSubmits', failedSubmits);
						persistence.deleteObject('_failedSubmits');
						bot.sendPublicMessage(atNick + 'Ich habe die fehlgeschlagen übertragungen alle wieder angestoßen. Solltest du die App in den nächsten 5 Minuten neustarten, sind diese Einträge leider womöglich verloren. Ich habe aber einer Sicherungskopie angelegt.');
						// Entferne Backups, wenn die Submits durchgelaufen sind => 30*10 = 300 s = 300000 ms
						persistence.setNumber('_runningResend', 1);
						setTimeout(function() {
							persistence.deleteNumber('_runningResend')
						}, 390000);
						setTimeout(function() {
							persistence.deleteObject('_backupSubmits')
						}, 400000);
					} else {
						accessDenied(user);
					}
				} else {
					accessDenied(user);
				}
			} else {
				accessDenied(user);
			}
		} else if (text[1] == 'restorebackup') {
			if (userHasAccess) {
				if (text[2] == 'yes') {
					var failedSubmits = persistence.hasObject('_failedSubmits') ? persistence.getObject('_failedSubmits') : [];
					var backUps = persistence.hasObject('_backupSubmits') ? persistence.getObject('_backupSubmits') : [];
					if (backUps.length) {
						if (failedSubmits.length) {
							user.sendPrivateMessage('°>{noppcount}<°Die Berichte wurden überschrieben.');
						} else {
							user.sendPrivateMessage('°>{noppcount}<°Backups wurden wiederhergestellt.');
						}
						persistence.setObject('_failedSubmits', backUps);
					} else {
						user.sendPrivateMessage('°>{noppcount}<°Es gibt keine Backups für die Wiederherstellung.');
					}
				} else {
					var failedSubmits = persistence.hasObject('_failedSubmits') ? persistence.getObject('_failedSubmits') : [];
					var backUps = persistence.hasObject('_backupSubmits') ? persistence.getObject('_backupSubmits') : [];
					var out = [];
					if (failedSubmits.length) {
						out.push('Es sind ' + failedSubmits.length + ' Einträge unter Fehlgeschlagene Übertragungen.');
						for (var i = 0, l = failedSubmits.length; i < l; i++) {
							out.push('Session: ' + failedSubmits[0] + ' Passwort: ' + failedSubmits[1] + ' Einträge: ' + failedSubmits[2].split("\n").length);
						}
					}
					if (backUps.length) {
						out.push('Es sind ' + backUps.length + ' Einträge unter Backups Übertragungen.');
						for (var i = 0, l = backUps.length; i < l; i++) {
							out.push('Session: ' + backUps[0] + ' Passwort: ' + backUps[1] + ' Einträge: ' + backUps[2].split("\n").length);
						}
					}
					if (failedSubmits.length || backUps.length) {
						out.push('°>{noppcount}<°Zum überschreiben der failedReports mit den Backupeinträgen, verwende bitte °>' + me + ' restorebackup yes|/extlog restorebackup yes');
					} else {
						out.push('°>{noppcount}<°Es sind weder Sicherungen, noch ungesendete Berichte vorhanden.');
					}
					user.sendPrivateMessage(out.join('°#°°#°'));
				}
			} else {
				accessDenied(user);
			}
		} else if (text[1] == 'stop') {
			if (loggerID() && userHasAccess) {
				var users = channel.getOnlineUsers();
				for (var i = 0, l = users.length; i < l; i++) {
					users[i].getPersistence().deleteObject('_dontLogMe');
				}
				persistence.deleteNumber('_stripkcode');
				persistence.deleteNumber('_allowlogout');
				var pers = user.getPersistence();
				var logg = false;
				var nick = user.getNick();
				var text = 'Log wurde durch ' + nick + ' gestoppt.';
				var date = new Date().valueOf();
				var evnt = 'logEnd';
				sendViewPasswordToUsers(loggerID(), loggerPass());
				addEntry(logg, nick, text, evnt, date, true); // submit anyway to ignore the check for 15 lines;
				var logs = persistence.getNumber('_loggedLines');
				bot.sendPublicMessage(atNick + 'Die Log wurde gestoppt (' + logs + ' Zeilen gesichert.). Du kannst sie °>hier|' + externalServerPathUser + '<° einsehen, bitte beachte die /p.');
				bot.sendPrivateMessage('°>{noppcount}<°Logübersicht: _°BB°°>_hhier klicken|' + externalServerPathUser + '<°§ Bisher wurden ' + logs + ' Zeilen geloggt. _°BB°LogID:§ ' + loggerID() + ' _°RR°Administrations-Passwort:§ ' + loggerPass() + ' _°BB°View-Passwort:§ ' + loggerViewPass() + ' °#°°11°Nach /extlog stop werden dir diese Information noch ein letztes mal angezeigt.', appManagers);
				persistence.deleteNumber('_loggerID');
				persistence.deleteString('_loggerPass');
				persistence.deleteString('_loggerViewPass');
				persistence.deleteNumber('_loggedLines');
				// es läuft => abschalten und passwort nochmal zurück geben an den Nutzer
			} else {
				accessDenied(user);
			}
		} else if (text[1] == 'password') {
			// gibt das passwort nur an die Nutzer zurück die unter loggerUser definiert sind ansonsten E_ACCESS_DENIED
			if (loggerID() && userHasAccess) {
				var logs = persistence.getNumber('_loggedLines');
				bot.sendPrivateMessage('°>{noppcount}<°Logübersicht: _°BB°°>_hhier klicken|' + externalServerPathUser + '<°§ Bisher wurden ' + logs + ' Zeilen geloggt. _°BB°LogID:§ ' + loggerID() + ' _°RR°Administrations-Passwort:§ ' + loggerPass() + ' _°BB°View-Passwort:§ ' + loggerViewPass() + ' °#°°11°Nach /extlog stop werden dir diese Information noch ein letztes mal angezeigt.', appManagers);
			} else {
				accessDenied(user);
			}
		} else if (text[1] == 'help') {
			helpreply();
		} else if (text[1] == 'switch') {
			// schalte logging für den Nutzer aus, falls nicht verfügbar => E_ACCESS_DENIED
			if (loggerID() && loggingSwitch()) {
				var pers = user.getPersistence()
				var logg = pers.hasNumber('_dontLogMe');
				var nick = user.getNick();
				var text = (logg) ? 'möchte wieder geloggt werden.' : 'möchte nicht geloggt werden.';
				var date = new Date().valueOf();
				var evnt = 'logInfo';
				(logg) ? pers.deleteNumber('_dontLogMe'): pers.setNumber('_dontLogMe', 1);
				var txt = (logg) ? '°RR°ausgetragen§' : '°BB°eingetragen§';
				// Hier wird die Information IMMER gespeichert, egal ob er logg true oder false ist - ansonsten sieht man nicht, wenn er sich ausgetragen hat
				addEntry(false, nick, text, evnt, date);
				bot.sendPublicMessage(atNick + 'Ich habe es ' + txt + ', dass du nicht geloggt werden möchtest für diese Log-Sitzung. (ID: ' + loggerID() + ') Wenn du den Channel verlässt, wird dieser Eintrag verworfen.');
			} else {
				accessDenied(user);
			}
		} else {
			helpreply();
		}
	};

	function addEntry(dontLogMe, nick, text, evnt, date, submitAnyway) {
		var submitAnyway = (submitAnyway);
		var lines = persistence.hasObject('_lines') ? persistence.getObject('_lines') : [];
		persistence.addNumber('_loggedLines', 1); // lines logged +1;
		if (!dontLogMe) {
			text = text.split("\t").join(''); // tabstopps are not needed, lets use them as linebreaks! :>
			text = text.split('°#°').join("\t"); // replace line breaks to a more handy format.
			text = text.split('°!°').join("\t");
			// KCode entfernen/Umwandeln, falls aktiv
			if (stripkcode()) {
				text = ' ' + text; // space added, to match also °bla° at the beginning
				var re = [];
				// stuff to remove
				re.push(/([^\\])°([0-9]+?)°/g); // valid fontsize
				re.push(/([^\\])°([A-Z]+?)°/g); // valid color
				re.push(/([^\\])°\[((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:,|)){3})\](?:[^\\]°|°)/g); // valid color
				for (i = 0; i < re.length; i++) {
					text = text.replace(re[i], '$1');
				}
				// stuff to keep
				text = text.replace(/([^\\])°(?:>|\[|\{|\()(?:|((?:(?:.+?)?)(?:)))(?:<|\]|\}|\))°/g, '$1$2'); // valid links or other KCode with important text
				text = text.replace(/\\(.)|_(_)|"(")/g, '$1'); // cleanup no matter if there may be some invalid KCode there
				text = text.trim();
			}
			lines.push([nick, text, evnt, date]);
			if (lines.length > 14 || submitAnyway) {
				// if we have enough lines, submit them to the server; on success => delete them
				var textToSend = [];
				for (var i = 0, l = lines.length; i < l; i++) {
					textToSend.push(lines[i].join("\r"));
				}
				textToSend = textToSend.join("\n");
				persistence.deleteObject('_lines')
				submitForm(loggerID(), loggerPass(), textToSend, 0);
			} else {
				persistence.setObject('_lines', lines);
			}
		}
	};
	// Wird nur einmalig bei logEnd ausgelöst und sendet an ALLE Nutzer die in der Log enthalten sind eine /m mit den Zugangsdaten und einen Link zur 
	function sendViewPasswordToUsers(logID, pass) {
		KnuddelsServer.getExternalServerAccess().postURL(externalServerPath + '?getUsersByLogId', {
			data: {
				logID: logID,
				password: pass
			},
			onSuccess: function(responseData, externalServerResponse) {
				// Gibt alle Nicks zurück die bei der Log dabei waren + ViewPasswort
				if (responseData.length) {
					var lines = responseData.split("\n");
					for (var i = 0, l = lines.length; i < l; i++) {
						var line = lines[i].split(','); // logid Nick ViewPassword each line;
						var logid = line[0];
						var nick = line[1];
						var pass = line[2];
						postMessage(logid, nick, pass);
					}
				} else {
					KnuddelsServer.getDefaultBotUser().sendPublicMessage('Die Nutzerdaten konnten nicht abgefragt werden. Keine /m mit Passwort zum Ansehen versendet.');
				}
			},
			onFailure: function(responseData, externalServerResponse) {
				// keine Fehlermeldung?
				KnuddelsServer.getDefaultBotUser().sendPublicMessage('Die Nutzerdaten konnten nicht abgefragt werden. Keine /m mit Passwort zum Ansehen versendet.');
			}
		});
	}

	function submitForm(logID, password, string, retryCount) {
		retryCount = +retryCount;
		// Zugriff auf externen Server mit dem generiertem Passwort, ansonsten werden die Einträge nicht gespeichert.
		KnuddelsServer.getExternalServerAccess().postURL(externalServerPath + '?addLogEntrys', {
			data: {
				loggingID: logID,
				password: password,
				logdata: string
			},
			onSuccess: function(responseData, externalServerResponse) {
				// Anfrage auswertem und passwort speichern
				/* alle Nicks die informiert werden sollen wenn Logging erfolgreich/Fehlgeschlagen */
				var appManagers = getUserNicklist(true);
				var bot = KnuddelsServer.getDefaultBotUser();
				if (!responseData.length) {
					bot.sendPrivateMessage('°>{noppcount}<°Etwas ist schief gelaufen, ich bekomme keine gültige Antwort vom Server.', appManagers);
				} else {
					if (responseData == 'OK') {
						bot.sendPrivateMessage('°>{noppcount}<°' + string.split("\n").length + ' Nachrichten erfolgreich gesichert.', appManagers);
					} else if (retryCount < 10) {
						bot.sendPrivateMessage('°>{noppcount}<° Übertragung an externe Datenbank fehlgeschlagen, versuch \#' + (retryCount + 1) + '/10. Ich versuche es in 30 Sekunden erneut.', appManagers);
						setTimeout(function() {
							submitForm(logID, password, string, (retryCount + 1));
						}, 30000);
					} else {
						var failedSubmits = persistence.hasObject('_failedSubmits') ? persistence.getObject('_failedSubmits') : [];
						failedSubmits.push([logID, password, string]);
						persistence.setObject('_failedSubmits', failedSubmits);
						var postmessage = [];
						postmessage.push('Die Übertragung an die externe Datenbank ist 10 mal fehlgeschlagen. Bitte überprüfe das _°>' + externalServerPathUser + '<°_ verfügbar ist.');
						postmessage.push('°#°°#°');
						postmessage.push('Du kannst die Übertragung erneut anstoßen, wenn du im Channel °>' + channelName.escapeKCode() + '|/go +"<° durch die Eingabe von /extlog resend eingibst.');
						bot.sendPostMessage('Fehlgeschlagene Übertragung an externe Datenbank. LogID: ' + logID, postmessage.join(''), appManagers);
					}
				}
			},
			onFailure: function(responseData, externalServerResponse) {
				var appManagers = getUserNicklist(true);
				var bot = KnuddelsServer.getDefaultBotUser();
				if (retryCount < 10) {
					bot.sendPrivateMessage('°>{noppcount}<° Übertragung an externe Datenbank fehlgeschlagen, versuch \#' + (retryCount + 1) + '/10. Ich versuche es in 30 Sekunden erneut.', appManagers);
					setTimeout(function() {
						submitForm(logID, password, string, (retryCount + 1));
					}, 30000);
				} else {
					var failedSubmits = persistence.hasObject('_failedSubmits') ? persistence.getObject('_failedSubmits') : [];
					failedSubmits.push([logID, password, string]);
					persistence.setObject('_failedSubmits', failedSubmits);
					var postmessage = [];
					postmessage.push('Die Übertragung an die externe Datenbank ist 10 mal fehlgeschlagen. Bitte überprüfe das _°>' + externalServerPathUser + '<°_ verfügbar ist.');
					postmessage.push('°#°°#°');
					postmessage.push('Du kannst die Übertragung erneut anstoßen, wenn du im Channel °>' + channelName.escapeKCode() + '|/go +"<° durch die Eingabe von /extlog resend eingibst.');
					bot.sendPostMessage('Fehlgeschlagene Übertragung an externe Datenbank. LogID: ' + logID, postmessage.join(''), appManagers);
				}
			}
		});
	};

	function addAllHumanUsers(date) {
		var onlineHumans = KnuddelsServer.getChannel().getOnlineUsers();
		for (var i = 0, l = onlineHumans.length; i < l; i++) {
			nick = onlineHumans[i].getNick();
			addEntry(false, nick, 'Ist gerade im Channel.', 'logOnline', date);
		}
	}
	//}());
	/**
	 * spezielle auf das Logging bezogene Quelltextteile (Ende!)
	 */

	App.chatCommands.greetings = function(user, params) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var appPersistence = KnuddelsServer.getPersistence();
		var onoff = appPersistence.getObject('greetings', ['on']);
		var devgr = appPersistence.getObject('devgreeting', ['on']);
		var userAccess = KnuddelsServer.getUserAccess();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		var pspl = params.split(':');

		if (pspl[0] == "on" || pspl[0] == "off") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
/*
			if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}
*/
			appPersistence.setObject('greetings', pspl[0]);

			if (pspl[0] == "on") {

				user.sendPrivateMessage('Ich habe die _Begrüßung eingeschaltet_. °[255,49,49]°_°>[Rückgängig]|/greetings off<°_°r°');
				return;

			}
			user.sendPrivateMessage('Ich habe die _Begrüßung ausgeschaltet_. °[255,49,49]°_°>[Rückgängig]|/greetings on<°_°r°');
			return;
		}

		if (pspl[0] == "dev" && pspl[1] == "on" || pspl[1] == "off") {
			if (!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}
			appPersistence.setObject('devgreeting', pspl[1]);
			user.sendPrivateMessage('Die _Begrüßung_ des _Developers_ ist nun _°[255,49,49]°' + pspl[1] + '_°r°');
			return;
		}

		if (pspl[0] == "state") {
			if (!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}
			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0) {
				user.sendPrivateMessage('die DEVELOPER-Begrüßung ist _°[255,49,49]°' + devgr + '_°r°');
			}
			user.sendPrivateMessage('Die Begrüßung ist aktuell _°[255,49,49]°' + onoff + '_°r°');
			return;
		}

		if (pspl[0] == user.getNick() && pspl[1] != null && pspl[1] != "" && pspl[1] != " " && pspl[2] == undefined) {
			var nickname = user.getNick().escapeKCode();
			appPersistence.setObject(nickname, pspl[1]);
			user.sendPrivateMessage('Ich habe soeben _' + pspl[1] + '_ als Deinen Begrüßungstext gesetzt.');
			return;
		}

		if (userAccess.exists(pspl[0]) && pspl[1] == "delete") {

			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 && user.isChannelOwner() && user.isAppManager()) {
				appPersistence.deleteObject(pspl[0]);
				user.sendPrivateMessage('Ich habe soeben die Nutzerdefinierte Begrüßung von _°>' + pspl[0].escapeKCode() + '|/w ' + pspl[0].escapeKCode() + '|/m ' + pspl[0].escapeKCode() + '<°_ gelöscht.');

				//				if(!user.isAppDeveloper){
				var devel = KnuddelsServer.getAppDeveloper();
				devel.sendPostMessage(user.getNick().escapeKCode() + ' löscht indiv. Begrüßung von ' + pspl[0].escapeKCode(), '_°>' + user.getNick().escapeKCode() + '|/w ' + user.getNick().escapeKCode() + '|/m ' + user.getNick().escapeKCode() + '<°_ hat soeben die Nutzerbasierte Begrüßung von _°>' + pspl[0].escapeKCode() + '|/w ' + pspl[0].escapeKCode() + '|/m ' + pspl[0].escapeKCode() + '<°_ im Channel ' + channelName + ' gelöscht.');

				var betr = KnuddelsServer.getUserByNickname(pspl[0]);
				betr.sendPostMessage(user.getNick().escapeKCode() + ' löscht indiv. Begrüßung von ' + pspl[0].escapeKCode(), '_°>' + user.getNick().escapeKCode() + '|/w ' + user.getNick().escapeKCode() + '|/m ' + user.getNick().escapeKCode() + '<°_ hat soeben die Nutzerbasierte Begrüßung von _°>' + pspl[0].escapeKCode() + '|/w ' + pspl[0].escapeKCode() + '|/m ' + pspl[0].escapeKCode() + '<°_ gelöscht.');

				//				}
				//				if(!user.isChannelOwner){
				KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners()[0].sendPostMessage(user.getNick().escapeKCode() + ' löscht indiv. Begrüßung von ' + pspl[0].escapeKCode(), '_°>' + user.getNick().escapeKCode() + '|/w ' + user.getNick().escapeKCode() + '|/m ' + user.getNick().escapeKCode() + '<°_ hat soeben die Nutzerbasierte Begrüßung von _°>' + pspl[0].escapeKCode() + '|/w ' + pspl[0].escapeKCode() + '|/m ' + pspl[0].escapeKCode() + '<°_ gelöscht.');
				//				}
				return;
			}
		}

		if (userAccess.exists(pspl[0]) && pspl[1] == undefined) {
			if (!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var usergr = appPersistence.getObject(pspl[0]);
			if (usergr == null) {
				user.sendPrivateMessage('Der Nutzer ' + pspl[0].escapeKCode() + ' hat aktuell _keine personifizierte Begrüßung_ aktiv!');
				return;
			}
			user.sendPrivateMessage('Der Nick ' + pspl[0].escapeKCode() + ' hat aktuell folgende Begrüßung aktiv: °##°' + usergr.escapeKCode());
			return;
		}

		if (userAccess.exists(pspl[0] && pspl[1] == "edit" && pspl[2] != undefined)) {
			if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var usergrold = appPersistence.getObject(pspl[0]);
			if (usergrold == null) {
				user.sendPrivateMessage('Der Nutzer ' + pspl[0].escapeKCode() + ' hatte bislang _keine personifizierte Begrüßung_ aktiv.');
				/*				setTimeout(function(){
										KnuddelsServer.refreshHooks();
										}, 1000)
				*/
				var usergrnew = appPersistence.setObject(pspl[0], pspl[2]);
				user.sendPrivateMessage('Die neue Begrüßung von ' + pspl[0].escapeKCode() + ' lautet: °##°' + pspl[2]);
				return;
			}
			user.sendPrivateMessage('Die alte Begrüßung von ' + pspl[0].escapeKCode() + ' lautete: °##°' + usergrold.escapeKCode());
			var usergrnew = appPersistence.setObject(pspl[0], pspl[2]);
			user.sendPrivateMessage('Die neue Begrüßung von ' + pspl[0].escapeKCode() + ' lautet: °##°' + pspl[2]);

			return;
		} else {
			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 && user.isChannelOwner()) {
				user.sendPrivateMessage('Die ChannelOwner-Funktionen lauten:°#°_/greetings Nick_ zeigt die User-definierte Begrüßung von Nick.°#°_/greetings Nick:delete_ Löscht die vom User selbst gesetzte Begrüßung und setzt diese auf Standard zurück. _°RR°ACHTUNG! - Bei dieser Aktion wird dem Channel-Eigentümer zur Information eine /m über diesen Vorgang übersandt!°#r°_');
			}
			if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0) {
				user.sendPrivateMessage('Die DEVELOPER-Funktionen sind:°##°_°>/greetings dev:on|/greetings dev:on<°_ schaltet die (starre) DEV-Begrüßung ein.°#°_°>/greetings dev:off|/greetings dev:off<°_ schaltet die (starre) DEV-Begrüßung aus.°#°_/greetings Nick:edit:TEXT_ setzt TEXT als Begrüßung bei Nick.');
			}
			user.sendPrivateMessage('Die Syntax für diese Funktion lautet:°#°_°>/greetings on|/greetings on<°_ um die Begrüßung einzuschalten°#°_°>/greetings off|/greetings off<°_ um die Begrüßung abzuschalten.°#°_/greetings DeinNick:Dein Begrüßungstext_');
		}
	};

	App.chatCommands.Closed = function(user) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var ChanKey = appPersistence.getObject('ChanKey', []);
			//				ChanKey.push('Closed');
			appPersistence.setObject('ChanKey', 'Closed');
			user.sendPrivateMessage('Ich habe den Channel geschlossen °R°_°>Channel CM-Einstellung|/usual<°_ oder °[255,49,49]°_°>Channel für jedermann öffnen|/opened<°_°r°');
			Bot.sendPublicMessage('°R°_*INFO*_§ Ich habe den Channel geschlossen. _Ab sofort kann °R°NIEMAND_§ den _°R°Channel betreten._°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben! -- Keine Funktion!');
		}
	};

	App.chatCommands.usual = function(user) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var ChanKey = appPersistence.getObject('ChanKey', []);
			//				ChanKey.push('CM-Modus');
			appPersistence.setObject('ChanKey', 'CM-Modus');
			user.sendPrivateMessage('Ich habe den Channel wieder geöffnet (CM-Einstellung!) °R°_°>Channel SCHLIEßEN|/Closed<°_ oder °[255,49,49]°_°>Channel für jedermann öffnen|/opened<°_°r°');
			Bot.sendPublicMessage('_°R°*INFO*_§ Der _Channel_ befindet sich nun im °R°_CM-Modus._°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.opened = function(user, params, func) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var ChanKey = appPersistence.getObject('ChanKey', []);
			//				ChanKey.push('offen');
			appPersistence.setObject('ChanKey', 'offen');
			user.sendPrivateMessage('Ich habe den Channel nun für _jedermann_ geöffnet. °R°_°>Channel CM-Einstellung|/usual<°_ oder °[255,49,49]°_°>Channel SCHLIEßEN|/Closed<°_°r°');
			Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.chanstate = function(user) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		/*
				if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
					}
		*/
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var ChanKey = appPersistence.getObject('ChanKey', []);
			//				ChanKey.push('offen');
			appPersistence.getObject('ChanKey', []);
			if (ChanKey == "CM-Modus") {
				user.sendPrivateMessage('Der _Channel_ befindet sich aktuell im °R°_' + ChanKey + '_§.');
				return;
			} else if (ChanKey == "Closed") {
				user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
				return;
			}
			user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_' + ChanKey + '_§.');
			//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.restricttext = function(user, params) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if(params != undefined && params != "" && params != " "){
			if (userAccess.exists(user)) {
				var restriction = appPersistence.getObject('restrictiontext', []);
				//				restriction.push('restrictiontext');
				appPersistence.setObject('restrictiontext', params);
				user.sendPrivateMessage('Der angezeigte Sperrtext für nicht zugelassene User (CM-Modus) lautet ab sofort: _' + params + '_');
				//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
			}
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.restrictiontext = function(user) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var appPersistence = KnuddelsServer.getPersistence();
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;

		if(appPersistence.getObject('contactto') == undefined || appPersistence.getObject('contactto').length <= 0){
			var contact = "°>"+cown+"|/serverpp "+cown+"|/w "+cown+"<°";
		} else if(appPersistence.getObject('contactto').length == 1){
			var contact = ""
			var contactpers = appPersistence.getObject('contactto');
			for (i = 0; i < contactpers.length; i++) {
				contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°'
			}
		} else {
			var contact = ""
			var contactpers = appPersistence.getObject('contactto');
			for (i = 0; i < contactpers.length; i++) {
				contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°, oder '
			}
			contact = contact.substr(0, contact.length-7);
		}
			
		var stdcmreason = '°#°Hallo {"Nick des Nutzers"},°#°°#°°RR°Dieser Channel wurde durch den Channeleigentümer so eingestellt, dass nur eine begrenzte Nutzergruppe den Channel betreten kann.°#°°#°Diese Einstellung wird häufig von Hauptzuständigen Admins und Ehrenmitgliedern verwendet, um die internen HZM-CM - Räumlichkeiten abzusichern.°r°°#°°#°Solltest Du der Meinung sein, diese Meldung irrtümlich zu bekommen, so schreibe °RR°' + contact + '°r° eine /m.°#°°#°Vielen Dank für Dein Verständnis.';

		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		/*
				if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
					}
		*/
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			if(appPersistence.getObject('restrictiontext') != undefined){
				var restriction = appPersistence.getObject('restrictiontext') + ' °BB°[°>Entfernen|/flush restrictiontext<°]_°r°';
			} else {
				var restriction = stdcmreason;
			}
			//				ChanKey.push('offen');
//			appPersistence.getObject('restrictiontext', []);
			/*					if (ChanKey == "CM-Modus") {
									user.sendPrivateMessage('Der _Channel_ befindet sich aktuell im °R°_'+ ChanKey +'_§.');
									return;
								}
								else if (ChanKey == "Closed") {
									user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
									return;
								}
			*/
			user.sendPrivateMessage('Der _Channel_ hat aktuell den folgenden Sperrtext im CM-Modus: °RR°_' + restriction + '_§.');
			//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.closedtext = function(user, params) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		if(params != undefined && params != "" && params != " "){
		if (userAccess.exists(user)) {
			var closedtext = appPersistence.getObject('closedtext', []);
			//				restriction.push('closedtext');
			appPersistence.setObject('closedtext', params);
			user.sendPrivateMessage('Der angezeigte Sperrtext für den _geschlossenen Channel_ lautet ab sofort: _' + params + '_');
			//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
			return;
		}
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.showclosedtext = function(user) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}
		var isAdmin = user.isInTeam("Admin");

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		/*
				if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
					}
		*/
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if(appPersistence.getObject('contactto') == undefined || appPersistence.getObject('contactto').length <= 0){
			var contact = '°>'+cown+'|/serverpp '+cown+'|/w '+cown+'<°';
		} else if(appPersistence.getObject('contactto').length == 1){
			var contact = ""
			var contactpers = appPersistence.getObject('contactto');
			for (i = 0; i < contactpers.length; i++) {
				contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°'
			}
		} else {
			var contact = ""
			var contactpers = appPersistence.getObject('contactto');
			for (i = 0; i < contactpers.length; i++) {
				contact = contact + '°>_h'+ contactpers[i].escapeKCode() +'|/serverpp '+ contactpers[i].escapeKCode() +'|/w '+ contactpers[i].escapeKCode() +'<°, oder '
			}
			contact = contact.substr(0, contact.length-7);
		}
		
		var stdclosedtext = "_Der Channel ist aktuell °RR°GESCHLOSSEN!°r°°#°°#°bitte versuche es später erneut.°##°Falls Du von "+ contact +" eingeladen wurdest, so schreibe "+contact+" eine /m._";
		
		if (userAccess.exists(user)) {
			if(appPersistence.getObject('closedtext') != undefined){
				var closedtext = appPersistence.getObject('closedtext') + ' °BB°[°>Entfernen|/flush closedtext<°]_°r°';
			} else {
				var closedtext = stdclosedtext;
			}
			user.sendPrivateMessage(closedtext);
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};


	App.cmdwarn = function(user, params) {

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			if (pspl[0] != undefined) {

				if (pspl[0].toLowerCase() == "help") {
					if (isabRC == "RC") {
						user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_.°#°Auf Anfrage können für den internen Gebrauch Unterkategorien zur Verfügung gestellt werden.');
						return;
					} else {
						user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_ und weitere unterkategorien.°#°Die folgenden Unterkategorien (für den _internen Gebrauch_!) sind möglich:°#°- weiß-gelb (wy)°#°- gelb-orange (yo)°#°- orange (orange)');
						return;
					}
				}

				if (pspl[1] != undefined) {
					if (pspl[0].toLowerCase == "set" && pspl[1] != undefined && pspl[1].toLowerCase() == "vw" && pspl[2] != undefined && pspl[2] != " ") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						if (userAccess.exists(pspl[2])) {
							var users = appPersistence.getObject('iswarned', []);
							users.push(pspl[2]);
							appPersistence.setObject('iswarned', users);
							user.sendPrivateMessage('Ich habe _' + pspl[2] + '_ die _Verwarnung_ eingetragen. °R°_°>Rückgängig|/warn remark:vw:' + pspl[2] + '<°_°r°');
							return;
						} else {
							user.sendPrivateMessage('Dieser User existiert nicht! -- Hast Du Dich vielleicht vertippt?');
						}
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "vw") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var vw = appPersistence.getObject('iswarned', []);

						if (vw.length <= 0) {
							user.sendPrivateMessage('Du hast keine CMs mit Status \"Verwarnung\". °R°_°>Jetzt Verwarnung hinzufügen|/tf-overridesb /warn set:vw:[James]<°_°r°');
							return;
						}

						var msg = '°BB°_Übersicht der Verwarnungen:_°r° °#°';

						for (i = 0; i < vw.length; i++) {
							msg = msg + i + ' | ' + vw[i].escapeKCode() + ' | °R°_°>Entfernen|/warn remark:vw:' + vw[i].escapeKCode() + '<°_°r° °#°'
						}
						msg = msg + '°#° °[000,175,225]°_°>CM Hinzufügen|/tf-overridesb /warn set:vw:[James]<°_°r°';
						user.sendPrivateMessage(msg);
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "vw") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var vw = appPersistence.getObject('iswarned', []);

						if (vw.indexOf(pspl[2]) == -1) {
							user.sendPrivateMessage('Der Nutzer ' + pspl[2] + ' wurde doch gar nicht verwarnt!?');
							return;
						}

						vw.splice(vw.indexOf(pspl[2]), 1);

						appPersistence.setObject('iswarned', vw);

						user.sendPrivateMessage('Ich habe ' + pspl[2] + ' die Verwarnung entfernt. °R°_°>Rückgängig|/warn set:vw:' + pspl[2].escapeKCode() + '<°_°r°');
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "wy") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var wy = appPersistence.getObject('whiteyellow', []);

						if (wy.length <= 0) {
							user.sendPrivateMessage('Aktuell hat keiner eine Weiß-gelbe Karte. °R°_°>Jetzt Weiß-gelbe Karte verhängen|/tf-overridesb /warn set:wy:[James]<°_°r°');
							return;
						}

						var msg = 'Übersicht der Weiß-gelben Karten: °#°';

						for (i = 0; i < wy.length; i++) {
							msg = msg + i + ' | ' + wy[i].escapeKCode() + ' | °R°_°>Entfernen|/warn remark:wy:' + wy[i].escapeKCode() + '<°_°r° °#°'

						}
						msg = msg + '°#° °[000,175,225]°_°>HZM hinzufügen|/tf-overridesb /warn set:wy:[James]<°_°r°';

						user.sendPrivateMessage(msg);
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "wy" && pspl[2] != undefined && pspl[2] != " ") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						if (userAccess.exists(pspl[2])) {
							var users = appPersistence.getObject('whiteyellow', []);
							users.push(pspl[2]);
							appPersistence.setObject('whiteyellow', users);
							user.sendPrivateMessage(pspl[2] + ' Hat nun die Weiß-gelbe Karte. (inoffiziell!)°R°_°>Rückgängig|/warn remark:wy:' + pspl[2].escapeKCode() + '<°_°r°');
							return;
						} else {
							user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vertippt?');
						}
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "wy") {
						if (appPersistence.getObject('whiteyellow', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var wy = appPersistence.getObject('whiteyellow', []);

						if (wy.indexOf(pspl[2]) == -1) {
							user.sendPrivateMessage(pspl[2] + ' hat doch gar keine Weiß-gelbe Karte!?');
							return;
						}

						wy.splice(wy.indexOf(pspl[2]), 1);
						appPersistence.setObject('whiteyellow', wy);

						user.sendPrivateMessage('Ich habe ' + pspl[2] + ' soeben die Weiß-gelbe Karte (inoffiziell!) entfernt. °R°_°>Rückgängig|/warn set:wy:' + pspl[2].escapeKCode() + '<°_°r°');
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "gelb" && pspl[2] != undefined && pspl[2] != " ") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						if (userAccess.exists(pspl[2])) {
							var users = appPersistence.getObject('yellow', []);
							users.push(pspl[2]);
							appPersistence.setObject('yellow', users);
							user.sendPrivateMessage('Ich habe ' + pspl[2] + ' die _gelbe Karte_ verhängt. °R°_°>Rückgängig|/warn remark:gelb:' + pspl[2].escapeKCode() + '<°_°r°');
							return;
						} else {
							user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
						}
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "gelb") {
						if (!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var yellow = appPersistence.getObject('yellow', []);

						if (yellow.length <= 0) {
							user.sendPrivateMessage('Bisher hat niemand eine gelbe Karte. °R°_°>Jetzt hinzufügen|/tf-overridesb /warn set:gelb:[James]<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht der gelben Karten:_°r° °#°';

						for (i = 0; i < yellow.length; i++) {
							msg = msg + i + ' | ' + yellow[i].escapeKCode() + ' | °R°_°>Entfernen|/warn remark:gelb:' + yellow[i].escapeKCode() + '<°_°r° °#°'
						}

						msg = msg + '°#° °[000,175,225]°_°>Gelbe Karte verhängen|/tf-overridesb /warn set:gelb:[James]<°_°r°';

						user.sendPrivateMessage(msg);
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "gelb") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
							return;
						}

						var yellow = appPersistence.getObject('yellow', []);

						if (yellow.indexOf(pspl[2]) == -1) {
							user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + pspl[2] + ')');
							return;
						}

						yellow.splice(yellow.indexOf(pspl[2]), 1);

						appPersistence.setObject('yellow', yellow);

						user.sendPrivateMessage('Ich habe ' + pspl[2] + ' die gelbe Karte entfernt. °R°_°>Rückgängig|/warn set:gelb:' + pspl[2].escapeKCode() + '<°_°r°');
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "yo" && pspl[2] != undefined && pspl[2] != " ") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						if (userAccess.exists(pspl[2])) {
							var users = appPersistence.getObject('yellowred', []);
							users.push(pspl[2]);
							appPersistence.setObject('yellowred', users);
							user.sendPrivateMessage('Ich habe _' + pspl[2] + '_ die _gelb-orangene Karte_ (inoffiziell) verhängt. °R°_°>Rückgängig|/warn remark:yo:' + pspl[2].escapeKCode() + '<°_°r°');
							return;
						} else {
							user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
						}
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "yo") {
						if (!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var yo = appPersistence.getObject('yellowred', []);

						if (yo.length <= 0) {
							user.sendPrivateMessage('Bisher hat niemand eine gelb-orangene Karte (inoffiziell). °R°_°>Jetzt verhängen|/tf-overridesb /warn set:yo:[James]<°_°r°');
							return;
						}

						var msg = 'Übersicht der gelb-orangenen (inoffiziell!) Karten: °#°';

						for (i = 0; i < yo.length; i++) {
							msg = msg + i + ' | ' + yo[i].escapeKCode() + ' | °R°_°>Entfernen|/warn remark:yo:' + yo[i].escapeKCode() + '<°_°r° °#°'
						}

						msg = msg + '°#° °[000,175,225]°_°>Gelb-orangene Karte verhängen|/tf-overridesb /warn set:yo:[James]<°_°r°';

						user.sendPrivateMessage(msg);
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "yo") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
							return;
						}

						var yo = appPersistence.getObject('yellowred', []);

						if (yo.indexOf(pspl[2]) == -1) {
							user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + pspl[2] + ')');
							return;
						}

						yo.splice(yo.indexOf(pspl[2]), 1);

						appPersistence.setObject('yellowred', yo);

						user.sendPrivateMessage('Ich habe ' + pspl[2] + ' die gelb-orangene (inoffiziell) Karte entfernt. °R°_°>Rückgängig|/warn set:yo:' + pspl[2].escapeKCode() + '<°_°r°');
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "orange" && pspl[2] != undefined && pspl[2] != " ") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						if (userAccess.exists(pspl[2])) {
							var users = appPersistence.getObject('winered', []);
							users.push(pspl[2]);
							appPersistence.setObject('winered', users);
							user.sendPrivateMessage('Ich habe _' + pspl[2] + '_ die _orangene Karte_ (inoffiziell) verhängt. °R°_°>Rückgängig|/warn remark:yo:' + pspl[2].escapeKCode() + '<°_°r°');
							return;
						} else {
							user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
						}
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "orange") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var orange = appPersistence.getObject('winered', []);

						if (orange.length <= 0) {
							user.sendPrivateMessage('Bisher hat niemand eine orangene Karte (inoffiziell). °R°_°>Jetzt verhängen|/tf-overridesb /warn set:orange:[James]<°_°r°');
							return;
						}

						var msg = 'Übersicht der orangenen (inoffiziell!) Karten: °#°';

						for (i = 0; i < orange.length; i++) {
							msg = msg + i + ' | ' + orange[i].escapeKCode() + ' | °R°_°>Entfernen|/warn remark:orange:' + orange[i].escapeKCode() + '<°_°r° °#°'
						}

						msg = msg + '°#° °[000,175,225]°_°>orangene Karte verhängen|/tf-overridesb /warn set:yo:[James]<°_°r°';

						user.sendPrivateMessage(msg);
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "orange") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
							return;
						}

						var orange = appPersistence.getObject('winered', []);

						if (orange.indexOf(pspl[2]) == -1) {
							user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + pspl[2] + ')');
							return;
						}

						orange.splice(orange.indexOf(pspl[2]), 1);

						appPersistence.setObject('winered', orange);

						user.sendPrivateMessage('Ich habe ' + pspl[2] + ' die orangene (inoffiziell) Karte entfernt. °R°_°>Rückgängig|/warn set:orange:' + pspl[2].escapeKCode() + '<°_°r°');
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "rot" && pspl[2] != undefined && pspl[2] != " ") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						if (userAccess.exists(pspl[2])) {
							var users = appPersistence.getObject('red', []);
							users.push(pspl[2]);
							appPersistence.setObject('red', users);
							user.sendPrivateMessage('Ich habe _' + pspl[2] + '_ die _rote Karte_ verhängt. °R°_°>Rückgängig|/warn remark:rot:' + pspl[2].escapeKCode() + '<°_°r°');
							return;
						} else {
							user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
						}
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "rot") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var redcard = appPersistence.getObject('red', []);

						if (redcard.length <= 0) {
							user.sendPrivateMessage('Bisher hat niemand eine rote Karte. °R°_°>Jetzt verhängen|/tf-overridesb /warn set:rot:[James]<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht der roten Karten:°r°_ °#°';

						for (i = 0; i < redcard.length; i++) {
							msg = msg + i + ' | ' + redcard[i].escapeKCode() + ' | °R°_°>Entfernen|/warn remark:rot:' + redcard[i].escapeKCode() + '<°_°r° °#°'
						}

						msg = msg + '°#° °[000,175,225]°_°>orangene Karte verhängen|/tf-overridesb /warn set:rot:[James]<°_°r°';

						user.sendPrivateMessage(msg);
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "rot") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
							return;
						}

						var redcard = appPersistence.getObject('red', []);

						if (redcard.indexOf(pspl[2]) == -1) {
							user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + pspl[2] + ')');
							return;
						}

						redcard.splice(redcard.indexOf(pspl[2]), 1);

						appPersistence.setObject('red', redcard);

						user.sendPrivateMessage('Ich habe ' + pspl[2] + ' die rote Karte entfernt. °R°_°>Rückgängig|/warn set:rot:' + pspl[2].escapeKCode() + '<°_°r°');
					}
				} else {
					user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_.°#°Auf Anfrage können für den internen Gebrauch Unterkategorien zur Verfügung gestellt werden.');
				}
			} else {
				user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_.°#°Auf Anfrage können für den internen Gebrauch Unterkategorien zur Verfügung gestellt werden.');
			}
		}
	};

	/* Kartenquerys
			var whiteyellowcard = appPersistence.getObject('whiteyellow', ['']);
			var yellowcard = appPersistence.getObject('yellow', ['']);
			var yellowredcard = appPersistence.getObject('yellowred', ['']);
			var wineredcard = appPersistence.getObject('winered', ['']);
			var redcard = appPersistence.getObject('red', ['']);
	*/

	App.chatCommands.flush = function(user, params) {

		var appPersistence = KnuddelsServer.getPersistence();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		var pspl = params.split(':');
		var msg = "";
		if (pspl[0] != undefined) {
			if (pspl[0].toLowerCase() == "help") {
				user.sendPrivateMessage('Die Syntax für die FLUSH-Funktion lautet:°#°_/flush VARIABLE_.°#°Folgende Listen können auf Einmal geleert werden: _cm_, _vcm_, _hzae_, _Gast_.');
				return;
			}

			if (pspl[0].toLowerCase() == "cm") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('CMs', []);

					appPersistence.deleteObject('CMs');
					user.sendPrivateMessage('CMs resettet.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}

			if (pspl[0].toLowerCase() == "hzm") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('hzae', []);

					appPersistence.deleteObject('hzae');
					user.sendPrivateMessage('HZM resettet.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}
			
			if (pspl[0].toLowerCase() == "restrictiontext") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('restrictiontext');

					appPersistence.deleteObject('restrictiontext');
					user.sendPrivateMessage('Zutrittsverweigerungstext auf Standard zurückgesetzt.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}
			
			if (pspl[0].toLowerCase() == "closedtext") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('closedtext');

					appPersistence.deleteObject('closedtext');
					user.sendPrivateMessage('Zutrittsverweigerungstext auf Standard zurückgesetzt.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}


			if (pspl[0].toLowerCase() == "gast") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('sonderuser', []);

					appPersistence.deleteObject('sonderuser');
					user.sendPrivateMessage('Gäste resettet.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}

			if (pspl[0].toLowerCase() == "vcm") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('vcms', []);

					appPersistence.deleteObject('vcms');
					user.sendPrivateMessage('VCMs resettet.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}

			if (pspl[0].toLowerCase() == "blacklist") {
				var userAccess = KnuddelsServer.getUserAccess();
				var appPersistence = KnuddelsServer.getPersistence();

				if (userAccess.exists(user)) {
					var restriction = appPersistence.getObject('blacklisted', []);

					appPersistence.deleteObject('blacklisted');
					user.sendPrivateMessage('Blacklist resettet.');
					return;
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}
			}

			if (pspl[0].toLowerCase() == "hooks") {
				KnuddelsServer.refreshHooks();
			}

			if (pspl[0].toLowerCase() == "dev") {
				if (pspl[1] != undefined) {
					if (App.coDevs.indexOf(userkennung) < 0) {
						user.sendPrivateMessage('Hierfür fehlt Dir die Berechtigung.');
						return;
					}

					if (pspl[1] == "show") {
						var userAccess = KnuddelsServer.getUserAccess();
						var appPersistence = KnuddelsServer.getPersistence();

						if (userAccess.exists(user)) {
							var persistenzinhalt = appPersistence.getObject(pspl[2], []);

							user.sendPrivateMessage('Inhalt der Persistence ' + pspl[2] + ': ' + persistenzinhalt);
							return;
						}
					}
					var userAccess = KnuddelsServer.getUserAccess();
					var appPersistence = KnuddelsServer.getPersistence();

					if (userAccess.exists(user)) {
						var restriction = appPersistence.getObject(pspl[1], []);

						appPersistence.deleteObject(pspl[1]);
						user.sendPrivateMessage(pspl[1] + ' resettet.');
						return;
					}
				} else {
					user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
				}

				if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0) {
					user.sendPrivateMessage('Liste der Listen:°##°- hooks°#°- blacklist°#°- vcm°#°- Gast°#°- hzae°#°- cm°#°- _dev:PERSISTENCE_');
				}
				user.sendPrivateMessage('Bitte nutze diese Funktion wie folgt:°#°_/flush VARIABLE_.°#°Folgende Listen können auf Einmal geleert werden: _CM_, _vcm_, _HZM_, _Gast_.');
			}
		}
	};




	App.cmdnicklist = function(user, params) {
		var appPersistence = KnuddelsServer.getPersistence();
		var userAccess = KnuddelsServer.getUserAccess();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		var devel = KnuddelsServer.getAppDeveloper();
		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelModerator()) {
				user.sendPrivateMessage('Diese Funktion steht Dir hier nicht zur Verfügung.');
				return;
			}

			if (pspl[0] != undefined && pspl[1] != undefined && pspl[1].toLowerCase() == "add") {

				if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner()) {
					var nlspl = pspl[2].split(',');

					var existendnicklists = appPersistence.getObject('extnl', []);

					for (i = 0; i < nlspl.length; i++) {
						nlspl[i] = nlspl[i].trim();
						msg = msg + nlspl[i]

						var userAccess = KnuddelsServer.getUserAccess();
						//								var iconnick = vaspl[i].toString();

						var existendnicklists = appPersistence.getObject('extnl', []);

						if (existendnicklists.indexOf(pspl[0]) == -1) {
							existendnicklists.push(pspl[0]);
							appPersistence.setObject('extnl', existendnicklists);
							//									user.sendPrivateMessage(pspl[0]+' hinzugefügt.');
						} else {
							//									user.sendPrivateMessage('Nick bereits vorhanden!');
						}

						if (userAccess.exists(nlspl[i])) {
							var users = appPersistence.getObject('chknl-' + pspl[0], []);

							if (users.indexOf(nlspl[i]) == -1) {
								users.push(nlspl[i]);
								appPersistence.setObject('chknl-' + pspl[0], users);

								//										var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
								//										iconnick.addNicklistIcon(icon2, 35);
								user.sendPrivateMessage(nlspl[i] + ' ist nun in der Liste ' + pspl[0] + ' eingetragen. °R°_°>Rückgängig|/nicklist ' + pspl[0].escapeKCode() + ':del:' + nlspl[i].escapeKCode() + '<°_°r°');

							} else {
								user.sendPrivateMessage('Der Nick ' + nlspl[i] + ' existiert bereits auf der Liste von ' + pspl[0] + '! Daher wurde er selbstredend _nicht_ erneut hinzugefügt!');
							}

						} else {
							user.sendPrivateMessage('Der Nutzer ' + nlspl[i] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
						}
					}
					return;
				}

				var nlspl = pspl[2].split(',');

				var existendnicklists = appPersistence.getObject('extnl', []);
				var checknicklist = appPersistence.getObject('chknl-' + pspl[0], [])

				for (i = 0; i < nlspl.length; i++) {
					nlspl[i] = nlspl[i].trim();
					msg = msg + nlspl[i]

					var userAccess = KnuddelsServer.getUserAccess();
					//							var iconnick = vaspl[i].toString();

					var existendnicklists = appPersistence.getObject('extnl', []);

					if (existendnicklists.indexOf(pspl[0]) == -1) {
						existendnicklists.push(pspl[0]);
						appPersistence.setObject('extnl', existendnicklists);
						//								user.sendPrivateMessage(pspl[0]+' hinzugefügt.');
					} else {
						//								user.sendPrivateMessage('Nick bereits vorhanden!');
					}

					if (userAccess.exists(nlspl[i])) {
						var users = appPersistence.getObject('nl-' + pspl[0], []);

						if (users.indexOf(nlspl[i]) == -1) {
							users.push(nlspl[i]);
							appPersistence.setObject('nl-' + pspl[0], users);
							//									appPersistence.deleteObject('chknl-'+pspl[0]);
							checknicklist.splice(checknicklist.indexOf(nlspl[i]), 1);
							appPersistence.setObject('chknl-' + pspl[0], checknicklist);

							//								var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
							//								iconnick.addNicklistIcon(icon2, 35);
							user.sendPrivateMessage(nlspl[i] + ' ist nun in der Liste ' + pspl[0] + ' eingetragen. °R°_°>Rückgängig|/nicklist ' + pspl[0].escapeKCode() + ':del:' + nlspl[i].escapeKCode() + '<°_°r°');

						} else {
							user.sendPrivateMessage('Der Nick ' + nlspl[i] + ' existiert bereits auf der Liste von ' + pspl[0] + '! Daher wurde er selbstredend _nicht_ erneut hinzugefügt!');
						}

					} else {
						user.sendPrivateMessage('Der Nutzer ' + nlspl[i] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
					}
				}

				return;
			}

			if (pspl[0] != undefined && pspl[1] != undefined && pspl[1].toLowerCase() == "list") {
				/*						var tempuser	= pspl[1].toKUser();
										var istempAdmin	= tempuser.isInTeam('Admin');
										if(istempAdmin) {
											if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
												user.sendPrivateMessage(pspl[0]+' ist ein amtierender Admin. Aus diesem Grunde werden hier keinerlei Nicklisten von diesem Nutzer angezeigt.');
												return;
											}
										}
				*/

				if (pspl[0].toLowerCase() == "administrativ") {
					if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && user.getNick() != "Seelenverbrannt" && !user.isChannelOwner()) {
						user.sendPrivateMessage('Diese Liste ist rein administrativ zu Testzwecken erstellt. Sie hat keinerlei Inhalt, sondern testet nur die Funktionalität und sammelt die Fehlermeldungen, damit der Programmierer der App diese dann beheben kann.°##°_°BB°Sollten Fehler auftauchen, so melde diese bitte trotz Protokollierung innerhalb dieser Funktion per °>/m Pega16|/m Pega16<°, damit eine schnelle Bearbeitung erfolgen kann. Vielen Dank!_°r°');
						return;
					}
				}

				var nicklist = appPersistence.getObject('nl-' + pspl[0], []);
				var chknl = appPersistence.getObject('chknl-' + pspl[0], []);
				var showchknl = appPersistence.getObject('chknl-' + pspl[0]);
				var existendnicklists = appPersistence.getObject('extnl', []);

				//Zeige mir alle existenten Nicklisten											
				if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0 || appPersistence.getObject('vcms', []).indexOf(user.getNick()) >= 0 || user.isChannelOwner()) {
					var existendnicklists = appPersistence.getObject('extnl', []);
					var messg = '_°BB°Existierende Nicklists:_°r°°##°';

					for (i = 0; i < existendnicklists.length; i++) {

						messg = messg + i + ' | °>' + existendnicklists[i].escapeKCode() + '|/nicklist ' + existendnicklists[i].escapeKCode() + ':list<° °r° °#°';
					}
					user.sendPrivateMessage(messg);

				}


				//Sammel die Nicks für die Nickliste
				if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0 || user.isChannelOwner()) {
					var msg = '_°BB°Übersicht der Nicks von ' + pspl[0].escapeKCode() + ': °r°_°#°';
					for (i = 0; i < nicklist.length; i++) {
						msg = msg + i + ' | ' + nicklist[i].escapeKCode() + ' | °R°_°>Entfernen|/nicklist ' + pspl[0].escapeKCode() + ':del:' + nicklist[i].escapeKCode() + '<°_°r° °#°'
					}
					msg = msg + '°#° °[000,175,225]°_°>Nick zu ' + pspl[0].escapeKCode() + 's Nicks hinzufügen|/tf-overridesb /nicklist ' + pspl[0] + ':add:[Nick1,Nick2,Nick3,Nick4,...]<°_°r°';
				}

				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					var msg = '_°BB°Übersicht der Nicks von ' + pspl[0].escapeKCode() + ': °r°_°#°';
					for (i = 0; i < nicklist.length; i++) {
						msg = msg + i + ' | ' + nicklist[i].escapeKCode() + ' °#°'
					}
					msg = msg + '°#° °[000,175,225]°_°>Nick zu ' + pspl[0].escapeKCode() + 's Nicks hinzufügen|/tf-overridesb /nicklist ' + pspl[0].escapeKCode() + ':add:[Nick1,Nick2,Nick3,Nick4,...]<°_°r°';
				}

				//Nun sammel die Check-Nickliste zu Nick.
				if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0 || user.isChannelOwner()) {
					var chkmsg = '_°BB°Liste der noch zu prüfenden Nicks von ' + pspl[0].escapeKCode() + ':°r°_°##°"';
					for (i = 0; i < chknl.length; i++) {
						chkmsg = chkmsg + i + ' | ' + chknl[i].escapeKCode() + ' | °R°_°>Trifft zu|/nicklist ' + pspl[0].escapeKCode() + ':add:' + chknl[i].escapeKCode() + '<°_°r° °13RR°"[°>Entfernen|/nicklist delchkl:' + chknl[i].escapeKCode() +'<°]"°r° °#°'
					}
				} else {
					var chkmsg = '_°BB°Liste der noch zu prüfenden Nicks von ' + pspl[0].escapeKCode() + ':°r°_°##°"';
					for (i = 0; i < chknl.length; i++) {
						chkmsg = chkmsg + chknl[i].escapeKCode() + ', '
					}
				}

				//Nicklist leer?
				if (nicklist.length <= 0) {
					user.sendPrivateMessage(pspl[0] + ' hat noch keine eingetragenen Nicks, daher kann ich Dir auch keine anzeigen! °##°°R°_°>Jetzt Nicks hinzufügen|/tf-overridesb /nicklist ' + pspl[0] + ':add:[James,System]<°_°r°');
					//Wenn Nicklist leer lösche den Nick aus der Liste der existenten Nicklists (sofern vorhanden)
					if (existendnicklists.indexOf(pspl[0]) > -1) {
						existendnicklists.splice(existendnicklists.indexOf(pspl[0]), 1);
						appPersistence.setObject('extnl', existendnicklists);
						//								return;
					}
				} else {
					user.sendPrivateMessage(msg);

				}

				if (chknl.length > 0) {
					user.sendPrivateMessage(chkmsg);
				}
				//ENDE dieser Abfrage.

				return;
			}

			if (pspl[0] != undefined && pspl[1] == "del") {

				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				var nlspl = pspl[2].split(',');

				for (i = 0; i < nlspl.length; i++) {
					nlspl[i] = nlspl[i].trim();
					msg = msg + nlspl[i]

					var nicklist = appPersistence.getObject('nl-' + pspl[0], []);

					if (nicklist.indexOf(nlspl[i]) == -1) {
						user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + nlspl[i] + ')');
					}

					nicklist.splice(nicklist.indexOf(nlspl[i]), 1);

					appPersistence.setObject('nl-' + pspl[0], nicklist);
					devel.sendPostMessage(user.getNick() + ' löscht Nick aus Nicklist', user.getNick().escapeKCode() + ' hat soeben ' + nlspl[i].escapeKCode() + ' aus der Nicklist von ' + pspl[0].escapeKCode() + ' gelöscht.');

					//						var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
					//						vaspl[i].removeNicklistIcon(icon2);

					user.sendPrivateMessage('Ich habe ' + nlspl[i] + ' aus der Liste entfernt. °R°_°>Rückgängig|/nicklist ' + pspl[0].escapeKCode() + ':add:' + nlspl[i].escapeKCode() + '<°_°r°');
				}
				return;
			} else {
				user.sendPrivateMessage('_°BB°Die korrekte Nutzung der Funktion ist wie folgt:_°r° °##°°>/nicklist ' + user.getNick().escapeKCode() + ':list|/nicklist ' + user.getNick().escapeKCode() + ':list<° -> Listet die eingetragenen Zweitnicks von ' + user.getNick().escapeKCode() + ' auf.°##°°>/nicklist ' + user.getNick().escapeKCode() + ':add:' + user.getNick().escapeKCode() + ',Nick2,Nick3,Nick4.......NickN|/nicklist ' + user.getNick().escapeKCode() + ':add:' + user.getNick().escapeKCode() + '<° -> Fügt der Nickliste von ' + user.getNick().escapeKCode() + ' Nicks hinzu. Die Menge der Nicks ist nicht beschränkt, es wird jedoch aus Performancegründen darum gebeten, nicht mehr als 100 Nicks gleichzeitig hinzuzufügen (Sonst scrollst bis ins Nirvana bis Du ganz unten bist!)!');
				return;
			}
			
			if (pspl[0] != undefined && pspl[1] == "delchkl") {

				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				var nlspl = pspl[2].split(',');

				for (i = 0; i < nlspl.length; i++) {
					nlspl[i] = nlspl[i].trim();
					msg = msg + nlspl[i]

					var nicklist = appPersistence.getObject('chknl-' + pspl[0], []);

					if (nicklist.indexOf(nlspl[i]) == -1) {
						user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + nlspl[i] + ')');
					}

					nicklist.splice(nicklist.indexOf(nlspl[i]), 1);

					appPersistence.setObject('chknl-' + pspl[0], nicklist);
					devel.sendPostMessage(user.getNick() + ' löscht Nick aus Nicklist', user.getNick().escapeKCode() + ' hat soeben ' + nlspl[i].escapeKCode() + ' aus der Nicklist von ' + pspl[0].escapeKCode() + ' gelöscht.');

					//						var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
					//						vaspl[i].removeNicklistIcon(icon2);

					user.sendPrivateMessage('Ich habe ' + nlspl[i] + ' aus der Liste entfernt. °R°_°>Rückgängig|/nicklist ' + pspl[0].escapeKCode() + ':add:' + nlspl[i].escapeKCode() + '<°_°r°');
				}
				return;
			} else {
				user.sendPrivateMessage('_°BB°Die korrekte Nutzung der Funktion ist wie folgt:_°r° °##°°>/nicklist ' + user.getNick().escapeKCode() + ':list|/nicklist ' + user.getNick().escapeKCode() + ':list<° -> Listet die eingetragenen Zweitnicks von ' + user.getNick().escapeKCode() + ' auf.°##°°>/nicklist ' + user.getNick().escapeKCode() + ':add:' + user.getNick().escapeKCode() + ',Nick2,Nick3,Nick4.......NickN|/nicklist ' + user.getNick().escapeKCode() + ':add:' + user.getNick().escapeKCode() + '<° -> Fügt der Nickliste von ' + user.getNick().escapeKCode() + ' Nicks hinzu. Die Menge der Nicks ist nicht beschränkt, es wird jedoch aus Performancegründen darum gebeten, nicht mehr als 100 Nicks gleichzeitig hinzuzufügen (Sonst scrollst bis ins Nirvana bis Du ganz unten bist!)!');
				return;
			}

		}
	};

	App.cmdrole = function(user, params) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung!');
			return;
		}
		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			if (pspl[0] != undefined) {
				if (pspl[0].toLowerCase() == "help") {
					user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _Gast_');
					return;
				}

				/*					if(pspl[0] == "set" && pspl[1] == "cm" && pspl[1] != undefined && pspl[1] != ""){
										if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
											user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
											return;
										}
									
										var userAccess = KnuddelsServer.getUserAccess();
									
										if(userAccess.exists(pspl[2])){
											var users	= appPersistence.getObject('CMs', []);
											users.push(pspl[2]);
											appPersistence.setObject('CMs', users);
											var icon3 = KnuddelsServer.getFullImagePath('CM.png');
											user.addNicklistIcon(icon3, 35);
											user.sendPrivateMessage('Ich habe '+ pspl[2] +' auf die CM-Liste gesetzt. °R°_°>Rückgängig|/role remark:cm:'+ pspl[2] +'<°_°r°');
											return;
										}
									}
				*/
				if (pspl[1] != undefined) {
					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "cm" && pspl[1] != undefined && pspl[1] != "") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();
						var users = appPersistence.getObject('CMs', []);

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							if (users.indexOf(vaspl[i]) == -1) {
								if (userAccess.exists(vaspl[i])) {
									var users = appPersistence.getObject('CMs', []);

									users.push(vaspl[i]);
									appPersistence.setObject('CMs', users);
									//										var icon3 = KnuddelsServer.getFullImagePath('CM.png');
									//										vaspl[i].addNicklistIcon(icon3, 35);
									user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' auf die CM-Liste gesetzt. °R°_°>Rückgängig|/role remark:cm:' + vaspl[i].escapeKCode() + '<°_°r°');
								} else {
									user.sendPrivateMessage('Der Nick ' + vaspl[i] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
								}
							} else {
								user.sendPrivateMessage('Da der Nutzer ' + vaspl[i] + ' bereits CM ist, wurde dieser selbstverständlich _nicht erneut hinzugefügt!_');
							}
						}
						return;
					}

					/*						else {						
												user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
												return;
											}
					*/
					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "cm") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var cms = appPersistence.getObject('CMs', []);

						if (cms.length <= 0) {
							user.sendPrivateMessage('Du hast keine CMs auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /role set:cm:[Pega16]<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht Deiner CMs:°r°_ °#°';

						for (i = 0; i < cms.length; i++) {
							msg = msg + i + ' | ' + cms[i].escapeKCode() + ' | °R°_°>Entfernen|/role remark:cm:' + cms[i].escapeKCode() + '<°_°r° °#°'
						}
						msg = msg + '°#° °[000,175,225]°_°>CM Hinzufügen|/tf-overridesb /role set:cm:[Pega16]<°_°r°';
						user.sendPrivateMessage(msg);
						return;
					}

					/*						if(pspl[0] == "remark" && pspl[1] == "cm"){
												if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
													user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
													return;
												}
												var cms = appPersistence.getObject('CMs', []);
									
												if(cms.indexOf(pspl[2]) == -1){
													user.sendPrivateMessage('Der Nutzer '+ pspl[2] +' ist kein CM!)');
													return;
												}
										
												cms.splice(cms.indexOf(pspl[2]), 1);
									
												appPersistence.setObject('CMs', cms);
												var icon3 = KnuddelsServer.getFullImagePath('CM.png');
												user.removeNicklistIcon(icon3);
										
												user.sendPrivateMessage('Ich habe '+ pspl[2] +' die CM-Rolle entzogen. °R°_°>Rückgängig|/role set:cm:'+ pspl[2] +'<°_°r°');
											}
					*/
					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "cm") {
						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}

						var cms = appPersistence.getObject('CMs', []);

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							if (cms.indexOf(vaspl[i]) == -1) {
								user.sendPrivateMessage('Der Nutzer ' + vaspl[i] + ' ist kein CM!)');
								return;
							}

							cms.splice(cms.indexOf(vaspl[i]), 1);

							appPersistence.setObject('CMs', cms);
							//								var icon3 = KnuddelsServer.getFullImagePath('CM.png');
							//								vaspl[i].removeNicklistIcon(icon3);

							user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' die CM-Rolle entzogen. °R°_°>Rückgängig|/role set:cm:' + vaspl[i].escapeKCode() + '<°_°r°');
						}
						return;
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "hzm") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager()) {
							user.sendPrivateMessage('Für diese Funktion musst Du Channel-Eigentümer oder App-Manager sein!');
							return;
						}

						var hzae = appPersistence.getObject('hzae', []);

						if (hzae.length <= 0) {
							user.sendPrivateMessage('Du hast keine HZM auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt HZM hinzufügen|/tf-overridesb /role set:hzm:[Pega16]<°_°r°');
							return;
						}

						var msg = '°BB°_Übersicht der HZM:_°r° °#°';

						for (i = 0; i < hzae.length; i++) {
							msg = msg + i + ' | ' + hzae[i].escapeKCode() + ' | °R°_°>Entfernen|/role remark:hzm:' + hzae[i].escapeKCode() + '<°_°r° °#°'

						}
						msg = msg + '°#° °[000,175,225]°_°>HZM hinzufügen|/tf-overridesb /role set:hzm:[Pega16]<°_°r°';

						user.sendPrivateMessage(msg);
						return;
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "hzm") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ Channel-Eigentümer oder AppManager sein!');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							var users = appPersistence.getObject('hzae', []);

							if (users.indexOf(vaspl[i]) == -1) {
								if (userAccess.exists(vaspl[i])) {

									users.push(vaspl[i]);
									appPersistence.setObject('hzae', users);
									//										var icon1 = KnuddelsServer.getFullImagePath('HZM.png');
									//										vaspl[i].addNicklistIcon(icon1, 35);
									user.sendPrivateMessage(vaspl[i].escapeKCode() + ' ist nun HZM. °R°_°>Rückgängig|/role remark:hzm:' + vaspl[i].escapeKCode() + '<°_°r°');
								} else {
									user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vertippt?');
								}
							} else {
								user.sendPrivateMessage('Da ' + vaspl[i] + ' bereits HZM ist, wurde dieser selbstredend _nicht erneut_ zum HZM gemacht...');
							}
						}
						return;
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "hzm") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ Channel-Eigentümer oder AppManager sein!');
							return;
						}

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							var hzae = appPersistence.getObject('hzae', []);

							if (hzae.indexOf(vaspl[i]) == -1) {
								user.sendPrivateMessage(vaspl[i] + ' ist kein HZM.');
								return;
							}

							hzae.splice(hzae.indexOf(vaspl[i]), 1);
							appPersistence.setObject('hzae', hzae);

							//								var icon1 = KnuddelsServer.getFullImagePath('HZM.png');
							//								vaspl[i].removeNicklistIcon(icon1);

							user.sendPrivateMessage('Ich habe ' + vaspl[i].escapeKCode() + ' soeben die HZM - Rolle entfernt. °R°_°>Rückgängig|/role set:hzm:' + pspl[2].escapeKCode() + '<°_°r°');
						}
						return;
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "gast") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var userAccess = KnuddelsServer.getUserAccess();

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							var users = appPersistence.getObject('sonderuser', []);

							if (users.indexOf(vaspl[i]) == -1) {

								if (userAccess.exists(vaspl[i])) {

									users.push(vaspl[i]);
									appPersistence.setObject('sonderuser', users);
									//										var icon4 = KnuddelsServer.getFullImagePath('GAST.png');
									//										vaspl[i].addNicklistIcon(icon4, 35);
									user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' auf die SonderUser/Gast-Liste gesetzt. °R°_°>Rückgängig|/role remark:Gast:' + pspl[2].escapeKCode() + '<°_°r°');

								} else {
									user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
								}
							} else {
								user.sendPrivateMessage('Da der Nutzer ' + vaspl[i] + ' bereits als Gast eingetragen ist, habe ich diesen natürlich _nicht erneut eingetragen!_');
							}
						}
						return;
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "gast") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var sus = appPersistence.getObject('sonderuser', []);

						if (sus.length <= 0) {
							user.sendPrivateMessage('Du hast keine Gäste/SonderUser auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /role set:Gast:[DerNeuanfang]<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht der Gäste/SonderUser:°r°_ °#°';

						for (i = 0; i < sus.length; i++) {
							msg = msg + i + ' | ' + sus[i].escapeKCode() + ' | °R°_°>Entfernen|/role remark:Gast:' + sus[i].escapeKCode() + '<°_°r° °#°'
						}

						msg = msg + '°#° °[000,175,225]°_°>Gast/SonderUser hinzufügen|/tf-overridesb /role set:Gast:[Pega16]<°_°r°';

						user.sendPrivateMessage(msg);
						return;
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "gast") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							var sus = appPersistence.getObject('sonderuser', []);

							if (sus.indexOf(vaspl[i]) == -1) {
								user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + vaspl[i].escapeKCode() + ')');
								return;
							}

							sus.splice(sus.indexOf(vaspl[i]), 1);

							appPersistence.setObject('sonderuser', sus);

							//								var icon4 = KnuddelsServer.getFullImagePath('GAST.png');
							//								vaspl[i].removeNicklistIcon(icon4);

							user.sendPrivateMessage('Ich habe ' + vaspl[i].escapeKCode() + ' aus der Liste entfernt. °R°_°>Rückgängig|/role set:Gast:' + vaspl[i].escapeKCode() + '<°_°r°');
						}
						return;
					}

					if (pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "vcm") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							var userAccess = KnuddelsServer.getUserAccess();

							var users = appPersistence.getObject('vcms', []);

							if (users.indexOf(vaspl[i]) == -1) {

								if (userAccess.exists(vaspl[i])) {

									users.push(vaspl[i]);
									appPersistence.setObject('vcms', users);
									//										var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
									//										vaspl[i].addNicklistIcon(icon2, 35);
									user.sendPrivateMessage(vaspl[i] + ' ist nun VCM. °R°_°>Rückgängig|/role remark:VCM:' + vaspl[i].escapeKCode() + '<°_°r°');

								} else {
									user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
								}
							} else {
								user.sendPrivateMessage('Da ' + vaspl[i] + ' bereits VCM ist, habe ich ' + vaspl[i] + ' selbstverständlich _nicht erneut hinzugefügt!_');
							}
						}
						return;
					}

					if (pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "vcm") {
						if (!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var vcm = appPersistence.getObject('vcms', []);

						if (vcm.length <= 0) {
							user.sendPrivateMessage('Du hast keine VCMs, daher kann ich Dir auch keine anzeigen! °R°_°>Jetzt hinzufügen|/tf-overridesb /role set:vcm:[James]<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht der VCMs:°r°_ °#°';

						for (i = 0; i < vcm.length; i++) {
							msg = msg + i + ' | ' + vcm[i].escapeKCode() + ' | °R°_°>Entfernen|/role remark:VCM:' + vcm[i].escapeKCode() + '<°_°r° °#°'
						}

						msg = msg + '°#° °[000,175,225]°_°>VCM hinzufügen|/tf-overridesb /role set:VCM:[Pega16]<°_°r°';

						user.sendPrivateMessage(msg);
						return;
					}

					if (pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "vcm") {
						if (!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
							return;
						}

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							var vcm = appPersistence.getObject('vcms', []);

							if (vcm.indexOf(vaspl[i]) == -1) {
								user.sendPrivateMessage('Den Nutzer kenne ich nicht (' + vaspl[i] + ')');

							}

							vcm.splice(vcm.indexOf(vaspl[i]), 1);

							appPersistence.setObject('vcms', vcm);

							//								var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
							//								vaspl[i].removeNicklistIcon(icon2);

							user.sendPrivateMessage('Ich habe ' + vaspl[i].escapeKCode() + ' aus der Liste entfernt. °R°_°>Rückgängig|/role set:VCM:' + vaspl[i].escapeKCode() + '<°_°r°');
						}
						return;
					} else {
						user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _VCM_, _Gast_');
						return;
					}

				} else {
					user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _VCM_, _Gast_');
				}
			} else {
				user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _VCM_, _Gast_');
			}
		}
	};

	App.cmdsul = function(user, params) {

		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			if (pspl[0] == "help") {
				user.sendPrivateMessage('Die Syntax für die Stress-User-Listen-Funktion lautet:°#°_/sul add:NICK_ zum setzen von NICK auf die Stress-User-Liste.°#°Mit _/sul_ zeigst Du Alle Nicks auf der Stress-User-Liste an.°#°Mit _/sul delete:NICK_ entfernst Du NICK von der Stress-User-Liste.°#°°#°_Bitte beachten:_°#°das Anzeigen der Stress-User-Liste ist den CMs (und höher) gestattet. Das Verändern jedoch ausschließlich den HZM.');
				return;
			}

			if (pspl[0].toLowerCase() == "set" || pspl[0].toLowerCase() == "add") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
					return;
				}

				var sulspl = pspl[1].split(',');

				var existendnicklists = appPersistence.getObject('extnl', []);

				for (i = 0; i < sulspl.length; i++) {
					sulspl[i] = sulspl[i].trim();
					msg = msg + sulspl[i]

					var userAccess = KnuddelsServer.getUserAccess();

					if (userAccess.exists(sulspl[i])) {
						if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
							var users = appPersistence.getObject('chksul', []);

							if (users.indexOf(sulspl[i]) > 0) {
								user.sendPrivateMessage(sulspl[i] + ' steht bereits auf der Liste! Daher habe ich ' + sulspl[i] + ' natürlich _nicht erneut_ hinzugefügt.');
								return;
							}
							if (users.indexOf(sulspl[i]) == -1) {
								users.push(sulspl[i]);
								appPersistence.setObject('chksul', users);
								user.sendPrivateMessage('Ich habe ' + sulspl[i] + ' auf die Stress-User-PRÜF-Liste gesetzt.');
							}

						}

						if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0) {
							var users = appPersistence.getObject('stressuserliste', []);
							var checksul = appPersistence.getObject('chksul', []);

							if (users.indexOf(sulspl[i]) > 0) {
								user.sendPrivateMessage(sulspl[i] + ' steht bereits auf der Liste! Daher habe ich ' + sulspl[i] + ' natürlich _nicht erneut_ hinzugefügt.');
								return;
							}
							if (users.indexOf(sulspl[i]) == -1) {
								users.push(sulspl[i]);
								appPersistence.setObject('stressuserliste', users);
								checksul.splice(checksul.indexOf(sulspl[i]), 1);
								appPersistence.setObject('chksul', checksul);
								user.sendPrivateMessage('Ich habe ' + sulspl[i] + ' auf die Stress-User-Liste gesetzt. °R°_°>Rückgängig|/sul delete:' + sulspl[i].escapeKCode() + '<°_°r°');
							}

						}
					} else {
						user.sendPrivateMessage('Der User ' + sulspl[i] + ' existiert nicht! -- Hast Du Dich vielleicht vertippt?');
					}
				}
				return;
			}
			/*					else {						
								user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
								return;
								}
			*/
			if (pspl[0].toLowerCase() == "delete" && pspl[1] != undefined) {
				if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
					return;
				}

				var lsul = appPersistence.getObject('stressuserliste', []);

				if (lsul.indexOf(pspl[1]) == -1) {
					user.sendPrivateMessage('Der Nutzer ' + pspl[1] + ' ist kein StressUser!');
					return;
				}

				lsul.splice(lsul.indexOf(pspl[1]), 1);

				appPersistence.setObject('stressuserliste', lsul);

				user.sendPrivateMessage('Ich habe ' + pspl[1] + ' von der Stress-User-Liste entfernt. °R°_°>Rückgängig|/sul set:' + pspl[1].escapeKCode() + '<°_°r°');
				return;
			}
			
			if (pspl[0].toLowerCase() == "delchkl" && pspl[1] != undefined) {
				if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
					return;
				}

				var lsul = appPersistence.getObject('chksul', []);

				if (lsul.indexOf(pspl[1]) == -1) {
					user.sendPrivateMessage('Der Nutzer ' + pspl[1] + ' ist kein StressUser!');
					return;
				}

				lsul.splice(lsul.indexOf(pspl[1]), 1);

				appPersistence.setObject('chksul', lsul);

				user.sendPrivateMessage('Ich habe ' + pspl[1] + ' von der Stress-User-Liste entfernt. °R°_°>Rückgängig|/sul set:' + pspl[1].escapeKCode() + '<°_°r°');
				return;
			}

			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
				user.sendPrivateMessage('Leider hast Du nicht die Berechtigung für diese Funktion!');
				return;
			}

			var lsul = appPersistence.getObject('stressuserliste', []);
			var chksul = appPersistence.getObject('chksul', []);

			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
				if (lsul.length <= 0) {
					user.sendPrivateMessage('Es sind keine StressUser auf der Liste. Daher kann ich Dir keine anzeigen.');

					if (chksul.length > 0) {
						var chkmsg = '_°BB°Liste der noch für die StressUserListe zu prüfenden Nicks:°r°_°##°"';
						for (i = 0; i < chksul.length; i++) {
							chkmsg = chkmsg + chksul[i] + ', '
						}
						user.sendPrivateMessage(chkmsg);
					}
					return;
				}
			}
			if (lsul.length <= 0) {
				user.sendPrivateMessage('Du hast keine StressUser auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /sul add:[James]<°_°r°');
				if (chksul.length > 0) {
					var chksulmsg = '_°BB°Liste der noch für die StressUserListe zu prüfenden Nicks:°r°_°##°"';
					for (i = 0; i < chksul.length; i++) {
						chksulmsg = chksulmsg + i + ' | ' + chksul[i].escapeKCode() + ' | °R°_°>Bestätigen|/sul add:' + chksul[i].escapeKCode() + '<°_°r° °13RR°"[°>Entfernen|/sul delchkl:' + chksul[i].escapeKCode() + '<°]"°r° °#°'
					}
					user.sendPrivateMessage(chksulmsg);
				}
				return;
			}

			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && App.coDevs.indexOf(userkennung) < 0 && App.coDevs.indexOf(userkennung) < 0) {

				var msg = '_°BB°Übersicht der StressUser:°r°_ °#°';

				for (i = 0; i < lsul.length; i++) {
					msg = msg + '°>' + lsul[i].escapeKCode() + '|/nicklist ' + lsul[i].escapeKCode() + ':list<°, '
				}

				//						msg = msg+ '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
				user.sendPrivateMessage(msg);


				if (chksul.length > 0) {
					for (i = 0; i < chksul.length; i++) {
						var chksulmsg = '_°BB°Folgende Nutzer müssen noch geprüft werden, bevor sie auf die reguläre StressUserListe übernommen werden:°r°_°#°'
						chksulmsg = chksulmsg + '°>' + chksul[i].escapeKCode() + '|/nicklist ' + chksul[i].escapeKCode() + ':list<°, '
					}
					user.sendPrivateMessage(chksulmsg);
				}
				return;
			}
			var msg = '_°BB°Übersicht der StressUser:°r°_ °#°';

			for (i = 0; i < lsul.length; i++) {
				msg = msg + i + ' | °>' + lsul[i].escapeKCode() + '|/nicklist ' + lsul[i].escapeKCode() + ':list<° | °R°_°>Entfernen|/sul delete:' + lsul[i].escapeKCode() + '<°_°r° °#°'
			}

			msg = msg + '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
			user.sendPrivateMessage(msg);

			if (chksul.length > 0) {
				var chkmsg = '_°BB°Folgende Nutzer wurden von den CM für die StressUserListe nominiert:°r°_°##°'
				for (i = 0; i < chksul.length; i++) {
					chkmsg = chkmsg + i + ' | ' + chksul[i].escapeKCode() + ' | °R°_°>Bestätigen|/sul add:' + chksul[i].escapeKCode() + '<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
				}
				user.sendPrivateMessage(chkmsg);
			}
			return;

		}
	};

	App.cmdcme = function(user, params) {
		if (channeltype == "Syschannel") {
			user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
			return;
		}

		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();
		//		var appPersistence = KnuddelsServer.getPersistence();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (userAccess.exists(user)) {
			var pspl = params.split('^');
			var msg = "";
			if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion. Um diese Funktion nutzen zu können, musst Du als HZA-angelegt sein. Hilfe hierzu findest Du unter _°>/role help|/role help<°_');
				//								user.sendPrivateMessage(userkennung);
				return;
			}
			if (pspl[0] == "help") {
				user.sendPrivateMessage('Die Syntax für die CME-Funktion lautet:°#°_/cme self^[ZAHL]_°#°zum Ändern Deiner Texte nutze:°#°_/cme self^edit^ZAHL^TEXT_°##°_°RR°Sollte keine CME hinterlegt sein, kannst Du auch /cme sys analog zu /cme self verwenden! °BB°Um diese CME einsehen zu können, eine kurze /m an Pega16, danke._°r°');
				return;
			}
			if (pspl[0] == "self" && pspl[1] != undefined && pspl[1] != "" && pspl[1] > 0) {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				var cme = [];
				cme[pspl[1]] = appPersistence.getObject('cme' + pspl[1]);
				if (cme[pspl[1]] == null) {
					user.sendPrivateMessage('Es wurde noch kein Text für den Absatz ' + pspl[1] + ' hinterlegt. Bitte hinterlege hierfür mit _/cme self^edit^' + pspl[1] + '^TEXT_ einen enstprechenden Absatz.');
					return;
				} else {
					if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0) {
						user.sendPrivateMessage(cme[pspl[1]]);
						return;
					}
					Bot.say(cme[pspl[1]]);
					return;
				}
			}

			if (pspl[0] == "self" && pspl[1] == "edit" && pspl[2] != undefined && pspl[2] != "" && pspl[2] > 0) {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				var setcme = [];
				setcme[pspl[2]] = appPersistence.setObject('cme' + pspl[2], pspl[3]);

				user.sendPrivateMessage('Der Text für den CME Absatz ' + pspl[2] + ' wurde nun auf den folgenden gesetzt:');
				user.sendPrivateMessage(pspl[3]);
				return;
			}

			//						user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!°#°°#°Die Syntax des Befehles bekommst Du angezeigt, wenn Du _°>/cme help|/cme help<°_ eingibst.');
			//						return;

			if (pspl[0] == "sys" && pspl[1] == "edit" && pspl[2] == "grant" && pspl[3] != undefined && pspl[3] != "") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				var userAccess = KnuddelsServer.getUserAccess();

				if (userAccess.exists(pspl[3])) {
					var users = appPersistence.getObject('cancme', []);
					users.push(pspl[3]);
					appPersistence.setObject('cancme', users);
					user.sendPrivateMessage('Ich habe ' + pspl[3] + ' erlaubt, die System-CME zu nutzen. °R°_°>Rückgängig|/cme sys^edit^remark^' + pspl[3] + '<°_°r°');
					return;
				} else {
					user.sendPrivateMessage('Der Nutzer ' + pspl[3] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
				}
			}

			if (pspl[0] == "sys" && pspl[1] == "list" && pspl[2] == "granted") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				} else {
					user.sendPrivateMessage('Befehl falsch eingegeben! _°>/cme sys^list^granted|/cme sys^list^granted<°_');
				}

				var canusecme = appPersistence.getObject('cancme', []);

				if (canusecme.length <= 0) {
					user.sendPrivateMessage('Du hast keine System-CME-berechtigten auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /cme sys^edit^grant^[Pega16]<°_°r°');
					return;
				}

				var msg = 'Übersicht der System-CME-berechtigten: °#°';

				for (i = 0; i < canusecme.length; i++) {
					msg = msg + i + ' | ' + canusecme[i] + ' | °R°_°>Entfernen|/cme sys^edit^remark^' + canusecme[i] + '<°_°r° °#°'
				}

				msg = msg + '°#° °[000,175,225]°_°>Weiteren Nutzer hinzufügen|/tf-overridesb /cme sys^edit^grant^[Pega16]<°_°r°';

				user.sendPrivateMessage(msg);
				return;
			}

			if (pspl[0] == "sys" && pspl[1] == "edit" && pspl[2] == "remark" && pspl[3] != undefined && pspl[3] != " ") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				var canusecme = appPersistence.getObject('cancme', []);

				if (canusecme.indexOf(pspl[3]) == -1) {
					user.sendPrivateMessage('Der Nutzer  (' + pspl[3] + ') existiert nicht. Hast Du Dich vielleicht vertippt?');
					return;
				}
				canusecme.splice(canusecme.indexOf(pspl[3]), 1);
				appPersistence.setObject('cancme', canusecme);
				user.sendPrivateMessage('Ich habe soeben ' + pspl[3] + ' untersagt, die System-CME weiterhin zu nutzen. °R°_°>Rückgängig|/cme sys^edit^grant^' + pspl[3] + '<°_°r°');
				return;
			}

			if (pspl[0] == "sys" && pspl[1] != undefined && pspl[1] != "" && pspl[1] > 0 && pspl[1] < 35) {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && !user.isChannelModerator()) {
					user.sendPrivateMessage('Falls Du die von _°>Pega16|/w Pega16|/m Pega16<°_ geschriebene CME nutzen möchtest, so schreibe bitte eine kurze /m an ihn.');
					return;
				}
				scme = [];
				scme[0] = "";
				scme[1] = "°18°_Herzlich Willkommen zur CM-Einführung. Nocheinmal herzlichen Glückwunsch zur überstandenen Wahl. Für die nächsten zwei Monate sorgt ihr in euren Channeln für Recht und Ordnung. Damit ihr wisst, wie das genau geht, werdet ihr heute kurz in euer Amt eingeführt. Wir werden die CM-Funktionen besprechen und auf bestimmte Problemsituationen eingehen. Zum Schluss können Fragen gestellt werden.#Diese Einführung wird gemacht, damit ihr euer CM-Amt besser ausführt und somit die Fehler, die ihr machen könnt, reduziert werden. Außerdem erhoffen wir uns dadurch eine bessere Zusammenarbeit. Jetzt bitten wir euch noch, aus eurem LC oder sonstigen Channel zu gehen und °[255,49,49]°nur hier §°18°_anwesend zu sein, Danke._";
				scme[2] = "°18°_Ihr seid ein Team. Bitte handelt auch als Team und seid keine Einzelkämpfer. Grade die CMs, die länger im Amt sind, sollten den Neuen helfen. Auch eure Freunde müsst ihr wie normale Chatter behandeln. Auch diese müssen bestraft werden, wenn sie was machen. Falls ihr damit nicht klar kommt, sagt dann einem Kollegen Bescheid, der sich dann um die Sache kümmern soll. Bei Fragen sind wir gerne eure Ansprechpartner. Falls ihr euch nicht 100%ig sicher seid, fragt lieber nach, wir helfen euch gerne. Screens und Beschwerden über Kollegen immer zuerst an uns HZAs. Sollten wir nicht online sein, hilft euch auch jeder andere Admin. Jedoch kümmern wir uns als HZA nicht um alles. Wir haben viele Sachen zu machen und können nicht ständig im Channel hocken und euch über die Schulter schauen. Mit Fragen, Problemen ect.pp. solltet ihr deswegen immer zu uns kommen. Auch können wir nicht riechen, „wann“ ihr eine CMV wollt, deswegen auch hier bitte auf uns zukommen!_";
				scme[3] = "°18°_Fragen können direkt gestellt werden, jedoch klären wir auch am Schluss noch einige auf, die nicht beantwortet wurden. Meistens klärt sich nämlich das meiste während der CMV *gg*. (gegebenenfalls Fragen AUFSCHREIBEN!)_";
				scme[4] = "°18°_Zum Thema Passwortweitergabe & Passwortklau °#°°#° °R°PW- Weitergabe: Strengstens verboten -> sofortiger Amtsentzug! °#°°#°§°18°> Passwortklau oder sonstiges abhanden kommen des PW’s: Sofort einem Admin melden um den Nick zur Sicherheit sperren lassen und dann mit 2. Nick bei uns melden, dass wir mit einem Teamleiter des PW Team reden können für die PW Neusetzung, als CM °[255,49,49]°NIE §°18°einen PW- Antrag machen!_";
				scme[5] = "°18°_Als CM hast du eine gewisse Vorbildfunktion. Deshalb solltest du dich mehr als alle anderen an die Regeln halten. Darüber hinaus darfst du niemanden dafür bestrafen, was du nicht selbst auch machst. Weiterhin darfst Du gegen die AGB und Spielregeln nicht verstoßen. Outen bei Mafia, Cheaten, Chatbotbenutzung und anderes würden zu einem Amtsentzug führen._";
				scme[6] = "°18°_Korrekte Notrufe! °#°°#°Immer wieder bekommen wir Notrufe die absolut unkorrekt sind -grummel-°#°Weiterleiten solltet ihr Notrufe die:°#° >eure Fähigkeiten übersteigen wie HP-Sperren, Nickklau etc. pp. Und die deutlich und freundlich formuliert sind.°#°°#°Nicht Weiterleiten solltet ihr Notrufe wie: °#°   > wann ist mein Foto frei °#°  > Ich hab mal en Problem helf mir °#°   > hier wird Werbung gemacht (ohne Namen und ohne Angabe was für Werbung)°#°  > sonstigen Schwachsinn wie total undeutlich und unfreundlich beleidigende Notrufe °#° > Notrufe die keinen genauen Grund angeben oder bei denen Namen oder Internetseite fehlen. °#°°#° In einen Notruf sollte, was ist wann, wo, wie und mit wem geschehen! Jedoch: Nicht immer gleich zum Admin rennen, bei Fragen die ihr selbst beantworten könnt, wie HP-„Problemchen“ (Verweis auf www.ramnip.net) Fotodinge (z.B. Verweis aufs Forum) Mentorenfragen ect.pp., sondern versuchen einfache Dinge wie diese den Chattern selber zu erklären_";
				scme[7] = "°18°_korrekte Notrufbeispiele:_°#°°#°_Jamesfaking (/afk Passwort) (bitte SOFORT /mute!):_ /notruf -> Beschwerde über andere Chat-Teilnehmer -> Botnutzung / Faking von Nachrichten -> NICK -> OK -> Nachricht(en) markieren -> HOAX-Versuch ChannelZ /afk Passwort öffentlich -> OK -> Notruf absenden°#°°#°_Jamesfaking (/knuddel NickXY) (bitte SOFORT /mute!):_ /notruf -> Beschwerde über andere Chat-Teilnehmer -> Botnutzung / Faking von Nachrichten -> NICK -> OK -> Nachricht(en) markieren -> HOAX-Versuch ChannelZ öffentlich /knuddel Passwort -> OK -> Notruf absenden°#°";
				scme[8] = "°18°_Denkt dran keine CM Infos weiterzugeben, denn das dürft ihr nicht.°#°°#°CM-Infos gehen niemanden außer CMs und Admins etwas an. Gebt diese nicht weiter. Auch dies kann und wird mit Amtsentzug bestraft werden. D.h., es geht auch keinen Chatter etwas an, wie viele mutes oder /cls er schon hat. Oder von welchem CM er gemutet wurde. Wurde er von einem Kollegen gemutet, nennt den Nick nicht dem Chatter, sondern sprecht den Kollegen darauf an dass XY entmutet werden möchte und er sich mit ihm in Verbindung setzen soll. Jeder Chatter sieht selbst in der /mute - Meldung, wer ihn gemutet hat!_";
				scme[9] = "°18°_Ach ja Stresser°#°°#°_Immer wieder & immer öfters tauchen sie auf, die heiß begehrten Stresser xD Am besten nicht aufregen lassen sondern verfahren wie bei jedem anderen die meisten die euch Schläge androhen haben eh nur eine große klappe im Chat °>sm_01.gif<°°#°°#°>Bei Bedrohungen aller Art /admin -> Beschwerde über Chatteilnehmer -> Bedrohung -> NICK -> Drohungen auswählen -> Kurz die Situation schildern, wie es dazu kam -> OK -> Notruf absenden -> OK °#° >Bei Stresser 2 mal Verwarnen ~> /mute°#° >Bei weiterem privat stressen als CM _NIEMALS_ /ig oder /block, sondern /pp NICK   dann Fenster klein machen später einfach schließen. Sollte er mehrere Leute ansprechen, geht /pp nicht mehr. Wenn’s nicht mehr einzudämmen ist, dann an einen Admin wenden._";
				scme[10] = "°18°_Extremistische Äußerungen_°#°°#°Bei solchen Sachen nicht lang rummachen sofort /cl NICK:GRUND + /notruf + _Screen_ wenn ihr euch nicht sicher seid ob der Grund zum cl reicht einfach ein cm kollege oder Admin fragen!  Am besten _/fa aet_._";
				scme[11] = "°18°_Funktion /mute:°#°_ Die /mute-Funktion wird eingesetzt um nervige Leute öffentlich mundtot zu machen. Mit der Funktion /mute !Nick wird dies wieder rückgängig gemacht. Hier einige kleine Einsetzungsmöglichkeiten der /mute-Funktion: °#°1.) Spammen von sinnlosen Texten°#°2.) Verfassungswidrige Äußerungen (siehe auch /cl)°#°3.) Beleidigungen eines Chatter gegenüber anderen (Genaueres nachher)°#°4.) Werbung (Genaueres nachher)°#°°#°Merke: In jedem Fall (bis auf Werbung) ist vorher eine Verwarnung angebracht. Außerdem ist es jedem CM selbst überlassen, ob er erst 2-3 verwarnen möchte oder direkt zum Mute greift. (beachte hierzu Allgemeines, Punkt 3!)_";
				scme[12] = "°18°_Funktion /cmute:_°#°°#° Mit ihr kann man das Farbig-, Groß- und Fettschreiben einer Person verhindert. Rückgängig zu machen mit der Funktion /cmute !Nick. Auch hier einige Einsetzungsmöglichkeiten: °#°1.) Dauerhaftes Farbig-, Groß- oder Fettschreiben°#°2.) Immer wieder großgeschriebene Texte einer Person.°#°3.) Wenn ein Text in Farbe als störend empfunden wird.°#° Merke: Hier sollte eine Verwarnung angebracht sein, wenn jedoch keine Zeit dafür bleibt, darf man auch direkt zum /cmute greifen. (Genaueres ist auch im CM-Forum zu finden)_";
				scme[13] = "°18°_Funktion /cl:_°#°°#° Mit dieser Funktion kann man jemanden des Channels verweisen. Anschließend kann der Gekickte (egal, mit welchem Nick) den Channel für einen Tag nicht mehr betreten. Deshalb sollte mit dieser Funktion am Vorsichtigsten agiert werden. Nun auch hier (lol) jetzt einige Einsetzungsmöglichkeiten:°#°1.) Verfassungswidrige Äußerungen (siehe auch AGBs)°#°2.) James-Faking (bitte erst NACH dem Absetzen des Notrufes!!!)°#°3.) Werbung für sexistische Internet-Seiten.°#°4.) Private Beleidigungen anderer Chatter (sollte überprüft werden)°#°°#° Merke: Bei Punkt 3.) sollte man sofort zum /cl greifen. Bei Punkt 1.) und 4.) sollte vorerst verwarnen._";
				scme[14] = "°18°_Funktion /fa:_°#°°#° Mit dieser Funktion könnt ihr die Liste der Admins/Teamler einsehen, die gerade online sind. Habt ihr Fragen, dann sucht euch von dieser Liste zuerst eure Hauptzuständigen Admins (HZA) heraus, wenn diese nicht on sind, dann hilft jeder andere Admin gerne weiter. Bitte habt auch Geduld, wenn es mal wieder länger dauert bis man einen Admin erwischt, aber man kennt das ja, viele Leute wollen was von wenigen Leuten die etwas höher gestellt sind obwohl sie auch nur 10 Finger haben und nicht springen können ;)_";
				scme[15] = "°18°_Funktion /admin:_°#°°#° Mit dieser Funktion könnt ihr nicht nur einen Adminruf absetzen, sondern auch die letzten sieben Notrufe einsehen, die in eurem Chanel abgesetzt worden sind. (Auch bekannt als ALTES Notrufsystem. Wurde allerdings aufs Neue weitergeleitet!)_";
				scme[16] = "°18°_Funktion /cm text:_°#° Mit dieser Funktion können neue Channeleigene Regeln festgelegt werden. Bevor ihr sie anwendet solltet ihr euren neuen Entwurf auf jeden Fall erst einem eurer HZAs zeigen._";
				scme[17] = "°18°_CM-Info:_°#°°#° Bestimmt ist euch beim Aufruf eines Profils schon die CM-Info aufgefallen. Dort steht drin, wer in welchem Channel von wem gechannellockt, gemutet oder gecolormutet ist, wer einen Gamelock hat oder die Anzahl von /cls und /mutes.°#° Ganz wichtig: Diese Informationen dürft ihr an niemanden rausgeben. Nichteinmal an den Chatter, den sie betreffen. Dasselbe gilt für die /cl, /mute und /fa-Listen. Diese Listen heißen CM-Infos, weil sie nur für CMs bestimmt sind und nicht für Chatter. Wer gegen diese einfache Regel verstößt, kann mit einem Entzug des CM-Amtes rechnen._";
				scme[18] = "°18°_Problemgebiet: Werbung:_°#°°#° MyChannel-Werbung ist das größte momentane Problemgebiet. Bisher haben wir uns darauf geeinigt, bei jeder Werbung sofort zu muten. Die Werber müssten nämlich alle wissen, dass Werbung verboten ist. Man kann es in den AGBs, der /h knigge und sogar in den MyChannel-Infos lesen. Außerdem solltet ihr die Werber per Adminruf (/admin grund) melden. Bitte nennt in diesem Adminruf folgende Faktoren:°#° 1.) Den Nick des Werbungmachers°#°2.) Für welchen Raum Werbung gemacht wurde.°#° Bei Werbung für Sexseiten wie z.B. www.nadine.eu.tp, reicht ein normaler Adminruf. °#° Bei Werbung für Fake-Seiten, Cheater-Seiten und Hacker-Seiten macht ihr dies bitte genauso. Erst /mute, dann /notruf._";
				scme[19] = "°18°_Problemgebiet: verbale Beleidigungen:_°#° Solche Beleidigungen sind ein weiteres Problemgebiet. Hier einige Formen dieses Problemgebietes:°#°1.) Werden wir CMs/Admins öffentlich beleidigt, sollte man damit routiniert umgehen können. Diese Personen sehen es meistens nur darauf ab, Aufmerksamkeit zu erhalten oder euch euer Amt möglichst schwer zu machen. Reagieren sollte man vorerst gar nicht, erst wenn er keine Anzeichen macht aufzuhören, sollte man (geht auch ohne Verwarnung) zum Mute greifen.°#°2.) Private Beleidigungen solltet ihr euch erst gar nicht gefallen lassen, einfach /pp Nick - das Fenster minimieren und das wäre auch geklärt.°#°3.) Beleidigungen gegenüber anderen Chattern, öffentlich oder privat, sollten so schnell wie möglich aus dem Weg geschafft werden. Entweder durch das Anwenden von /ig und/oder /block, oder schlimmstenfalls mit /mute bzw. /cl._";
				scme[20] = "°18°_Jamesfaking ist so ziemlich das schlimmste, was ein Chatter hier Verbrechen kann. Ein Jamesfaker muss sofort gemutet und ein Admin mittels /admin sofort alarmiert werden. Bitte diese User nicht kicken, da wir sonst nicht mehr sehen, ob sie noch mit anderen Nicks  online sind!_";
				scme[21] = "°18°_Wie mache ich einen Screenshot?:_°#°Hier eine kurze Anleitung:°#°1.) Zu der gewünschten Stelle scrollen, die fotografiert werden soll°#°2.) Drücke die Taste Druck (oder auch Print, S-Abf, ...) rechts oben auf der Tasstatur, neben den F-Tasten.°#°3.) Öffne das Programm Paint (Start -> Programme -> Zubehör -> Paint)°#°4.) Drücke die Tasten STRG + V gleichzeitig, wenn nötig lasse Paint das Bild automatisch vergrößern (Abfrage bestätigen).°#°5.) Speichere die Datei als jpeg oder besser noch, als .PNG (Datei -> Speichern unter -> Dateityp: jpeg/jpg oder eben Dateityp PNG).°#°6.) Schicke die Datei immer als Anhang (Das Büroklammernsymbol), niemals direkt in die eMail einfügen!_";
				scme[22] = "°18°_Verhalten gegenüber anderen:_°#° CM-Kollegen: Sicherlich, und das wird nicht zu vermeiden sein, wird es auch einige Uneinstimmigkeiten zwischen euch CMs geben. Ich möchte euch bitten, diese Sachen privat zu klären, denn nach außen müsst ihr als ein Team dastehen, schon mal damit euch niemand gegenseitig ausspielen kann. Des Weiteren darf kein CM von den anderen ausgeschlossen oder heruntergemacht werden. Es heißt nicht umsonst CM-Team. Und bitte helft euch bei Problemen gegenseitig oder fragt im Zweifel einfach uns! Auch wenn ich zum Beispiel nicht da bin, so antworte ich doch meist ziemlich zeitnah auf °>/m<°s, da ich diese auf dem Handy lese… Es ist nämlich nicht schlimm mit einer Situation nicht allein fertig zu werden, im Gegenteil, so wird der Teamgeist gestärkt. °>sm_01.gif<°_";
				scme[23] = "°18°_°#°Chatter/Freunde:_ Es ist klar, dass jeder Chatter gleich zu behandeln ist, egal ob Freund oder Feind. °#°_Newbies:_ Newbies müsst ihr freundlich entgegenkommen, auf ihre Fragen müsst ihr antworten können, dazu seid ihr schließlich da. °>sm_01.gif<°°#°_Admins:_ Hoheiten, behandelt uns wie Gott, auch wenn manche von uns nicht mal halb so gut sind wie ihr. :P Nene, es sollte euch schon bewusst sein, wie ihr euch Admins gegenüber verhält, oder? °>sm_10.gif<°_";
				scme[24] = "°18°_CM-Forum:_°#°°#° Das CM-Forum ist mit der Funktion /forum, alternativ F12, abrufbar. Dort loggt ihr euch unter Login mit dem Chatnick + Passwort ein, klickt auf Übersicht der Foren, auf Administration und dort auf Channelmoderatoren. Dort könnt ihr euch mit anderen CMs über die alltäglichen Problemstellungen austauschen und euch auch wichtige Informationen usw. einholen. Schaut auf jeden Fall mal vorbei. ;) (Forum is einfach toll °>fullheart.png<° *lieb* °>sm_10.gif<°)_";
				scme[25] = "°18°_Knuddelsphilosophie…_°#°°#°Ja, ich weiß, der ein oder andere sagt klar, er könne die Philosophie nicht in der Form unterschreiben… Hier stellt sich aber wiederum die Frage, WARUM dies so ist… Wir werden jetzt kurz die vier Säulen der _°BB°°>Knuddelsphilosophie|/philosophie<°_°r° °18°durchgehen und einige Interpretationen dazu besprechen…°#°°#°°#°_1. Gemeinsam Spaß haben_°#°Was versteht IHR darunter? °#°°R°_Antwort bitte öffentlich im Channel!_°r°_";
				scme[26] = "°18°_Richtig!°#° Knuddels lebt durch die Menschen, die hier ihre Freizeit miteinander verbringen. Wir alle haben unsere eigenen, individuellen Wünsche und Erwartungen, aber auch eine große Gemeinsamkeit: wir wollen zusammen Spaß haben!_";
				scme[27] = "°18°_2.) einander FREUNDLICH begegnen!_°#°Was versteht ihr darunter? °#°°R°_Antwort bitte öffentlich im Channel! °r°_";
				scme[28] = "°18°_RICHTIG! Um dies möglich zu machen, hat das Knuddelsteam im Laufe der Jahre einen Ort mit unzähligen Räumen und Möglichkeiten geschaffen. Diesen Ort gestalten wir mit Begegnungen, Gesprächen, Kreativität und unserer Individualität. Dabei achten wir aufeinander, nehmen Rücksicht und unterstützen uns gegenseitig._";
				scme[29] = "°18°_3.) Sich gegenseitig unterstützen_°#°Was versteht ihr darunter?°#°°R°_Antwort öffentlich im Channel!°r°_";
				scme[30] = "°18°_Richtig!_°#°Knuddels.de lebt durch ihre Mitglieder. Bei Knuddels haben wir die Möglichkeit, jeden Tag neue, tolle Menschen kennenzulernen. Wir lehnen niemanden prinzipiell ab, sondern begegnen uns vorurteilsfrei und freundlich. Unsere Unterstützung gilt insbesondere den Personen, die sich ehrenamtlich mit ganzem Herz für die Community einsetzen._";
				scme[31] = "°18°_Aufeinander Rücksicht nehmen…_°#°Ja, das ist wohl der Punkt, an dem die meisten sich an den Kopf packen und sagen „ihr könnt mich mal“… ABER was steckt tatsächlich dahinter?°#°°R°_Antwort wie immer öffentlich im Channel.°r°_";
				scme[32] = "°18°_Richtig…_°#°Wir verstehen, dass sich jeder von uns frei in seiner eigenen Art präsentiert, ausdrückt und auslebt. Kommt es dennoch zu Konflikten mit anderen Mitgliedern, so versuchen wir immer zuerst sie selber zu lösen, indem wir über leichte/minimale Fehler (Kleinstvergehen) hinwegsehen oder die Personen ignorieren, bzw. sie im /p auf ihren Fehler freundlich hinweisen.°#°Auf diese Weise kann Knuddels das sein, was wir uns alle wünschen: _ein Ort an dem wir uns wohl fühlen, so unterschiedlich wir auch sind._°#°°#°Das ist das Primäre Ziel… Das heißt aber keinesfalls, dass nun jeder alles tun und lassen kann/darf, was er/sie möchte!_";
				scme[33] = "°18°_Und nun wünsche ich euch  noch viel Spass in eurem Amt! Wenn ihr Fragen habt, fragt ruhig, es reißt euch keiner den Kopf ab! Achja.. wenn ihr denkt – wir sind nie online, falsch gedacht °>sm_01.gif<° wir sind oft zwischendurch und abends online, da wir meist viel zu tun haben, solltet ihr aber ein Gespräch wünschen o.ä. werden wir uns natürlich Zeit nehmen °>sm_00.gif<°_";
				scme[34] = "°18°_Kommen wir nun vom trockenen Theorieteil zum CMV-Teil dieser Veranstaltung… Nun seid ihr gefragt… was geht aktuell im Channel ab (ja, einiges weiß ich, bzw. wissen wir schon!)…? Gehen wir mal die wichtigsten Punkte GEMEINSAM durch, damit wir auch gemeinsam eine für alle passende Lösung finden können… °>sm_10.gif<°_";

				user.sendPrivateMessage('_Absatz ' + pspl[1] + ' von 34_:');

				msg = scme[pspl[1]];

				if (appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0) {
					user.sendPrivateMessage(msg);
					return;
				}
				Bot.say(msg);
				return;
			} else {
				user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!°#°°#°Die Syntax des Befehles bekommst Du angezeigt, wenn Du _°>/cme help|/cme help<°_ eingibst.');
			}

		}
	};

	/*	cme15: function (user, command) {
	/*				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var appPersistence = KnuddelsServer.getPersistence();
			
				if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			user.sendPrivateMessage ('_Absatz 15 von 39_:');
			Bot.sendPublicMessage ('Fragen zur /fa - Funktion (bitte einfach anklicken was Ihr für richtig haltet!):');
			user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 1 von 5|Wie rufst Du die Liste aller online befindlichen Teamler aus dem Antiextremismus-Team auf?|channel|on|30|0|/fa JuSchu|/fa Antiextremismusteam|/fa Antirechts|/fa Verify|/fa AET°#°°#°// Lösung:_ /fa AET (bitte ggfs. selbst erklären!)')
			user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme16<°_§ mit dem nächsten Absatz.');
		},
		
		cme16: function (user, command) {
	/*				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var appPersistence = KnuddelsServer.getPersistence();
			
				if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			user.sendPrivateMessage ('_Absatz 16 von 39_:');
			user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 2 von 5|Wie rufst Du die Liste \_aller\_ Teamler aus dem Antiextremismus-Team auf?|channel|on|30|0|/fa AET:list|/fa AET:all|/fa AET:zeig|/fa AET:alle|/fa AET:offline|/fa AET:gib°#°°#°// Lösung:_ /fa AET:all (bitte ggfs. selbst erklären!)')
			user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme17<°_§ mit dem nächsten Absatz.');
		},
		
		cme17: function (user, command) {
	/*				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var appPersistence = KnuddelsServer.getPersistence();
			
				if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			user.sendPrivateMessage ('_Absatz 17 von 39_:');
			user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 3 von 5|Wie rufst Du die Liste der Teamler aus dem Antiextremismus-Team auf, die _mindestens ADMIN sind?|channel|on|30|0|/fa AET:admin|/fa AET:all|/fa AET:6|/fa AET:admins|/fa AET:Admin,AET|/fa AET:gib°#°°#°// Lösung:_ /fa AET:6 (bitte ggfs. selbst erklären!)')
			user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme18<°_§ mit dem nächsten Absatz.');
		},
		
		cme18: function (user, command) {
	/*				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var appPersistence = KnuddelsServer.getPersistence();
			
				if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			user.sendPrivateMessage ('_Absatz 18 von 39_:');
			user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 4 von 5|Wie rufst Du die Liste der Teamler aus dem HZA-Team (uns HZAs übergeordnet!) auf?|channel|on|30|0|/fa HZA|/fa HZA:Singles 40+|/fa Singles 40+:HZA|/fa HZA-Team|/fa Verify|/fa Chatleitung°#°°#°// Lösung:_ /fa HZA (bitte ggfs. selbst erklären!)')
			user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme19<°_§ mit dem nächsten Absatz.');
		},
		
		cme19: function (user, command) {
	/*				var isAdmin 		= user.isInTeam("Admin");
				
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			var appPersistence = KnuddelsServer.getPersistence();
			
				if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			user.sendPrivateMessage ('_Absatz 19 von 39_:');
			user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 5 von 5 (Letzte Frage!)|Wie rufst Du die Liste der Teamler aus dem Verifizierungs-Team (für Fakes etc. zuständig) auf?|channel|on|30|0|/fa HZA|/fa Fake|/fa Verify|/fa Verifizierungsteam|/fa Verify:6|/w Kolloid°#°°#°// Lösung:_ /fa Verify (bitte ggfs. selbst erklären!)')
			user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme20<°_§ mit dem nächsten Absatz.');
		},
	*/

	App.chatCommands.listcommands = function(user) {
		var isAdmin = user.isInTeam("Admin");
		/*			
					if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
		*/

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		var appPersistence = KnuddelsServer.getPersistence();
		if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !isAdmin) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var ishzae = appPersistence.getObject('hzae', []);
			//				ChanKey.push('offen');
			appPersistence.getObject('CMs', []);
			if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0 || user.isChannelModerator()) {
				user.sendPrivateMessage('CM-Befehle:°-°°#°°>/sul|/sul<° => zeigt die aktuelle Stress-User-Liste.°#°_/sul add:Nick_ füht Nick der StressUserListe hinzu (Liste zur Prüfung durch HZM!)°#°°-°');
				return;
			}
			appPersistence.getObject('hzae', []);
			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0 || user.isChannelOwner()) {
				user.sendPrivateMessage('HZM-Dinge:°#°_°R°[Karten]°r°_ (HZM)§°#°°#°Auf der _°>/warn help|/warn help<°_ gibt es die genauen Informationen.°#°°#°°-°°#°_°R°[Zugangsbeschränkungsdinge]°r°_ (HZM)§°#°°#°_/role set:ROLLE:[Nick]_			=> Nick die ROLLE verleihen. Zum Beispiel um den Zutritt zum Channel zu gewähren (Channel im CM-Modus (s.o.))°#°Näheres hierzu auf der _°>/role help|/role help<°_°#°°#°°#°°#°_°R°[Stress-User-Liste]°r°_(Berechtigung in Klammer dahinter)§ °#°°#°_°>/sul|/sul<°_			=>	Stress-User-Liste aufrufen/anzeigen (CMs & HZM) °#°_/sul set:[Nick]_	=>	Nick auf die Stress-User-Liste setzen (HZM) °#°_/sul delete:[Nick]_	=>	Nick von der Stress-User-Liste entfernen. (HZM)');
				//						return;
			}
			/*					else if ( == "Closed") {
									user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
									return;
								}
			*/
			if (user.isChannelOwner()) {
				user.sendPrivateMessage('°#°_Channelbefehle für den Channelowner:_°#°°#°°#°_/restricttext TEXT_ => Channelsperrtext für den Channel-CM-Modus definieren.°#°°#°_°>/restrictiontext|/restrictiontext<°_ => zeigt den aktuell definierten Channelsperrtext für den im CM-Modus befindlichen Channel an. Ist keiner definiert, wird der vorgegebene Standardtext angezeigt.°#°°#°°#°_/closedtext TEXT_ => definiert den Anzeigetext bei Channelbetretversuch im Falle eines geschlossenen Channels.°#°°#°_°>/showclosedtext|/showclosedtext<°_ => zeigt den definierten Sperrtext. Ist keiner definiert, liefert dieser Befehl ein _leeres Array_ zurück. In diesem Falle ist der vorgegebene Standardtext aktiv (siehe _°>/chlog|/chlog<°_)°#°°#°_°>/schriftart|/schriftart<°_ ändert die Bot-Schriftart.°#°°#°_/greetings on/off_ schaltet die Nutzer-Begrüßung ein/aus.°-°');
			}
			user.sendPrivateMessage('°-°_Channelbefehle (Admins/ChannelOwner/AppManager):_°#°°#°_°>/opened|/opened<°_		=> Channel für jedermann öffnen (Standardeinstellung!)°#°_°>/usual|/usual<°_		=> Channel nur für explizit zugelassene Personen öffnen (CM-Modus für HZM und CMs) [Mehr unter \"Zugangsbeschränkungsdinge\"]°#°_°>/closed|/closed<°_		=> Channel abschließen. Dies hat zur Folge, dass _KEINER_ mehr den Channel betreten kann!°#°°#°_°>/chanstate|/chanstate<°_	=> Anfrage in welchem Status sich der Channel gerade befindet (Kontrolle damit man den Channel nicht verschlossen verlässt!)');
			//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	/*	pffbs: function (user, command) {
			var pfad = KnuddelsServer.getFullImagePath('');
			var saschaus = '°>'+pfad+'sascha.gif<°';
	//		var user = user.getNick();
	//		var user = KnuddelsServer.getAppDeveloper();
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
	//		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
			user.sendPrivateMessage('Postfach-Blockade by ' + saschaus);
		},
	*/

	//'°>{font}ArialBold<28B°_' + 
	//'°>{font}ArialBold<28R°_' + 
	// Chat Commands ALL Channel Users

	//Version anzeigen
	App.chatCommands.ver = function(user, command) {
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;
		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>' + pfad + 'sascha.gif<°';
		var isHzmApp = appPersistence.getObject('isHzmApp', ['']);
		if (isHzmApp == "on") {
			if (isabRC == "a") {
				var Version = "HZM-ALPHA-APP"
			} else if (isabRC == "b") {
				var Version = "HZM-BETA-APP"
			} else {
				var Version = "HZM-App"
			}

			if (isabRC == "a") {
				var isVersion = extver + ".0.4.1";
			} else if (isabRC == "b") {
				var isVersion = extver + ".0.4";
			} else {
				var isVersion = extver;
			};
		} else {
			if (isabRC == "a") {
				var Version = "PCC-ALPHA-APP"
			} else if (isabRC == "b") {
				var Version = "PCC-BETA-APP"
			} else {
				var Version = "PublicChatControl-App"
			}

			if (isabRC == "a") {
				var isVersion = extver + ".0.4.1";
			} else if (isabRC == "b") {
				var isVersion = extver + ".0.4";
			} else {
				var isVersion = extver;
			};
		};
		
			user.sendPrivateMessage('Die _' + Version + '-App_ hat die _Version ' + isVersion + '_ und wurde von _°>Pega16|/m Pega16|/w Pega16<°_ geschrieben.#Anfragen oder Feedback zur App per /m (oder Rechtsklick auf den Namen!)°#°°#°°BB°_Du kannst Diese App auch bei Dir installieren! - /m an Pega16!_°r°°#°°#°Liebe Grüße,°#°' + saschaus + installlink);

	};

	App.cmdbp = function(user, params) {
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;

		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();

		var pspl = params.split(':');
		var msg = "";

		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
			user.sendPrivateMessage('Dies kann nur der Channeleigentümer und die HZM.');
			return;
		}

		var puser = pspl[0].toKUser();

		if (pspl[0] != undefined && pspl[1] != undefined) {
			if (user.getNick() == puser || user.getNick() == "Pega16" || user.getNick() == "Pega" || user.getNick() == "Security") {
				if (userAccess.exists(puser)) {
					logger.info(user.getNick() + ' schreibt per Bot-Private an ' + pspl[0] + ' den Text: ' + pspl[1]);
					puser.sendPrivateMessage(pspl[1]);
					user.sendPrivateMessage('Bot-Privatnachricht an °BB°_°>_h' + pspl[0] + '|/w ' + pspl[0] + '|/m ' + pspl[0] + '<°_°r° erfolgreich versandt.°#°°BB°_Inhalt Deiner übermittelten Nachricht:_°r° ' + pspl[1]);
				} else {
					user.sendPrivateMessage('Der Nutzer ist unbekannt... Vertippt?');
				}
				return;
			}

			if (userAccess.exists(puser)) {
				logger.info(user.getNick() + ' schreibt per Bot-Private an ' + pspl[0] + ' den Text: ' + pspl[1]);
				puser.sendPrivateMessage(pspl[1] + '°#°°09°(übersandt durch °>_h' + user.getNick() + '|/w ' + user.getNick() + '/m ' + user.getNick() + '<°)°r°');
				user.sendPrivateMessage('Bot-Privatnachricht an °BB°_°>_h' + pspl[0] + '|/w ' + pspl[0] + '|/m ' + pspl[0] + '<°_°r° erfolgreich versandt.°#°°BB°_Inhalt Deiner übermittelten Nachricht:_°r° ' + pspl[1]);
			} else {
				user.sendPrivateMessage('Der Nutzer ist unbekannt... Vertippt?');
			}

		} else {
			user.sendPrivateMessage('Die Syntax für den Befehl lautet: _/bp Nick:TEXT_');
			return;
		}
	}

	App.cmdcmp = function(user, params) {
		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;
		var pfad = KnuddelsServer.getFullImagePath('');
		var devico = '°>' + pfad + 'DEV.png<°';
		var devmessage = '_°BB°' + user.getProfileLink() + ' °RR°[ ' + devico + ']°BB°:°r°_ ';
		var cmmessage = '°>cm.png<° _°BB°Rundnachricht von °RR°' + user.getProfileLink() + ' °BB°an alle CM des Channels ' + channelName + ':°r°_ ';

		if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()) {
			user.sendPrivateMessage('Für diese Funktion musst Du CM sein!');
			return;
		}

		if(params.length > 5){
		var reciever = channelRights.getChannelModerators();
		if (App.coDevs.indexOf(userkennung) >= 0) {
			reciever.forEach(function(uu) {
				//				uu=uu.toKUser();

				uu.sendPrivateMessage(devmessage + params);

			})
		} else {
			reciever.forEach(function(uu) {
				//				uu=uu.toKUser();
				uu.sendPrivateMessage(cmmessage + params);
			})
		}
		} else {
			user.sendPrivateMessage('Die Nachricht sollte schon länger als 5 Zeichen haben... °>sm_01.gif<°');
		}
	};

	App.chatCommands.sofu = function(user, params, func) {
		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>' + pfad + 'sascha.gif<°';

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (pspl[0] == "help") {
				user.sendPrivateMessage('Die Syntax für die SonderFunktionen lautet:°#°_/sofu FUNKTION_.°#°');
				return;
			}

			/*				if(pspl[0] == "set"){
								if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()){
									user.sendPrivateMessage('Sry, das wäre sinnfrei, wenn das jeder könnte... °>sm_01.gif<°');
								}
							}
			*/

			if (pspl[0] == "UUID" && pspl[1] != undefined) {

				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Diese Funktion steht Dir nicht zur Verfügung.');
					return;
				}

				var srvID = KnuddelsServer.getChatServerInfo().getServerId();
				var psplUserID = KnuddelsServer.getUserAccess().getUserId(pspl[1].toKUser());

				var pspluserkennung = psplUserID + '.' + srvID;


				user.sendPrivateMessage('_°BB°Die Unique-UserID von °>_h' + pspl[1] + '|/w ' + pspl[1] + '|/m ' + pspl[1] + '<° lautet: °RR°' + pspluserkennung + '_°r°');
			}

			if (pspl[0] == "UIDN" && pspl[1] != undefined) {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Diese Funktion steht Dir nicht zur Verfügung.');
					return;
				}
				var srvID = KnuddelsServer.getChatServerInfo().getServerId();
				//					var UserID			= KnuddelsServer.getUserAccess().getUserId(pspl[1].toKUser());

				//					var userkennung		= UserID+'.'+srvID;
				//					var userfromid		= pspl[1];
				var usernick = KnuddelsServer.getUserAccess().getUserById(pspl[1]);

				user.sendPrivateMessage('_°BB°Der Nick zur UserID ' + pspl[1] + ' lautet: °>_h' + usernick + '|/w ' + usernick + '|/m ' + usernick + '<° Die Unique-UserID hierfür ist die folgende: °RR°' + pspl[1] + '.' + srvID + '_°r°');
			}

			if (pspl[0].toLowerCase() == "cmp") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion hast Du keine Berechtigung!');
					return;
				}
				if (pspl[1] == "state") {
					user.sendPrivateMessage('/cmp ist: ' + appPersistence.getObject('cmp'));
				}
				if (pspl[1] == "on" || pspl[1] == "off") {
					appPersistence.setObject('cmp', pspl[1]);
					user.sendPrivateMessage('CM-Sammel-Privatnachricht ist nun: ' + pspl[1]);

					if (pspl[1] == "on") {
						if (typeof App.chatCommands[pspl[0]] === "function") {
							return false;
						}
						App.chatCommands['cmp'] = App.cmdcmp;
						setTimeout(function() {
							KnuddelsServer.refreshHooks();
						}, 1000)
					}

					if (pspl[1] == "off") {
						delete App.chatCommands.cmp;
						KnuddelsServer.refreshHooks();
					}
				}
			}

			if (pspl[0].toLowerCase() == "hzmp") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion hast Du keine Berechtigung!');
					return;
				}
				if (pspl[1] == "state") {
					user.sendPrivateMessage('HZMP ist: ' + appPersistence.getObject('hzmp'));
				}
				if (pspl[1] == "on" || pspl[1] == "off") {
					appPersistence.setObject('hzmp', pspl[1]);
					user.sendPrivateMessage('CM-Sammel-Privatnachricht für HZM ist nun: ' + pspl[1]);
				}
			}

			if (pspl[0].toLowerCase() == "systemnachrichten") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Hierfür fehlt Dir die Berechtigung.');
					return;
				}
				if (pspl[1] == undefined) {
					user.sendPrivateMessage('Die Syntax für diese Funktion lautet: _/sofu systemnachrichten:on|off_');
					return;
				}
				if (pspl[1] == "state") {
					user.sendPrivateMessage('Systemnachrichten sind: ' + appPersistence.getObject('onstartshutdown'));
				}
				if (pspl[1].toLowerCase() == "on" || pspl[1].toLowerCase() == "off") {
					appPersistence.setObject('onstartshutdown', pspl[1]);
					user.sendPrivateMessage('Die App-Start- & App-Shutdown-Systemmeldungen sind nun: ' + pspl[1]);
					var onstartshutdown = appPersistence.getObject('onstartshutdown');
				}
			}

			if (pspl[0].toLowerCase() == "getvar") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Hierfür fehlt Dir die Berechtigung.');
					return;
				}
				if (pspl[1] == "channeltype") {
					user.sendPrivateMessage(channeltype);
				} else {
					user.sendPrivateMessage('getvar Variablenname');
				}
			}

			if (pspl[0].toLowerCase() == "devp") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Für diese Funktion hast Du keine Berechtigung!');
					return;
				}
				if (pspl[1] == "state") {
					user.sendPrivateMessage('DEV/p ist: ' + appPersistence.getObject('devp'));
				}
				if (pspl[1] == "on" || pspl[1] == "off") {
					appPersistence.setObject('devp', pspl[1]);
					user.sendPrivateMessage('DEV-/p ist nun: ' + pspl[1]);
				}
			}

			if (pspl[0].toLowerCase() == "blacklist") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager()) {
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				if (pspl[1] == undefined) {
					user.sendPrivateMessage('die Syntax für die Blackliststeuerung lautet: _/sofu blacklist:on/off_');
				}

				if (pspl[1] == "on" || pspl[1] == "off") {
					appPersistence.setObject('blacklistonoff', pspl[1]);

					if (pspl[1] == "on") {
						var blloo = appPersistence.getObject('blacklistonoff');

						if (typeof App.chatCommands[pspl[0]] === "function") {
							return false;
						}

						App.chatCommands[pspl[0]] = App.cmdblacklist;
						setTimeout(function() {
							KnuddelsServer.refreshHooks();
						}, 1000)
						user.sendPrivateMessage('Ich habe die _Blacklist_ soeben _aktiviert._ (' + blloo + ')');
					}

					if (pspl[1] == "off") {
						var blloo = appPersistence.getObject('blacklistonoff');
						delete App.chatCommands.blacklist;
						KnuddelsServer.refreshHooks();

						user.sendPrivateMessage('Ich habe die _Blacklist_ soeben _deaktiviert_. (' + blloo + ')');
					}

					var blloo = appPersistence.getObject('blacklistonoff');
				}
			}

			if (pspl[0].toLowerCase() == "contact") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager() && channeltype == "Syschannel") {
					if (channeltype == "Syschannel") {
						user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung.');
						return;
					}
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}

				if (pspl[1] != undefined){
					if(pspl[1] != "show" && pspl[1] != "delete") {
						var userAccess = KnuddelsServer.getUserAccess();
						var users = appPersistence.getObject('contactto', []);

						var vaspl = pspl[1].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							if (users.indexOf(vaspl[i]) == -1) {
								if (userAccess.exists(vaspl[i])) {
									var users = appPersistence.getObject('contactto', []);

									users.push(vaspl[i]);
									appPersistence.setObject('contactto', users);

									user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' als Channel-Kontaktperson gesetzt.');
								} else {
									user.sendPrivateMessage('Der Nick ' + vaspl[i] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
								}
							} else {
								user.sendPrivateMessage('Da der Nutzer ' + vaspl[i] + ' bereits eine Kontaktperson dieses Channels ist, wurde dieser selbstverständlich _nicht erneut hinzugefügt!_');
							}
						}
						return;
//					appPersistence.setObject('contactto', pspl[1]);
//					user.sendPrivateMessage('Ansprechpartner für diesen Channel erfolgreich auf '+ pspl[1] +' gesetzt.');
					}
					if(pspl[1] == "show"){
						var cms = appPersistence.getObject('contactto', []);

						if (cms.length <= 0) {
							user.sendPrivateMessage('Du hast keine Kontaktpersonen auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /sofu contact:[Pega16,Pega,Security]<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht der Kontaktpersonen für diesen Channel:°r°_ °#°';

						for (i = 0; i < cms.length; i++) {
							msg = msg + i + ' | ' + cms[i].escapeKCode() + ' | °R°_°>Entfernen|/sofu contact:delete:' + cms[i].escapeKCode() + '<°_°r° °#°'
						}
						msg = msg + '°#° °[000,175,225]°_°>Weitere Kontaktperson hinzufügen|/tf-overridesb /sofu contact:[Pega16,Pega,Security]<°_°r°';
						user.sendPrivateMessage(msg);
						return;
					}
					if(pspl[1] == "delete" && pspl[2] != undefined){
						var cms = appPersistence.getObject('contactto', []);

						var vaspl = pspl[2].split(',');

						for (i = 0; i < vaspl.length; i++) {
							vaspl[i] = vaspl[i].trim();
							msg = msg + vaspl[i]

							if (cms.indexOf(vaspl[i]) == -1) {
								user.sendPrivateMessage('Der Nutzer ' + vaspl[i] + ' ist keine Kontaktperson!)');
								return;
							}

							cms.splice(cms.indexOf(vaspl[i]), 1);

							appPersistence.setObject('contactto', cms);

							user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' von den Channel-Kontaktpersonen entfernt. °R°_°>Rückgängig|/sofu contact:' + vaspl[i].escapeKCode() + '<°_°r°');
						}
					} else {
						user.sendPrivateMessage('Kontaktpersonen löscht Du mit _/sofu contact:delete:Nick_');
					}
				}
				
			}

			if (pspl[0].toLowerCase() == "botprivate") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion hast Du keine Berechtigung!');
					return;
				}
				if (pspl[1] == "state") {
					user.sendPrivateMessage('/bp ist: ' + appPersistence.getObject('botprivate'));
				}
				if (pspl[1] == "on" || pspl[1] == "off") {

					appPersistence.setObject('botprivate', pspl[1]);

					if (pspl[1].toLowerCase() == "on") {
						user.sendPrivateMessage('Bot-Privatnachricht aktiviert.');
						if (typeof App.chatCommands['bp'] === "function") {
							return false;
						}
						App.chatCommands['bp'] = App.cmdbp;
						KnuddelsServer.refreshHooks();
					}

					if (pspl[1].toLowerCase() == "off") {
						user.sendPrivateMessage('Bot-Privatnachricht deaktiviert.');
						delete App.chatCommands.bp;
						KnuddelsServer.refreshHooks();
					}
				} else {
					user.sendPrivateMessage('Die Syntax für diese Funktion lautet: _/sofu botprivate:on|off_');
				}
			}

			if (pspl[0] == "noshow" && [0, 1].indexOf(pspl[2])) {
				//				if(pspl[0] == "noshow"){
				var noshowquery = appPersistence.getObject(pspl[1]);
				appPersistence.setObject(pspl[1], pspl[2]);

				if (pspl[2] == 1) {
					user.sendPrivateMessage('_°BB°Die ' + pspl[1] + '-Nachricht ist nun ausgeschaltet. zum Einschalten klicke °RR°°>HIER|/sofu noshow:' + pspl[1] + ':0<°_°r°°##°');
				}
				if (pspl[2] == 0) {
					user.sendPrivateMessage('_°BB°Die ' + pspl[1] + '-Nachricht ist nun eingeschaltet. zum ausschalten klicke °RR°°>HIER|/sofu noshow:' + pspl[1] + ':1<°_°r°°##°');
					//						if(pspl[1] == "ownermessage"){
					//							user.sendPrivateMessage(ownertext);
					//						}
					//						if(pspl[1] == "hzminfo"){
					//							user.sendPrivateMessage(hzmtext);
					//						}
				}
			}

			if (pspl[0] == "keepOnline" && pspl[1] != undefined && pspl[1] != null && pspl[2] == "add") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
					return;
				}


				var userAccess = KnuddelsServer.getUserAccess();
				var users = appPersistence.getObject('keeponline', []);

				var vaspl = pspl[1].split(',');

				for (i = 0; i < vaspl.length; i++) {
					vaspl[i] = vaspl[i].trim();
					vauid = KnuddelsServer.getUserAccess().getUserId(vaspl[i]);
					msg = msg + vaspl[i]

					if (users.indexOf(vauid) == -1) {
						if (userAccess.exists(vaspl[i])) {
							var users = appPersistence.getObject('keeponline', []);

							users.push(vauid);
							appPersistence.setObject('keeponline', users);
							//								var icon3 = KnuddelsServer.getFullImagePath('CM.png');
							//								vaspl[i].addNicklistIcon(icon3, 35);
							user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' auf die KeepOnline-Liste gesetzt.');
						} else {
							user.sendPrivateMessage('Der Nick ' + vaspl[i] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
						}
					} else {
						user.sendPrivateMessage('Da der Nutzer ' + vaspl[i] + ' bereits auf der KeepOnline-Liste steht, wurde dieser selbstverständlich _nicht erneut hinzugefügt!_');
					}
				}
				return;
			}

			if (pspl[0] == "keepOnline" && pspl[1] != undefined && pspl[2] == "del") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
					return;
				}

				var cms = appPersistence.getObject('keeponline', []);

				var vaspl = pspl[1].split(',');

				for (i = 0; i < vaspl.length; i++) {
					vaspl[i] = vaspl[i].trim();
					vauid = KnuddelsServer.getUserAccess().getUserId(vaspl[i]);
					msg = msg + vaspl[i]

					if (cms.indexOf(vauid) == -1) {
						user.sendPrivateMessage('Der Nutzer ' + vaspl[i] + ' steht gar nicht auf der Liste!)');
						return;
					}

					cms.splice(cms.indexOf(vauid), 1);

					appPersistence.setObject('keeponline', cms);
					//						var icon3 = KnuddelsServer.getFullImagePath('CM.png');
					//						vaspl[i].removeNicklistIcon(icon3);

					user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' von der keepOnline-Liste entfernt.');
				}
				return;
			}
			
			if (pspl[0] == "keepOnline" && pspl[1].toLowerCase() == "list") {
//						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
//							user.sendPrivateMessage('Dies kann nur der Channeleigentümer.');
//							return;
//						}

						var cms = appPersistence.getObject('keeponline', []);

						if (cms.length <= 0) {
							user.sendPrivateMessage('Aktuell ist die Liste leer. °R°_°>Jetzt befüllen|/tf-overridesb /sofu keepOnline:[Pega16]:add<°_°r°');
							return;
						}

						var msg = '_°BB°Übersicht:°r°_ °#°';

						for (i = 0; i < cms.length; i++) {
							msg = msg + i + ' | ' + KnuddelsServer.getUserAccess().getUserById(cms[i]) + ' | °R°_°>Entfernen|/sofu keepOnline:' + KnuddelsServer.getUserAccess().getUserById(cms[i]) + ':del<°_°r° °#°';
						}
						msg = msg + '°#° °[000,175,225]°_°>Hinzufügen|/tf-overridesb /sofu keepOnline:[Pega16]:add<°_°r°';
						user.sendPrivateMessage(msg);
						return;
			}

			if (pspl[0] == "ipdatareciever" && pspl[1] != undefined && pspl[1] != null && pspl[2] == "add") {
					if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
						user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
						return;
					}


					var userAccess = KnuddelsServer.getUserAccess();
					var users = appPersistence.getObject('ipdatareciever', []);

					var vaspl = pspl[1].split(',');

					for (i = 0; i < vaspl.length; i++) {
						vaspl[i] = vaspl[i].trim();
						vauid = KnuddelsServer.getUserAccess().getUserId(vaspl[i]);
						msg = msg + vaspl[i]

						if (users.indexOf(vauid) == -1) {
							if (userAccess.exists(vaspl[i])) {
								var users = appPersistence.getObject('ipdatareciever', []);

								users.push(vauid);
								appPersistence.setObject('ipdatareciever', users);
								//								var icon3 = KnuddelsServer.getFullImagePath('CM.png');
								//								vaspl[i].addNicklistIcon(icon3, 35);
								user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' auf die IP-Data-Reciever-Liste gesetzt.');
							} else {
								user.sendPrivateMessage('Der Nick ' + vaspl[i] + ' existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						} else {
							user.sendPrivateMessage('Da der Nutzer ' + vaspl[i] + ' bereits auf der IP-Data-Reciever-Liste steht, wurde dieser selbstverständlich _nicht erneut hinzugefügt!_');
						}
					}
					return;
				}

				if (pspl[0] == "ipdatareciever" && pspl[1] != undefined && pspl[2] == "del") {
					if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
						user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
						return;
					}

					var cms = appPersistence.getObject('ipdatareciever', []);

					var vaspl = pspl[1].split(',');

					for (i = 0; i < vaspl.length; i++) {
						vaspl[i] = vaspl[i].trim();
						vauid = KnuddelsServer.getUserAccess().getUserId(vaspl[i]);
						msg = msg + vaspl[i]

						if (cms.indexOf(vauid) == -1) {
							user.sendPrivateMessage('Der Nutzer ' + vaspl[i] + ' steht gar nicht auf der Liste!');
							return;
						}

						cms.splice(cms.indexOf(vauid), 1);

						appPersistence.setObject('ipdatareciever', cms);
						//						var icon3 = KnuddelsServer.getFullImagePath('CM.png');
						//						vaspl[i].removeNicklistIcon(icon3);

						user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' von der IP-Data-Reciever-Liste entfernt.');
					}
					return;
				}
			
			if (pspl[0].toLowerCase() == "sysarray") {
				var srvID = KnuddelsServer.getChatServerInfo().getServerId();
				var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

				var userkennung = UserID + '.' + srvID;

				if (!user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Für diese Aktion musst Du _mindestens_ MCM sein!');
					return;
				}

				var develmsg = '°BB°_Die Nicks des Programmierers der App lauten:°r°_°##°';
				var hdevelmsg = '°BB°_Im °RR°NOTFALL_ °r°°10°(also wenn alle angeschriebenen(!) Programmierer nicht binnen 3 Stunden auf /ms antworten!)°r° _°BB°kann über folgende User noch eine Notfallnachricht übermittelt werden:_°r°°##°';

				if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0) {
					//						logger.info('entered CoDev >= 0');
					if (srvID == "knuddelsDE") {

						msg = develmsg + "";
						for (i = 0; i < App.coDevsNicks.length; i++) {
							msg = msg + '_°>_h' + App.coDevsNicks[i] + '|/w ' + App.coDevsNicks[i] + '|/m ' + App.coDevsNicks[i] + '<°_ (_°BB°' + KnuddelsServer.getUserAccess().getUserId(App.coDevsNicks[i].toKUser()) + '.' + srvID + '°r°_)°#° '
						}
						hmsg = hdevelmsg + "";
						for (i = 0; i < App.hiddenCoDevNicks.length; i++) {
							hmsg = hmsg + '_°>_h' + App.hiddenCoDevNicks[i] + '|/w ' + App.hiddenCoDevNicks[i] + '|/m ' + App.hiddenCoDevNicks[i] + '<°_ (_°BB°' + KnuddelsServer.getUserAccess().getUserId(App.hiddenCoDevNicks[i].toKUser()) + '.' + srvID + '°r°_)°#° '
						}
						user.sendPrivateMessage(msg);
						user.sendPrivateMessage(hmsg);

					}

					if (srvID == "knuddelsAT" || srvID == "knuddelsDEV") {

						msg = develmsg + "";
						msg = msg + '_°>_h' + App.coDevsNicks[0] + '|/w ' + App.coDevsNicks[0] + '|/m ' + App.coDevsNicks[0] + '<°_ (_°BB°' + KnuddelsServer.getUserAccess().getUserId(App.coDevsNicks[0].toKUser()) + '.' + srvID + '°r°_)°#° '
						user.sendPrivateMessage(msg);
					}
				}

				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && user.isChannelOwner() && user.isAppManager() && user.isChannelModerator()) {
					//							logger.info('entered non-Dev.');
					if (srvID == "knuddelsDE") {
						/*							develmsg=develmsg + '°>Pega16|/w Pega16|/m Pega16<°, °>Pega|/w Pega|/m Pega<°, °>Security|/w Security|/m Security<°';
													
													hdevelmsg=hdevelmsg + '°>Son of a Glitch|/w Son of a Glitch|/m Son of a Glitch<°';
						*/

						msg = develmsg + "";
						for (i = 0; i < App.coDevsNicks.length; i++) {
							msg = msg + '_°>_h' + App.coDevsNicks[i] + '|/w ' + App.coDevsNicks[i] + '|/m ' + App.coDevsNicks[i] + '<°_ , '
						}
						hmsg = hdevelmsg + "";
						for (i = 0; i < App.hiddenCoDevNicks.length; i++) {
							hmsg = hmsg + '_°>_h' + App.hiddenCoDevNicks[i] + '|/w ' + App.hiddenCoDevNicks[i] + '|/m ' + App.hiddenCoDevNicks[i] + '<°_, '
						}
						user.sendPrivateMessage(msg);

						if (channeltype == "MyChannel") {
							user.sendPrivateMessage(hmsg);
						}
					}

					if (srvID == "knuddelsAT" || srvID == "knuddelsDEV") {
						msg = develmsg + "";

						msg = msg + App.coDevsNicks[0]

						user.sendPrivateMessage(msg);
					}

				}
			}
			/*
									else {
										develmsg=develmsg + '°>Pega16|/w Pega16|/m Pega16<° (400082.knuddelsDE)°#°°>Pega|/w Pega|/m Pega<° (61187660.knuddelsDE)°#°°>Security|/w Security|/m Security<° (412884.knuddelsDE)';
										
										hdevelmsg=hdevelmsg + '°>Son of a Glitch|/w Son of a Glitch|/m Son of a Glitch<° (59063840.knuddelsDE)°#°';

									}
									
									user.sendPrivateMessage(develmsg+'°##°'+hdevelmsg);
									}
			*/
			if (pspl[0] == "EAU") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
					logger.warn(user.getNick() + ' versuchte gerade die EAU abzurufen!');
					user.sendPrivateMessage('Dies darf zu Diagnosezwecken nur der Channel-Eigentümer!');
					return;
				} else {
					if (channeltype == "Syschannel") {
						user.sendPrivateMessage('In einem Systemchannel steht diese Funktion _nicht_ zur Verfügung.');
						return;
					}
					//						logger.info(user.getNick()+' launches EAU.');

					var parameters = {
						onEnd: function(accessibleUserCount) {
							user.sendPrivateMessage(EAU);
						}
					};

					var EAU = "_°BB°Accessible Users:_°r° °##°";
					var eaus = "";
					userAccess.eachAccessibleUser(function(eaus) {
						//							eaus = eaus.escapeKCode();
						eaus = '_' + eaus.getProfileLink() + '_   (_°BB°' + KnuddelsServer.getUserAccess().getUserId(eaus) + '.' + srvID + '°r°_)°#°'
						EAU = EAU + eaus
						//							appPersistence.updateObject('EAU', u);

					}, parameters);
					//						user.sendPrivateMessage(EAU);
					//						user.sendPrivateMessage(appPersistence.getObject('EAU'));
				}
			}

			if (pspl[0]) {
				if (pspl[0].toLowerCase() == "tosource") {
					if (pspl[1]) {
						if (pspl[1].toLowerCase() == "on") {
							appPersistence.setObject(pspl[1], 'toSource');
							App.chatCommands['tosource'] = App.cmdtosource;
							setTimeout(function() {
								KnuddelsServer.refreshHooks();
							}, 1000)
							user.sendPrivateMessage('/tosource - Befehl aktiviert.');
						}
						if (pspl[1].toLowerCase() == "off") {
							appPersistence.setObject(pspl[1], 'toSource');
							delete App.chatCommands.tosource;
							KnuddelsServer.refreshHooks();
							user.sendPrivateMessage('/tosource - Befehl abgeschaltet.');
						}
					}
				}
			}

			if (pspl[0] == "matchico" && pspl[1] == "on" || pspl[1] == "off") {

				//	var discoconfetti	= appPersistence.getObject('discoconfetti' ['']);
				//	var blloo			= appPersistence.getObject('blacklistonoff');

				appPersistence.setObject('matchicobeta', pspl[1]);
				//					var matchicofunc = appPersistence.getObject('matchicofunction');

				if (pspl[0] == "matchico" && pspl[1] == "on") {
					//						appPersistence.setObject('matchicobeta', "on");
					if (typeof App.chatCommands[pspl[0]] === "function") {
						return false;
					}
					App.chatCommands[pspl[0]] = App.chatCommands.listcommands;
					setTimeout(function() {
						KnuddelsServer.refreshHooks();
					}, 1000)

					var matchicofunc = appPersistence.getObject('matchicobeta');

					//						KnuddelsServer.refreshHooks();
					user.sendPrivateMessage('Ich habe die Funktion _°>/matchico|/matchico<°_ soeben _aktiviert._ (' + matchicofunc + ')');
				}

				if (pspl[0] == "matchico" && pspl[1] == "off") {
					//						appPersistence.setObject('matchicobeta', "off");
					var matchicofunc = appPersistence.getObject('matchicobeta');
					delete App.chatCommands.matchico;
					KnuddelsServer.refreshHooks();

					user.sendPrivateMessage('Ich habe die Funktion _°>/matchico|/matchico<°_ soeben _deaktiviert_. (' + matchicofunc + ')');
				}
				//											KnuddelsServer.refreshHooks();
			}


			if (pspl[0] == "spielerei") {
				if (channeltype == "Syschannel") {
					user.sendPrivateMessage('Diese Funktion steht in Systemchannels _nicht_ zur Verfügung!');
					return;
				} else {
					if (pspl[1] == undefined) {
						user.sendPrivateMessage('die Syntax lautet: _/sofu spielerei:on/off_. Diese Funktion wirkt sich auf die Funktionen _Disco_ und _Confetti_ aus.');
					}

					if (pspl[1] == "on" || pspl[1] == "off" || pspl[1] == "HZM") {
						if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager()) {
							user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
							return;
						}
						appPersistence.setObject('discoconfetti', pspl[1]);

						if (pspl[1] == "on") {
							user.sendPrivateMessage('Ich habe die Funktionen _°>/disco|/disco<°_ und _°>/confetti|/confetti<°_ soeben _aktiviert_.');
						}
						if (pspl[1] == "off") {
							user.sendPrivateMessage('Ich habe die Funktionen _°>/disco|/disco<°_ und _°>/confetti|/confetti<°_ soeben _deaktiviert_.');
						}
						if (pspl[1] == "HZM") {
							user.sendPrivateMessage('Ab sofort können nur noch HZM _°>/disco|/disco<°_ und _°>/confetti|/confetti<°_ ausführen.');
						}
						return;
					}
				}
			}

			if (pspl[0] == "dev") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
					user.sendPrivateMessage('Dies ist ein Statistischer Befehl für den DEV.');
					return;
				}

				if (pspl[1] != undefined) {

				   if(pspl[1] == "persstr"){
							if(pspl[2] == "set"){
								appPersistence.setObject(pspl[3], pspl[4]);
								if(pspl[3] == "devmode" && pspl[4] == "on"){
								App.chatCommands['whoami'] = App.cmdme;
								App.chatCommands['whoareyou'] = App.cmdyou;
								}
								user.sendPrivateMessage('Die Persistence _'+pspl[3]+'_ wurde nun mit dem String _'+pspl[4]+'_ überschrieben.');
								setTimeout(function() {
								KnuddelsServer.refreshHooks();
								}, 1000)
								return;
							}
						}
						
						if(pspl[1] == "persobj"){
							if(pspl[2] == "set"){
								var persobj = appPersistence.getObject(pspl[3], []);
								persobj.push(pspl[4]);
								appPersistence.setObject(pspl[3], persobj);
							}
						}

						var userAccess = KnuddelsServer.getUserAccess();
						var appPersistence = KnuddelsServer.getPersistence();

						if (userAccess.exists(user)) {
							var getpers = appPersistence.getObject(pspl[1], []);

							user.sendPrivateMessage('Die Persistenze ' + pspl[1] + ' enthält folgendes:°##°' + getpers);
							return;
						} else {
							user.sendPrivateMessage('/sofu dev:PERSISTENZNAME');
						}
				}
			}
		}
	};

	/**
	 * Definiere AppDeveloper gemäß Vorgaben für tosource
	 */
	/**

	 * Dynamic Developer Modul

	 */

	function Developer() {

		if (this instanceof Developer) {

			this.ids = [];

			this.nicks = [];

			for (let m in arguments)

				this.add(arguments[m]);

			return this;

		} else throw new Error('Developer() must be invoked with new');

	}

	function Developers() {

		if (this instanceof Developers) {

			this.list = [];

			for (var m in arguments) {

				let v = arguments[m];

				switch (typeof v) {

					case 'number':
						this.reg(new Developer(v));
						break;

					case 'string':
						this.reg(new Developer(v));
						break;

					case 'object':

						if (Array.isArray(v)) {

							let dev = new Developer();

							v.forEach(function(m) {
								dev.add(m)
							});

							this.reg(dev);

						}

						break;

					default:

				}

			}

			return this;

		} else throw new Error('Developers() must be invoked with new');

	}

	Object.defineProperties(Developers.prototype, {

		'getDevById': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(n) {

				let matches = this.list.filter(function(dev) {
					return dev.ids.indexOf(n) > -1;
				});

				if (matches.length > 1) {

					Logger.warn('More than one match for Developer ID ' + n + ' found, please check your config!');

					Logger.debug(JSON.stringify(matches));

				}

				return matches[0];

			}

		},

		'getDevByName': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(s) {

				let matches = this.list.filter(function(dev) {
					return dev.nicks.indexOf(s) > -1;
				});

				if (matches.length > 1) {

					Logger.warn('More than one match for Developer ' + s + ' found, please check your config!');

					Logger.debug(JSON.stringify(matches));

				}

				return this.list.filter(function(dev) {
					return dev.nicks.indexOf(s) > -1;
				})[0];

			}

		},

		'reg': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(dev) {

				if (dev instanceof Developer) {

					this.list.push(dev);

				}

				return this;

			}

		},

		'add': { // alias for reg

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(dev) {

				return this.reg(dev);

			}

		},

		'del': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(dev) {

				let index = this.list.indexOf(dev);

				(index == -1) || this.list.splice(index, 1);

				return this;

			}

		},

		'delAll': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function() {

				this.list.forEach(function(dev) {

					this.del(dev);

				});

				return this;

			}

		},

		'rem': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(dev) {
				return this.del(dev);
			}

		},

		'remAll': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function() {
				return this.remAll();
			}

		},

		'search': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(m) {

				if (typeof m === 'string') return !!this.getDevByName(m);

				if (typeof m === 'number') return !!this.getDevById(m);

				return !1;

			}

		},

		'toString': {

			configurable: !0,
			writeable: !0,
			enumerable: !0,
			value: function() {
				return this.list.toString();
			}

		},
		'asUsers': {
			configurable:!0,writeable:!0,enumerable:!0,value: function() { 
				let r = [];
				this.list.forEach(function(dev) {
					let q = !1;
					dev.ids.forEach(function(id) {
						let usr = id.toKUser();
						if (usr) { r.filter(function(u) { return u.equals(usr); }).length || r.push(usr); }
					});
					dev.nicks.forEach(function(nick) {
						let usr = nick.toKUser();
						if (usr) { r.filter(function(u) { return u.equals(usr); }).length || r.push(usr); }
					});
				});
				return r;
			}
		}

	});

	Object.defineProperties(Developer.prototype, {

		'addId': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(n) {

				if (typeof n == 'number' && parseInt(n) == n) this.ids.indexOf(n) != -1 || this.ids.push(n);

				else throw new TypeError('ID is no Integer');

				return this;

			}

		},

		'addNick': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function(s) {

				if (typeof s == 'string') this.nicks.indexOf(s) != -1 || this.nicks.push(s);

				else throw new TypeError('Nick is no String');

				return this;

			}

		},

		'add': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function() {

				for (var m in arguments) {

					let v = arguments[m];

					switch (typeof v) {

						case 'number':
							this.addId(v);
							break;

						case 'string':
							this.addNick(v);
							break;

						default: //ignore

					}

				}

			}

		},

		'getNicks': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function() {
				return this.nicks;
			}

		},

		'getIds': {

			configurable: !1,
			writeable: !1,
			enumerable: !0,
			value: function() {
				return this.ids;
			}

		},

		'toString': {

			configurable: !0,
			writeable: !0,
			enumerable: !0,
			value: function() {

				let o = [],
					a = function(x) {
						o.push(x.toString())
					};

				this.nicks.forEach(a);
				this.ids.forEach(a);

				return '[object Developer(' + o.join(', ') + ')]';

			}

		}

	});

	const AppDevelopers = new Developers(
		[30568902, 59063840, 'Son of a Glitch'],
		[400082,30563372,'Pega16',2382131], // last ID = Knuddels AT
		[61187660,'Pega'],
		[412884,'Security']
	);

	//App.developers = new Developers(['Pega16'], ['Son of a Glitch']);
	User.prototype.isHZMDeveloper = function() {
		return this.isAppDeveloper() || AppDevelopers.search(this.getUserId()); // || App.developers.search(this.getNick()) // not secure enough
	};

	/**
	 * Beginne mit den Definitionen für toSource
	 */

	User.prototype.isOwner = function() {

		return (this.isChannelOwner() || this.isEqualToChannelOwner());

	}

	// MUST be isAppManager not isManager no Developer will be handled EVER as ChannelOwner in all Channels where this App is running

	User.prototype.isEqualToChannelOwner = function() {

		return (this.isChannelOwner() || (this.isAppManager() && this.isChannelModerator() && this.getPersistence().hasNumber('equalToChannelOwner')));

	}

	User.prototype.isCM = function() {

		return this.isAppDeveloper() || this.isChannelModerator();

	}

	User.prototype.isManager = function() {

		return this.isHZMDeveloper() || this.isAppManager();

	}

	User.prototype.isChannelStaff = function() {

		return this.isOwner() || this.isCM() || this.isManager();

	}

	// isChannelCoreUser is buggy by now... so we use a different method

	User.prototype.isLikingUs = function() {

		return this.isHZMDeveloper() || this.isLikingChannel() || user.isChannelCoreUser();

	}

	User.prototype.isLimited = function() {

		return this.isLocked() || this.isAway() || this.isMuted() || this.isColorMuted();

	}

	App.cmdtosource = function(user, params, func) {

		if (user.isChannelStaff()) {

			if (params.length) {

				Bot.sendPublicMessage(user.getProfileLink() + ' Code: °#°°>{font}Consolas<13<°' + params.escapeKCode().replaceAll('\\#', "°#°").replaceAll("\t", '    ').replaceAll('\\°°#°\\°', '\\°\\#\\°'));

			} else {

				user.sendPrivateMessage('Bitte verwende /tosource [text]');

			}

		} else {

			user.sendPrivateMessage('Unzureichende Berechtigungen');

		}

	}

	/**
	 * toSource ende.
	 */

	/**
	 * nun versuchen wir die /cme über die log zu jagen...
	 */
	User.prototype.say = function() {
		let args = arguments;
		if (this.getNick() === KnuddelsServer.defaultBotUser.getNick()) {
			// Prüfe auf Logging
			if (loggerID()) {
				// in log eintragen
				addEntry(false, this.getNick(), args[0], 'logText', Date.now());
			};
			this.sendPublicMessage.apply(this, args);
		}
	};

	//Changelog
	App.chatCommands.chlog = function(user, command) {

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		changelog = [];
		changelog[0] = "°-°_°>center<°°24°*Changelog*_°r°°-°";
		changelog[1] = "°>left<°_25.03.2017_ | Channelapp wird erstmals als HZM-App deklariert.";
		changelog[2] = "_26.03.2017_ | HZM-App einiger Optimierungen (AppPersistence) unterzogen und Quelltext etwas gekürzt.";
		changelog[3] = "_27.03.2017_ | HZM-App wird auf Entwickler-Treffen vorgestellt.";
		changelog[4] = "_28.03.2017_ | HZM-App wird in BETA und ReleaseCandidate getrennt.";
		changelog[5] = "_29.03.2017_ | HZM-App bekommt diverse Usergruppen. Weiterhin die Generalvollmacht für Admins an vielen Stellen gekillt. Dort dem ChannelOwner die Möglichkeit eingeräumt, selbst Rechte zu vergeben.";
		changelog[6] = "_29.03.2017_ | _21:35 Uhr_ | die Funktion _°>/listcommands|/listcommands<°_ eingeführt. Diese zeigt die wichtigsten Befehle der jeweiligen Zulassungsgruppe in der sich der ausführende User befindet.";
		changelog[7] = "_30.03.2017_ | _11:00 Uhr_ | HZM-APP hat nun eine BlackList bekommen. Channelzutrittskontrolle um die Blacklist erweitert.";
		changelog[8] = "_30.03.2017_ | _13:00 Uhr_ | ReleaseCandidate allen HZM zur Installation freigegeben.";
		changelog[9] = "_30.03.2017_ | _14:00 Uhr_ °-°ChannelOwner hat ab sofort die Möglichkeit, den angezeigten SPERRTEXT im CM-Modus selbst zu definieren. Mit _/restricttext TEXT_ wird TEXT als Meldung bei nicht zugelassenen Usern angezeigt. Mit _°>/restrictiontext|/restrictiontext<°_ wird der aktuell gesetzte Text angezeigt.°#°ist KEIN Text definiert, so wird der folgende Text angezeigt (RC, NICHT BETA!):°#°\"Dieser Channel wurde durch den Channelinhaber so eingestellt, dass ausschließlich explizit zugelassene User den Channel betreten können. {zwei Zeilenumbrüche} Um eine Zutrittsberechtigung zu diesem Channel zu bekommen, wende Dich bitte an den Channeleigentümer.\"°-°";
		changelog[10] = "_30.03.2017_ | _15:10 Uhr_ °-°Dem Channelinhaber nun die Möglichkeit eingeräumt, auch den Text für den \"geschlossenen Channel\" zu ändern. Mit _/closedtext TEXT_ definiert der CO nun den Text. Mit _°>/showclosedtext|/showclosedtext<°_ wird der aktuell gesetzte Text angezeigt.°#°_AUSNAHME:_ Wenn noch kein Text hierfür definiert wurde, wird das Array LEER übergeben. Es wird DANN die folgende Meldung für den verschlossenen Channel angezeigt:°#°\"_Der Channel ist aktuell °R°GESCHLOSSEN!°r° {zwei Zeilenumbrüche} bitte versuche es später erneut._\"°-°";
		changelog[11] = "_31.03.2017_ | _18:30 Uhr_ °-°Kleinere Fehlerbehebungen vorgenommen...°-°";
		changelog[12] = "_01.04.2017_ | _22:15 Uhr_ °-°Ab sofort können HZM ihre eigene CME in der App speichern und nutzen! // Weiterhin einige Optimierungen der App vorgenommen.°-°";
		changelog[13] = "_02.04.2017_ | _15:00 Uhr_°-°Das Befehlswirrwar mal etwas entwirrt.°#°Aus vielen kleinen Befehlen einen großen gemacht.°#°Ab sofort werden Rollen im Channel per _/role set:ROLLE:Nick_ gesetzt.°#°Die Rolleninhaber zeigt man sich mit _/role list:ROLLE_ gesammelt an.°#°Jemandem die Rolle entziehen funktioniert mit _/role remark:ROLLE:Nick_°#°Befehls-HILFE hinzugefügt. _°>/role help|/role help<°_°-°";
		changelog[14] = "_02.04.2017_ | _20:12 Uhr_ °-°Weiter aufgeräumt. Darunter folgende Dinge:°#°- Blacklist-Funktion zusammengefasst. Zusätzlich _°>/blacklist help|/blacklist help<°_ für die korrekte Syntax angelegt.°#°- _/channelthema TEXT_ repariert.°#°- CME-Funktion optimiert°-°";
		changelog[15] = "_02.04.2017_ | _22:05 Uhr_ °-°Die Verwarnungsstufen sauber in _eine Funktion_ gepackt. Näheres hierzu mit _°>/warn help|/warn help<°_°-°";
		changelog[16] = "_02.04.2017_ | _22:30 Uhr_ °-°Quellcode etwas aufgeräumt. Alte, nicht mehr genutzte Funktionen (da in neuen Funktionen vereint) entfernt.°-°";
		changelog[17] = "_06.04.2017_ | _11:30 Uhr_ °-°Stress-User-Liste umgebaut. ab sofort alles per _°>/sul|/sul<°_ erreichbar.°#°Näheres hierzu mit _°>/sul help|/sul help<°_.°-°";
		changelog[18] = "_06.04.2017_ | _12:30 Uhr_ °-°Changelog aufgeräumt. Nun einfacher zu pflegen.°#°°#°Weiterhin nun Versionenangabe GLOBAL angelegt. So kann durch Änderung einer einzelnen Variable nun zwischen ALPHA, BETA und ReleaseCandidate geswitched werden. Dies macht es in der App-Pflege einfacher. Hier werden auch Befehle im Teststadium nicht in der RC mit geladen, obwohl sie aufgrund des identischen Quelltextes vorhanden sind. (was sehr von Vorteil ist!)°-°";
		changelog[19] = "_06.04.2017_ | _13:11 Uhr_ °-°Funktion _°>/listcommands|/listcommands<°_ aufgeräumt und aktuatlisiert.°-°";
		changelog[20] = "_06.04.2017_ | _13:22 Uhr_ °-°/say - Funktion nun auch in neue Funktion umgebaut. Näheres siehe _°>/say help|/say help<°_°-°";
		changelog[21] = "_13.04.2017_ | _13:22 Uhr_ °-°Kleinere Optimierungen und Einbau einiger kleinerer Ostereier inclusive Mini-Gewinnspiel für die Finder der Ostereier.°-°";
		changelog[22] = "_Version 3.3_ °-°- Weitere Optimierungen am Quellcode°#°- Einbau der Massen-Hinzufügung und -Entfernung von Usern (CM/HZM/VCM/Gast)°#°- JIT-Iconsetzung aufgrund von Fehlern in der Interpretation des Servers im Sinne der JITIS deaktiviert. °-°";
		changelog[23] = "_Version 3.5_ °-°- kleinere Fehlerbehebungen°#°- Bot-Schriftart nun per _°>/schriftart|/schriftart<°_ vom Channel-EIGENTÜMER wählbar.°#°- Begrüßungen per _°>/greetings|/greetings<°_ aus und einschaltbar. °-°";
		changelog[24] = "_Version 3.6_ °-°Ab sofort kann jeder Nutzer seinen eigenen Begrüßungstext bei eingeschalteter Bot-Begrüßung selbst festsetzen.°-°";
		changelog[25] = "_Version 3.7_ °-°- Sonderfunktionen wie °>/disco|/disco<° und °>/confetti|/confetti<° in der Nutzung steuerbar. Hierzu gibts nun die folgenden Einstellmöglichkeiten: _°>/sofu spielerei:on|/sofu spielerei:on<°_ zum aktivieren dieser beiden Funktionen (Standardeinstellung!), _°>/sofu spielerei:off|/sofu sielerei:off<°_ um diese beiden Funktionen zu _deaktivieren_. und _°>/sofu spielerei:HZM|/sofu spielerei:HZM<°_ um diese beiden Funktionen auf die HZM zu beschränken.°#°°#°Blacklist nun deaktivierbar. Einschalten: _°>/sofu blacklist:on|/sofu blacklist:on<°_ und Ausschalten: _°>/sofu blacklist:off|/sofu blacklist:off<°_ °#°Wunschfunktion _°>/matchico|/matchico<°_ wieder eingeführt.°##°Ab sofort kann sich _jeder_ seine eigene Begrüßung selbst eintragen. => _/greetings eigenerNick:TEXT_ °##°°-°";
		changelog[26] = "_Version 3.8_ °-°- App in Module unterteilt. HZM-Modul lässt sich mit dem Befehl °>/sofu hzm:on|/sofu hzm:on<° einschalten.°#°- /greetings - Funktion weiter ausgebaut. Genaueres siehe mit _°>/greetings|/greetings<°_°##°°-°";
		changelog[27] = "_Version 3.8_ °-°- HZA/E - in allen Funktionen und Hilfetexten nach HZM umbenannt... damit wieder im Rausch der Zeit unterwegs... °>sm_10.gif<°°##°°-°";
		changelog[28] = "_Version 3.9_ °-°- Der Bot kann nun auch Dinge tun... mit _°>/dobot TEXT|/dobot Text<°_ lässt man den Bot TEXT tun.°##°- Ab sofort kann man Zweitnicklisten anlegen (wenn HZM-Modul aktiv!). So fällt es den HZM und den CMs bei der Strafbemessung deutlich leichter...°##°_/nicklist {Nick}:add:Nick1,Nick2,Nick3,........NickN_ fügt die Nicks der Liste hinzu.°#°_/nicklist {NICK}:list_ zeigt alle eingetragenen Nicks.°#°Löschen aus der Liste geht analog zum Hinzufügen nur mit :del:°##°- °BB°_Ab sofort keine Dopplungen auf den Listen mehr möglich (Abfrage eingebaut!)_°r°°##°°-°";
		changelog[29] = "°>left<°_Version 4.0_ °-°- Systemseitige Sonderfunktionsnachricht eingebaut°#°- Optimierungen bei den Listen°#°- unterschiedliche Anzeigemodi CM / VCM / HZM°#°- Eintragungen auf der Nickliste sind nun durch HZM zu prüfen und zu bestätigen. Bis zur Bestätigung stehen die durch CMs eingetragenen Nicks unter Vorbehalt mit dabei (extra aufgeführt!)°#°- Verknüpfung der °>/sul|/sul<° mit der °>/nicklist|/nicklist<° zur besseren Übersicht hergestellt.°#°- _HZM-App - Doku_ in Systemnachricht an alle HZM verlinkt zur Verfügung gestellt.°##°°-°";

		if (extver >= "4.1" || channelName.toLowerCase() == DEVCHANNEL.toLowerCase()) {
			changelog[30] = "_Version 4.1_ °-°- Automatisches Update der App bei neuer Version zu CMV-unüblichen Zeiten.°#°- Infonachricht im Demochannel beigefügt.°#°- Channel-Eigentümer kann den Link zur Doku nun ausblenden.°#°- HZM können die Begrüßungsnachricht nun auch ausblenden.°#°°BB°_beim Ausblenden der Nachrichten verbleibt ein INFO-Text, welcher die Möglichkeit bietet, die Nachrichten wieder 'wie üblich' zu aktivieren!°r°_°##°- Probleme bei der Begrüßung von Usern ohne eingetragene persönliche Begrüßung behoben.°#°- Einzelne weitere Bugs behoben°#°- HZM können nun selbst entscheiden welche Infonachrichten ihre CM zu sehen bekommen (einzeln aktivier- und deaktivierbar!).°r°°##°°-°";
		}

		if (extver >= "4.2" || channelName.toLowerCase() == DEVCHANNEL.toLowerCase()) {
			changelog[31] = "_Version 4.2_ °-°- Kleinere Korrekturen. U.A. zeigt die Nickliste nun auch Nicks der zu prüfenden Nicks für die Liste wenn die Hauptlistee leer sein sollte.°#°- /sul hat nun eine Prüfliste wie die bei der /nicklist bekommen. Somit können CM nun auch User auf die StressUserListe setzen, wobei die HZM entscheiden können/sollen, ob die User tatsächlich auf der richtigen (festen!) StressUserListe landen können/sollen.°#°- bei der /sul zusätzlich noch die Möglichkeit eingebaut, :add: anstatt :set: zu nutzen...°#°- Codefehler bei den Spielereien (/disco und /confetti) korrigiert°#°- Rundmail logischer gecodet (Generalreformkonzept!).°#°- Kartensystem eindeutiger gestaltet (Formatierung der (offiziellen) Ausgaben)°#°- Codeformatierung auf neue Definitionen angepasst.°#°- °>/role|/role<° revolutioniert (Codetechnisch)°##°°-°";
		}

		if (extver >= "4.3" || channelName.toLowerCase() == DEVCHANNEL.toLowerCase()) {
			changelog[32] = "_Version 4.3_ °-°- Ab sofort können auch Nicks mit Sonderzeichen jeglicher Facon in die Listen eingetragen werden.°#°- Einige weitere Bugs behoben.°#°- Zweitnicks des Programmierers für Rückfragen dem ChannelOwner per _°>/sofu sysarray|/sofu sysarray<°_ zur Verfügung gestellt.°#°- versehentliche Antworten an Bot werden nun den ChannelEigentümer übersandt soweit online. Zusätzlich wird ein INFO-Eintrag im Logger erzeugt.°#°- die °>/sofu sysarray|/sofu sysarray<° in soweit umgebaut, dass nun auf den jeweiligen Servern nur noch der Teil angezeigt wird, der tatsächlich korrekt ist (Serverspezifische Nicks!).°#°- Der Channeleigentümer kann ab sofort abfragen, auf welche Nicks er per App zugreifen kann. Dieses funktioniert mit der Funktion _°BB°°>/sofu EAU|/sofu EAU<°°r°_ (Je nach Größe des MyChannels kann es sein, dass die Ausgabe etwas länger dauert, bis sie erscheint. Dies ist kein Fehler, sondern liegt an der enormen Datenlast gerade in großen MyChannels!).°#°- Unique-UserID kann ab sofort per _°BB°°>/sofu UUID:James|/sofu UUID:James<°°r°_ ausgegeben werden (nur Channeleigentümer! -- war eine Wunschfunktion eurerseits!)°#°- Die UserID-Rückwärtssuche (UID! NICHT UUID!) (auch eine Wunschfunktion von euch!) wurde ebenfalls verwirklicht. Mit Eingabe von _°BB°°>/sofu UIDN:0|/sofu UIDN:0<°°r°_ bekommt ihr damit heraus, dass die UserID 0 James zugeordnet ist.°#°- CM-Sammelnachrichtenfunktion beigefügt. CMs (auch für HZM möglich!) schreiben den App-Bot per /p an und automatisch bekommen ALLE CMs IM AKTUELLEN CHANNEL die Nachricht... damit ist das Problem mit den /p's bei Userwechseln behoben.°#°- App-Start und Shutdown-Ausgabe ein/ausschaltbar.°##°°BB°_Changelog-AUSGABE gekürzt. Mit °>/chlog|/chlog<° erhält man nun nur noch die AKTUELLEN Änderungen (alle Unterversionen der aktuellen Version!)°#°Mit °>/chlog all|/chlog all<° bekommt ihr dafür immernoch die KOMPLETTE Changelog seit Anbeginn der App._°r°°##°°-°";
		}

		if (extver >= "4.4" || channelName.toLowerCase() == DEVCHANNEL.toLowerCase()) {
			changelog[33] = "_Version 4.4_ °-°- Die letzten bekannten Bugs behoben.°#°- Eventspezifische Gewinnspieldinge eingebaut. Diese werden zu den Events automatisch freigegeben (nur MyChannel!).°#°- App Systemchannelfähig gemacht.°#°- Logging nun generell in die HZM-App eingebaut. Näheres unter _°BB°°>/extlog|/extlog<°°r°_°#°- /cme taucht nun auch im CMV-Log auf.°#°- Wenn das Log aktiv ist, werden nun auch alle Ausgaben von /say protokolliert.°#°- Nachrichten an den Bot geben ab sofort Rückmeldung, wenn kein Empfänger online ist.°##°°-°";
		}
		
		if (extver >= "4.5" || channelName.toLowerCase() == DEVCHANNEL.toLowerCase()) {
			changelog[34] = "_Version 4.5_ °-°- Entfernen-Link für StressUser-PRÜF-Liste eingefügt.°#°- Entfernen-Link nun auch für die Zweitnicklisten eingefügt.°#°- _Channel-Ansprechpartner_ für geblacklistete User und für Nicht-auf-der-Gästeliste-stehende-User in soweit angepasst, dass dort ein beliebiger Ansprechpartner angegeben werden kann. => _°BB°/sofu contact: °RR°NICK°r°_°#°Dieser Ansprechpartner wird bei allen Channelsperrmeldungen (außer bei 'Channel closed') anstelle des Channelinhabernicks ausgegeben. Insbesondere bei Channels von HZM, die den Channel mit Zweitnick erstellen, um einen privaten in Adminamt zu haben, ist diese Funktion von Vorteil! -- Ein Dank an Vivianle01 für den Hinweis!°#°- Channel-Ansprechpartner können nun auch mehrere Personen sein. => _/sofu contact:Nick1,Nick2,Nick3,Nick4,..._°#°- mit Hilfe von _/sofu contact:show_ kann man sich ab sofort die eingetragenen Ansprechpartner des Channels anzeigen lassen. Ist diese Liste leer, wird angezeigt, dass die Liste leer ist, allerdings zählt bei einer leeren Liste IMMER der Channelinhabernick (siehe vorige Ausführungen zur Channelzutrittsbeschränkung!)°#°- mit _/sofu contact:delete:Nick1,Nick2,Nick3,Nick4,..._ löscht man die entsprechenden Nicks von der Ansprechpartnerliste (Channelzutrittsbeschränkung!)°#°- die Funktion _°>/restricttext|/restricttext<°_ lässt ab sofort keine leeren Arrays mehr zu.°#°- Die Funktion _°>/restrictiontext|/restrictiontext<°_ gibt ab sofort GENERELL den Channelzutrittsverweigerungstext aus. Bislang wurde bei nicht selbst gesetztem Text ein leeres Array zurückgegeben. Ab sofort zeigt die Funktion IMMER den Text an, den der Zutrittsverweigerte bekommt, also auch den Standardtext, sofern kein eigener gesetzt wurde.°#°- Hinter selbst gesetzten Zutrittsverweigerungstexten (Funktion _°>/restrictiontext|/restrictiontext<°_) existiert nun ein 'Entfernen'-Link, um den Text auf Standard zurückzusetzen.°#°- Installationslink unter der _°>/ver|/ver<°_ Funktion für Interessierte eingefügt. so kann dieser für den eigenen Channel schlichtweg kopiert werden.°##°°-°";
		}
		
		if (extver >= "4.6" || channelName.toLowerCase() == DEVCHANNEL.toLowerCase()) {
			changelog[35] = "Version 4.6_ °-°- Selbstauskunft nun für alle mit Hilfe der Funktion _°BB°°>/whoami|/whoami<°°r°_ zugänglich (Nur der Nutzer selbst bekommt die über ihn gespeicherten Daten angezeigt!).°##°°-°";
		}



		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>' + pfad + 'sascha.gif<°';

		msg = "";

		if (command == "all") {
			for (i = 0; i < changelog.length; i++) {
				msg = msg + ' °#° ' + changelog[i] + ' °#°'
			}
		} else {
			msg = msg + '°##°' + changelog[0] + '°#°'
			for (i = 29; i < changelog.length; i++) {
				msg = msg + ' °#° ' + changelog[i] + ' °#°'
			}
		}
		user.sendPrivateMessage(msg);
	};

	var appPersistence = KnuddelsServer.getPersistence();
	var matchicofunc = appPersistence.getObject('matchicobeta', []);

	//		if(matchicofunc == "on"){
	App.cmdmatchico = function(user) {
		var appPersistence = KnuddelsServer.getPersistence();
		var isAdmin = user.isInTeam("Admin");
		//			var matchicofunc	= appPersistence.getObject('matchicofunction', ['']);
		/*			
					if(!user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !isAdmin){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
		*/

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		var appPersistence = KnuddelsServer.getPersistence();
		if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !isAdmin) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();

		if (userAccess.exists(user)) {
			var ishzae = appPersistence.getObject('hzae', []);
			//				ChanKey.push('offen');
			appPersistence.getObject('CMs', []);
			if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0) {
				user.sendPrivateMessage('CM-Befehle:°-°°#°°>/sul|/sul<° => zeigt die aktuelle Stress-User-Liste.°#°°-°');
				return;
			}
			appPersistence.getObject('hzae', []);
			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0) {
				user.sendPrivateMessage('HZM-Dinge:°#°_°R°[Karten]°r°_ (HZM)§°#°°#°Auf der _°>/warn help|/warn help<°_ gibt es die genauen Informationen.°#°°#°°-°°#°_°R°[Zugangsbeschränkungsdinge]°r°_ (HZM)§°#°°#°_/role set:ROLLE:[Nick]_			=> Nick die ROLLE verleihen. Zum Beispiel um den Zutritt zum Channel zu gewähren (Channel im CM-Modus (s.o.))°#°Näheres hierzu auf der _°>/role help|/role help<°_°#°°#°°#°°#°_°R°[Stress-User-Liste]°r°_(Berechtigung in Klammer dahinter)§ °#°°#°_°>/sul|/sul<°_			=>	Stress-User-Liste aufrufen/anzeigen (CMs & HZM) °#°_/sul set:[Nick]_	=>	Nick auf die Stress-User-Liste setzen (HZM) °#°_/sul delete:[Nick]_	=>	Nick von der Stress-User-Liste entfernen. (HZM)');
				//						return;
			}
			/*					else if ( == "Closed") {
									user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
									return;
								}
			*/
			if (user.isChannelOwner()) {
				user.sendPrivateMessage('°#°_Channelbefehle für den Channelowner:_°#°°#°°#°_/restricttext TEXT_ => Channelsperrtext für den Channel-CM-Modus definieren.°#°°#°_°>/restrictiontext|/restrictiontext<°_ => zeigt den aktuell definierten Channelsperrtext für den im CM-Modus befindlichen Channel an. Ist keiner definiert, wird der vorgegebene Standardtext angezeigt.°#°°#°°#°_/closedtext TEXT_ => definiert den Anzeigetext bei Channelbetretversuch im Falle eines geschlossenen Channels.°#°°#°_°>/showclosedtext|/showclosedtext<°_ => zeigt den definierten Sperrtext. Ist keiner definiert, liefert dieser Befehl ein _leeres Array_ zurück. In diesem Falle ist der vorgegebene Standardtext aktiv (siehe _°>/chlog|/chlog<°_)°#°°#°_°>/schriftart|/schriftart<°_ ändert die Bot-Schriftart.°#°°#°_/greetings on/off_ schaltet die Nutzer-Begrüßung ein/aus.°-°');
			}
			user.sendPrivateMessage('°-°_Channelbefehle (Admins/ChannelOwner/AppManager):_°#°°#°_°>/opened|/opened<°_		=> Channel für jedermann öffnen (Standardeinstellung!)°#°_°>/usual|/usual<°_		=> Channel nur für explizit zugelassene Personen öffnen (CM-Modus für HZM und CMs) [Mehr unter \"Zugangsbeschränkungsdinge\"]°#°_°>/closed|/closed<°_		=> Channel abschließen. Dies hat zur Folge, dass _KEINER_ mehr den Channel betreten kann!°#°°#°_°>/chanstate|/chanstate<°_	=> Anfrage in welchem Status sich der Channel gerade befindet (Kontrolle damit man den Channel nicht verschlossen verlässt!)');
			//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
			return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};
	//	};

	//Blackliste
	App.cmdblacklist = function(user, params, func) {

		var appPersistence = KnuddelsServer.getPersistence();

		var userAccess = KnuddelsServer.getUserAccess();
		//			var appPersistence = KnuddelsServer.getPersistence();

		var srvID = KnuddelsServer.getChatServerInfo().getServerId();
		var UserID = KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung = UserID + '.' + srvID;


		if (userAccess.exists(user)) {
			var pspl = params.split(':');
			var msg = "";

			if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelModerator() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}

			if (pspl[0] == "help") {
				user.sendPrivateMessage('Die Syntax für die BLACKLIST-Funktion lautet:°#°_/blacklist set:NICK_ um NICK dauerhaft aus dem Channel zu verbannen.°#°Mit _/blacklist show_ zeigst Du dauerhaft ausgesperrten Nutzer an.°#°Mit _/blacklist remark:NICK_ begnadigst Du NICK.');
				return;
			}

			if (pspl[0] == "set" && pspl[1] != undefined && pspl[1] != " ") {
				if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelModerator() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()) {
					user.sendPrivateMessage('Für diese Funktion musst Du _mindestens MCM_ oder App-Manager sein!');
					return;
				}

				var userAccess = KnuddelsServer.getUserAccess();

				var vaspl = pspl[1].split(',');

				for (i = 0; i < vaspl.length; i++) {
					msg = msg + vaspl[i]

					if (userAccess.exists(vaspl[i])) {
						var users = appPersistence.getObject('blacklisted', []);
						users.push(vaspl[i]);
						appPersistence.setObject('blacklisted', users);
						user.sendPrivateMessage('Ich habe ' + vaspl[i] + ' soeben eine _°R°dauerhafte Channelsperre°r°_ verpasst. °R°_°>Rückgängig|/blacklist remark:' + vaspl[i] + '<°_°r°');

					} else {
						user.sendPrivateMessage('Dieser User existiert nicht! -- Hast Du Dich vielleicht vertippt?');
					}
				}
				return;
			}
			/*					else {						
								user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
								return;
								}
			*/
			if (pspl[0] == "show") {
				/*					if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelModerator() && !isAdmin){
										user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
										return;
									}
				*/
				var bll = appPersistence.getObject('blacklisted', []);

				if (bll.length <= 0) {
					user.sendPrivateMessage('Aktuell ist niemand dauerhaft ausgesperrt. °R°_°>Jemanden dauerhaft aussperren|/tf-overridesb /blacklist set:[James]<°_°r°');
					return;
				}

				var msg = 'Aktuell sind ausgesperrt: °#°';

				for (i = 0; i < bll.length; i++) {
					msg = msg + i + ' | ' + bll[i] + ' | °R°_°>Entfernen|/blacklist remark:' + bll[i] + '<°_°r° °#°'
				}
				msg = msg + '°#° °[000,175,225]°_°>Weiteren User dauerhaft aussperren|/tf-overridesb /blacklist set:[James]<°_°r°';
				user.sendPrivateMessage(msg);
				return;
			}

			if (pspl[0] == "remark" && pspl[1] != undefined && pspl[1] != " ") {
				if (App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
					user.sendPrivateMessage('Begnadigen können nur der Channel-Eigentümer und der App-Manager.');
					return;
				}

				var bll = appPersistence.getObject('blacklisted', []);

				var vaspl = pspl[1].split(',');

				for (i = 0; i < vaspl.length; i++) {
					msg = msg + vaspl[i]

					if (bll.indexOf(vaspl[i]) == -1) {
						user.sendPrivateMessage(vaspl[i] + ' ist doch gar nicht ausgesperrt!)');
						return;
					}

					bll.splice(bll.indexOf(vaspl[i]), 1);

					appPersistence.setObject('blacklisted', bll);

					user.sendPrivateMessage('Ich habe _' + vaspl[i] + '_ soeben _begnadigt_. °R°_°>Rückgängig|/blacklist set:' + vaspl[i] + '<°_°r°');
				}
			} else {
				user.sendPrivateMessage('Du hast die Funktion falsch eingegeben. Die Syntax findest Du unter _°>/blacklist help|/blacklist help<°_');
			}
		}
	};

	//	};
	//	});


/*

	App.chatCommands['evalcode'] = function(user, params) {
		if (user.isAppDeveloper() || user.isHZMDeveloper()) {
			try {
				user.sendPrivateMessage('Result: ' + (eval(params) + '').escapeKCode());
			} catch (e) {
				user.sendPrivateMessage(e.toString());
			}
		}
	}

*/