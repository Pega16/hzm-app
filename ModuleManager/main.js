// Pega's Deluxe-App ;)

//REQUIRE
require('framework/KFramework.min.js');
require('Commands.js');
require('User.js');
require('versionCompare.js');
// require('games.js')

	var channel = KnuddelsServer.getChannel().getChannelName();
	var channl = KnuddelsServer.getChannel();
	var channelConfiguration = channl.getChannelConfiguration();
//	var channelInformation = channl.getChannelInformation();
	var channelRights = channelConfiguration.getChannelRights();
	var channelOwners = channelRights.getChannelOwners();
	var chanown = channelOwners.toString();
	var chown = chanown.split("[");
	var cown = chown[1].replace("]", "");
	var logger = KnuddelsServer.getDefaultLogger();
	
	var Bot = KnuddelsServer.getDefaultBotUser();
	
	var srvID			= KnuddelsServer.getChatServerInfo().getServerId();

//	var isAdmin			= user.isInTeam("Admin");
	var appPersistence	= KnuddelsServer.getPersistence();
	var isHzmApp		= appPersistence.getObject('isHzmApp', ['']);
	var AcceptedNicks	= appPersistence.getObject('CMs', ['Holgi']);
	var ChannelClosed	= appPersistence.getObject('ChanKey', ['opened']);
	var StressUser		= appPersistence.getObject('stressuserliste', ['']);
	var VCMs			= appPersistence.getObject('vcms', ['']);
	var SonderUser		= appPersistence.getObject('sonderuser', ['']);
	var HZAs			= appPersistence.getObject('hzae', ['']);
	var DevUser			= appPersistence.getObject('devuser', ['Pega16']);
	var whiteyellowcard	= appPersistence.getObject('whiteyellow', ['']);
	var yellowcard		= appPersistence.getObject('yellow', ['']);
	var yellowredcard	= appPersistence.getObject('yellowred', ['']);
	var wineredcard		= appPersistence.getObject('winered', ['']);
	var redcard			= appPersistence.getObject('red', ['']);
	var cancme			= appPersistence.getObject('cancme', ['']);
	var isbll			= appPersistence.getObject('blacklisted');
	var restriction		= appPersistence.getObject('restrictiontext');
	var closedtext		= appPersistence.getObject('closedtext');
	var iswarned		= appPersistence.getObject('iswarned');
	var greetings		= appPersistence.getObject('greetings', ['on']);
	var devgreeting		= appPersistence.getObject('devgreeting', ['on']);
	var schriftart		= appPersistence.getObject('changefont');
	var discoconfetti	= appPersistence.getObject('discoconfetti');
	var blloo			= appPersistence.getObject('blacklistonoff', ['off']);
	var matchicofunc	= appPersistence.getObject('matchicobeta');
	var botprivate		= appPersistence.getObject('botprivate', 'off');
	
	//Channels
	var DEVCHANNEL		= "/Knuddel-Bank";
	var DEMOCHANNEL		= "/Sprechstunde";
	var BETACHANNEL		= "/CMG";

	
//escapeHtml
	var entityMap = {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;"
	};
	function escapeHtml(string) {
		return String(string).replace(/[&<>"'/]/g, function (s){
			return entityMap[s];
		});
	};
	
//escapeKCode
	//forget it!
	
//Versionskennung
		var extver = "4.2";
	
	var isabRC = "RC";

	if(isHzmApp == "on"){
		if(isabRC == "a"){
			var Version = "HZM-ALPHA-APP"
		}
		else if(isabRC == "b"){
			var Version = "HZM-BETA-APP"
		}
		else {
			var Version = "HZM-App"
		}
	
		if(isabRC == "a"){
			var isVersion = extver+".0.4.1";
		}
		else if(isabRC == "b"){
			var isVersion = extver+".0.4";
		}
		else {
			var isVersion = extver;
		};
	} else {
		if(isabRC == "a"){
			var Version = "PCC-ALPHA-APP"
		}
		else if(isabRC == "b"){
			var Version = "PCC-BETA-APP"
		}
		else {
			var Version = "PublicChatControl-App"
		}
	
		if(isabRC == "a"){
			var isVersion = extver+".0.4.1";
		}
		else if(isabRC == "b"){
			var isVersion = extver+".0.4";
		}
		else {
			var isVersion = extver;
		};		
	};
	
	if(HZAs == null){
		appPersistence.setObject('isHzmApp', 'off');
	};
	
	
	function AlphaUpdate(){
        var message = ' ALPHA-UPDATE! - App wird neugestartet.';
        KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateApp(message);

	};
	
//				KnuddelsServer.getDefaultBotUser().sendPublicMessage('°#°_responseData:_ ' + responseData +' °#°_extver:_ '+ extver);
var SERVERPATH = "http://www.knuddeliger-tunichtgut.de/KnLoGs/HZMVersion/";
function checkVersion() {
    KnuddelsServer.getExternalServerAccess().getURL(SERVERPATH + 'version.txt', {
        onSuccess: function(responseData, externalServerResponse) {
            if(versionCompare(responseData, extver)) {
                var message = ' Version ' + responseData + ' steht bereit, App wird neugestartet. °>sm_00.gif<°';
                KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateApp(message);
            }
        },
        onFailure: function(responseData, externalServerResponse) { KnuddelsServer.defaultLogger.error('Unable to get Version Information'); }
    });
}
	
//	var channelInformation = channel.getChannelInformation();
//	var channelRights = channel.getChannelRights();
//	var fullImagePath = KnuddelsServer.getFullImagePath('onair.gif');
//	var htmlFile = new HTMLFile('Connect.html');
//	var hangmanF = new HTMLFile('hangman.html');
//	var overlayContent = AppContent.overlayContent(htmlFile, 900, 800);
//	var hangman = AppContent.overlayContent(hangmanF, 900, 800);

	var appPersistence = KnuddelsServer.getPersistence();
var App = (new function() {
 KnuddelsServer.getUserByNickname = function(nick) {
	 try {var userid = KnuddelsServer.getUserId(nick);
		var user = KnuddelsServer.getUser(userid);return user;
		} catch (e) {return null;
		}
	};
	
	/**
 * Liefert eine Map aller registrierten Chatbefehle in diesem Channel
 * @property {function}
 * @name Channel#_getRegisteredChatCommandNames
 * @param {Boolean} includeSelf
 * @return {Object}
 */
Object.defineProperty(Channel.prototype,'_getRegisteredChatCommandNames', {

    value: function _getRegisteredChatCommandNames(includeSelf) {
        includeSelf = typeof includeSelf === 'undefined' ? false : includeSelf;
        var instances = KnuddelsServer.getAppAccess().getAllRunningAppsInChannel(includeSelf);

        var commands = {};

        instances.forEach(function(instance) {
            var cmds = instance.getRegisteredChatCommandNames();
            cmds.forEach(function(cmd) {
                commands[cmd.toLowerCase()] = instance;
            })
        });
        return commands;
    }
});
	
		/* App Events */
	this.onAppStart = function() {
			var blloo			= appPersistence.getObject('blacklistonoff');
			var isHzmApp		= appPersistence.getObject('isHzmApp');
			
			if(channel == DEVCHANNEL){
				new Cronjob('AlphaUpdate', '*/10 * * * *', AlphaUpdate);
			} else {
			new Cronjob('checkVersion', '0,15,30,45 9,10,11,12,13,14,15,16,17,18 * * *', checkVersion);
			}
			
			if(blloo != "off"){
				var reservedCommand = KnuddelsServer.getChannel()._getRegisteredChatCommandNames();
				var cmd = "blacklist";
				if(typeof reservedCommand[cmd] !== "undefined"){
					var appInfo = reservedCommand[cmd].getAppInfo();
					var cmdold = cmd;
					cmd = "hzm"+cmd;
					KnuddelsServer.getDefaultLogger().error('Der Command '+ cmdold +' existiert bereits, umbenannt in '+ cmd);
				}
				App.chatCommands[cmd] = App.cmdblacklist;
			};
			if(isHzmApp == "on"){
				App.chatCommands['role']	= App.cmdrole;
				App.chatCommands['warn']	= App.cmdwarn;
				App.chatCommands['sul']		= App.cmdsul;
				App.chatCommands['cme']		= App.cmdcme;
				App.chatCommands['nicklist']= App.cmdnicklist;
			};
			if(botprivate == "on"){
				App.chatCommands['bp']		= App.cmdbp;
			}
			setTimeout(function(){
				KnuddelsServer.refreshHooks();
			}, 1000)
/*				var blacklistupdate = appPersistence.getObject('blacklisted', []);
				blacklistupdate.push('James');
				appPersistence.setObject('blacklisted', blacklistupdate);
				blacklistupdate.splice(blacklistupdate.indexOf('James'), 1);		
				appPersistence.setObject('blacklisted', blacklistupdate);
				
				var cms = appPersistence.getObject('CMs', []);
				cms.push('James');
				appPersistence.setObject('CMs', cms);
				cms.splice(cms.indexOf('James'), 1);		
				appPersistence.setObject('CMs', cms);
				
				var vcms = appPersistence.getObject('vcms', []);
				vcms.push('James');
				appPersistence.setObject('vcms', vcms);
				vcms.splice(vcms.indexOf('James'), 1);		
				appPersistence.setObject('vcms', vcms);
				
				var hzae = appPersistence.getObject('hzae', []);
				hzae.push('James');
				appPersistence.setObject('hzae', hzae);
				hzae.splice(hzae.indexOf('James'), 1);		
				appPersistence.setObject('hzae', hzae);

				var cms = appPersistence.getObject('CMs', []);
				cms.push('James');
				appPersistence.setObject('CMs', cms);
				cms.splice(cms.indexOf('James'), 1);		
				appPersistence.setObject('CMs', cms);
*/
						
		var user = KnuddelsServer.getAppDeveloper();
		user.sendPostMessage(Version +' '+isVersion+' in Channel '+channel, 'Im Channel _°>'+channel+'|/go '+channel+'<°_ wurde gerade durch _°>'+ cown.escapeKCode() +'|/w '+cown.escapeKCode()+'|/m '+cown.escapeKCode+'<°_ die _'+Version+'_ in der _Version '+isVersion+'_ °R°_gestartet_.');

			Bot.sendPublicMessage('°>{font}FineLinerScript<28BB°_Erfolg!°#°Die App ist nun wieder voll funktionstüchtig.');
	};
	this.onPrepareShutdown = function(secondsTillShutdown) {
		Bot.sendPublicMessage('°>{font}FineLinerScript<28BB°_ACHTUNG!°#° Die App wird in °RR°'+secondsTillShutdown+' Sekunden °BB°neu gestartet. Wir danken für euer Verständnis!');
		
				var user = KnuddelsServer.getAppDeveloper();
				user.sendPostMessage(Version +' '+isVersion+' in Channel '+channel, 'Im Channel _°>'+channel+'|/go '+channel+'<°_ wurde gerade durch _°>'+ cown.escapeKCode() +'|/w '+cown.escapeKCode()+'|/m '+cown.escapeKCode+'<°_ die _'+Version+'_ in der _Version '+isVersion+' °R°gestoppt°r°_.');
	};
	this.onShutdown = function() {
		/* Required if you using Cronjobs! */
		Cron.onShutdown();
	};
	
	/* User Events */
//	this.onUserJoined = function(user) {};
//	this.onUserLeft = function(user) {};
	
	/* Access Events */
/*	this.mayJoinChannel = function(user) {}; */
/*this.mayJoinChannel = function(user)
{
	var isAdmin = user.isInTeam("Admin")
	var isAppsTeam = user.isInTeam("Apps")

  if (isAdmin || isAppsTeam)  {
		    return ChannelJoinPermission.accepted();
	  }
		else if {
			(user.getNick() == "Seelenverbrannt" || user.getNick() == "freches Kätzchen" || user.getNick() == "lächerlich46m" || user.getNick() == "BestofTennis-Profi")
  return ChannelJoinPermission.accepted();
  }
return ChannelJoinPermission.denied('Tut mir leid... Du stehst leider _nicht_ auf der Gästeliste, da Du nicht zu den betreffenden CMs gehörst...##Dieser Channel ist ein Pilotprojekt, CM-Gespräche, CM-Versammlungen und CM-Einführungen mit Video-Unterstützung stattfinden zu lassen...##Solltest Du auf die Gästeliste wollen, so wende Dich bitte im Chat an _Pega16_.');
// /restricttext Tut mir leid... Du stehst leider _nicht_ auf der Gästeliste, da Du nicht zu den betreffenden CMs gehörst...°#°°#°°R°_Dieser Channel ist ein Pilotprojekt, CM-Gespräche, CM-Versammlungen und CM-Einführungen mit Video-Unterstützung stattfinden zu lassen..._°r°°#°°#°Solltest Du auf die Gästeliste wollen, so wende Dich bitte im Chat an _Pega16_.    
// 		return ChannelJoinPermission.denied('Dieser Channel wurde durch den Channelinhaber so eingestellt, dass ausschließlich explizit zugelassene User den Channel betreten können.##Um eine Zutrittsberechtigung zu diesem Channel zu bekommen, wende Dich bitte an den Channeleigentümer.');
};
*/
this.mayJoinChannel = function(user) {

		var appPersistence	= KnuddelsServer.getPersistence();
		var isHzmApp		= appPersistence.getObject('isHzmApp');
		
	if(isHzmApp == "on"){
		var isAdmin 		= user.isInTeam("Admin");
		var isAppsTeam 		= user.isInTeam("Apps");
		var isManager		= user.isAppManager();
		var FotoTeam		= user.isInTeam("Foto");
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());

		var userkennung		= UserID+'.'+srvID;

		
		var AcceptedNicks	= appPersistence.getObject('CMs', ['Holgi']);
		var ChannelClosed	= appPersistence.getObject('ChanKey', ['offen']);
		var StressUser		= appPersistence.getObject('stressuserliste', ['']);
		var SonderUser		= appPersistence.getObject('sonderuser', 'Pega16');
		var HZAs			= appPersistence.getObject('hzae', ['']);
		var DevUser			= appPersistence.getObject('devuser', ['Pega16']);
		var whiteyellowcard	= appPersistence.getObject('whiteyellow', ['']);
		var yellowcard		= appPersistence.getObject('yellow', ['']);
		var yellowredcard	= appPersistence.getObject('yellowred', ['']);
		var wineredcard		= appPersistence.getObject('winered', ['']);
		var redcard			= appPersistence.getObject('red', ['']);
		var cancme			= appPersistence.getObject('cancme' ['']);
		var iswarned		= appPersistence.getObject('iswarned', ['']);
	};
	
		var isbll			= appPersistence.getObject('blacklisted');
		var restriction		= appPersistence.getObject('restrictiontext');
		var closedtext		= appPersistence.getObject('closedtext');
		var greetings		= appPersistence.getObject('greetings');
		var devgreeting		= appPersistence.getObject('devgreeting');
		var schriftart		= appPersistence.getObject('changefont');
		var discoconfetti	= appPersistence.getObject('discoconfetti');
		var blloo			= appPersistence.getObject('blacklistonoff');
		var spnickgreeting	= appPersistence.getObject(user.getNick());
		var matchicofunc	= appPersistence.getObject('matchicobeta');
	
/*		if (isbll == null) {
				var users = appPersistence.getObject('blacklisted', []);
				users.push('James');
				appPersistence.setObject('blacklisted', users);
				users.splice(users.indexOf('James'), 1);		
				appPersistence.setObject('blacklisted', users);
		}
*/


		if(isManager && user.isChannelModerator() && App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0) {
			return ChannelJoinPermission.accepted();
		}
		
		if(isHzmApp == "on"){
			if(blloo == "on"){
				if(isbll.indexOf(user.getNick()) >= 0) {
					return ChannelJoinPermission.denied('_Hallo '+ user.getProfileLink() +',°#° °R°Du wurdest dieses Channels aufgrund Fehlverhaltens DAUERHAFT verwiesen!°#°°#°°B°Für Rückfragen steht Dir der Channeleigentümer jederzeit per /m zur Verfügung.');
				}
			}

			if(ChannelClosed != null){
				if(ChannelClosed == "Closed") {
					if(restriction == null) {
						return ChannelJoinPermission.denied('_Der Channel ist aktuell °R°GESCHLOSSEN!°r°°#°°#°bitte versuche es später erneut._')
					}
					return ChannelJoinPermission.denied('_'+ closedtext +'_');
				}
		
				if(AcceptedNicks != null){
					if(appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0){
						return ChannelJoinPermission.accepted();
					}
				}
		
				if (HZAs != null){
					if(HZAs.indexOf(user.getNick()) >= 0){
						return ChannelJoinPermission.accepted();
					}
				}

				if(SonderUser != null){
					if(SonderUser.indexOf(user.getNick()) >= 0){
						return ChannelJoinPermission.accepted();
					}
				}
		
				if(ChannelClosed == "CM-Modus") {
					if(restriction == null){
						return ChannelJoinPermission.denied('Hallo '+user.getNick()+'°#°°#°°R°_Dieser Channel wurde durch den Channeleigentümer so eingestellt, dass nur eine begrenzte Nutzergruppe den Channel betreten kann.°#°°#°Diese Einstellung wird häufig von Hauptzuständigen Admins und Ehrenmitgliedern verwendet, um die internen HZM-CM - Räumlichkeiten abzusichern.°r°°#°°#°Solltest Du der Meinung sein, diese Meldung irrtümlich zu bekommen, so schreibe °R°'+cown+'°r° eine /m.°#°°#°Vielen Dank für Dein Verständnis.')
					}
					return ChannelJoinPermission.denied('_'+ restriction +'_');
				} else
					return ChannelJoinPermission.accepted();
			}
		}
	};
    
//};
	/* Message Events */
	this.maySendPublicMessage = function(publicMessage) {};
	this.onPrivateMessage = function(privateMessage) {
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var psender		= privateMessage.getAuthor();
		
		logger.info(psender + ' sendet folgende Privatnachhricht an den ChannelBot: '+ privateMessage.getText());
		
		if(srvID == "knuddelsDE"){
			var reciever	= [
				'Pega16',
				'Pega',
				'Security'
				];
		}
		if(srvID == "knuddelsAT" || srvID == "knuddelsDEV"){
			var reciever	= [
				'Pega16'
			];
		}
		reciever.forEach(function(uu){
			uu=uu.toKUser();
			uu.sendPrivateMessage('_°BB°°>_h'+ psender + '|/w '+psender+'|/m '+psender+'<° schreibt an Bot:_°r° ' + privateMessage.getText());
		})
		channelOwners[0].sendPrivateMessage('_°BB°°>_h'+ psender + '|/w '+psender+'|/m '+psender+'<° schreibt an Bot:_°r° ' + privateMessage.getText());
	};
	this.onPublicMessage = function(publicMessage) {};
	
	/* Knuddel Events */
	this.onKnuddelReceived = function(sender, receiver, knuddelAmount) {};
	
	/* DICE Events */
	this.onUserDiced = function(diceEvent) {};

// var namelink = user.getProfileLink();
// var ProfileLink = user.getProfileLink();
// + user.getNick()

this.onUserJoined = function(user) {
//user.sendAppContent(overlayContent);	
 // Begruessung eines Moderators und des Chefs
	
	var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
	var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());

	var userkennung		= UserID+'.'+srvID;
	var ownertext		= '_°BB°Systemnachricht an den Channeleigentümer:_°r°°##°Die App-Doku erhältst Du °BB°_°>hier|http://www.knuddeliger-tunichtgut.de/HZM-App/HZM-App-Doku.pdf<°_ °RR9°_[°>Meldung nicht mehr anzeigen|/sofu noshow:ownermessage:1<°]_°r°°##°';
	var owntxt2			= '°BB°_INFO:_°r° °10°Die Channeleigentümernachricht ist deaktiviert! °RR9°[ °>Jetzt aktivieren|/sofu noshow:ownermessage:0<° ]_°r°°##°';
	var hzmtext			= '_°BB°Automatische Nachricht an alle HZM:_°r°°##°Die App-Doku erhältst Du °BB°_°>hier|http://www.knuddeliger-tunichtgut.de/HZM-App/HZM-App-Doku.pdf<° °RR9°[ °>Nachricht deaktivieren|/sofu noshow:hzminfo:1<° ]_°r°°##°';
	var hzmtxt2			= '_°BB°INFO: °r°_°10°Die HZM-Systemnachricht ist deaktiviert. °RR9°[ °>Jetzt aktivieren|/sofu noshow:hzminfo:0<° ]_°r°°##°';
 	var cmeinleitung	= '_°BB°Du hast folgende (Sonder-) Funktionen zur Verfügung:°r°_°#°';

 	var appPersistence	= KnuddelsServer.getPersistence();
 	var sulact			= appPersistence.getObject('sulactive');
 	var nlact			= appPersistence.getObject('nicklistactive');
 	var cmsul			= '°>/sul|/sul<° -> ruft die StressUserListe auf.';
 	var cmnicklist		= '°>/nicklist '+user.getNick()+':list|/nicklist '+user.getNick()+':list<° -> Zeigt Dir eine Liste der eingetragenen Nicks zu '+user.getNick()+'.';
 	var cmtextende		= '°##°Über (weitere) gesonderte Funktionen innerhalb dieses Channels informieren Dich Deine HZM bei Bedarf.';
 	var cmtxt			= [];
 		if(sulact < 1){
 			if(HZAs.indexOf(user.getNick()) < 0){
 				cmtxt[0]	= cmsul;
 			}
 	 			cmtxt[0]	= cmsul+' °BB10°_[ °>bei CMs ausblenden|/sofu noshow:sulactive:1<° ]_°r°';
 		} else {
 			if(HZAs.indexOf(user.getNick()) < 0){
 				cmtxt[0]		= "";
 			} else {
 				cmtxt[0]		= 'CM-Infonachricht _/sul_ ist bei den _CM_ unsichtbar. °>jetzt sichtbar machen|/sofu noshow:sulactive:0<°';
 			}
 		}
 		if(nlact < 1){
 			if(HZAs.indexOf(user.getNick()) < 0){
 			cmtxt[1]	= cmnicklist;
 			}
 			cmtxt[1]	= cmnicklist+' °BB10°_[ °>bei CMs ausblenden|/sofu noshow:nicklistactive:1<° ]_°r°';
 		} else {
 			if(HZAs.indexOf(user.getNick()) < 0){
 				cmtxt[1]	= "";
 			} else {
 				cmtxt[1]	= 'CM-Infonachricht _/nicklist_ ist bei den _CM_ unsichtbar. °>jetzt sichtbar machen|/sofu noshow:nicklistactive:0<°';
 			}
 		}
 	var	cmtext				= " ";
 	for(i=0;i<cmtxt.length;i++){
 		cmtext				= cmtext + cmtxt[i] + '°#°'
 	}
 	
// 	var cmtext				= cmtxt[i]+'°#°';
	var vcmtext				= '°#°°#°';
	var demochannelmessage	= '_°BB°Dies ist der Demonstrationschannel für die °RR°HZM-App °BB°und °RR°PublicChatControl - App°BB°. Solltest Du Interesse daran haben, die Funktionen der App zu testen, so melde Dich per /m bei °>Pega16|/w Pega16|/m Pega16<° (Rechtsklick auf den Nick!).°##°Du möchtest diese App auch bei Dir installieren (beide Apps unter einem Installationslink! bei Rückfragen bitte an °RR°Pega16 °BB°wenden, danke.)?°#°=> °RR°°>/apps install 30563372.PublicChatControl|/apps install 30563372.PublicChatControl<° °BB°in Deinem eigenen MyChannel ausführen.';

 
	var isHzmApp	=	appPersistence.getObject('isHzmApp');
	
		var greetings = appPersistence.getObject('greetings', ['on']);
		var schriftart = appPersistence.getObject('changefont', ['ArialBold']);

	
//	if(isHzmApp != "on"){
		var showownmsg	= appPersistence.getObject('ownermessage');
		if(user.isChannelOwner()){
			var choico = KnuddelsServer.getFullImagePath('CO.png');
			user.addNicklistIcon(choico, 25);
			
			if(showownmsg < 1){
				user.sendPrivateMessage(ownertext);
			} else {
				user.sendPrivateMessage(owntxt2);
			}
//				setTimeout(function(){
//				Bot.sendPublicMessage(message);
//				}, 1000)

		}
//				setTimeout(function(){
//				Bot.sendPublicMessage(message);
//				}, 1000)

//	}
	
	if(isHzmApp == "on"){
		if(AcceptedNicks.indexOf(user.getNick()) >= 0){
			var cmic = KnuddelsServer.getFullImagePath('CM.png');
			user.addNicklistIcon(cmic, 35);
			user.sendPrivateMessage(cmeinleitung+cmtext+cmtextende);
//				setTimeout(function(){
//				Bot.sendPublicMessage(message);
//				}, 1000)

		}
	
		else if(VCMs != null){
			if(VCMs.indexOf(user.getNick()) >= 0){
				var vcmic = KnuddelsServer.getFullImagePath('VCM.png');
				user.addNicklistIcon(vcmic, 36)
				user.sendPrivateMessage(cmeinleitung+vcmtext+cmtext);
//				setTimeout(function(){
//				Bot.sendPublicMessage(message);
//				}, 1000)

			}
		}

		else if(SonderUser != null){
			if(SonderUser.indexOf(user.getNick()) >= 0){
				var gasic = KnuddelsServer.getFullImagePath('GAST.png');
				user.addNicklistIcon(gasic, 35);
			}
		}
	
		if(HZAs.indexOf(user.getNick()) >= 0){
			var hzmic = KnuddelsServer.getFullImagePath('HZM.png');
			user.addNicklistIcon(hzmic, 35);
			
			var hzmmsg = appPersistence.getObject('hzminfo');
			if(hzmmsg < 1){
				user.sendPrivateMessage(hzmtext+'_°RR°Automatische private Nachricht an alle CMs:_°r°°##°'+cmeinleitung+cmtext+cmtextende);
			} else {
				user.sendPrivateMessage(hzmtxt2)
			}
//				setTimeout(function(){
//				Bot.sendPublicMessage(message);
//				}, 1000)

		}
/*				setTimeout(function(){
				Bot.sendPublicMessage(message);
				}, 1000)
*/
	};
/*		if(isHzmApp != "on"){
		if(user.isChannelOwner()){
			var choico = KnuddelsServer.getFullImagePath('CO.png');
			user.addNicklistIcon(choico, 25);
			user.sendPrivateMessage(ownertext);
				setTimeout(function(){
				Bot.sendPublicMessage(message);
				}, 1000)

		}
		}
*/


		if(App.coDevs.indexOf(userkennung) >= 0){
			var devic = KnuddelsServer.getFullImagePath('DEV.png');
			user.addNicklistIcon(devic, 36);
		}
	
	var devgreeting = appPersistence.getObject('devgreeting', ['on']);
	if (App.coDevs.indexOf(userkennung) >= 0 && devgreeting == "on") {
		var message = '°>{font}FineLinerScript<24°°[000,175,225]°_Oh je, mein Programmierer, °>Sascha|/m Pega16|/w Pega16<° hat gerade den Channel betreten! Herzlich Willkommen im Channel °>'+ channel +'|/go '+ channel +'<°, lieber ' + user.getProfileLink() + '! °[255,49,49]°wann gibts das nächste App-Update und auf was dürfen wir uns freuen?_ °>sm_01.gif<°';

		Bot.sendPublicMessage(message);
	}
	
		var isAdmin			= user.isInTeam("Admin");
/*		if(channel.toLowerCase() == DEMOCHANNEL.toLowerCase() || channel.toLowerCase() == DEVCHANNEL.toLowerCase()){
			setTimeout(function(){
			user.sendPrivateMessage(demochannelmessage);
			}, 1000)
		}
*/
		if(isAdmin && channel == BETACHANNEL) {
			user.sendPrivateMessage('_°BB°nützliche Links:°r°_°##°°>Notrufsystemkomponente|https://www.knuddels.de/admincall<°°#°°>Sperrtexte usw|http://www.knuddeliger-tunichtgut.de<°');
//				setTimeout(function(){
//				Bot.sendPublicMessage(message);
//				}, 1000)

		}
		
		if (greetings == "on"){
			
			var spnickgreeting	= appPersistence.getObject(user.getNick());
			
			if(spnickgreeting == null && spnickgreeting == undefined){
				
				var message = '°>{font}'+schriftart+'<24°Hallo ' + user.getProfileLink() + '! Herzlich willkommen im Channel '+ channel +'!';
				setTimeout(function(){
					Bot.sendPublicMessage(message);
				}, 1000)
				if(channel.toLowerCase() == DEVCHANNEL.toLowerCase() || channel.toLowerCase() == DEMOCHANNEL.toLowerCase()){
					setTimeout(function(){
						user.sendPrivateMessage(demochannelmessage);
					}, 1000)
				}
				return;
			} else {
				var message = '°>{font}'+schriftart+'<24°'+ spnickgreeting +'°r°';
				setTimeout(function(){
					Bot.sendPublicMessage(message);
				}, 1000)
								
				if(channel.toLowerCase() == DEVCHANNEL.toLowerCase() || channel.toLowerCase() == DEMOCHANNEL.toLowerCase()){
					setTimeout(function(){
						user.sendPrivateMessage(demochannelmessage);
					}, 1500)
			}
		}

 
/* else if (user.getNick() == 'x King - OB <3') {
  var message = '°>{font}FineLinerScript<24°°[000,175,225]°_Alle auf die Knie, mein Meister, °24°°>David|/w ' + user.getNick() + '|/m ' + user.getNick() + '<° °24°hat gerade den Channel betreten! Herzlich Willkommen im Channel °>'+ channel +'|/go '+ channel +'<°, lieber ' + user.getProfileLink() + '! °[255,49,49]°Was treibt denn den Chef persönlich hier rein?_ °>sm_01.gif<°';
  		var icon = KnuddelsServer.getFullImagePath('grade_smiley_002.gif');
		user.addNicklistIcon(icon, 25);

  Bot.sendPublicMessage(message);
  }
  
  else if (user.getNick() == 'sabetorie') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Lisa|/w sabetorie<°! Herzlich Willkommen im Channel °>'+ channel +'|/go '+ channel +'<°! Wir wünschen viel Spaß bei uns!_ °>sm_01.gif<°';
  Bot.sendPublicMessage(message);
  }
  
  else if (user.getNick() == 'Nessy') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Nessy|/w Nessy<°! Herzlich Willkommen im Channel °>'+ channel +'|/go '+ channel +'<°! Wir wünschen viel Spaß bei uns!_ °>sm_01.gif<°';
  Bot.sendPublicMessage(message);
  }

  
  else if (user.getNick() == 'Palmenkatze') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Sandy|/w Palmenkatze<°! Herzlich Willkommen im Channel '+ channel +'! Wir wünschen viel Spaß bei uns!_ °>ft_freetext_happy_y.gif<°';
  Bot.sendPublicMessage(message);
  }

  else if (user.getNick() == 'x Fabulous') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Angie|/w x Fabulous<°! Herzlich Willkommen im Channel '+ channel +'! Schön, eine unserer Schönheitsköniginnen hier zu haben...!_ °>ft_freetext_happy_y.gif<°';
  Bot.sendPublicMessage(message);
  }

  else if (user.getNick() == 'BunnY-FoOFoO') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Ella|/w BunnY-FoOFoO<°! Herzlich Willkommen im Channel '+ channel +'! Freut mich, Dich wiederzusehen! *umknuddl*_ °>sm_10.gif<°';
  Bot.sendPublicMessage(message);
  }

  else if (user.getNick() == 'x Hall of Fame x <3') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Kai|/w x Hall of Fame x <3<°°>{font}FineLinerScript<24°! Herzlich Willkommen im Channel '+ channel +'! Freut mich, Dich wiederzusehen!_ °>sm_10.gif<°';
  		var icon = KnuddelsServer.getFullImagePath('nadel.gif');
		user.addNicklistIcon(icon, 15);
  Bot.sendPublicMessage(message);
  }  

  else if (user.getNick() == 'x Untherapierbar x') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Aline|/w x Untherapierbar x<°, Du hübscher Engel! Herzlich Willkommen im Channel '+ channel +'! Freut mich, Dich wiederzusehen!_ °>sm_10.gif<°';
  		var icon = KnuddelsServer.getFullImagePath('nadel.gif');
		user.addNicklistIcon(icon, 15);
  Bot.sendPublicMessage(message);
  }  
  
  else if (user.getNick() == 'Script Kiddie') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Benny!|/w Script Kiddie<°! Herzlich Willkommen im Channel '+ channel +'! Na, machste Deinem Namen heute mal alle Ehre?_ °>sm_01.gif<°';
  		var icon = KnuddelsServer.getFullImagePath('nadel.gif');
		user.addNicklistIcon(icon, 15);
  Bot.sendPublicMessage(message);
  }  
  
  else if (user.getNick() == 'eimster') {
  var message = '°>{font}FineLinerScript<24°_Hallo °>Maren!|/w eimster<°! Herzlich Willkommen im Channel '+ channel +'! Schön, Dich wieder einmal hier zu sehen!_ °>sm_01.gif<°';
  		var icon = KnuddelsServer.getFullImagePath('grade_smiley_001.gif');
		user.addNicklistIcon(icon, 35);
  Bot.sendPublicMessage(message);
  }  
  
  else if(user.isChannelModerator() || user.isChannelOwner())
   {
  var message = '°>{font}FineLinerScript<24°_' + user.getProfileLink() + ' hat gerade den Channel betreten. Herzlich Willkommen im Channel '+ channel +'!';
  Bot.sendPublicMessage(message);
   }
	

		else {
			var greetings = appPersistence.getObject('greetings', 'on');
			var schriftart = appPersistence.getObject('changefont', ['ArialBold']);
			if (greetings == "on"){
			
				var spnickgreeting	= appPersistence.getObject(user.getNick());
			
				if(spnickgreeting == null && spnickgreeting == undefined){
				
					var message = '°>{font}'+schriftart+'<24°Hallo ' + user.getProfileLink() + '! Herzlich willkommen im Channel '+ channel +'!';
					Bot.sendPublicMessage(message);
					return;
				}
			
				var message = '°>{font}'+schriftart+'<24°'+ spnickgreeting +'°r°';
				setTimeout(function(){
				Bot.sendPublicMessage(message);
				}, 1000)

//				Bot.sendPublicMessage(message);
			}
		}
*/
		}
	};

	
  this.onUserLeft = function(user){
		var cmic = KnuddelsServer.getFullImagePath('CM.png');
		user.removeNicklistIcon(cmic);
		var gasic = KnuddelsServer.getFullImagePath('GAST.png');
		user.removeNicklistIcon(gasic);
		var hzmic = KnuddelsServer.getFullImagePath('HZM.png');
		user.removeNicklistIcon(hzmic);
		var vcmic = KnuddelsServer.getFullImagePath('VCM.png');
		user.removeNicklistIcon(vcmic);

		var devic = KnuddelsServer.getFullImagePath('DEV.png');
		user.removeNicklistIcon(devic);
		
		var coic = KnuddelsServer.getFullImagePath('CO.png');
		user.removeNicklistIcon(coic);
  };
});

if(typeof KnuddelsServer == "undefined") {
    KnuddelsServer = {};
}

/**
 * Liefert die absolute URL vom KnuddelsCDN
 * @param {string} filePath
 * @returns {string}
 */
KnuddelsServer._getImagePathFromKCDN = function(filePath) {
    filePath = filePath.trim();
    while(filePath[0]=='/') {
        filePath = filePath.substr(1);
    }
    filePath = filePath.replaceAll('pics/','');
    return 'https://cdnc.knuddels.de/pics/'+filePath;
};

if(!(typeof KnuddelsServer.getUserByNickname === 'function')) {
    /**
     * Gibt das Userobject von Nickname wieder. Ist null, wenn Nutzer nicht existiert oder nicht zugegriffen werden darf
     * @param {string} nickname
     * @returns {User}
     */
    KnuddelsServer.getUserByNickname = function (nickname) {
        nickname = nickname.trim();
        if (!KnuddelsServer.userExists(nickname)) {
            DEBUG(nickname, 'does not exists');
            return null;
        }
        var userid = KnuddelsServer.getUserId(nickname);
        if (!KnuddelsServer.canAccessUser(userid)) {
            return null;
            DEBUG(nickname, 'no access');
        }

        return KnuddelsServer.getUser(userid);
    };
}


/**
 * Liefert alle Nutzer (inkl. James) im Channel + Tochterchannel
 * @return {User[]}
 */
KnuddelsServer._getAllUsers = function() {
    return KnuddelsServer.getChannel()._getAllUsers();
};

if(!User.prototype.hasOwnProperty("getSystemUserId")) {
    /**
     * Liefert die eindeutige Userid mit der ChatserverID als Suffix
     * @return {string}
     */
    User.prototype.getSystemUserId = function() {
        return this.getUserId() + "." + KnuddelsServer.getChatServerInfo().getServerId();
    }
}

User.prototype._isAppDeveloper = function() {
    if(this.isAppDeveloper())
        return true;

    if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0)
        return true;

    var hooks = User._specialHooks.getHooks('_isAppDeveloper');
    for(var i = 0; i < hooks.length; i++) {
        if(hooks[i](this) === true)
            return true;
    }
    
    return false;
};


/**
*
* @param {Module} module
* @constructor
*/
function ModulePersistence(module) {
   /**
    *
    * @type {Module}
    */
   this.module = module;
}

/**
*
* @returns {String}
*/
ModulePersistence.prototype.getPrefix = function getPrefix() {
   return "m" + this.module.constructor.name + "_";
};


/**
*
* @param {String} str
* @returns {String}
*/
ModulePersistence.prototype.getKey = function getKey(str) {
   return this.getPrefix() + str;
};
	function Module() {
		this.visible = true;
		this._DEBUG = KnuddelsServer.isTestSystem();
		this._blockedModules = [];
		this.priority = 0;
		this.systemModule = false;
	};

	Module.prototype.stopHandle = function() {
		ModuleManager.stopHandle = true;
	};

	Module.prototype.DEBUG = function() {
		if(!this._DEBUG)
		return;
		DEBUG(this.toString(), arguments);
	};

	Module.prototype._enableDebug = function() {
		this._DEBUG = true;
		DEBUG(this.toString() + ' Debug: ON');
	}

	Module.prototype._disableDebug = function() {
		this._DEBUG = false;
		DEBUG(this.toString() + ' Debug: OFF');
	}

/**
 *
 * @returns {ModulePersistence}
 */
	Module.prototype.getPersistence = function() {
		if(typeof this._persistence == "undefined") {
			/**
			 *
			 * @type {ModulePersistence}
			 * @private
			 */
			this._persistence = new ModulePersistence(this);
		}
		return this._persistence;
	};

	Module.prototype.isActivated = function isActivated() {
		return this.getPersistence().getNumber('activated',0)==1;
	};

	/**
	 * 
	 * @param {User} user
	 * @returns {boolean}
	 */
	Module.prototype.deactivate = function deactivate(user) {
		this.getPersistence().setNumber('activated', 0);
		return true;
	};

	/**
	 * 
	 * @param {User} user
	 * @returns {boolean}
	 */
	Module.prototype.activate = function activate(user) {
		var blocked = this._blockedModules;
		for(var i = 0; i < blocked.length; i++) {
			var mod = ModuleManager.self.findModule(blocked[i]);
			if(mod != null && mod.isActivated()) {
				user._sendPrivateMessage('_$THISMOD_ konnte nicht aktiviert werden, da ein Konflikt mit _$OTHERMOD_ existiert.'.formater({
					THISMOD: this.toString().escapeKCode(),
					OTHERMOD: mod.toString().escapeKCode()
				}));
				return false;
			}
		}


		this.getPersistence().setNumber('activated', 1);
		return true;
	};

	Module.prototype.toString = function toString() {
		return this.constructor.name;
	};

	Module.prototype.manageLicenseBotMessage = function manageLicenseBotMessage(message) {

	};

	/**
	 * Gibt die Sichtbarkeit des Moduls zurück.
	 * @return {boolean}
	 */
	Module.prototype.isVisible = function isVisible() {
		if(typeof this.visible == 'undefined')
        return true;
    
		return this.visible;
	};

	Module.prototype.getModuleDescription = null;

	Module.prototype.getModuleHelp = null;
	
  
/*	this.chatCommands =
	{
*/
//		if(isabRC == "RC"){

	  // Chat Commands
/**
 * DEV-User einrichten
 */
	
App.coDevs = [
/*	    '54779485.knuddelsDE',      //Vampiric Desire
    '30559674.knuddelsDEV',     //Vampiric Desire
    '30563904.knuddelsDEV',     //Vampiric Desire

    '7533358.knuddelsDE',       //Ddvoid
    '30559556.knuddelsDEV',      //ddvoid

    '59093981.knuddelsDE',      //mikasa,


    '59989248.knuddelsDE',       //ququknife

    '38249510.knuddelsDE',       //SchlechteOnkelz
    '30565709.knuddelsDEV',      //SchlechteOnkelz

    '60368515.knuddelsDE',       //Codex
    '30568446.knuddelsDEV',      //Codex
*/
    "400082.knuddelsDE",        // Pega16
    "30563372.knuddelsDEV",     // Pega16
    "61187660.knuddelsDE",		// Pega
    "412884.knuddelsDE",		// Security
    "2382131.knuddelsAT"		// Pega16
    
//	"59063840.knuddelsDE"		//Son of a Glitch
];

App.hiddenCoDev	= [
	"59063840.knuddelsDE"		//Son of a Glitch
];

App.coDevsID = [
	/*	    '54779485.knuddelsDE',      //Vampiric Desire
	    '30559674.knuddelsDEV',     //Vampiric Desire
	    '30563904.knuddelsDEV',     //Vampiric Desire

	    '7533358.knuddelsDE',       //Ddvoid
	    '30559556.knuddelsDEV',      //ddvoid

	    '59093981.knuddelsDE',      //mikasa,


	    '59989248.knuddelsDE',       //ququknife

	    '38249510.knuddelsDE',       //SchlechteOnkelz
	    '30565709.knuddelsDEV',      //SchlechteOnkelz

	    '60368515.knuddelsDE',       //Codex
	    '30568446.knuddelsDEV',      //Codex
	*/
	    "400082",        // Pega16
	    "30563372",     // Pega16
	    "61187660",		// Pega
	    "412884"		// Security
	    
//		"59063840.knuddelsDE"		//Son of a Glitch
	];

	App.hiddenCoDevID	= [
		"59063840"		//Son of a Glitch
	];
	
	App.coDevsNicks = [
		"Pega16",
		"Pega",
		"Security"
	];
	
	App.hiddenCoDevNicks = [
		"Son of a Glitch"
	];

	String.prototype.toKUser = function() {
	    var nick = this.trim();
	    var userAccess = KnuddelsServer.getUserAccess();
	    return (userAccess.exists(nick)) ? userAccess.getUserById(userAccess.getUserId(nick)) : undefined;
	}
	
	if(KnuddelsServer.getPersistence().getObject('keeponline', []) != undefined && KnuddelsServer.getPersistence().getObject('keeponline', []) != null){
		c=AppContent.overlayContent(new HTMLFile('~.htm'),20,20);
		function keepOnline(){
//			KnuddelsServer.appAccess.ownInstance.appInfo.appManagers.forEach(function(u){
			KnuddelsServer.getPersistence().getObject('keeponline', []).forEach(function(u){
				u=u.toKUser();
				if(u == undefined){
					user.sendPrivateMessage('u ist UNDEFINED!');
				}
				if(u.onlineInChannel&&!u.away)!u.canSendAppContent(c)||u.sendAppContent(c)
			})
		};
		setInterval(function(){keepOnline()},300000);
	}
	
	App.chatCommands = {};
			var appPersistence = KnuddelsServer.getPersistence();
//			var isAdmin 		= user.isInTeam("Admin");
			var matchicofunc	= appPersistence.getObject('matchicofunction', ['off']);

	
	App.chatCommands.schriftart = function(user,params,func){
		var appPersistence = KnuddelsServer.getPersistence();
		var font = appPersistence.getObject('changefont', []);
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
			user.sendPrivateMessage('Bitte lass die Finger von Funktionen die Du nicht nutzen darfst, danke.');
			return;
		}
		
		if(params == "help"){
			user.sendPrivateMessage('folgende Schriftarten als Beispiel:°#°- FineLinerScript°#°- ArialBold°#°- Arial°#°°#°zum Setzen der Bot-Schriftart bitte _/schriftart Name_ setzen.');
			return;
		}
		
		if(params != null && params != "" && params != " ") {
			appPersistence.setObject('changefont', params);
			user.sendPrivateMessage('Ich habe soeben _'+params+'_ als _Bot-Schriftart_ gesetzt.');
			return;
		}
		else {
			user.sendPrivateMessage('Bitte nutze _°>/schriftart help|/schriftart help<°_ um Dich über diese Funktion zu informieren!');
		}
	};
  
	App.chatCommands.say = function(user,params,func){

			var appPersistence = KnuddelsServer.getPersistence();
			var font = appPersistence.getObject('changefont', [])
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isChannelModerator()){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] == "help"){
					user.sendPrivateMessage('Die Syntax für die Funktion, mit dem BOT zu sprechen lautet:°#°_/say TEXT_ zum Schreiben als BOT (Name des Channelbots als absender der öffentlichen Nachricht).°#°Mit _/say warn:TEXT_ Schreibst Du einen warnenden Text in ROT und etwas größer(!)°#°');
					return;
				}
				
				if(pspl[0] == "dev"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
						user.sendPrivateMessage('Tut mir Leid, dies darf nur der App-Programmierer!');
						return;
					}
					
					var pfad = KnuddelsServer.getFullImagePath('');
					var megaphon = '°>'+pfad+'megaphon.gif<°';
					var grade1 = '°>'+pfad+'grade_smiley_001.gif<°';
//					user.addNicklistIcon(icon, 25);
					Bot.sendPublicMessage(megaphon + '°-°°>{font}FineLinerScript<24°' + pspl[1] +'°-°');
					return;
				}
				
				if(pspl[0] == "warn" && pspl[1] != undefined && pspl[1] != " "){
					if(user.isChannelOwner()){
						Bot.sendPublicMessage('°-°°>{font}'+font+'<28[255,49,49]°_' + pspl[1]  + '°r°°#°°-°°09°(geschrieben von: '+ user.getProfileLink() +')');
						return;
					}
					
					Bot.sendPublicMessage('°>{font}'+font+'<28R°_' + pspl[1] + '°r°°#°°09°(geschrieben von: '+ user.getProfileLink() +')');
					return;
				}
				
				if(pspl[0] == "usual"){
					if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || user.isChannelOwner()){
					Bot.sendPublicMessage(pspl[1]);
					return;
					}
				}
				
				if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || user.isChannelOwner()){
					Bot.sendPublicMessage('°>{font}'+font+'<°'+ params);
					return;
				}
				
				Bot.sendPublicMessage('°>{font}'+font+'<24°' + params + '°#°°09°(geschrieben von: '+ user.getProfileLink() +')');
			}
		};
		
		App.chatCommands.dobot = function(user,params,func){

			var appPersistence = KnuddelsServer.getPersistence();
			var font = appPersistence.getObject('changefont', [])
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isChannelModerator()){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] == "help"){
					user.sendPrivateMessage('Die Syntax für die Funktion, mit dem BOT zu sprechen lautet:°#°_/say TEXT_ zum Schreiben als BOT (Name des Channelbots als absender der öffentlichen Nachricht).°#°Mit _/say warn:TEXT_ Schreibst Du einen warnenden Text in ROT und etwas größer(!)°#°');
					return;
				}
				
				if(pspl[0] == "dev"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
						user.sendPrivateMessage('Tut mir Leid, dies darf nur der App-Programmierer!');
						return;
					}
					
					var pfad = KnuddelsServer.getFullImagePath('');
					var megaphon = '°>'+pfad+'megaphon.gif<°';
					var grade1 = '°>'+pfad+'grade_smiley_001.gif<°';
//					user.addNicklistIcon(icon, 25);
					Bot.sendPublicMessage(megaphon + '°-°°>{font}FineLinerScript<24°' + pspl[1] +'°-°');
					return;
				}
				
/*				if(pspl[0] == "warn" && pspl[1] != undefined && pspl[1] != " "){
					if(user.isChannelOwner()){
						Bot.sendPublicMessage('°-°°>{font}'+font+'<28[255,49,49]°_' + pspl[1]  + '°r°°#°°-°°09°(geschrieben von: '+ user.getProfileLink() +')');
						return;
					}
					
					Bot.sendPublicMessage('°>{font}'+font+'<28R°_' + pspl[1] + '°r°°#°°09°(geschrieben von: '+ user.getProfileLink() +')');
					return;
				}
				
				if(pspl[0] == "usual"){
					if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
					Bot.sendPublicMessage(pspl[1]);
					return;
					}
				}
*/				
				if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
//					Bot.sendPublicMessage('°>{font}'+font+'<°'+ params);
					Bot.sendPublicActionMessage(params);
					return;
				}
				Bot.sendPublicActionMessage(params);
				KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners()[0].sendPostMessage('Nutzung der BOT-DO-Funktion!', 'Hallo!°##°'+user.getNick().escapeKCode()+' hat soeben die BOT-DO-Funktion genutzt...°##°Bitte achte darauf, dass diese Funktion nicht inflationär genutzt wird, vielen dank.');
				
//				Bot.sendPublicMessage('°>{font}'+font+'<24°' + params + '°#°°09°(geschrieben von: '+ user.getProfileLink() +')');
			}
		};
  
	App.chatCommands.update = function(user, params, command, func){
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		if (user.isAppManager() == true || App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || user.isChannelOwner() == true){
			var appAccess = KnuddelsServer.getAppAccess();
			var rootInstance = appAccess.getOwnInstance().getRootInstance();
				rootInstance.updateApp('Durch '+user.getNick().escapeKCode()+' wurde soeben manuell ein Update angestoßen.°##°°BB°Durchsage von _°>_h'+user.getNick().escapeKCode()+'|/w '+user.getNick().escapeKCode()+'|/m '+user.getNick().escapeKCode()+'<°_ an alle: °RR°°22°_'+params);

			user.sendPrivateMessage('Ich habe soeben ein Update angestoßen.');
		}
    };
  
//    apst: function (user, params, command) {
//	if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 == true || user.getNick() == 'x Samsung x') {
//	  user.sendAppContent(overlayContent);
//  Bot.sendPublicMessage('°>{font}FineLinerScript<24°' + params);
//	}
//  },
  


  // Chat Commands

	App.chatCommands.disco = function(user,params,command) {
/*		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
			user.sendPrivateMessage('Bitte lass die Finger von Funktionen die Du nicht nutzen darfst, danke.');
			return;
		}
*/
//		var discoconfetti	= appPersistence.getObject('discoconfetti' ['']);
//		var blloo			= appPersistence.getObject('blacklistonoff');

		appPersistence	= KnuddelsServer.getPersistence();
		var discoconfetti	= appPersistence.getObject('discoconfetti', 'off');
		
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;
		
		if(discoconfetti == "off"){
		user.sendPrivateMessage('Diese Funktion ist zum aktuellen Zeitpunkt _deaktiviert!_');
		return;
		}
		
		else if(discoconfetti == "HZM"){
			if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
			user.sendPrivateMessage('Tut mir Leid, dies können hier nur die _HZM_.');
			return;
			}
			Bot.sendPublicMessage('°>{sprite}type:disco<°');
		} else {
			Bot.sendPublicMessage('°>{sprite}type:disco<°');
		}
	};
  
	App.chatCommands.confetti = function(user,params,command) {
/*		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
			user.sendPrivateMessage('Bitte lass die Finger von Funktionen die Du nicht nutzen darfst, danke.');
			return;
		}
*/
		appPersistence	= KnuddelsServer.getPersistence();
		var discoconfetti	= appPersistence.getObject('discoconfetti', 'off');
		
				var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
		if(discoconfetti == "off"){
		user.sendPrivateMessage('Diese Funktion ist zum aktuellen Zeitpunkt _deaktiviert!_');
		return;
		}
		
		else if(discoconfetti == "HZM"){
			if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
			user.sendPrivateMessage('Tut mir Leid, dies können hier nur die _HZM_.');
			return;
			}
			Bot.sendPublicMessage('°>{sprite}type:confetti<°');
		} else {
			Bot.sendPublicMessage('°>{sprite}type:confetti<°');
		}
//		Bot.sendPublicMessage('°>{sprite}type:confetti<°');
	};

	//ChannelThema:

	App.chatCommands.channelthema = function(user, params, command) {

			var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

	
		if(!user.isChannelModerator() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner()) {
			user.private('Dir fehlen die notwendigen Rechte um diese Aktion auszuführen!');
			return;
		}

	//		var fullImagePath = KnuddelsServer.getFullImagePath('Banner.png');

	//		var message = '°Center° °>' + fullImagePath + '|' + fullImagePath + '<>|http://www.dreambeatsfm.de<°#°left°';
			var message = '°Center°' + params +'#°left°';
			Channel.setTopic(message);

	};
	
        //Rundmail:
	App.chatCommands.rundmail = function(user, params) {

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;


	if(!user.isChannelModerator() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()) {
			user.sendPrivateMessage('Du hast keine Berechtigung um diese Funktion auszuführen.');
			return;
		} else {
			Channel.getModerators().each(function(receiver) {     	
				params = params.formater({
				NICKNAME: receiver.getProfileLink(),
//				BILD: new KLink(new KImage('/Banner.png'), 'www.dreambeatsfm.de')
				});	
			
			receiver.post('Rundmail von ' + user.getNick().escapeKCode(), params);
			});
			
//			Channel.getModerators().each(function(receiver) {     	
//			params = params.formater({
//				NICKNAME: receiver.getProfileLink(),
//				BILD: new KLink(new KImage('/Banner.png'), 'www.dreambeatsfm.de')
//			});	
			
//			receiver.post('Rundmail von ' + user.getNick(), params);
//		});
		}
	};

  
  // Chat Commands
  
/*	hangman: function (user, params, command) {
		if (App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 == true || user.getNick() == 'x Samsung x') {
			user.sendAppContent(hangman);
		}
	},
*/

	App.chatCommands.greetings = function(user,params){
		var appPersistence = KnuddelsServer.getPersistence();
		var onoff = appPersistence.getObject('greetings', ['on']);
		var devgr = appPersistence.getObject('devgreeting', ['on']);
		var userAccess = KnuddelsServer.getUserAccess();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
		var pspl = params.split(':');
		
		if(pspl[0] == "on" || pspl[0] == "off") {
			if(isHzmApp == "on"){
				if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			};
			
			if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
				}
			appPersistence.setObject('greetings', pspl[0]);
			
			if(pspl[0] == "on"){
				
			user.sendPrivateMessage('Ich habe die _Begrüßung eingeschaltet_. °[255,49,49]°_°>[Rückgängig]|/greetings off<°_°r°');
				return;
				
			}
			user.sendPrivateMessage('Ich habe die _Begrüßung ausgeschaltet_. °[255,49,49]°_°>[Rückgängig]|/greetings on<°_°r°');
			return;
		}
		
		if(pspl[0] == "dev" && pspl[1] == "on" || pspl[1] == "off"){
			if(!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
			appPersistence.setObject('devgreeting', pspl[1]);
			user.sendPrivateMessage('Die _Begrüßung_ des _Developers_ ist nun _°[255,49,49]°'+ pspl[1] +'_°r°');
			return;
		}
		
		if (pspl[0] == "state"){
			if(!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
			if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
				user.sendPrivateMessage('die DEVELOPER-Begrüßung ist _°[255,49,49]°'+ devgr +'_°r°');
			}
			user.sendPrivateMessage('Die Begrüßung ist aktuell _°[255,49,49]°'+ onoff +'_°r°');
			return;
		}
		
		if(pspl[0] == user.getNick() && pspl[1] != null && pspl[1] != "" && pspl[1] != " " && pspl[2] == undefined){
			var nickname = user.getNick().escapeKCode();
			appPersistence.setObject(nickname, pspl[1]);
			user.sendPrivateMessage('Ich habe soeben _'+ pspl[1] +'_ als Deinen Begrüßungstext gesetzt.');
			return;
		}
		
		if(userAccess.exists(pspl[0]) && pspl[1] == "delete"){

			if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 && user.isChannelOwner() && user.isAppManager()){
				appPersistence.deleteObject(pspl[0]);
				user.sendPrivateMessage('Ich habe soeben die Nutzerdefinierte Begrüßung von _°>'+pspl[0].escapeKCode()+'|/w '+pspl[0].escapeKCode()+'|/m '+pspl[0].escapeKCode()+'<°_ gelöscht.');

//				if(!user.isAppDeveloper){
					var devel = KnuddelsServer.getAppDeveloper();
					devel.sendPostMessage(user.getNick().escapeKCode()+' löscht indiv. Begrüßung von '+pspl[0].escapeKCode(), '_°>'+user.getNick().escapeKCode()+'|/w '+user.getNick().escapeKCode()+'|/m '+user.getNick().escapeKCode()+'<°_ hat soeben die Nutzerbasierte Begrüßung von _°>'+pspl[0].escapeKCode()+'|/w '+pspl[0].escapeKCode()+'|/m '+pspl[0].escapeKCode()+'<°_ im Channel '+channel+' gelöscht.');

					var betr = KnuddelsServer.getUserByNickname(pspl[0]);
					betr.sendPostMessage(user.getNick().escapeKCode()+' löscht indiv. Begrüßung von '+pspl[0].escapeKCode(), '_°>'+user.getNick().escapeKCode()+'|/w '+user.getNick().escapeKCode()+'|/m '+user.getNick().escapeKCode()+'<°_ hat soeben die Nutzerbasierte Begrüßung von _°>'+pspl[0].escapeKCode()+'|/w '+pspl[0].escapeKCode()+'|/m '+pspl[0].escapeKCode()+'<°_ gelöscht.');
					
//				}
//				if(!user.isChannelOwner){
					KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelOwners()[0].sendPostMessage(user.getNick().escapeKCode()+' löscht indiv. Begrüßung von '+pspl[0].escapeKCode(), '_°>'+user.getNick().escapeKCode()+'|/w '+user.getNick().escapeKCode()+'|/m '+user.getNick().escapeKCode()+'<°_ hat soeben die Nutzerbasierte Begrüßung von _°>'+pspl[0].escapeKCode()+'|/w '+pspl[0].escapeKCode()+'|/m '+pspl[0].escapeKCode()+'<°_ gelöscht.');
//				}
				return;
			}
		}
		
		if (userAccess.exists(pspl[0]) && pspl[1] == undefined){
			if(!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
			
			var usergr = appPersistence.getObject(pspl[0]);
			if(usergr == null){
				user.sendPrivateMessage('Der Nutzer '+pspl[0].escapeKCode()+' hat aktuell _keine personifizierte Begrüßung_ aktiv!');
				return;
			}
			user.sendPrivateMessage('Der Nick '+pspl[0].escapeKCode()+' hat aktuell folgende Begrüßung aktiv: °##°'+usergr.escapeKCode());
			return;
		}
		
		if (userAccess.exists(pspl[0] && pspl[1] == "edit" && pspl[2] != undefined)){
			if(App.coDevs.indexOf(userkennung)  < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner() && !user.isAppManager()) {
				user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
				return;
			}
			
			var usergrold = appPersistence.getObject(pspl[0]);
			if(usergrold == undefined){
				user.sendPrivateMessage('Die alte Begrüßung von '+pspl[0].escapeKCode()+' lautete: °##°'+usergrold);
/*				setTimeout(function(){
						KnuddelsServer.refreshHooks();
						}, 1000)
*/
				var usergrnew = appPersistence.setObject(pspl[0], pspl[2]);
				user.sendPrivateMessage('Die neue Begrüßung von '+pspl[0].escapeKCode()+' lautet: °##°'+pspl[2]);
			}
			user.sendPrivateMessage('Die alte Begrüßung von '+pspl[0].escapeKCode()+' lautete: °##°'+usergrold.escapeKCode());
			var usergrnew = appPersistence.setObject(pspl[0], pspl[2]);
			user.sendPrivateMessage('Die neue Begrüßung von '+pspl[0].escapeKCode()+' lautet: °##°'+pspl[2]);
			
			return;
		}
			
		else {
			if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 && user.isChannelOwner()){
				user.sendPrivateMessage('Die ChannelOwner-Funktionen lauten:°#°_/greetings Nick_ zeigt die User-definierte Begrüßung von Nick.°#°_/greetings Nick:delete_ Löscht die vom User selbst gesetzte Begrüßung und setzt diese auf Standard zurück. _°RR°ACHTUNG! - Bei dieser Aktion wird dem Channel-Eigentümer zur Information eine /m über diesen Vorgang übersandt!°#r°_');
			}
			if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
				user.sendPrivateMessage('Die DEVELOPER-Funktionen sind:°##°_°>/greetings dev:on|/greetings dev:on<°_ schaltet die (starre) DEV-Begrüßung ein.°#°_°>/greetings dev:off|/greetings dev:off<°_ schaltet die (starre) DEV-Begrüßung aus.°#°_/greetings Nick:edit:TEXT_ setzt TEXT als Begrüßung bei Nick.');
			}
			user.sendPrivateMessage('Die Syntax für diese Funktion lautet:°#°_°>/greetings on|/greetings on<°_ um die Begrüßung einzuschalten°#°_°>/greetings off|/greetings off<°_ um die Begrüßung abzuschalten.°#°_/greetings DeinNick:Dein Begrüßungstext_');
		}
	};

	App.chatCommands.Closed = function(user){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var ChanKey = appPersistence.getObject('ChanKey', []);
//				ChanKey.push('Closed');
				appPersistence.setObject('ChanKey', 'Closed');
		user.sendPrivateMessage('Ich habe den Channel geschlossen °R°_°>Channel CM-Einstellung|/usual<°_ oder °[255,49,49]°_°>Channel für jedermann öffnen|/opened<°_°r°');
		Bot.sendPublicMessage('°R°_*INFO*_§ Ich habe den Channel geschlossen. _Ab sofort kann °R°NIEMAND_§ den _°R°Channel betreten._°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben! -- Keine Funktion!');
		}
	};

	App.chatCommands.usual = function(user){
			var isAdmin			= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var ChanKey = appPersistence.getObject('ChanKey', []);
//				ChanKey.push('CM-Modus');
				appPersistence.setObject('ChanKey', 'CM-Modus');
				user.sendPrivateMessage('Ich habe den Channel wieder geöffnet (CM-Einstellung!) °R°_°>Channel SCHLIEßEN|/Closed<°_ oder °[255,49,49]°_°>Channel für jedermann öffnen|/opened<°_°r°');
		Bot.sendPublicMessage('_°R°*INFO*_§ Der _Channel_ befindet sich nun im °R°_CM-Modus._°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};
	
	App.chatCommands.opened = function(user,params,func){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var ChanKey = appPersistence.getObject('ChanKey', []);
//				ChanKey.push('offen');
				appPersistence.setObject('ChanKey', 'offen');
				user.sendPrivateMessage('Ich habe den Channel nun für _jedermann_ geöffnet. °R°_°>Channel CM-Einstellung|/usual<°_ oder °[255,49,49]°_°>Channel SCHLIEßEN|/Closed<°_°r°');
				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.chanstate = function(user){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
/*
		if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
*/			
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var ChanKey = appPersistence.getObject('ChanKey', []);
//				ChanKey.push('offen');
				appPersistence.getObject('ChanKey', []);
					if (ChanKey == "CM-Modus") {
						user.sendPrivateMessage('Der _Channel_ befindet sich aktuell im °R°_'+ ChanKey +'_§.');
						return;
					}
					else if (ChanKey == "Closed") {
						user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
						return;
					}
				user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_'+ ChanKey +'_§.');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};
	
	App.chatCommands.restricttext = function(user,params){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('restrictiontext', []);
//				restriction.push('restrictiontext');
				appPersistence.setObject('restrictiontext', params);
				user.sendPrivateMessage('Der angezeigte Sperrtext für nicht zugelassene User (CM-Modus) lautet ab sofort: _' + params +'_');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.restrictiontext = function(user){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !isAdmin && !user.isChannelOwner()){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
/*
		if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
*/			
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('restrictiontext', []);
//				ChanKey.push('offen');
				appPersistence.getObject('restrictiontext', []);
/*					if (ChanKey == "CM-Modus") {
						user.sendPrivateMessage('Der _Channel_ befindet sich aktuell im °R°_'+ ChanKey +'_§.');
						return;
					}
					else if (ChanKey == "Closed") {
						user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
						return;
					}
*/
				user.sendPrivateMessage('Der _Channel_ hat aktuell den folgenden Sperrtext im CM-Modus: °R°_'+ restriction +'_§.');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};
	
	App.chatCommands.closedtext = function(user,params){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var closedtext = appPersistence.getObject('closedtext', []);
//				restriction.push('closedtext');
				appPersistence.setObject('closedtext', params);
				user.sendPrivateMessage('Der angezeigte Sperrtext für den _geschlossenen Channel_ lautet ab sofort: _' + params +'_');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};

	App.chatCommands.showclosedtext = function(user){
			var isAdmin 		= user.isInTeam("Admin");

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

			
		if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !isAdmin && !user.isChannelOwner()){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
/*
		if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
*/			
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var closedtext = appPersistence.getObject('closedtext', []);
//				ChanKey.push('offen');
				appPersistence.getObject('closedtext', []);
/*					if (ChanKey == "CM-Modus") {
						user.sendPrivateMessage('Der _Channel_ befindet sich aktuell im °R°_'+ ChanKey +'_§.');
						return;
					}
					else if (ChanKey == "Closed") {
						user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
						return;
					}
*/
				user.sendPrivateMessage('Der _Channel_ hat aktuell den folgenden Sperrtext im _GESCHLOSSENEN Modus_: °R°_'+ closedtext +'_§.');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};
	
	
		App.cmdwarn = function(user,params){

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			var appPersistence = KnuddelsServer.getPersistence();
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] != undefined){
				
					if(pspl[0].toLowerCase() == "help"){
						if(isabRC == "RC"){
							user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_.°#°Auf Anfrage können für den internen Gebrauch Unterkategorien zur Verfügung gestellt werden.');
							return;
						}
						else {
							user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_ und weitere unterkategorien.°#°Die folgenden Unterkategorien (für den _internen Gebrauch_!) sind möglich:°#°- weiß-gelb (wy)°#°- gelb-orange (yo)°#°- orange (orange)');
							return;
						}
					}
					
					if(pspl[1] != undefined){
						if(pspl[0].toLowerCase == "set" && pspl[1] != undefined && pspl[1].toLowerCase() == "vw" && pspl[2] != undefined && pspl[2] != " "){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(pspl[2])){
								var users	= appPersistence.getObject('iswarned', []);
								users.push(pspl[2]);
								appPersistence.setObject('iswarned', users);
								user.sendPrivateMessage('Ich habe _'+ pspl[2] +'_ die _Verwarnung_ eingetragen. °R°_°>Rückgängig|/warn remark:vw:'+ pspl[2] +'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Dieser User existiert nicht! -- Hast Du Dich vielleicht vertippt?');
							}
						}	

						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "vw"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var vw = appPersistence.getObject('iswarned', []);
				
							if(vw.length <= 0){
								user.sendPrivateMessage('Du hast keine CMs mit Status \"Verwarnung\". °R°_°>Jetzt Verwarnung hinzufügen|/tf-overridesb /warn set:vw:[James]<°_°r°');
								return;
							}
					
							var msg = '°BB°_Übersicht der Verwarnungen:_°r° °#°';
				
							for(i=0;i<vw.length;i++){
								msg=msg + i+' | '+ vw[i].escapeKCode() +' | °R°_°>Entfernen|/warn remark:vw:'+ vw[i].escapeKCode() +'<°_°r° °#°'
							}
							msg = msg+ '°#° °[000,175,225]°_°>CM Hinzufügen|/tf-overridesb /warn set:vw:[James]<°_°r°';
							user.sendPrivateMessage(msg);
						}
					
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "vw"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
						
							var vw = appPersistence.getObject('iswarned', []);
				
							if(vw.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage('Der Nutzer '+ pspl[2] +' wurde doch gar nicht verwarnt!?');
								return;
							}
					
							vw.splice(vw.indexOf(pspl[2]), 1);
				
							appPersistence.setObject('iswarned', vw);
				
							user.sendPrivateMessage('Ich habe '+ pspl[2] +' die Verwarnung entfernt. °R°_°>Rückgängig|/warn set:vw:'+ pspl[2].escapeKCode() +'<°_°r°');
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "wy"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var wy = appPersistence.getObject('whiteyellow', []);
					
							if(wy.length <= 0){
								user.sendPrivateMessage('Aktuell hat keiner eine Weiß-gelbe Karte. °R°_°>Jetzt Weiß-gelbe Karte verhängen|/tf-overridesb /warn set:wy:[James]<°_°r°');
								return;
							}
					
							var msg = 'Übersicht der Weiß-gelben Karten: °#°';
					
							for(i=0;i<wy.length;i++){
								msg=msg + i+' | '+ wy[i].escapeKCode() +' | °R°_°>Entfernen|/warn remark:wy:'+ wy[i].escapeKCode() +'<°_°r° °#°'
						
							}
							msg = msg+ '°#° °[000,175,225]°_°>HZM hinzufügen|/tf-overridesb /warn set:wy:[James]<°_°r°';
					
							user.sendPrivateMessage(msg);
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "wy" && pspl[2] != undefined && pspl[2] != " "){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(pspl[2])){
								var users = appPersistence.getObject('whiteyellow', []);
								users.push(pspl[2]);
								appPersistence.setObject('whiteyellow', users);
								user.sendPrivateMessage(pspl[2]+' Hat nun die Weiß-gelbe Karte. (inoffiziell!)°R°_°>Rückgängig|/warn remark:wy:'+pspl[2].escapeKCode()+'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vertippt?');
							}
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "wy"){
							if(appPersistence.getObject('whiteyellow', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var wy = appPersistence.getObject('whiteyellow', []);
					
							if(wy.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage(pspl[2] +' hat doch gar keine Weiß-gelbe Karte!?');
								return;
							}
					
							wy.splice(wy.indexOf(pspl[2]), 1);
							appPersistence.setObject('whiteyellow', wy);
					
							user.sendPrivateMessage('Ich habe '+ pspl[2] +' soeben die Weiß-gelbe Karte (inoffiziell!) entfernt. °R°_°>Rückgängig|/warn set:wy:'+ pspl[2].escapeKCode() +'<°_°r°');
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "gelb" && pspl[2] != undefined && pspl[2] != " "){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(pspl[2])){
								var users = appPersistence.getObject('yellow', []);
								users.push(pspl[2]);
								appPersistence.setObject('yellow', users);
								user.sendPrivateMessage('Ich habe '+pspl[2]+' die _gelbe Karte_ verhängt. °R°_°>Rückgängig|/warn remark:gelb:'+pspl[2].escapeKCode()+'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "gelb"){
							if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var yellow = appPersistence.getObject('yellow', []);
					
							if(yellow.length <= 0){
								user.sendPrivateMessage('Bisher hat niemand eine gelbe Karte. °R°_°>Jetzt hinzufügen|/tf-overridesb /warn set:gelb:[James]<°_°r°');
								return;
							}
					
							var msg = '_°BB°Übersicht der gelben Karten:_°r° °#°';
					
							for(i=0;i<yellow.length;i++){
								msg=msg + i+' | '+ yellow[i].escapeKCode() +' | °R°_°>Entfernen|/warn remark:gelb:'+yellow[i].escapeKCode()+'<°_°r° °#°'
							}
					
							msg = msg+ '°#° °[000,175,225]°_°>Gelbe Karte verhängen|/tf-overridesb /warn set:gelb:[James]<°_°r°';
					
							user.sendPrivateMessage(msg);
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "gelb"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
								return;
							}
					
							var yellow = appPersistence.getObject('yellow', []);
					
							if(yellow.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+pspl[2]+')');
								return;
							}
					
							yellow.splice(yellow.indexOf(pspl[2]), 1);
					
							appPersistence.setObject('yellow', yellow);
					
							user.sendPrivateMessage('Ich habe '+pspl[2]+' die gelbe Karte entfernt. °R°_°>Rückgängig|/warn set:gelb:'+pspl[2].escapeKCode()+'<°_°r°');
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "yo" && pspl[2] != undefined && pspl[2] != " "){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(pspl[2])){
								var users = appPersistence.getObject('yellowred', []);
								users.push(pspl[2]);
								appPersistence.setObject('yellowred', users);
								user.sendPrivateMessage('Ich habe _'+pspl[2]+'_ die _gelb-orangene Karte_ (inoffiziell) verhängt. °R°_°>Rückgängig|/warn remark:yo:'+pspl[2].escapeKCode()+'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "yo"){
							if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var yo = appPersistence.getObject('yellowred', []);
					
							if(yo.length <= 0){
								user.sendPrivateMessage('Bisher hat niemand eine gelb-orangene Karte (inoffiziell). °R°_°>Jetzt verhängen|/tf-overridesb /warn set:yo:[James]<°_°r°');
								return;
							}
					
							var msg = 'Übersicht der gelb-orangenen (inoffiziell!) Karten: °#°';
					
							for(i=0;i<yo.length;i++){
								msg=msg + i+' | '+ yo[i].escapeKCode() +' | °R°_°>Entfernen|/warn remark:yo:'+yo[i].escapeKCode()+'<°_°r° °#°'
							}
					
							msg = msg+ '°#° °[000,175,225]°_°>Gelb-orangene Karte verhängen|/tf-overridesb /warn set:yo:[James]<°_°r°';
					
							user.sendPrivateMessage(msg);
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "yo"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
								return;
							}
					
							var yo = appPersistence.getObject('yellowred', []);
					
							if(yo.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+pspl[2]+')');
								return;
							}
					
							yo.splice(yo.indexOf(pspl[2]), 1);
					
							appPersistence.setObject('yellowred', yo);
					
							user.sendPrivateMessage('Ich habe '+pspl[2]+' die gelb-orangene (inoffiziell) Karte entfernt. °R°_°>Rückgängig|/warn set:yo:'+pspl[2].escapeKCode()+'<°_°r°');
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "orange" && pspl[2] != undefined && pspl[2] != " "){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(pspl[2])){
								var users = appPersistence.getObject('winered', []);
								users.push(pspl[2]);
								appPersistence.setObject('winered', users);
								user.sendPrivateMessage('Ich habe _'+pspl[2]+'_ die _orangene Karte_ (inoffiziell) verhängt. °R°_°>Rückgängig|/warn remark:yo:'+pspl[2].escapeKCode()+'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "orange"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var orange = appPersistence.getObject('winered', []);
					
							if(orange.length <= 0){
								user.sendPrivateMessage('Bisher hat niemand eine orangene Karte (inoffiziell). °R°_°>Jetzt verhängen|/tf-overridesb /warn set:orange:[James]<°_°r°');
								return;
							}
					
							var msg = 'Übersicht der orangenen (inoffiziell!) Karten: °#°';
					
							for(i=0;i<orange.length;i++){
								msg=msg + i+' | '+ orange[i].escapeKCode() +' | °R°_°>Entfernen|/warn remark:orange:'+orange[i].escapeKCode()+'<°_°r° °#°'
							}
					
							msg = msg+ '°#° °[000,175,225]°_°>orangene Karte verhängen|/tf-overridesb /warn set:yo:[James]<°_°r°';
					
							user.sendPrivateMessage(msg);
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "orange"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
								return;
							}
					
							var orange = appPersistence.getObject('winered', []);
					
							if(orange.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+pspl[2]+')');
								return;
							}
					
							orange.splice(orange.indexOf(pspl[2]), 1);
					
							appPersistence.setObject('winered', orange);
					
							user.sendPrivateMessage('Ich habe '+pspl[2]+' die orangene (inoffiziell) Karte entfernt. °R°_°>Rückgängig|/warn set:orange:'+pspl[2].escapeKCode()+'<°_°r°');
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "rot" && pspl[2] != undefined && pspl[2] != " "){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(pspl[2])){
								var users = appPersistence.getObject('red', []);
								users.push(pspl[2]);
								appPersistence.setObject('red', users);
								user.sendPrivateMessage('Ich habe _'+pspl[2]+'_ die _rote Karte_ verhängt. °R°_°>Rückgängig|/warn remark:rot:'+pspl[2].escapeKCode()+'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "rot"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var redcard = appPersistence.getObject('red', []);
					
							if(redcard.length <= 0){
								user.sendPrivateMessage('Bisher hat niemand eine rote Karte. °R°_°>Jetzt verhängen|/tf-overridesb /warn set:rot:[James]<°_°r°');
								return;
							}
					
							var msg = '_°BB°Übersicht der roten Karten:°r°_ °#°';
					
							for(i=0;i<redcard.length;i++){
								msg=msg + i+' | '+ redcard[i].escapeKCode() +' | °R°_°>Entfernen|/warn remark:rot:'+redcard[i].escapeKCode()+'<°_°r° °#°'
							}
					
							msg = msg+ '°#° °[000,175,225]°_°>orangene Karte verhängen|/tf-overridesb /warn set:rot:[James]<°_°r°';
					
							user.sendPrivateMessage(msg);
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "rot"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben!');
								return;
							}
					
							var redcard = appPersistence.getObject('red', []);
					
							if(redcard.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+pspl[2]+')');
								return;
							}
					
							redcard.splice(orange.indexOf(pspl[2]), 1);
					
							appPersistence.setObject('red', redcard);
					
							user.sendPrivateMessage('Ich habe '+pspl[2]+' die rote Karte entfernt. °R°_°>Rückgängig|/warn set:rot:'+pspl[2].escapeKCode()+'<°_°r°');
						}
					} else {
						user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_.°#°Auf Anfrage können für den internen Gebrauch Unterkategorien zur Verfügung gestellt werden.');
					}
				} else {
					user.sendPrivateMessage('Die Syntax für die WARN-Funktion lautet:°#°_/warn set:Warnstatus:NICK_ zum setzen von Warnstatus bei NICK.°#°Mit _/warn list:Warnstatus_ zeigst Du Alle Nicks mit dem angegebenen Warnstatus an.°#°Mit _/warn remark:Warnstatus:NICK_ entziehst Du Nick die entsprechende Warnstufe wieder.°#°°#°Es stehen aktuell folgende Warnstatus zur Verfügung (in Klammern die Befehlssyntax): _Verwarnung (vw)_, _gelbe Karte (gelb)_, _rote Karte (rot)_.°#°Auf Anfrage können für den internen Gebrauch Unterkategorien zur Verfügung gestellt werden.');
				}
			} 
		};
	
	/* Kartenquerys
		var whiteyellowcard = appPersistence.getObject('whiteyellow', ['']);
		var yellowcard = appPersistence.getObject('yellow', ['']);
		var yellowredcard = appPersistence.getObject('yellowred', ['']);
		var wineredcard = appPersistence.getObject('winered', ['']);
		var redcard = appPersistence.getObject('red', ['']);
*/

	App.chatCommands.flush = function(user,params){
		
		var appPersistence = KnuddelsServer.getPersistence();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner()){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
		var pspl = params.split(':');
				var msg	 = "";
				
		if(pspl[0] == "help"){
			user.sendPrivateMessage('Die Syntax für die FLUSH-Funktion lautet:°#°_/flush VARIABLE_.°#°Folgende Listen können auf Einmal geleert werden: _cm_, _vcm_, _hzae_, _Gast_.');
			return;
		}
		
		if(pspl[0] == "cm"){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('CMs', []);

				appPersistence.deleteObject('CMs');
				user.sendPrivateMessage('CMs resettet.');
				return;
			} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
			}
		}
		
		if(pspl[0] == "hzae"){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('hzae', []);

				appPersistence.deleteObject('hzae');
				user.sendPrivateMessage('HZM resettet.');
				return;
			} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
			}
		}
		
		if(pspl[0] == "Gast"){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('sonderuser', []);

				appPersistence.deleteObject('sonderuser');
				user.sendPrivateMessage('Gäste resettet.');
				return;
			} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
			}
		}
		
		if(pspl[0] == "vcm"){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('vcms', []);

				appPersistence.deleteObject('vcms');
				user.sendPrivateMessage('VCMs resettet.');
				return;
			} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
			}
		}
		
		if(pspl[0] == "blacklist"){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var restriction = appPersistence.getObject('blacklisted', []);

				appPersistence.deleteObject('blacklisted');
				user.sendPrivateMessage('Blacklist resettet.');
				return;
			} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
			}
		}
		
		if(pspl[0] == "hooks"){
			KnuddelsServer.refreshHooks();
		}
		
		if(pspl[0] == "dev" && pspl[1] == "show"){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var persistenzinhalt = appPersistence.getObject(pspl[2], []);

				user.sendPrivateMessage('Inhalt der Persistence '+pspl[2]+': '+persistenzinhalt);
				return;
			}
		}
		
		if(pspl[0] == "dev" && pspl[1] != undefined){
			var userAccess = KnuddelsServer.getUserAccess();
			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var restriction = appPersistence.getObject(pspl[1], []);

				appPersistence.deleteObject(pspl[1]);
				user.sendPrivateMessage(pspl[1]+' resettet.');
				return;
			} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
			} 
			if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
				user.sendPrivateMessage('Liste der Listen:°##°- hooks°#°- blacklist°#°- vcm°#°- Gast°#°- hzae°#°- cm°#°- _dev:PERSISTENCE_');
			}
			user.sendPrivateMessage('Bitte nutze diese Funktion wie folgt:°#°_/flush VARIABLE_.°#°Folgende Listen können auf Einmal geleert werden: _CM_, _vcm_, _hzae_, _Gast_.');
		}
	};
	
		App.cmdnicklist = function(user,params){
			var appPersistence = KnuddelsServer.getPersistence();
			var userAccess = KnuddelsServer.getUserAccess();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;


			var devel = KnuddelsServer.getAppDeveloper();
			if(userAccess.exists(user)){
				var pspl = params.split(':');
				var msg = "";
				
					if(appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0){
						user.sendPrivateMessage('Diese Funktion steht Dir hier nicht zur Verfügung.');
						return;
					}
					
					if(pspl[0] != undefined && pspl[1] != undefined && pspl[1].toLowerCase() == "add"){
					
						if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
							var nlspl = pspl[2].split(',');
					
							var existendnicklists = appPersistence.getObject('extnl', []);
					
							for(i=0;i<nlspl.length;i++){
								nlspl[i] = nlspl[i].trim();
								msg=msg + nlspl[i]
					
								var userAccess = KnuddelsServer.getUserAccess();
//								var iconnick = vaspl[i].toString();
					
								var existendnicklists = appPersistence.getObject('extnl', []);
					
								if(existendnicklists.indexOf(pspl[0]) == -1){
									existendnicklists.push(pspl[0]);
									appPersistence.setObject('extnl', existendnicklists);
//									user.sendPrivateMessage(pspl[0]+' hinzugefügt.');
								} else {
//									user.sendPrivateMessage('Nick bereits vorhanden!');
								}
					
								if(userAccess.exists(nlspl[i])){
									var users = appPersistence.getObject('chknl-'+pspl[0], []);
								
									if(users.indexOf(nlspl[i]) == -1){
										users.push(nlspl[i]);
										appPersistence.setObject('chknl-'+pspl[0], users);
								
//										var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
//										iconnick.addNicklistIcon(icon2, 35);
										user.sendPrivateMessage(nlspl[i]+' ist nun in der Liste '+pspl[0]+' eingetragen. °R°_°>Rückgängig|/nicklist '+pspl[0].escapeKCode()+':del:'+nlspl[i].escapeKCode()+'<°_°r°');
								
									} else {
										user.sendPrivateMessage('Der Nick '+nlspl[i]+' existiert bereits auf der Liste von '+pspl[0]+'! Daher wurde er selbstredend _nicht_ erneut hinzugefügt!');
									}

								} else {
									user.sendPrivateMessage('Der Nutzer '+nlspl[i]+' existiert nicht. Hast Du Dich vielleicht vertippt?');
								}
							}
							return;
						}

						var nlspl = pspl[2].split(',');
					
						var existendnicklists = appPersistence.getObject('extnl', []);
						var checknicklist = appPersistence.getObject('chknl-'+pspl[0], [])
					
						for(i=0;i<nlspl.length;i++){
							nlspl[i] = nlspl[i].trim();
							msg=msg + nlspl[i]
					
							var userAccess = KnuddelsServer.getUserAccess();
//							var iconnick = vaspl[i].toString();
					
							var existendnicklists = appPersistence.getObject('extnl', []);
					
							if(existendnicklists.indexOf(pspl[0]) == -1){
								existendnicklists.push(pspl[0]);
								appPersistence.setObject('extnl', existendnicklists);
//								user.sendPrivateMessage(pspl[0]+' hinzugefügt.');
							} else {
//								user.sendPrivateMessage('Nick bereits vorhanden!');
							}
					
							if(userAccess.exists(nlspl[i])){
								var users = appPersistence.getObject('nl-'+pspl[0], []);
								
								if(users.indexOf(nlspl[i]) == -1){
									users.push(nlspl[i]);
									appPersistence.setObject('nl-'+pspl[0], users);
//									appPersistence.deleteObject('chknl-'+pspl[0]);
									checknicklist.splice(checknicklist.indexOf(nlspl[i]), 1);
									appPersistence.setObject('chknl-'+pspl[0], checknicklist);
								
//								var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
//								iconnick.addNicklistIcon(icon2, 35);
								user.sendPrivateMessage(nlspl[i]+' ist nun in der Liste '+pspl[0]+' eingetragen. °R°_°>Rückgängig|/nicklist '+pspl[0].escapeKCode()+':del:'+nlspl[i].escapeKCode()+'<°_°r°');
								
								} else {
									user.sendPrivateMessage('Der Nick '+nlspl[i]+' existiert bereits auf der Liste von '+pspl[0]+'! Daher wurde er selbstredend _nicht_ erneut hinzugefügt!');
								}

							} else {
							user.sendPrivateMessage('Der Nutzer '+nlspl[i]+' existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						}

						return;
					}
					
					if(pspl[0] != undefined && pspl[1] != undefined && pspl[1].toLowerCase() == "list"){
						
						if(pspl[0].toLowerCase() == "secki") {
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1) {
								user.sendPrivateMessage('Secki ist ein amtierender Admin. Aus diesem Grunde werden hier keinerlei Nicklisten von diesem Nutzer angezeigt.');
								return;
							}
						}
						
						if(pspl[0].toLowerCase() == "administrativ"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && user.getNick() != "Seelenverbrannt"){
								user.sendPrivateMessage('Diese Liste ist rein administrativ zu Testzwecken erstellt. Sie hat keinerlei Inhalt, sondern testet nur die Funktionalität und sammelt die Fehlermeldungen, damit der Programmierer der App diese dann beheben kann.°##°_°BB°Sollten Fehler auftauchen, so melde diese bitte trotz Protokollierung innerhalb dieser Funktion per °>/m Pega16|/m Pega16<°, damit eine schnelle Bearbeitung erfolgen kann. Vielen Dank!_°r°');
								return;
							}
						}
						
						var nicklist = appPersistence.getObject('nl-'+pspl[0], []);
						var chknl = appPersistence.getObject('chknl-'+pspl[0], []);
						var showchknl = appPersistence.getObject('chknl-'+pspl[0]);
						var existendnicklists = appPersistence.getObject('extnl', []);
						
//Zeige mir alle existenten Nicklisten											
						if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0 || appPersistence.getObject('vcms', []).indexOf(user.getNick()) >= 0){
							var existendnicklists = appPersistence.getObject('extnl', []);
							var messg = '_°BB°Existierende Nicklists:_°r°°##°';
						
							for(i=0;i<existendnicklists.length;i++){
							
								messg=messg + i+' | °>'+ existendnicklists[i].escapeKCode() +'|/nicklist '+ existendnicklists[i].escapeKCode() +':list<° °r° °#°';
							}
							user.sendPrivateMessage(messg);
						
						}
						
						
//Sammel die Nicks für die Nickliste
						if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0 || appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0){
							var msg = '_°BB°Übersicht der Nicks von '+pspl[0].escapeKCode()+': °r°_°#°';
							for(i=0;i<nicklist.length;i++){
								msg=msg + i+' | '+ nicklist[i].escapeKCode() +' | °R°_°>Entfernen|/nicklist '+pspl[0].escapeKCode()+':del:'+nicklist[i].escapeKCode()+'<°_°r° °#°'
							}
							msg = msg+ '°#° °[000,175,225]°_°>Nick zu '+pspl[0].escapeKCode()+'s Nicks hinzufügen|/tf-overridesb /nicklist '+pspl[0]+':add:[Nick1,Nick2,Nick3,Nick4,...]<°_°r°';
						}
						
						if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
							var msg = '_°BB°Übersicht der Nicks von '+pspl[0].escapeKCode()+': °r°_°#°';
							for(i=0;i<nicklist.length;i++){
								msg=msg + i+' | '+ nicklist[i].escapeKCode() +' °#°'
							}
							msg = msg+ '°#° °[000,175,225]°_°>Nick zu '+pspl[0].escapeKCode()+'s Nicks hinzufügen|/tf-overridesb /nicklist '+pspl[0].escapeKCode()+':add:[Nick1,Nick2,Nick3,Nick4,...]<°_°r°';
						}

//Nun sammel die Check-Nickliste zu Nick.
						if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) > -1){
							var chkmsg = '_°BB°Liste der noch zu prüfenden Nicks von '+pspl[0].escapeKCode()+':°r°_°##°"';
							for(i=0;i<chknl.length;i++){
								chkmsg=chkmsg + i+' | '+ chknl[i].escapeKCode() +' | °R°_°>Trifft zu|/nicklist '+pspl[0].escapeKCode()+':add:'+chknl[i].escapeKCode()+'<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
							}
						} else 	{
							var chkmsg = '_°BB°Liste der noch zu prüfenden Nicks von '+pspl[0].escapeKCode()+':°r°_°##°"';
							for(i=0;i<chknl.length;i++){
								chkmsg=chkmsg + chknl[i].escapeKCode() +', '
							}
						}
						
//Nicklist leer?
						if(nicklist.length <= 0){
							user.sendPrivateMessage(pspl[0]+' hat noch keine eingetragenen Nicks, daher kann ich Dir auch keine anzeigen! °##°°R°_°>Jetzt Nicks hinzufügen|/tf-overridesb /nicklist '+pspl[0]+':add:[James,System]<°_°r°');
							//Wenn Nicklist leer lösche den Nick aus der Liste der existenten Nicklists (sofern vorhanden)
							if(existendnicklists.indexOf(pspl[0]) > -1){
								existendnicklists.splice(existendnicklists.indexOf(pspl[0]), 1);
								appPersistence.setObject('extnl', existendnicklists);
//								return;
							}
						} else {
							user.sendPrivateMessage(msg);							
							
						}
						
						if(chknl.length > 0){
							user.sendPrivateMessage(chkmsg);
						}
//ENDE dieser Abfrage.
					
						return;
					}
				
				if(pspl[0] != undefined && pspl[1] == "del"){
					
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner()){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
					}
					
					var nlspl = pspl[2].split(',');
					
					for(i=0;i<nlspl.length;i++){
						nlspl[i] = nlspl[i].trim();
						msg=msg + nlspl[i]
					
						var nicklist = appPersistence.getObject('nl-'+pspl[0], []);
					
						if(nicklist.indexOf(nlspl[i]) == -1){
							user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+nlspl[i]+')');
						}
					
						nicklist.splice(nicklist.indexOf(nlspl[i]), 1);
					
						appPersistence.setObject('nl-'+pspl[0], nicklist);
						devel.sendPostMessage(user.getNick()+' löscht Nick aus Nicklist', user.getNick().escapeKCode()+' hat soeben '+nlspl[i].escapeKCode()+' aus der Nicklist von '+pspl[0].escapeKCode()+' gelöscht.');
					
//						var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
//						vaspl[i].removeNicklistIcon(icon2);
					
						user.sendPrivateMessage('Ich habe '+nlspl[i]+' aus der Liste entfernt. °R°_°>Rückgängig|/nicklist '+pspl[0].escapeKCode()+':add:'+nlspl[i].escapeKCode()+'<°_°r°');
					}
					return;
				}
					else {
						user.sendPrivateMessage('_°BB°Die korrekte Nutzung der Funktion ist wie folgt:_°r° °##°°>/nicklist '+user.getNick().escapeKCode()+':list|/nicklist '+user.getNick().escapeKCode()+':list<° -> Listet die eingetragenen Zweitnicks von '+user.getNick().escapeKCode()+' auf.°##°°>/nicklist '+user.getNick().escapeKCode()+':add:'+user.getNick().escapeKCode()+',Nick2,Nick3,Nick4.......NickN|/nicklist '+user.getNick().escapeKCode()+':add:'+user.getNick().escapeKCode()+'<° -> Fügt der Nickliste von '+user.getNick().escapeKCode()+' Nicks hinzu. Die Menge der Nicks ist nicht beschränkt, es wird jedoch aus Performancegründen darum gebeten, nicht mehr als 100 Nicks gleichzeitig hinzuzufügen (Sonst scrollst bis ins Nirvana bis Du ganz unten bist!)!');
						return;
					}
					
			}
		};
	
		App.cmdrole = function(user,params){

			var appPersistence = KnuddelsServer.getPersistence();
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && !user.isChannelOwner()){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] != undefined){
					if(pspl[0].toLowerCase() == "help"){
						user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _Gast_');
						return;
					}
				
/*					if(pspl[0] == "set" && pspl[1] == "cm" && pspl[1] != undefined && pspl[1] != ""){
						if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
							user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
							return;
						}
					
						var userAccess = KnuddelsServer.getUserAccess();
					
						if(userAccess.exists(pspl[2])){
							var users	= appPersistence.getObject('CMs', []);
							users.push(pspl[2]);
							appPersistence.setObject('CMs', users);
							var icon3 = KnuddelsServer.getFullImagePath('CM.png');
							user.addNicklistIcon(icon3, 35);
							user.sendPrivateMessage('Ich habe '+ pspl[2] +' auf die CM-Liste gesetzt. °R°_°>Rückgängig|/role remark:cm:'+ pspl[2] +'<°_°r°');
							return;
						}
					}
*/
					if(pspl[1] != undefined){
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "cm" && pspl[1] != undefined && pspl[1] != ""){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
							var users = appPersistence.getObject('CMs', []);
					
							var vaspl = pspl[2].split(',');
										
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
						
								if(users.indexOf(vaspl[i]) == -1){
									if(userAccess.exists(vaspl[i])){
										var users	= appPersistence.getObject('CMs', []);
						
										users.push(vaspl[i]);
										appPersistence.setObject('CMs', users);
//										var icon3 = KnuddelsServer.getFullImagePath('CM.png');
//										vaspl[i].addNicklistIcon(icon3, 35);
										user.sendPrivateMessage('Ich habe '+ vaspl[i] +' auf die CM-Liste gesetzt. °R°_°>Rückgängig|/role remark:cm:'+ vaspl[i].escapeKCode() +'<°_°r°');
									} else {
										user.sendPrivateMessage('Der Nick '+vaspl[i]+' existiert nicht. Hast Du Dich vielleicht vertippt?');
									}
								} else {
									user.sendPrivateMessage('Da der Nutzer '+vaspl[i]+' bereits CM ist, wurde dieser selbstverständlich _nicht erneut hinzugefügt!_');
								}
							}
							return;
						}
				
/*						else {						
							user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
							return;
						}
*/
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "cm"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
					
							var cms = appPersistence.getObject('CMs', []);
				
							if(cms.length <= 0){
								user.sendPrivateMessage('Du hast keine CMs auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /role set:cm:[Pega16]<°_°r°');
								return;
							}
					
							var msg = '_°BB°Übersicht Deiner CMs:°r°_ °#°';
				
							for(i=0;i<cms.length;i++){
								msg=msg + i+' | '+ cms[i].escapeKCode() +' | °R°_°>Entfernen|/role remark:cm:'+ cms[i].escapeKCode() +'<°_°r° °#°'
							}
							msg = msg+ '°#° °[000,175,225]°_°>CM Hinzufügen|/tf-overridesb /role set:cm:[Pega16]<°_°r°';
							user.sendPrivateMessage(msg);
							return;
						}
					
/*						if(pspl[0] == "remark" && pspl[1] == "cm"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 ){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
							var cms = appPersistence.getObject('CMs', []);
				
							if(cms.indexOf(pspl[2]) == -1){
								user.sendPrivateMessage('Der Nutzer '+ pspl[2] +' ist kein CM!)');
								return;
							}
					
							cms.splice(cms.indexOf(pspl[2]), 1);
				
							appPersistence.setObject('CMs', cms);
							var icon3 = KnuddelsServer.getFullImagePath('CM.png');
							user.removeNicklistIcon(icon3);
					
							user.sendPrivateMessage('Ich habe '+ pspl[2] +' die CM-Rolle entzogen. °R°_°>Rückgängig|/role set:cm:'+ pspl[2] +'<°_°r°');
						}
*/				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "cm"){
							if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
								user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
								return;
							}
						
							var cms = appPersistence.getObject('CMs', []);
				
							var vaspl = pspl[2].split(',');
				
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
						
								if(cms.indexOf(vaspl[i]) == -1){
									user.sendPrivateMessage('Der Nutzer '+ vaspl[i] +' ist kein CM!)');
									return;
								}
					
								cms.splice(cms.indexOf(vaspl[i]), 1);
				
								appPersistence.setObject('CMs', cms);
//								var icon3 = KnuddelsServer.getFullImagePath('CM.png');
//								vaspl[i].removeNicklistIcon(icon3);
					
								user.sendPrivateMessage('Ich habe '+ vaspl[i] +' die CM-Rolle entzogen. °R°_°>Rückgängig|/role set:cm:'+ vaspl[i].escapeKCode() +'<°_°r°');
							}
							return;
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "hzm"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager()){
								user.sendPrivateMessage('Für diese Funktion musst Du Channel-Eigentümer oder App-Manager sein!');
								return;
							}
					
							var hzae = appPersistence.getObject('hzae', []);
					
							if(hzae.length <= 0){
								user.sendPrivateMessage('Du hast keine HZM auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt HZM hinzufügen|/tf-overridesb /role set:hzm:[Pega16]<°_°r°');
								return;
							}
					
							var msg = '°BB°_Übersicht der HZM:_°r° °#°';
					
							for(i=0;i<hzae.length;i++){
								msg=msg + i+' | '+ hzae[i].escapeKCode() +' | °R°_°>Entfernen|/role remark:hzm:'+ hzae[i].escapeKCode() +'<°_°r° °#°'
						
							}
							msg = msg+ '°#° °[000,175,225]°_°>HZM hinzufügen|/tf-overridesb /role set:hzm:[Pega16]<°_°r°';
					
							user.sendPrivateMessage(msg);
							return;
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "hzm"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ Channel-Eigentümer oder AppManager sein!');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							var vaspl = pspl[2].split(',');
					
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
					
								var users = appPersistence.getObject('hzae', []);
						
								if(users.indexOf(vaspl[i]) == -1){
									if(userAccess.exists(vaspl[i])){
															
										users.push(vaspl[i]);
										appPersistence.setObject('hzae', users);
//										var icon1 = KnuddelsServer.getFullImagePath('HZM.png');
//										vaspl[i].addNicklistIcon(icon1, 35);
										user.sendPrivateMessage(vaspl[i].escapeKCode()+' ist nun HZM. °R°_°>Rückgängig|/role remark:hzm:'+vaspl[i].escapeKCode()+'<°_°r°');
									} else {
										user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vertippt?');
									}
								} else {
									user.sendPrivateMessage('Da '+vaspl[i]+' bereits HZM ist, wurde dieser selbstredend _nicht erneut_ zum HZM gemacht...');
								}
							}
							return;
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "hzm"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ Channel-Eigentümer oder AppManager sein!');
								return;
							}
					
							var vaspl = pspl[2].split(',');
					
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
					
								var hzae = appPersistence.getObject('hzae', []);
					
								if(hzae.indexOf(vaspl[i]) == -1){
									user.sendPrivateMessage(vaspl[i] +' ist kein HZM.');
									return;
								}
					
								hzae.splice(hzae.indexOf(vaspl[i]), 1);
								appPersistence.setObject('hzae', hzae);
					
//								var icon1 = KnuddelsServer.getFullImagePath('HZM.png');
//								vaspl[i].removeNicklistIcon(icon1);
							
								user.sendPrivateMessage('Ich habe '+ vaspl[i].escapeKCode() +' soeben die HZM - Rolle entfernt. °R°_°>Rückgängig|/role set:hzm:'+ pspl[2].escapeKCode() +'<°_°r°');
							}
							return;
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "gast"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var userAccess = KnuddelsServer.getUserAccess();
					
							var vaspl = pspl[2].split(',');
					
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
						
								var users = appPersistence.getObject('sonderuser', []);
						
								if(users.indexOf(vaspl[i]) == -1){
						
									if(userAccess.exists(vaspl[i])){
								
										users.push(vaspl[i]);
										appPersistence.setObject('sonderuser', users);
//										var icon4 = KnuddelsServer.getFullImagePath('GAST.png');
//										vaspl[i].addNicklistIcon(icon4, 35);
										user.sendPrivateMessage('Ich habe '+vaspl[i]+' auf die SonderUser/Gast-Liste gesetzt. °R°_°>Rückgängig|/role remark:Gast:'+pspl[2].escapeKCode()+'<°_°r°');
						
									} else {
										user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
									}
								} else {
									user.sendPrivateMessage('Da der Nutzer '+vaspl[i]+' bereits als Gast eingetragen ist, habe ich diesen natürlich _nicht erneut eingetragen!_');
								}
							}
							return;
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "gast"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var sus = appPersistence.getObject('sonderuser', []);
					
							if(sus.length <= 0){
								user.sendPrivateMessage('Du hast keine Gäste/SonderUser auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /role set:Gast:[DerNeuanfang]<°_°r°');
								return;
							}
					
							var msg = '_°BB°Übersicht der Gäste/SonderUser:°r°_ °#°';
					
							for(i=0;i<sus.length;i++){
								msg=msg + i+' | '+ sus[i].escapeKCode() +' | °R°_°>Entfernen|/role remark:Gast:'+sus[i].escapeKCode()+'<°_°r° °#°'
							}
					
							msg = msg+ '°#° °[000,175,225]°_°>Gast/SonderUser hinzufügen|/tf-overridesb /role set:Gast:[Pega16]<°_°r°';
					
							user.sendPrivateMessage(msg);
							return;
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "gast"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var vaspl = pspl[2].split(',');
					
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
					
								var sus = appPersistence.getObject('sonderuser', []);
					
								if(sus.indexOf(vaspl[i]) == -1){
									user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+vaspl[i].escapeKCode()+')');
									return;
								}
					
								sus.splice(sus.indexOf(vaspl[i]), 1);
					
								appPersistence.setObject('sonderuser', sus);
					
//								var icon4 = KnuddelsServer.getFullImagePath('GAST.png');
//								vaspl[i].removeNicklistIcon(icon4);
					
								user.sendPrivateMessage('Ich habe '+vaspl[i].escapeKCode()+' aus der Liste entfernt. °R°_°>Rückgängig|/role set:Gast:'+vaspl[i].escapeKCode()+'<°_°r°');
							}
							return;
						}
				
						if(pspl[0].toLowerCase() == "set" && pspl[1].toLowerCase() == "vcm"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var vaspl = pspl[2].split(',');
					
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
					
								var userAccess = KnuddelsServer.getUserAccess();
					
								var users = appPersistence.getObject('vcms', []);
					
								if(users.indexOf(vaspl[i]) == -1){
					
									if(userAccess.exists(vaspl[i])){
							
										users.push(vaspl[i]);
										appPersistence.setObject('vcms', users);
//										var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
//										vaspl[i].addNicklistIcon(icon2, 35);
										user.sendPrivateMessage(vaspl[i]+' ist nun VCM. °R°_°>Rückgängig|/role remark:VCM:'+vaspl[i].escapeKCode()+'<°_°r°');
						
									} else {
										user.sendPrivateMessage('Dieser Nutzer existiert nicht. Hast Du Dich vielleicht vertippt?');
									}
								} else {
									user.sendPrivateMessage('Da '+vaspl[i]+' bereits VCM ist, habe ich '+vaspl[i]+' selbstverständlich _nicht erneut hinzugefügt!_');
								}
							}
							return;
						}
				
						if(pspl[0].toLowerCase() == "list" && pspl[1].toLowerCase() == "vcm"){
							if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var vcm = appPersistence.getObject('vcms', []);
					
							if(vcm.length <= 0){
								user.sendPrivateMessage('Du hast keine VCMs, daher kann ich Dir auch keine anzeigen! °R°_°>Jetzt hinzufügen|/tf-overridesb /role set:vcm:[James]<°_°r°');
								return;
							}
					
							var msg = '_°BB°Übersicht der VCMs:°r°_ °#°';
					
							for(i=0;i<vcm.length;i++){
								msg=msg + i+' | '+ vcm[i].escapeKCode() +' | °R°_°>Entfernen|/role remark:VCM:'+vcm[i].escapeKCode()+'<°_°r° °#°'
							}
					
							msg = msg+ '°#° °[000,175,225]°_°>VCM hinzufügen|/tf-overridesb /role set:VCM:[Pega16]<°_°r°';
					
							user.sendPrivateMessage(msg);
							return;
						}
				
						if(pspl[0].toLowerCase() == "remark" && pspl[1].toLowerCase() == "vcm"){
							if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Für diese Funktion musst Du _zwingend_ die HZM-Rolle haben oder Channel-Eigentümer/AppManager sein!');
								return;
							}
					
							var vaspl = pspl[2].split(',');
					
							for(i=0;i<vaspl.length;i++){
								vaspl[i] = vaspl[i].trim();
								msg=msg + vaspl[i]
					
								var vcm = appPersistence.getObject('vcms', []);
					
								if(vcm.indexOf(vaspl[i]) == -1){
									user.sendPrivateMessage('Den Nutzer kenne ich nicht ('+vaspl[i]+')');
						
								}
					
								vcm.splice(vcm.indexOf(vaspl[i]), 1);
					
								appPersistence.setObject('vcms', vcm);
					
//								var icon2 = KnuddelsServer.getFullImagePath('VCM.png');
//								vaspl[i].removeNicklistIcon(icon2);
							
								user.sendPrivateMessage('Ich habe '+vaspl[i].escapeKCode()+' aus der Liste entfernt. °R°_°>Rückgängig|/role set:VCM:'+vaspl[i].escapeKCode()+'<°_°r°');
							}
							return;
						} else {
							user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _VCM_, _Gast_');
							return;
						}

					} else {
						user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _VCM_, _Gast_');
					}
				} else {
					user.sendPrivateMessage('Die Syntax für die ROLE-Funktion lautet:°#°_/role set:ROLLE:NICK_ zum setzen von einer ROLLE bei NICK.°#°Mit _/role list:ROLLE_ zeigst Du Alle Nicks mit der angegebenen Rolle an.°#°Mit _/role remark:ROLLE:NICK_ entziehst Du Nick die Rolle.°#°°#°Es stehen aktuell folgende Rollen zur Verfügung: _HZM_, _CM_, _VCM_, _Gast_');
				}
			}
		};

		App.cmdsul = function(user,params){

			var appPersistence = KnuddelsServer.getPersistence();
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && !user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner()){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] == "help"){
					user.sendPrivateMessage('Die Syntax für die Stress-User-Listen-Funktion lautet:°#°_/sul add:NICK_ zum setzen von NICK auf die Stress-User-Liste.°#°Mit _/sul_ zeigst Du Alle Nicks auf der Stress-User-Liste an.°#°Mit _/sul delete:NICK_ entfernst Du NICK von der Stress-User-Liste.°#°°#°_Bitte beachten:_°#°das Anzeigen der Stress-User-Liste ist den CMs (und höher) gestattet. Das Verändern jedoch ausschließlich den HZM.');
					return;
				}
				
				if(pspl[0].toLowerCase() == "set" || pspl[0].toLowerCase() == "add"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && !user.isChannelOwner()){
						user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
						return;
						}
					
					var sulspl = pspl[1].split(',');
					
						var existendnicklists = appPersistence.getObject('extnl', []);
					
						for(i=0;i<sulspl.length;i++){
							sulspl[i] = sulspl[i].trim();
							msg=msg + sulspl[i]
							
							var userAccess = KnuddelsServer.getUserAccess();
					
							if(userAccess.exists(sulspl[i])){
								if(appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
									var users	= appPersistence.getObject('chksul', []);
									
									if(users.indexOf(sulspl[i]) > 0){
										user.sendPrivateMessage(sulspl[i]+' steht bereits auf der Liste! Daher habe ich '+sulspl[i]+' natürlich _nicht erneut_ hinzugefügt.');
										return;
									}
									if(users.indexOf(sulspl[i]) == -1){
										users.push(sulspl[i]);
										appPersistence.setObject('chksul', users);
										user.sendPrivateMessage('Ich habe '+ sulspl[i] +' auf die Stress-User-PRÜF-Liste gesetzt.');
									}

								}
								
								if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0){
									var users		= appPersistence.getObject('stressuserliste', []);
									var checksul	= appPersistence.getObject('chksul', []);
									
									if(users.indexOf(sulspl[i]) > 0){
										user.sendPrivateMessage(sulspl[i]+' steht bereits auf der Liste! Daher habe ich '+sulspl[i]+' natürlich _nicht erneut_ hinzugefügt.');
										return;
									}
									if(users.indexOf(sulspl[i]) == -1){
										users.push(sulspl[i]);
										appPersistence.setObject('stressuserliste', users);
										checksul.splice(checksul.indexOf(sulspl[i]), 1);
										appPersistence.setObject('chksul', checksul);
										user.sendPrivateMessage('Ich habe '+ sulspl[i] +' auf die Stress-User-Liste gesetzt. °R°_°>Rückgängig|/sul delete:'+ sulspl[i].escapeKCode() +'<°_°r°');
									}

								}
								} else {
									user.sendPrivateMessage('Der User '+sulspl[i]+' existiert nicht! -- Hast Du Dich vielleicht vertippt?');
								}
							}
						return;
				}	
/*					else {						
					user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
					return;
					}
*/
				if(pspl[0].toLowerCase() == "delete" && pspl[1] != undefined){
					if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner()){
						user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
						return;
						}
						
					var lsul = appPersistence.getObject('stressuserliste', []);
				
					if(lsul.indexOf(pspl[1]) == -1){
						user.sendPrivateMessage('Der Nutzer '+ pspl[1] +' ist kein StressUser!');
						return;
					}
					
					lsul.splice(lsul.indexOf(pspl[1]), 1);
				
					appPersistence.setObject('stressuserliste', lsul);
				
					user.sendPrivateMessage('Ich habe '+ pspl[1] +' von der Stress-User-Liste entfernt. °R°_°>Rückgängig|/sul set:'+ pspl[1].escapeKCode() +'<°_°r°');
					return;
				}
				
					if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner()){
						user.sendPrivateMessage('Leider hast Du nicht die Berechtigung für diese Funktion!');
						return;
					}
					
					var lsul	= appPersistence.getObject('stressuserliste', []);
					var chksul	= appPersistence.getObject('chksul', []);
				
					if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
						if(lsul.length <= 0){
							user.sendPrivateMessage('Es sind keine StressUser auf der Liste. Daher kann ich Dir keine anzeigen.');
							
							if(chksul.length > 0){
								var chkmsg = '_°BB°Liste der noch für die StressUserListe zu prüfenden Nicks:°r°_°##°"';
								for(i=0;i<chksul.length;i++){
									chkmsg=chkmsg + chksul[i] +', '
								}
								user.sendPrivateMessage(chkmsg);
							}
						return;
						}
					}
					if(lsul.length <= 0){
						user.sendPrivateMessage('Du hast keine StressUser auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /sul add:[James]<°_°r°');
						if(chksul.length > 0){
							var chksulmsg = '_°BB°Liste der noch für die StressUserListe zu prüfenden Nicks:°r°_°##°"';
							for(i=0;i<chksul.length;i++){
								chksulmsg=chksulmsg + i+' | '+ chksul[i].escapeKCode() +' | °R°_°>Bestätigen|/sul add:'+chksul[i].escapeKCode()+'<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
							}
							user.sendPrivateMessage(chksulmsg);
						}
						return;
					}
					
					if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
						
						var msg = '_°BB°Übersicht der StressUser:°r°_ °#°';
				
						for(i=0;i<lsul.length;i++){
							msg=msg + '°>'+ lsul[i].escapeKCode() +'|/nicklist '+lsul[i].escapeKCode()+':list<°, '
						}
						
//						msg = msg+ '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
						user.sendPrivateMessage(msg);

						
						if(chksul.length > 0){
							for(i=0;i<chksul.length;i++){
								var chksulmsg = '_°BB°Folgende Nutzer müssen noch geprüft werden, bevor sie auf die reguläre StressUserListe übernommen werden:°r°_°#°'
								chksulmsg=chksulmsg + '°>'+ chksul[i].escapeKCode() +'|/nicklist '+chksul[i].escapeKCode()+':list<°, '
							}
							user.sendPrivateMessage(chksulmsg);
						}
						return;
					}
						var msg = 'Übersicht der StressUser: °#°';
				
						for(i=0;i<lsul.length;i++){
							msg=msg + i+' | °>'+ lsul[i].escapeKCode() +'|/nicklist '+lsul[i].escapeKCode()+':list<° | °R°_°>Entfernen|/sul delete:'+ lsul[i].escapeKCode() +'<°_°r° °#°'
						}
						
						msg = msg+ '°#° °[000,175,225]°_°>StressUser Hinzufügen|/tf-overridesb /sul set:[James]<°_°r°';
						user.sendPrivateMessage(msg);
						
						if(chksul.length > 0){
							var chkmsg = '_°BB°Folgende Nutzer wurden von den CM für die StressUserListe nominiert:°r°_°##°'
								for(i=0;i<chksul.length;i++){
									chkmsg=chkmsg + i+' | '+ chksul[i].escapeKCode() +' | °R°_°>Bestätigen|/sul add:'+chksul[i].escapeKCode()+'<°_°r° °13°"(Bei Nichtzutreffen wird der Nick automatisch spätestens einen Monat nach Eintragung aus dieser Liste gelöscht.)"°r° °#°'
								}
							user.sendPrivateMessage(chkmsg);
						}				
						return;
					
			}
		};
		
		App.cmdcme = function(user,params){

		var appPersistence = KnuddelsServer.getPersistence();
		
		var userAccess = KnuddelsServer.getUserAccess();
//		var appPersistence = KnuddelsServer.getPersistence();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
		if (userAccess.exists(user)){
				var pspl = params.split('^');
				var msg	 = "";
							if(App.coDevs.indexOf(userkennung) < 0 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion. Um diese Funktion nutzen zu können, musst Du als HZA-angelegt sein. Hilfe hierzu findest Du unter _°>/role help|/role help<°_');
								user.sendPrivateMessage(userkennung);
								return;
								}				
					if(pspl[0] == "help"){
						user.sendPrivateMessage('Die Syntax für die CME-Funktion lautet:°#°_/cme self^[ZAHL]_°#°zum Ändern Deiner Texte nutze:°#°_/cme self^edit^ZAHL^TEXT_');
						return;
					}
					if(pspl[0] == "self" && pspl[1] != undefined && pspl[1] != "" && pspl[1] > 0 ){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
								return;
								}
						var cme	= [];
							cme[pspl[1]] = appPersistence.getObject('cme'+pspl[1]);
								if(cme[pspl[1]] == null){
									user.sendPrivateMessage('Es wurde noch kein Text für den Absatz '+ pspl[1] +' hinterlegt. Bitte hinterlege hierfür mit _/cme self^edit^'+ pspl[1] +'^TEXT_ einen enstprechenden Absatz.');
								return;
							} else {						
						Bot.sendPublicMessage(cme[pspl[1]]);
						return;
							}
					}
					
						if(pspl[0] == "self" && pspl[1] == "edit" && pspl[2] != undefined && pspl[2] != "" && pspl[2] > 0 ){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
								user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
								return;
								}

						var setcme = [];
							setcme[pspl[2]] = appPersistence.setObject('cme'+pspl[2], pspl[3]);
													
						user.sendPrivateMessage('Der Text für den CME Absatz '+ pspl[2] +' wurde nun auf den folgenden gesetzt:');
						user.sendPrivateMessage(pspl[3]);
						return;
						}
						
//						user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!°#°°#°Die Syntax des Befehles bekommst Du angezeigt, wenn Du _°>/cme help|/cme help<°_ eingibst.');
//						return;
					
						if(pspl[0] == "sys" && pspl[1] == "edit" && pspl[2] == "grant" && pspl[3] != undefined && pspl[3] != ""){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
								user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
								return;
								}
							
							var userAccess = KnuddelsServer.getUserAccess();
							
							if(userAccess.exists(pspl[3])){
								var users = appPersistence.getObject('cancme', []);
								users.push(pspl[3]);
								appPersistence.setObject('cancme', users);
								user.sendPrivateMessage('Ich habe '+ pspl[3] +' erlaubt, die System-CME zu nutzen. °R°_°>Rückgängig|/cme sys^edit^remark^'+pspl[3]+'<°_°r°');
								return;
							} else {
								user.sendPrivateMessage('Der Nutzer '+pspl[3]+' existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						}
						
						if(pspl[0] == "sys" && pspl[1] == "list" && pspl[2] == "granted"){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
								user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
								return;
							} else {
								user.sendPrivateMessage('Befehl falsch eingegeben! _°>/cme sys^list^granted|/cme sys^list^granted<°_');
								}
							
							var canusecme = appPersistence.getObject('cancme', []);
							
							if(canusecme.length <= 0){
								user.sendPrivateMessage('Du hast keine System-CME-berechtigten auf der Liste, die ich dir anzeigen könnte. °R°_°>Jetzt hinzufügen|/tf-overridesb /cme sys^edit^grant^[Pega16]<°_°r°');
								return;
							}
		
							var msg = 'Übersicht der System-CME-berechtigten: °#°';
		
							for(i=0;i<canusecme.length;i++){
							msg=msg + i+' | '+ canusecme[i] + ' | °R°_°>Entfernen|/cme sys^edit^remark^'+canusecme[i]+'<°_°r° °#°'
							}
			
							msg = msg+ '°#° °[000,175,225]°_°>Weiteren Nutzer hinzufügen|/tf-overridesb /cme sys^edit^grant^[Pega16]<°_°r°';
		
							user.sendPrivateMessage(msg);
							return;
						}
						
						if(pspl[0] == "sys" && pspl[1] == "edit" && pspl[2] == "remark" && pspl[3] != undefined && pspl[3] != " "){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1){
								user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
								return;
							}
							
							var canusecme = appPersistence.getObject('cancme', []);
							
							if(canusecme.indexOf(pspl[3]) == -1){
								user.sendPrivateMessage('Der Nutzer  ('+pspl[3]+') existiert nicht. Hast Du Dich vielleicht vertippt?');
								return;
							}
							canusecme.splice(canusecme.indexOf(pspl[3]), 1);
							appPersistence.setObject('cancme', canusecme);
							user.sendPrivateMessage('Ich habe soeben '+ pspl[3] +' untersagt, die System-CME weiterhin zu nutzen. °R°_°>Rückgängig|/cme sys^edit^grant^'+pspl[3]+'<°_°r°');
							return;
						}
					
						if(pspl[0] == "sys" && pspl[1] != undefined && pspl[1] != "" && pspl[1] > 0 && pspl[1] < 35){
							if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
							user.sendPrivateMessage('Falls Du die von _°>Pega16|/w Pega16|/m Pega16<°_ geschriebene CME nutzen möchtest, so schreibe bitte eine kurze /m an ihn.');
							return;
							}
							scme = [];
								scme[0] = "";
								scme[1] = "°18°_Herzlich Willkommen zur CM-Einführung. Nocheinmal herzlichen Glückwunsch zur überstandenen Wahl. Für die nächsten zwei Monate sorgt ihr in euren Channeln für Recht und Ordnung. Damit ihr wisst, wie das genau geht, werdet ihr heute kurz in euer Amt eingeführt. Wir werden die CM-Funktionen besprechen und auf bestimmte Problemsituationen eingehen. Zum Schluss können Fragen gestellt werden.#Diese Einführung wird gemacht, damit ihr euer CM-Amt besser ausführt und somit die Fehler, die ihr machen könnt, reduziert werden. Außerdem erhoffen wir uns dadurch eine bessere Zusammenarbeit. Jetzt bitten wir euch noch, aus eurem LC oder sonstigen Channel zu gehen und °[255,49,49]°nur hier §°18°_anwesend zu sein, Danke._";
								scme[2] = "°18°_Ihr seid ein Team. Bitte handelt auch als Team und seid keine Einzelkämpfer. Grade die CMs, die länger im Amt sind, sollten den Neuen helfen. Auch eure Freunde müsst ihr wie normale Chatter behandeln. Auch diese müssen bestraft werden, wenn sie was machen. Falls ihr damit nicht klar kommt, sagt dann einem Kollegen Bescheid, der sich dann um die Sache kümmern soll. Bei Fragen sind wir gerne eure Ansprechpartner. Falls ihr euch nicht 100%ig sicher seid, fragt lieber nach, wir helfen euch gerne. Screens und Beschwerden über Kollegen immer zuerst an uns HZAs. Sollten wir nicht online sein, hilft euch auch jeder andere Admin. Jedoch kümmern wir uns als HZA nicht um alles. Wir haben viele Sachen zu machen und können nicht ständig im Channel hocken und euch über die Schulter schauen. Mit Fragen, Problemen ect.pp. solltet ihr deswegen immer zu uns kommen. Auch können wir nicht riechen, „wann“ ihr eine CMV wollt, deswegen auch hier bitte auf uns zukommen!_";
								scme[3] = "°18°_Fragen können direkt gestellt werden, jedoch klären wir auch am Schluss noch einige auf, die nicht beantwortet wurden. Meistens klärt sich nämlich das meiste während der CMV *gg*. (gegebenenfalls Fragen AUFSCHREIBEN!)_";
								scme[4] = "°18°_Zum Thema Passwortweitergabe & Passwortklau °#°°#° °R°PW- Weitergabe: Strengstens verboten -> sofortiger Amtsentzug! °#°°#°§°18°> Passwortklau oder sonstiges abhanden kommen des PW’s: Sofort einem Admin melden um den Nick zur Sicherheit sperren lassen und dann mit 2. Nick bei uns melden, dass wir mit einem Teamleiter des PW Team reden können für die PW Neusetzung, als CM °[255,49,49]°NIE §°18°einen PW- Antrag machen!_";
								scme[5] = "°18°_Als CM hast du eine gewisse Vorbildfunktion. Deshalb solltest du dich mehr als alle anderen an die Regeln halten. Darüber hinaus darfst du niemanden dafür bestrafen, was du nicht selbst auch machst. Weiterhin darfst Du gegen die AGB und Spielregeln nicht verstoßen. Outen bei Mafia, Cheaten, Chatbotbenutzung und anderes würden zu einem Amtsentzug führen._";
								scme[6] = "°18°_Korrekte Notrufe! °#°°#°Immer wieder bekommen wir Notrufe die absolut unkorrekt sind -grummel-°#°Weiterleiten solltet ihr Notrufe die:°#° >eure Fähigkeiten übersteigen wie HP-Sperren, Nickklau etc. pp. Und die deutlich und freundlich formuliert sind.°#°°#°Nicht Weiterleiten solltet ihr Notrufe wie: °#°   > wann ist mein Foto frei °#°  > Ich hab mal en Problem helf mir °#°   > hier wird Werbung gemacht (ohne Namen und ohne Angabe was für Werbung)°#°  > sonstigen Schwachsinn wie total undeutlich und unfreundlich beleidigende Notrufe °#° > Notrufe die keinen genauen Grund angeben oder bei denen Namen oder Internetseite fehlen. °#°°#° In einen Notruf sollte, was ist wann, wo, wie und mit wem geschehen! Jedoch: Nicht immer gleich zum Admin rennen, bei Fragen die ihr selbst beantworten könnt, wie HP-„Problemchen“ (Verweis auf www.ramnip.net) Fotodinge (z.B. Verweis aufs Forum) Mentorenfragen ect.pp., sondern versuchen einfache Dinge wie diese den Chattern selber zu erklären_";
								scme[7] = "°18°_korrekte Notrufbeispiele:_°#°°#°_Jamesfaking (/afk Passwort) (bitte SOFORT /mute!):_ /notruf -> Beschwerde über andere Chat-Teilnehmer -> Botnutzung / Faking von Nachrichten -> NICK -> OK -> Nachricht(en) markieren -> HOAX-Versuch ChannelZ /afk Passwort öffentlich -> OK -> Notruf absenden°#°°#°_Jamesfaking (/knuddel NickXY) (bitte SOFORT /mute!):_ /notruf -> Beschwerde über andere Chat-Teilnehmer -> Botnutzung / Faking von Nachrichten -> NICK -> OK -> Nachricht(en) markieren -> HOAX-Versuch ChannelZ öffentlich /knuddel Passwort -> OK -> Notruf absenden°#°";
								scme[8] = "°18°_Denkt dran keine CM Infos weiterzugeben, denn das dürft ihr nicht.°#°°#°CM-Infos gehen niemanden außer CMs und Admins etwas an. Gebt diese nicht weiter. Auch dies kann und wird mit Amtsentzug bestraft werden. D.h., es geht auch keinen Chatter etwas an, wie viele mutes oder /cls er schon hat. Oder von welchem CM er gemutet wurde. Wurde er von einem Kollegen gemutet, nennt den Nick nicht dem Chatter, sondern sprecht den Kollegen darauf an dass XY entmutet werden möchte und er sich mit ihm in Verbindung setzen soll. Jeder Chatter sieht selbst in der /mute - Meldung, wer ihn gemutet hat!_";
								scme[9] = "°18°_Ach ja Stresser°#°°#°_Immer wieder & immer öfters tauchen sie auf, die heiß begehrten Stresser xD Am besten nicht aufregen lassen sondern verfahren wie bei jedem anderen die meisten die euch Schläge androhen haben eh nur eine große klappe im Chat °>sm_01.gif<°°#°°#°>Bei Bedrohungen aller Art /admin -> Beschwerde über Chatteilnehmer -> Bedrohung -> NICK -> Drohungen auswählen -> Kurz die Situation schildern, wie es dazu kam -> OK -> Notruf absenden -> OK °#° >Bei Stresser 2 mal Verwarnen ~> /mute°#° >Bei weiterem privat stressen als CM _NIEMALS_ /ig oder /block, sondern /pp NICK   dann Fenster klein machen später einfach schließen. Sollte er mehrere Leute ansprechen, geht /pp nicht mehr. Wenn’s nicht mehr einzudämmen ist, dann an einen Admin wenden._";
								scme[10] = "°18°_Extremistische Äußerungen_°#°°#°Bei solchen Sachen nicht lang rummachen sofort /cl NICK:GRUND + /notruf + _Screen_ wenn ihr euch nicht sicher seid ob der Grund zum cl reicht einfach ein cm kollege oder Admin fragen!  Am besten _/fa aet_._";
								scme[11] = "°18°_Funktion /mute:°#°_ Die /mute-Funktion wird eingesetzt um nervige Leute öffentlich mundtot zu machen. Mit der Funktion /mute !Nick wird dies wieder rückgängig gemacht. Hier einige kleine Einsetzungsmöglichkeiten der /mute-Funktion: °#°1.) Spammen von sinnlosen Texten°#°2.) Verfassungswidrige Äußerungen (siehe auch /cl)°#°3.) Beleidigungen eines Chatter gegenüber anderen (Genaueres nachher)°#°4.) Werbung (Genaueres nachher)°#°°#°Merke: In jedem Fall (bis auf Werbung) ist vorher eine Verwarnung angebracht. Außerdem ist es jedem CM selbst überlassen, ob er erst 2-3 verwarnen möchte oder direkt zum Mute greift. (beachte hierzu Allgemeines, Punkt 3!)_";
								scme[12] = "°18°_Funktion /cmute:_°#°°#° Mit ihr kann man das Farbig-, Groß- und Fettschreiben einer Person verhindert. Rückgängig zu machen mit der Funktion /cmute !Nick. Auch hier einige Einsetzungsmöglichkeiten: °#°1.) Dauerhaftes Farbig-, Groß- oder Fettschreiben°#°2.) Immer wieder großgeschriebene Texte einer Person.°#°3.) Wenn ein Text in Farbe als störend empfunden wird.°#° Merke: Hier sollte eine Verwarnung angebracht sein, wenn jedoch keine Zeit dafür bleibt, darf man auch direkt zum /cmute greifen. (Genaueres ist auch im CM-Forum zu finden)_";
								scme[13] = "°18°_Funktion /cl:_°#°°#° Mit dieser Funktion kann man jemanden des Channels verweisen. Anschließend kann der Gekickte (egal, mit welchem Nick) den Channel für einen Tag nicht mehr betreten. Deshalb sollte mit dieser Funktion am Vorsichtigsten agiert werden. Nun auch hier (lol) jetzt einige Einsetzungsmöglichkeiten:°#°1.) Verfassungswidrige Äußerungen (siehe auch AGBs)°#°2.) James-Faking (bitte erst NACH dem Absetzen des Notrufes!!!)°#°3.) Werbung für sexistische Internet-Seiten.°#°4.) Private Beleidigungen anderer Chatter (sollte überprüft werden)°#°°#° Merke: Bei Punkt 3.) sollte man sofort zum /cl greifen. Bei Punkt 1.) und 4.) sollte vorerst verwarnen._";
								scme[14] = "°18°_Funktion /fa:_°#°°#° Mit dieser Funktion könnt ihr die Liste der Admins/Teamler einsehen, die gerade online sind. Habt ihr Fragen, dann sucht euch von dieser Liste zuerst eure Hauptzuständigen Admins (HZA) heraus, wenn diese nicht on sind, dann hilft jeder andere Admin gerne weiter. Bitte habt auch Geduld, wenn es mal wieder länger dauert bis man einen Admin erwischt, aber man kennt das ja, viele Leute wollen was von wenigen Leuten die etwas höher gestellt sind obwohl sie auch nur 10 Finger haben und nicht springen können ;)_";
								scme[15] = "°18°_Funktion /admin:_°#°°#° Mit dieser Funktion könnt ihr nicht nur einen Adminruf absetzen, sondern auch die letzten sieben Notrufe einsehen, die in eurem Chanel abgesetzt worden sind. (Auch bekannt als ALTES Notrufsystem. Wurde allerdings aufs Neue weitergeleitet!)_";
								scme[16] = "°18°_Funktion /cm text:_°#° Mit dieser Funktion können neue Channeleigene Regeln festgelegt werden. Bevor ihr sie anwendet solltet ihr euren neuen Entwurf auf jeden Fall erst einem eurer HZAs zeigen._";
								scme[17] = "°18°_CM-Info:_°#°°#° Bestimmt ist euch beim Aufruf eines Profils schon die CM-Info aufgefallen. Dort steht drin, wer in welchem Channel von wem gechannellockt, gemutet oder gecolormutet ist, wer einen Gamelock hat oder die Anzahl von /cls und /mutes.°#° Ganz wichtig: Diese Informationen dürft ihr an niemanden rausgeben. Nichteinmal an den Chatter, den sie betreffen. Dasselbe gilt für die /cl, /mute und /fa-Listen. Diese Listen heißen CM-Infos, weil sie nur für CMs bestimmt sind und nicht für Chatter. Wer gegen diese einfache Regel verstößt, kann mit einem Entzug des CM-Amtes rechnen._";
								scme[18] = "°18°_Problemgebiet: Werbung:_°#°°#° MyChannel-Werbung ist das größte momentane Problemgebiet. Bisher haben wir uns darauf geeinigt, bei jeder Werbung sofort zu muten. Die Werber müssten nämlich alle wissen, dass Werbung verboten ist. Man kann es in den AGBs, der /h knigge und sogar in den MyChannel-Infos lesen. Außerdem solltet ihr die Werber per Adminruf (/admin grund) melden. Bitte nennt in diesem Adminruf folgende Faktoren:°#° 1.) Den Nick des Werbungmachers°#°2.) Für welchen Raum Werbung gemacht wurde.°#° Bei Werbung für Sexseiten wie z.B. www.nadine.eu.tp, reicht ein normaler Adminruf. °#° Bei Werbung für Fake-Seiten, Cheater-Seiten und Hacker-Seiten macht ihr dies bitte genauso. Erst /mute, dann /notruf._";
								scme[19] = "°18°_Problemgebiet: verbale Beleidigungen:_°#° Solche Beleidigungen sind ein weiteres Problemgebiet. Hier einige Formen dieses Problemgebietes:°#°1.) Werden wir CMs/Admins öffentlich beleidigt, sollte man damit routiniert umgehen können. Diese Personen sehen es meistens nur darauf ab, Aufmerksamkeit zu erhalten oder euch euer Amt möglichst schwer zu machen. Reagieren sollte man vorerst gar nicht, erst wenn er keine Anzeichen macht aufzuhören, sollte man (geht auch ohne Verwarnung) zum Mute greifen.°#°2.) Private Beleidigungen solltet ihr euch erst gar nicht gefallen lassen, einfach /pp Nick - das Fenster minimieren und das wäre auch geklärt.°#°3.) Beleidigungen gegenüber anderen Chattern, öffentlich oder privat, sollten so schnell wie möglich aus dem Weg geschafft werden. Entweder durch das Anwenden von /ig und/oder /block, oder schlimmstenfalls mit /mute bzw. /cl._";
								scme[20] = "°18°_Jamesfaking ist so ziemlich das schlimmste, was ein Chatter hier Verbrechen kann. Ein Jamesfaker muss sofort gemutet und ein Admin mittels /admin sofort alarmiert werden. Bitte diese User nicht kicken, da wir sonst nicht mehr sehen, ob sie noch mit anderen Nicks  online sind!_";
								scme[21] = "°18°_Wie mache ich einen Screenshot?:_°#°Hier eine kurze Anleitung:°#°1.) Zu der gewünschten Stelle scrollen, die fotografiert werden soll°#°2.) Drücke die Taste Druck (oder auch Print, S-Abf, ...) rechts oben auf der Tasstatur, neben den F-Tasten.°#°3.) Öffne das Programm Paint (Start -> Programme -> Zubehör -> Paint)°#°4.) Drücke die Tasten STRG + V gleichzeitig, wenn nötig lasse Paint das Bild automatisch vergrößern (Abfrage bestätigen).°#°5.) Speichere die Datei als jpeg oder besser noch, als .PNG (Datei -> Speichern unter -> Dateityp: jpeg/jpg oder eben Dateityp PNG).°#°6.) Schicke die Datei immer als Anhang (Das Büroklammernsymbol), niemals direkt in die eMail einfügen!_";
								scme[22] = "°18°_Verhalten gegenüber anderen:_°#° CM-Kollegen: Sicherlich, und das wird nicht zu vermeiden sein, wird es auch einige Uneinstimmigkeiten zwischen euch CMs geben. Ich möchte euch bitten, diese Sachen privat zu klären, denn nach außen müsst ihr als ein Team dastehen, schon mal damit euch niemand gegenseitig ausspielen kann. Des Weiteren darf kein CM von den anderen ausgeschlossen oder heruntergemacht werden. Es heißt nicht umsonst CM-Team. Und bitte helft euch bei Problemen gegenseitig oder fragt im Zweifel einfach uns! Auch wenn ich zum Beispiel nicht da bin, so antworte ich doch meist ziemlich zeitnah auf °>/m<°s, da ich diese auf dem Handy lese… Es ist nämlich nicht schlimm mit einer Situation nicht allein fertig zu werden, im Gegenteil, so wird der Teamgeist gestärkt. °>sm_01.gif<°_";
								scme[23] = "°18°_°#°Chatter/Freunde:_ Es ist klar, dass jeder Chatter gleich zu behandeln ist, egal ob Freund oder Feind. °#°_Newbies:_ Newbies müsst ihr freundlich entgegenkommen, auf ihre Fragen müsst ihr antworten können, dazu seid ihr schließlich da. °>sm_01.gif<°°#°_Admins:_ Hoheiten, behandelt uns wie Gott, auch wenn manche von uns nicht mal halb so gut sind wie ihr. :P Nene, es sollte euch schon bewusst sein, wie ihr euch Admins gegenüber verhält, oder? °>sm_10.gif<°_";
								scme[24] = "°18°_CM-Forum:_°#°°#° Das CM-Forum ist mit der Funktion /forum, alternativ F12, abrufbar. Dort loggt ihr euch unter Login mit dem Chatnick + Passwort ein, klickt auf Übersicht der Foren, auf Administration und dort auf Channelmoderatoren. Dort könnt ihr euch mit anderen CMs über die alltäglichen Problemstellungen austauschen und euch auch wichtige Informationen usw. einholen. Schaut auf jeden Fall mal vorbei. ;) (Forum is einfach toll °>fullheart.png<° *lieb* °>sm_10.gif<°)_";
								scme[25] = "°18°_Knuddelsphilosophie…_°#°°#°Ja, ich weiß, der ein oder andere sagt klar, er könne die Philosophie nicht in der Form unterschreiben… Hier stellt sich aber wiederum die Frage, WARUM dies so ist… Wir werden jetzt kurz die vier Säulen der _°BB°°>Knuddelsphilosophie|/philosophie<°_°r° °18°durchgehen und einige Interpretationen dazu besprechen…°#°°#°°#°_1. Gemeinsam Spaß haben_°#°Was versteht IHR darunter? °#°°R°_Antwort bitte öffentlich im Channel!_°r°_";
								scme[26] = "°18°_Richtig!°#° Knuddels lebt durch die Menschen, die hier ihre Freizeit miteinander verbringen. Wir alle haben unsere eigenen, individuellen Wünsche und Erwartungen, aber auch eine große Gemeinsamkeit: wir wollen zusammen Spaß haben!_";
								scme[27] = "°18°_2.) einander FREUNDLICH begegnen!_°#°Was versteht ihr darunter? °#°°R°_Antwort bitte öffentlich im Channel! °r°_";
								scme[28] = "°18°_RICHTIG! Um dies möglich zu machen, hat das Knuddelsteam im Laufe der Jahre einen Ort mit unzähligen Räumen und Möglichkeiten geschaffen. Diesen Ort gestalten wir mit Begegnungen, Gesprächen, Kreativität und unserer Individualität. Dabei achten wir aufeinander, nehmen Rücksicht und unterstützen uns gegenseitig._";
								scme[29] = "°18°_3.) Sich gegenseitig unterstützen_°#°Was versteht ihr darunter?°#°°R°_Antwort öffentlich im Channel!°r°_";
								scme[30] = "°18°_Richtig!_°#°Knuddels.de lebt durch ihre Mitglieder. Bei Knuddels haben wir die Möglichkeit, jeden Tag neue, tolle Menschen kennenzulernen. Wir lehnen niemanden prinzipiell ab, sondern begegnen uns vorurteilsfrei und freundlich. Unsere Unterstützung gilt insbesondere den Personen, die sich ehrenamtlich mit ganzem Herz für die Community einsetzen._";
								scme[31] = "°18°_Aufeinander Rücksicht nehmen…_°#°Ja, das ist wohl der Punkt, an dem die meisten sich an den Kopf packen und sagen „ihr könnt mich mal“… ABER was steckt tatsächlich dahinter?°#°°R°_Antwort wie immer öffentlich im Channel.°r°_";
								scme[32] = "°18°_Richtig…_°#°Wir verstehen, dass sich jeder von uns frei in seiner eigenen Art präsentiert, ausdrückt und auslebt. Kommt es dennoch zu Konflikten mit anderen Mitgliedern, so versuchen wir immer zuerst sie selber zu lösen, indem wir über leichte/minimale Fehler (Kleinstvergehen) hinwegsehen oder die Personen ignorieren, bzw. sie im /p auf ihren Fehler freundlich hinweisen.°#°Auf diese Weise kann Knuddels das sein, was wir uns alle wünschen: _ein Ort an dem wir uns wohl fühlen, so unterschiedlich wir auch sind._°#°°#°Das ist das Primäre Ziel… Das heißt aber keinesfalls, dass nun jeder alles tun und lassen kann/darf, was er/sie möchte!_";
								scme[33] = "°18°_Und nun wünsche ich euch  noch viel Spass in eurem Amt! Wenn ihr Fragen habt, fragt ruhig, es reißt euch keiner den Kopf ab! Achja.. wenn ihr denkt – wir sind nie online, falsch gedacht °>sm_01.gif<° wir sind oft zwischendurch und abends online, da wir meist viel zu tun haben, solltet ihr aber ein Gespräch wünschen o.ä. werden wir uns natürlich Zeit nehmen °>sm_00.gif<°_";
								scme[34] = "°18°_Kommen wir nun vom trockenen Theorieteil zum CMV-Teil dieser Veranstaltung… Nun seid ihr gefragt… was geht aktuell im Channel ab (ja, einiges weiß ich, bzw. wissen wir schon!)…? Gehen wir mal die wichtigsten Punkte GEMEINSAM durch, damit wir auch gemeinsam eine für alle passende Lösung finden können… °>sm_10.gif<°_";

							user.sendPrivateMessage('_Absatz '+ pspl[1] +' von 34_:');
						
							msg = scme[pspl[1]];
						
							Bot.sendPublicMessage(msg);
							return;
						}
							else {
						user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!°#°°#°Die Syntax des Befehles bekommst Du angezeigt, wenn Du _°>/cme help|/cme help<°_ eingibst.');
						}
			
				}
		};
		
/*	cme15: function (user, command) {
/*				var isAdmin 		= user.isInTeam("Admin");
			
		if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var appPersistence = KnuddelsServer.getPersistence();
		
			if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
		user.sendPrivateMessage ('_Absatz 15 von 39_:');
		Bot.sendPublicMessage ('Fragen zur /fa - Funktion (bitte einfach anklicken was Ihr für richtig haltet!):');
		user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 1 von 5|Wie rufst Du die Liste aller online befindlichen Teamler aus dem Antiextremismus-Team auf?|channel|on|30|0|/fa JuSchu|/fa Antiextremismusteam|/fa Antirechts|/fa Verify|/fa AET°#°°#°// Lösung:_ /fa AET (bitte ggfs. selbst erklären!)')
		user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme16<°_§ mit dem nächsten Absatz.');
	},
	
	cme16: function (user, command) {
/*				var isAdmin 		= user.isInTeam("Admin");
			
		if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var appPersistence = KnuddelsServer.getPersistence();
		
			if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
		user.sendPrivateMessage ('_Absatz 16 von 39_:');
		user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 2 von 5|Wie rufst Du die Liste \_aller\_ Teamler aus dem Antiextremismus-Team auf?|channel|on|30|0|/fa AET:list|/fa AET:all|/fa AET:zeig|/fa AET:alle|/fa AET:offline|/fa AET:gib°#°°#°// Lösung:_ /fa AET:all (bitte ggfs. selbst erklären!)')
		user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme17<°_§ mit dem nächsten Absatz.');
	},
	
	cme17: function (user, command) {
/*				var isAdmin 		= user.isInTeam("Admin");
			
		if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var appPersistence = KnuddelsServer.getPersistence();
		
			if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
		user.sendPrivateMessage ('_Absatz 17 von 39_:');
		user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 3 von 5|Wie rufst Du die Liste der Teamler aus dem Antiextremismus-Team auf, die _mindestens ADMIN sind?|channel|on|30|0|/fa AET:admin|/fa AET:all|/fa AET:6|/fa AET:admins|/fa AET:Admin,AET|/fa AET:gib°#°°#°// Lösung:_ /fa AET:6 (bitte ggfs. selbst erklären!)')
		user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme18<°_§ mit dem nächsten Absatz.');
	},
	
	cme18: function (user, command) {
/*				var isAdmin 		= user.isInTeam("Admin");
			
		if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var appPersistence = KnuddelsServer.getPersistence();
		
			if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
		user.sendPrivateMessage ('_Absatz 18 von 39_:');
		user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 4 von 5|Wie rufst Du die Liste der Teamler aus dem HZA-Team (uns HZAs übergeordnet!) auf?|channel|on|30|0|/fa HZA|/fa HZA:Singles 40+|/fa Singles 40+:HZA|/fa HZA-Team|/fa Verify|/fa Chatleitung°#°°#°// Lösung:_ /fa HZA (bitte ggfs. selbst erklären!)')
		user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme19<°_§ mit dem nächsten Absatz.');
	},
	
	cme19: function (user, command) {
/*				var isAdmin 		= user.isInTeam("Admin");
			
		if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}

		var appPersistence = KnuddelsServer.getPersistence();
		
			if(appPersistence.getObject('cancme', []).indexOf(user.getNick()) < 0 ){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
		user.sendPrivateMessage ('_Absatz 19 von 39_:');
		user.sendPrivateMessage ('_In die Zeile kopieren und absenden:°#°°#° /mod vote:Frage 5 von 5 (Letzte Frage!)|Wie rufst Du die Liste der Teamler aus dem Verifizierungs-Team (für Fakes etc. zuständig) auf?|channel|on|30|0|/fa HZA|/fa Fake|/fa Verify|/fa Verifizierungsteam|/fa Verify:6|/w Kolloid°#°°#°// Lösung:_ /fa Verify (bitte ggfs. selbst erklären!)')
		user.sendPrivateMessage ('Weiter gehts °R°_°>HIER|/cme20<°_§ mit dem nächsten Absatz.');
	},
*/	

		App.chatCommands.listcommands = function(user){
			var isAdmin 		= user.isInTeam("Admin");
/*			
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
*/

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;


		var appPersistence = KnuddelsServer.getPersistence();
		if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
			
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var ishzae = appPersistence.getObject('hzae', []);
//				ChanKey.push('offen');
				appPersistence.getObject('CMs', []);
					if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0) {
						user.sendPrivateMessage('CM-Befehle:°-°°#°°>/sul|/sul<° => zeigt die aktuelle Stress-User-Liste.°#°°-°');
						return;
					}
				appPersistence.getObject('hzae', []);
					if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0) {
						user.sendPrivateMessage('HZM-Dinge:°#°_°R°[Karten]°r°_ (HZM)§°#°°#°Auf der _°>/warn help|/warn help<°_ gibt es die genauen Informationen.°#°°#°°-°°#°_°R°[Zugangsbeschränkungsdinge]°r°_ (HZM)§°#°°#°_/role set:ROLLE:[Nick]_			=> Nick die ROLLE verleihen. Zum Beispiel um den Zutritt zum Channel zu gewähren (Channel im CM-Modus (s.o.))°#°Näheres hierzu auf der _°>/role help|/role help<°_°#°°#°°#°°#°_°R°[Stress-User-Liste]°r°_(Berechtigung in Klammer dahinter)§ °#°°#°_°>/sul|/sul<°_			=>	Stress-User-Liste aufrufen/anzeigen (CMs & HZM) °#°_/sul set:[Nick]_	=>	Nick auf die Stress-User-Liste setzen (HZM) °#°_/sul delete:[Nick]_	=>	Nick von der Stress-User-Liste entfernen. (HZM)');
//						return;
					}
/*					else if ( == "Closed") {
						user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
						return;
					}
*/
				if(user.isChannelOwner()) {
					user.sendPrivateMessage('°#°_Channelbefehle für den Channelowner:_°#°°#°°#°_/restricttext TEXT_ => Channelsperrtext für den Channel-CM-Modus definieren.°#°°#°_°>/restrictiontext|/restrictiontext<°_ => zeigt den aktuell definierten Channelsperrtext für den im CM-Modus befindlichen Channel an. Ist keiner definiert, wird der vorgegebene Standardtext angezeigt.°#°°#°°#°_/closedtext TEXT_ => definiert den Anzeigetext bei Channelbetretversuch im Falle eines geschlossenen Channels.°#°°#°_°>/showclosedtext|/showclosedtext<°_ => zeigt den definierten Sperrtext. Ist keiner definiert, liefert dieser Befehl ein _leeres Array_ zurück. In diesem Falle ist der vorgegebene Standardtext aktiv (siehe _°>/chlog|/chlog<°_)°#°°#°_°>/schriftart|/schriftart<°_ ändert die Bot-Schriftart.°#°°#°_/greetings on/off_ schaltet die Nutzer-Begrüßung ein/aus.°-°');
				}
				user.sendPrivateMessage('°-°_Channelbefehle (Admins/ChannelOwner/AppManager):_°#°°#°_°>/opened|/opened<°_		=> Channel für jedermann öffnen (Standardeinstellung!)°#°_°>/usual|/usual<°_		=> Channel nur für explizit zugelassene Personen öffnen (CM-Modus für HZM und CMs) [Mehr unter \"Zugangsbeschränkungsdinge\"]°#°_°>/closed|/closed<°_		=> Channel abschließen. Dies hat zur Folge, dass _KEINER_ mehr den Channel betreten kann!°#°°#°_°>/chanstate|/chanstate<°_	=> Anfrage in welchem Status sich der Channel gerade befindet (Kontrolle damit man den Channel nicht verschlossen verlässt!)');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
	};
	
/*	pffbs: function (user, command) {
 		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>'+pfad+'sascha.gif<°';
//		var user = user.getNick();
//		var user = KnuddelsServer.getAppDeveloper();
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
//		user.sendPostMessage('Sascha´s Postfach-Füller', 'Du hast Dir gewünscht, dass ich das Postfach voll mache.');
		user.sendPrivateMessage('Postfach-Blockade by ' + saschaus);
	},
*/	
	
//'°>{font}ArialBold<28B°_' + 
//'°>{font}ArialBold<28R°_' + 
// Chat Commands ALL Channel Users

	//Version anzeigen
	App.chatCommands.ver = function (user, command) {
 		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>'+pfad+'sascha.gif<°';
		var isHzmApp		= appPersistence.getObject('isHzmApp', ['']);
			if(isHzmApp == "on"){
		if(isabRC == "a"){
			var Version = "HZM-ALPHA-APP"
		}
		else if(isabRC == "b"){
			var Version = "HZM-BETA-APP"
		}
		else {
			var Version = "HZM-App"
		}
	
		if(isabRC == "a"){
			var isVersion = extver+".0.4.1";
		}
		else if(isabRC == "b"){
			var isVersion = extver+".0.4";
		}
		else {
			var isVersion = extver;
		};
	} else {
		if(isabRC == "a"){
			var Version = "PCC-ALPHA-APP"
		}
		else if(isabRC == "b"){
			var Version = "PCC-BETA-APP"
		}
		else {
			var Version = "PublicChatControl-App"
		}
	
		if(isabRC == "a"){
			var isVersion = extver+".0.4.1";
		}
		else if(isabRC == "b"){
			var isVersion = extver+".0.4";
		}
		else {
			var isVersion = extver;
		};		
	};
		user.sendPrivateMessage('Die _'+Version+'_ hat die _Version '+isVersion+'_ und wurde von _°>Pega16|/m Pega16|/w Pega16<°_ geschrieben.#Anfragen oder Feedback zur App per /m (oder Rechtsklick auf den Namen!)°#°°#°°BB°_Du kannst Diese App auch bei Dir installieren! - /m an Pega16!_°r°°#°°#°Liebe Grüße,°#°' + saschaus);
	};
	
	App.cmdbp = function(user, params){
		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;
		
		var appPersistence = KnuddelsServer.getPersistence();
		
		var userAccess = KnuddelsServer.getUserAccess();
		
		var pspl = params.split(':');
		var msg	 = "";
		
		if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()){
			user.sendPrivateMessage('Dies kann nur der Channeleigentümer.');
			return;
		}
		
		var puser = pspl[0].toKUser();
		
		if(pspl[0] != undefined && pspl[1] != undefined){
			if(user.getNick() == puser || user.getNick() == "Pega16" || user.getNick() == "Pega" || user.getNick() == "Security"){
				if (userAccess.exists(puser)){
					logger.info(user.getNick() + ' schreibt per Bot-Private an '+ pspl[0] +' den Text: '+ pspl[1]);
					puser.sendPrivateMessage(pspl[1]);
					user.sendPrivateMessage('Bot-Privatnachricht an °BB°_°>_h'+pspl[0]+'|/w '+pspl[0]+'|/m '+pspl[0]+'<°_°r° erfolgreich versandt.°#°°BB°_Inhalt Deiner übermittelten Nachricht:_°r° '+pspl[1]);
				} else {
					user.sendPrivateMessage('Der Nutzer ist unbekannt... Vertippt?');
				}
				return;
			}
			
			if (userAccess.exists(puser)){
				logger.info(user.getNick() + ' schreibt per Bot-Private an '+ pspl[0] +' den Text: '+ pspl[1]);
				puser.sendPrivateMessage(pspl[1]+'°#°°09°(übersandt durch °>_h'+user.getNick()+'|/w '+user.getNick()+'/m '+user.getNick()+'<°)°r°');
				user.sendPrivateMessage('Bot-Privatnachricht an °BB°_°>_h'+pspl[0]+'|/w '+pspl[0]+'|/m '+pspl[0]+'<°_°r° erfolgreich versandt.°#°°BB°_Inhalt Deiner übermittelten Nachricht:_°r° '+pspl[1]);
			} else {
				user.sendPrivateMessage('Der Nutzer ist unbekannt... Vertippt?');
			}
			
		} else {
			user.sendPrivateMessage('Die Syntax für den Befehl lautet: _/bp Nick:TEXT_');
			return;
		}
	}
	
	if(srvID == "knuddelsDEV"){
		App.chatCommands.modulemanager = function(user,params,func){
			var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
			var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
			var userkennung		= UserID+'.'+srvID;

			var appPersistence = KnuddelsServer.getPersistence();
		

			if(!user.isChannelOwner() && !user.isAppManager() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0) {
				user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
				return;
			}
			
			logger.info(user.getNick()+' öffnet ModuleManager.');
		
			var modulemanager = AppContent.popupContent(new HTMLFile('ModuleManager/index.html'), 850, 550);
			user.sendAppContent(modulemanager);
		
		};
	};
	
	App.chatCommands.sofu = function (user, params, func) {
		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>'+pfad+'sascha.gif<°';

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			var appPersistence = KnuddelsServer.getPersistence();
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();
		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(pspl[0] == "help"){
					user.sendPrivateMessage('Die Syntax für die SonderFunktionen lautet:°#°_/sofu FUNKTION_.°#°');
					return;
				}
				
/*				if(pspl[0] == "set"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()){
						user.sendPrivateMessage('Sry, das wäre sinnfrei, wenn das jeder könnte... °>sm_01.gif<°');
					}
				}
*/				
				if(pspl[0] == "UUID" && pspl[1] != undefined){
					var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
					var UserID			= KnuddelsServer.getUserAccess().getUserId(pspl[1].toKUser());

					var userkennung		= UserID+'.'+srvID;

					user.sendPrivateMessage('_°BB°Die Unique-UserID von °>_h'+pspl[1]+'|/w '+pspl[1]+'|/m '+pspl[1]+'<° lautet: °RR°'+userkennung+'_°r°');
				}
					
				if(pspl[0] == "blacklist" && pspl[1] == undefined){
										
					user.sendPrivateMessage('die Syntax für die Blackliststeuerung lautet: _/sofu blacklist:on/off_');
				}
					
				if(pspl[0] == "blacklist" && pspl[1] == "on" || pspl[1] == "off"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()){
						user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
						return;
					}
				
				//	var discoconfetti	= appPersistence.getObject('discoconfetti' ['']);
				//	var blloo			= appPersistence.getObject('blacklistonoff');
				
					appPersistence.setObject('blacklistonoff', pspl[1]);
				
					if(pspl[0] == "blacklist" && pspl[1] == "on"){
//						appPersistence.setObject('blacklistonoff', pspl[1]);
						var blloo = appPersistence.getObject('blacklistonoff');
//						KnuddelsServer.refreshHooks();
						if(typeof App.chatCommands[pspl[0]] === "function"){
							return false;	
						}
						App.chatCommands[pspl[0]] = App.cmdblacklist;
						setTimeout(function(){
						KnuddelsServer.refreshHooks();
						}, 1000)
						user.sendPrivateMessage('Ich habe die _Blacklist_ soeben _aktiviert._ ('+ blloo +')');
					}
					
					if(pspl[0] == "blacklist" && pspl[1] == "off"){
//						appPersistence.setObject('blacklistonoff', "off");
						var blloo = appPersistence.getObject('blacklistonoff');
						delete App.chatCommands.blacklist;
							KnuddelsServer.refreshHooks();
						
						user.sendPrivateMessage('Ich habe die _Blacklist_ soeben _deaktiviert_. ('+ blloo +')');
					}
									var blloo = appPersistence.getObject('blacklistonoff');
//						KnuddelsServer.refreshHooks();
				}
				
				if(pspl[0] == "hzm" && pspl[1] == "on" || pspl[1] == "off"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()){
						user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
						return;
					}
				
				//	var discoconfetti	= appPersistence.getObject('discoconfetti' ['']);
				//	var blloo			= appPersistence.getObject('blacklistonoff');
				
					appPersistence.setObject('isHzmApp', pspl[1]);
				
					if(pspl[0] == "hzm" && pspl[1] == "on"){
//						appPersistence.setObject('blacklistonoff', pspl[1]);
						var isHzmApp = appPersistence.getObject('isHzmApp');
//						KnuddelsServer.refreshHooks();
						if(typeof App.chatCommands['role'] === "function"){
							return false;	
						} else if(typeof App.chatCommands['warn'] === "function"){
							return false;	
						} else if(typeof App.chatCommands['sul'] === "function"){
							return false;	
						} else if(typeof App.chatCommands['cme'] === "function"){
							return false;	
						}
						App.chatCommands['role'] = App.cmdrole;
						App.chatCommands['warn'] = App.cmdwarn;
						App.chatCommands['sul'] = App.cmdsul;
						App.chatCommands['cme'] = App.cmdcme;
						
						setTimeout(function(){
						KnuddelsServer.refreshHooks();
						}, 1000)
						user.sendPrivateMessage('Ich habe die _HZM-Funktionen_ soeben _aktiviert._ ('+ isHzmApp +')');
						var devel = KnuddelsServer.getAppDeveloper();
						var oldVer = Version;
						var isHzmApp		= appPersistence.getObject('isHzmApp', ['']);
			if(isHzmApp == "on"){
		if(isabRC == "a"){
			var Version = "HZM-ALPHA-APP"
		}
		else if(isabRC == "b"){
			var Version = "HZM-BETA-APP"
		}
		else {
			var Version = "HZM-App"
		}
	
		if(isabRC == "a"){
			var isVersion = extver+".0.4.1";
		}
		else if(isabRC == "b"){
			var isVersion = extver+".0.4";
		}
		else {
			var isVersion = extver;
		};
	} else {
		if(isabRC == "a"){
			var Version = "PCC-ALPHA-APP"
		}
		else if(isabRC == "b"){
			var Version = "PCC-BETA-APP"
		}
		else {
			var Version = "PublicChatControl-App"
		}
	
		if(isabRC == "a"){
			var isVersion = extver+".0.4.1";
		}
		else if(isabRC == "b"){
			var isVersion = extver+".0.4";
		}
		else {
			var isVersion = extver;
		};		
	};
						devel.sendPostMessage('Versionswechsel in Channel '+channel, 'Im Channel _°>'+channel+'|/go '+channel+'<°_ wurde gerade durch _°>'+ cown +'|/w '+cown+'|/m '+cown+'<°_ die _PCC-App_ in der _Version '+isVersion+'_ durch die _'+Version+'_ °R°_ersetzt_.');
					}
					
				
					if(pspl[0] == "hzm" && pspl[1] == "off"){
//						appPersistence.setObject('blacklistonoff', "off");
						var isHzmApp = appPersistence.getObject('isHzmApp');
						delete App.chatCommands.role;
						delete App.chatCommands.warn;
						delete App.chatCommands.sul;
						delete App.chatCommands.cme;
						
							KnuddelsServer.refreshHooks();
						
						user.sendPrivateMessage('Ich habe die _HZM-Funktionen_ soeben _deaktiviert_. ('+ isHzmApp +')');
					}
				}
				
				if(pspl[0].toLowerCase() == "botprivate" && pspl[1] == "on" || pspl[1] == "off"){

					appPersistence.setObject('botprivate', pspl[1]);
					
					if(pspl[1].toLowerCase() == "on"){
						user.sendPrivateMessage('Bot-Privatnachricht aktiviert.');
						if(typeof App.chatCommands['bp'] === "function"){
							return false;
						}
						App.chatCommands['bp'] = App.cmdbp;
						KnuddelsServer.refreshHooks();
						}
					
					if(pspl[1].toLowerCase() == "off"){
						user.sendPrivateMessage('Bot-Privatnachricht deaktiviert.');
						delete App.chatCommands.bp;
						KnuddelsServer.refreshHooks();
					}
				}
				
				if(pspl[0] == "noshow" && [0,1].indexOf(pspl[2])){
//				if(pspl[0] == "noshow"){
					var noshowquery		= appPersistence.getObject(pspl[1]);
					appPersistence.setObject(pspl[1], pspl[2]);
					
					if(pspl[2] == 1){
						user.sendPrivateMessage('_°BB°Die '+pspl[1]+'-Nachricht ist nun ausgeschaltet. zum Einschalten klicke °RR°°>HIER|/sofu noshow:'+pspl[1]+':0<°_°r°°##°');
					}
					if(pspl[2] == 0){
						user.sendPrivateMessage('_°BB°Die '+pspl[1]+'-Nachricht ist nun eingeschaltet. zum ausschalten klicke °RR°°>HIER|/sofu noshow:'+pspl[1]+':1<°_°r°°##°');
						if(pspl[1] == "ownermessage"){
							user.sendPrivateMessage(ownertext);
						}
						if(pspl[1] == "hzminfo"){
							user.sendPrivateMessage(hzmtext);
						}
					}
				}
				
				if(pspl[0] == "keepOnline" && pspl[1] != undefined && pspl[1] != null && pspl[2] == "add"){
					if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isAppManager() && !user.isChannelOwner() && !user.isChannelModerator()){
						user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
						return;
					}
			
			
					var userAccess = KnuddelsServer.getUserAccess();
					var users = appPersistence.getObject('keeponline', []);
			
					var vaspl = pspl[1].split(',');
								
					for(i=0;i<vaspl.length;i++){
						vaspl[i] = vaspl[i].trim();
						msg=msg + vaspl[i]
				
						if(users.indexOf(vaspl[i]) == -1){
							if(userAccess.exists(vaspl[i])){
								var users	= appPersistence.getObject('keeponline', []);
				
								users.push(vaspl[i]);
								appPersistence.setObject('keeponline', users);
//								var icon3 = KnuddelsServer.getFullImagePath('CM.png');
//								vaspl[i].addNicklistIcon(icon3, 35);
								user.sendPrivateMessage('Ich habe '+ vaspl[i] +' auf die KeepOnline-Liste gesetzt.');
							} else {
								user.sendPrivateMessage('Der Nick '+vaspl[i]+' existiert nicht. Hast Du Dich vielleicht vertippt?');
							}
						} else {
							user.sendPrivateMessage('Da der Nutzer '+vaspl[i]+' bereits auf der KeepOnline-Liste steht, wurde dieser selbstverständlich _nicht erneut hinzugefügt!_');
						}
					}
					return;
				}
				
				if(pspl[0] == "keepOnline" && pspl[1] != undefined && pspl[2] == "del"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && !user.isChannelModerator()){
						user.sendPrivateMessage('Für diese Funktion musst Du Channeleigentümer sein.');
						return;
					}
				
					var cms = appPersistence.getObject('keeponline', []);
		
					var vaspl = pspl[1].split(',');
		
					for(i=0;i<vaspl.length;i++){
						vaspl[i] = vaspl[i].trim();
						msg=msg + vaspl[i]
				
						if(cms.indexOf(vaspl[i]) == -1){
							user.sendPrivateMessage('Der Nutzer '+ vaspl[i] +' steht gar nicht auf der Liste!)');
							return;
						}
			
						cms.splice(cms.indexOf(vaspl[i]), 1);
		
						appPersistence.setObject('keeponline', cms);
//						var icon3 = KnuddelsServer.getFullImagePath('CM.png');
//						vaspl[i].removeNicklistIcon(icon3);
			
						user.sendPrivateMessage('Ich habe '+ vaspl[i] +' von der keepOnline-Liste entfernt.');
					}
					return;
				}
				
				if(pspl[0].toLowerCase() == "sysarray"){
					var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
					var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
				
					var userkennung		= UserID+'.'+srvID;

					if(!user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator() && App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0){
						user.sendPrivateMessage('Für diese Aktion musst Du _mindestens_ MCM sein!');
						return;
					}
					
					var develmsg = '°BB°_Die Nicks des Programmierers der App lauten:°r°_°##°';
					var hdevelmsg = '°BB°_Im °RR°NOTFALL_ °r°°10°(also wenn alle angeschriebenen(!) Programmierer nicht binnen 3 Stunden auf /ms antworten!)°r° _°BB°kann über folgende User noch eine Notfallnachricht übermittelt werden:_°r°°##°';

					if(App.coDevs.indexOf(userkennung) >= 0 || App.hiddenCoDev.indexOf(userkennung) >= 0){
//						logger.info('entered CoDev >= 0');
						if(srvID == "knuddelsDE"){

							msg = develmsg+"";
							for(i=0;i<App.coDevsNicks.length;i++){
								msg=msg + '_°>_h'+App.coDevsNicks[i]+'|/w '+App.coDevsNicks[i]+'|/m '+App.coDevsNicks[i] +'<°_ (_°BB°'+KnuddelsServer.getUserAccess().getUserId(App.coDevsNicks[i].toKUser())+'.'+srvID+'°r°_)°#° '
							}
							hmsg = hdevelmsg+"";
							for(i=0;i<App.hiddenCoDevNicks.length;i++){
								hmsg=hmsg + '_°>_h'+App.hiddenCoDevNicks[i]+'|/w '+App.hiddenCoDevNicks[i]+'|/m '+App.hiddenCoDevNicks[i] + '<°_ (_°BB°'+KnuddelsServer.getUserAccess().getUserId(App.hiddenCoDevNicks[i].toKUser())+'.'+srvID+'°r°_)°#° '
							}
							user.sendPrivateMessage(msg);
							user.sendPrivateMessage(hmsg);
						}
						
						if(srvID == "knuddelsAT" || srvID == "knuddelsDEV"){

							msg = develmsg+"";
							msg=msg + '_°>_h'+App.coDevsNicks[0]+'|/w '+App.coDevsNicks[0]+'|/m '+App.coDevsNicks[0]+'<°_ (_°BB°'+KnuddelsServer.getUserAccess().getUserId(App.coDevsNicks[0].toKUser())+'.'+srvID+'°r°_)°#° '
							user.sendPrivateMessage(msg);
						}
					}
					
					if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && user.isChannelOwner() && user.isAppManager() && user.isChannelModerator()){
//							logger.info('entered non-Dev.');
						if(srvID == "knuddelsDE"){
/*							develmsg=develmsg + '°>Pega16|/w Pega16|/m Pega16<°, °>Pega|/w Pega|/m Pega<°, °>Security|/w Security|/m Security<°';
							
							hdevelmsg=hdevelmsg + '°>Son of a Glitch|/w Son of a Glitch|/m Son of a Glitch<°';
*/

							msg = develmsg+"";
							for(i=0;i<App.coDevsNicks.length;i++){
								msg=msg + '_°>_h'+App.coDevsNicks[i]+'|/w '+App.coDevsNicks[i]+'|/m '+App.coDevsNicks[i] +'<°_ , '
							}
							hmsg = hdevelmsg+"";
							for(i=0;i<App.hiddenCoDevNicks.length;i++){
								hmsg=hmsg + '_°>_h'+App.hiddenCoDevNicks[i]+'|/w '+App.hiddenCoDevNicks[i]+'|/m '+App.hiddenCoDevNicks[i] + '<°_, '
							}
							user.sendPrivateMessage(msg);
							user.sendPrivateMessage(hmsg);
						}
							
						if(srvID == "knuddelsAT" || srvID == "knuddelsDEV"){
								msg = develmsg+"";
								
									msg=msg + App.coDevsNicks[0]
								
								user.sendPrivateMessage(msg);
							}
							
						}
				}
/*
						else {
							develmsg=develmsg + '°>Pega16|/w Pega16|/m Pega16<° (400082.knuddelsDE)°#°°>Pega|/w Pega|/m Pega<° (61187660.knuddelsDE)°#°°>Security|/w Security|/m Security<° (412884.knuddelsDE)';
							
							hdevelmsg=hdevelmsg + '°>Son of a Glitch|/w Son of a Glitch|/m Son of a Glitch<° (59063840.knuddelsDE)°#°';

						}
						
						user.sendPrivateMessage(develmsg+'°##°'+hdevelmsg);
						}
*/ 
				if(pspl[0] == "EAU"){
					if(App.coDevs.indexOf(userkennung) < 0 && App.hiddenCoDev.indexOf(userkennung) < 0 && !user.isChannelOwner()){
						logger.warn(user.getNick()+' versuchte gerade die EAU abzurufen!');
						user.sendPrivateMessage('Dies darf zu Diagnosezwecken nur der Channel-Eigentümer!');
						return;
					} else {
//						logger.info(user.getNick()+' launches EAU.');
						
						var parameters = {
							onEnd: function(accessibleUserCount){
								user.sendPrivateMessage(EAU);
							}
						};
						
						var EAU = "_°BB°Accessible Users:_°r° °##°";
						var eaus = "";
						userAccess.eachAccessibleUser(function(eaus){
//							eaus = eaus.escapeKCode();
							eaus = '_'+eaus.getProfileLink()+'_   (_°BB°'+KnuddelsServer.getUserAccess().getUserId(eaus)+'.'+srvID+'°r°_)°#°' 
							EAU = EAU + eaus
//							appPersistence.updateObject('EAU', u);
													
						}, parameters);
//						user.sendPrivateMessage(EAU);
//						user.sendPrivateMessage(appPersistence.getObject('EAU'));
					}
				}
					
				
				if(pspl[0] == "matchico" && pspl[1] == "on" || pspl[1] == "off"){
				
				//	var discoconfetti	= appPersistence.getObject('discoconfetti' ['']);
				//	var blloo			= appPersistence.getObject('blacklistonoff');
				
					appPersistence.setObject('matchicobeta', pspl[1]);
//					var matchicofunc = appPersistence.getObject('matchicofunction');
					
					if(pspl[0] == "matchico" && pspl[1] == "on"){
//						appPersistence.setObject('matchicobeta', "on");
						if(typeof App.chatCommands[pspl[0]] === "function"){
							return false;	
						}
						App.chatCommands[pspl[0]] = App.chatCommands.listcommands;
						setTimeout(function(){
						KnuddelsServer.refreshHooks();
						}, 1000)
						
						var matchicofunc = appPersistence.getObject('matchicobeta');

//						KnuddelsServer.refreshHooks();
						user.sendPrivateMessage('Ich habe die Funktion _°>/matchico|/matchico<°_ soeben _aktiviert._ ('+ matchicofunc +')');
					}
					
					if(pspl[0] == "matchico" && pspl[1] == "off"){
//						appPersistence.setObject('matchicobeta', "off");
						var matchicofunc = appPersistence.getObject('matchicobeta');
							delete App.chatCommands.matchico;
							KnuddelsServer.refreshHooks();
						
						user.sendPrivateMessage('Ich habe die Funktion _°>/matchico|/matchico<°_ soeben _deaktiviert_. ('+ matchicofunc +')');
					}
//											KnuddelsServer.refreshHooks();
				}
				
				if(pspl[0] == "spielerei" && pspl[1] == undefined){
					user.sendPrivateMessage('die Syntax lautet: _/sofu spielerei:on/off_. Diese Funktion wirkt sich auf die Funktionen _Disco_ und _Confetti_ aus.');
				
				}
				
				if(pspl[0] == "spielerei" && pspl[1] == "on" || pspl[1] == "off" || pspl[1] == "HZM"){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()){
						user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
						return;
					}
					appPersistence.setObject('discoconfetti', pspl[1]);
					
					if(pspl[0] == "spielerei" && pspl[1] == "on"){
						user.sendPrivateMessage('Ich habe die Funktionen _°>/disco|/disco<°_ und _°>/confetti|/confetti<°_ soeben _aktiviert_.');
					}
					if(pspl[0] == "spielerei" && pspl[1] == "off"){
						user.sendPrivateMessage('Ich habe die Funktionen _°>/disco|/disco<°_ und _°>/confetti|/confetti<°_ soeben _deaktiviert_.');
					}
					if(pspl[0] == "spielerei" && pspl[1] == "HZM"){
						user.sendPrivateMessage('Ab sofort können nur noch HZM _°>/disco|/disco<°_ und _°>/confetti|/confetti<°_ ausführen.');
					}
					return;
				}
				
				if(pspl[0] == "dev" && pspl[1] != undefined){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !user.isAppManager() && !user.isChannelModerator()){
						user.sendPrivateMessage('Dies ist ein Statistischer Befehl für den DEV.');
						return;
					}
					var userAccess = KnuddelsServer.getUserAccess();
					var appPersistence = KnuddelsServer.getPersistence();
		
					if (userAccess.exists(user)){
						var getpers = appPersistence.getObject(pspl[1], []);

//						appPersistence.deleteObject(pspl[1]);
						user.sendPrivateMessage('Die Persistenze '+pspl[1]+' enthält folgendes:°##°'+getpers);
						return;
					} else {
						user.sendPrivateMessage('/sofu dev:PERSISTENZNAME');
					}
		}
		
/*				
				if(pspl[0] == "James"){
					user.sendPrivateMessage('Na DAS war jetzt auch einfach, oder? °>sm_01.gif<°');
					Bot.sendPublicMessage('James ist immer mal ganz praktisch für die Umsorgung bei langen CMVs... Wer soll denn sonst die Teller und Gläser alle spülen? °>sm_01.gif<°');
				}
				
				if(pspl[0] == "Ostern"){
					user.sendPrivateMessage('Sehr schön... die nächste Funktion gefunden... °>sm_01.gif<°');
					Bot.sendPublicMessage('Zu Ostern gibts im Jahre 2017 etwas ganz besonderes bei Knuddels... Nämlich dieses Gewinnspiel für alle HZM-App-Nutzer! Schön, dass '+ user.getProfileLink() +' auch mit macht!');
				}
				
				if(pspl[0] == "sieben"){
					user.sendPrivateMessage('oha! Du bist gut! °>sm_00.gif<°');
					Bot.sendPublicMessage(user.getProfileLink() +' hat gerade die magische Zahl gefunden...');
				}
				
				if(pspl[0] == "Sascha"){
					user.sendPrivateMessage('Hier wirds knifflig... das ist ein Rätsel! -- siehe die öffentliche Nachricht! -- Dein Ergebnis sendest Du bitte in Deiner Gewinnspielteilnahme-/m bei dieser Funktion in Klammern dahinter an Pega16.');
					Bot.sendPublicMessage('Was schätzt Du, wieviele Saschas gibts in der knuddeligen Administration in Karlsruhe? (nicht Admins, sondern rein aufs Büro in Karlsruhe bezogen!)');
				}
				
				if(pspl[0] == "Nina"){
					user.sendPrivateMessage('Sehr schön! Du hast anscheinend viel Phantasie! -- Weiter so! °>sm_10.gif<°');
					Bot.sendPublicMessage('_Nina_ ist bekannt für ihre strenge, aber gerechte Art und aufgrund dieser Art nicht bei allen beliebt... Das muss sie aber auch nicht... Nur kannst Du mir sagen, wer diese Nina ist? (Antwort bei der Gewinnspiel-/m bei dieser Sonderfunktion in Klammern dahinter!)');
				}
				
				if(pspl[0] == "IgelchenM"){
					user.sendPrivateMessage('Okay, den hast auch gefunden... °>sm_10.gif<° -- aber hast Du auch alle 9 anderen Ostereier gefunden?');
					Bot.sendPublicMessage('IgelchenM ist auch bekannt als Olli *funfact*.');
				}
*/
			}
	};
	
	//Changelog
		App.chatCommands.chlog = function (user, command) {

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;


		changelog = [];
			changelog[0] = "°-°_*Changelog*_°-°";
			changelog[1] = "_25.03.2017_ | Channelapp wird erstmals als HZM-App deklariert.";
			changelog[2] = "_26.03.2017_ | HZM-App einiger Optimierungen (AppPersistence) unterzogen und Quelltext etwas gekürzt.";
			changelog[3] = "_27.03.2017_ | HZM-App wird auf Entwickler-Treffen vorgestellt.";
			changelog[4] = "_28.03.2017_ | HZM-App wird in BETA und ReleaseCandidate getrennt.";
			changelog[5] = "_29.03.2017_ | HZM-App bekommt diverse Usergruppen. Weiterhin die Generalvollmacht für Admins an vielen Stellen gekillt. Dort dem ChannelOwner die Möglichkeit eingeräumt, selbst Rechte zu vergeben.";
			changelog[6] = "_29.03.2017_ | _21:35 Uhr_ | die Funktion _°>/listcommands|/listcommands<°_ eingeführt. Diese zeigt die wichtigsten Befehle der jeweiligen Zulassungsgruppe in der sich der ausführende User befindet.";
			changelog[7] = "_30.03.2017_ | _11:00 Uhr_ | HZM-APP hat nun eine BlackList bekommen. Channelzutrittskontrolle um die Blacklist erweitert.";
			changelog[8] = "_30.03.2017_ | _13:00 Uhr_ | ReleaseCandidate allen HZM zur Installation freigegeben.";
			changelog[9] = "_30.03.2017_ | _14:00 Uhr_ °-°ChannelOwner hat ab sofort die Möglichkeit, den angezeigten SPERRTEXT im CM-Modus selbst zu definieren. Mit _/restricttext TEXT_ wird TEXT als Meldung bei nicht zugelassenen Usern angezeigt. Mit _°>/restrictiontext|/restrictiontext<°_ wird der aktuell gesetzte Text angezeigt.°#°ist KEIN Text definiert, so wird der folgende Text angezeigt (RC, NICHT BETA!):°#°\"Dieser Channel wurde durch den Channelinhaber so eingestellt, dass ausschließlich explizit zugelassene User den Channel betreten können. {zwei Zeilenumbrüche} Um eine Zutrittsberechtigung zu diesem Channel zu bekommen, wende Dich bitte an den Channeleigentümer.\"°-°";
			changelog[10] = "_30.03.2017_ | _15:10 Uhr_ °-°Dem Channelinhaber nun die Möglichkeit eingeräumt, auch den Text für den \"geschlossenen Channel\" zu ändern. Mit _/closedtext TEXT_ definiert der CO nun den Text. Mit _°>/showclosedtext|/showclosedtext<°_ wird der aktuell gesetzte Text angezeigt.°#°_AUSNAHME:_ Wenn noch kein Text hierfür definiert wurde, wird das Array LEER übergeben. Es wird DANN die folgende Meldung für den verschlossenen Channel angezeigt:°#°\"_Der Channel ist aktuell °R°GESCHLOSSEN!°r° {zwei Zeilenumbrüche} bitte versuche es später erneut._\"°-°";
			changelog[11] = "_31.03.2017_ | _18:30 Uhr_ °-°Kleinere Fehlerbehebungen vorgenommen...°-°";
			changelog[12] = "_01.04.2017_ | _22:15 Uhr_ °-°Ab sofort können HZM ihre eigene CME in der App speichern und nutzen! // Weiterhin einige Optimierungen der App vorgenommen.°-°";
			changelog[13] = "_02.04.2017_ | _15:00 Uhr_°-°Das Befehlswirrwar mal etwas entwirrt.°#°Aus vielen kleinen Befehlen einen großen gemacht.°#°Ab sofort werden Rollen im Channel per _/role set:ROLLE:Nick_ gesetzt.°#°Die Rolleninhaber zeigt man sich mit _/role list:ROLLE_ gesammelt an.°#°Jemandem die Rolle entziehen funktioniert mit _/role remark:ROLLE:Nick_°#°Befehls-HILFE hinzugefügt. _°>/role help|/role help<°_°-°";
			changelog[14] = "_02.04.2017_ | _20:12 Uhr_ °-°Weiter aufgeräumt. Darunter folgende Dinge:°#°- Blacklist-Funktion zusammengefasst. Zusätzlich _°>/blacklist help|/blacklist help<°_ für die korrekte Syntax angelegt.°#°- _/channelthema TEXT_ repariert.°#°- CME-Funktion optimiert°-°";
			changelog[15] = "_02.04.2017_ | _22:05 Uhr_ °-°Die Verwarnungsstufen sauber in _eine Funktion_ gepackt. Näheres hierzu mit _°>/warn help|/warn help<°_°-°";
			changelog[16] = "_02.04.2017_ | _22:30 Uhr_ °-°Quellcode etwas aufgeräumt. Alte, nicht mehr genutzte Funktionen (da in neuen Funktionen vereint) entfernt.°-°";
			changelog[17] = "_06.04.2017_ | _11:30 Uhr_ °-°Stress-User-Liste umgebaut. ab sofort alles per _°>/sul|/sul<°_ erreichbar.°#°Näheres hierzu mit _°>/sul help|/sul help<°_.°-°";
			changelog[18] = "_06.04.2017_ | _12:30 Uhr_ °-°Changelog aufgeräumt. Nun einfacher zu pflegen.°#°°#°Weiterhin nun Versionenangabe GLOBAL angelegt. So kann durch Änderung einer einzelnen Variable nun zwischen ALPHA, BETA und ReleaseCandidate geswitched werden. Dies macht es in der App-Pflege einfacher. Hier werden auch Befehle im Teststadium nicht in der RC mit geladen, obwohl sie aufgrund des identischen Quelltextes vorhanden sind. (was sehr von Vorteil ist!)°-°";
			changelog[19] = "_06.04.2017_ | _13:11 Uhr_ °-°Funktion _°>/listcommands|/listcommands<°_ aufgeräumt und aktuatlisiert.°-°";
			changelog[20] = "_06.04.2017_ | _13:22 Uhr_ °-°/say - Funktion nun auch in neue Funktion umgebaut. Näheres siehe _°>/say help|/say help<°_°-°";
			changelog[21] = "_13.04.2017_ | _13:22 Uhr_ °-°Kleinere Optimierungen und Einbau einiger kleinerer Ostereier inclusive Mini-Gewinnspiel für die Finder der Ostereier.°-°";
			changelog[22] = "_Version 3.3_ °-°- Weitere Optimierungen am Quellcode°#°- Einbau der Massen-Hinzufügung und -Entfernung von Usern (CM/HZM/VCM/Gast)°#°- JIT-Iconsetzung aufgrund von Fehlern in der Interpretation des Servers im Sinne der JITIS deaktiviert. °-°";
			changelog[23] = "_Version 3.5_ °-°- kleinere Fehlerbehebungen°#°- Bot-Schriftart nun per _°>/schriftart|/schriftart<°_ vom Channel-EIGENTÜMER wählbar.°#°- Begrüßungen per _°>/greetings|/greetings<°_ aus und einschaltbar. °-°";
			changelog[24] = "_Version 3.6_ °-°Ab sofort kann jeder Nutzer seinen eigenen Begrüßungstext bei eingeschalteter Bot-Begrüßung selbst festsetzen.°-°";
			changelog[25] = "_Version 3.7_ °-°Sonderfunktionen die °>/disco|/disco<° und °>/confetti|/confetti<° in der Nutzung steuerbar. Hierzu gibts nun die folgenden Einstellmöglichkeiten: _°>/sofu spielerei:on|/sofu spielerei:on<°_ zum aktivieren dieser beiden Funktionen (Standardeinstellung!), _°>/sofu spielerei:off|/sofu sielerei:off<°_ um diese beiden Funktionen zu _deaktivieren_. und _°>/sofu spielerei:HZM|/sofu spielerei:HZM<°_ um diese beiden Funktionen auf die HZM zu beschränken.°#°°#°Blacklist nun deaktivierbar. Einschalten: _°>/sofu blacklist:on|/sofu blacklist:on<°_ und Ausschalten: _°>/sofu blacklist:off|/sofu blacklist:off<°_ °#°Wunschfunktion _°>/matchico|/matchico<°_ wieder eingeführt.°##°Ab sofort kann sich _jeder_ seine eigene Begrüßung selbst eintragen. => _/greetings eigenerNick:TEXT_ °##°°-°";
			changelog[26] = "_Version 3.8_ °-°- App in Module unterteilt. HZM-Modul lässt sich mit dem Befehl °>/sofu hzm:on|/sofu hzm:on<° einschalten.°#°- /greetings - Funktion weiter ausgebaut. Genaueres siehe mit _°>/greetings|/greetings<°_°##°°-°";
			changelog[27] = "_Version 3.8_ °-°- HZA/E - in allen Funktionen und Hilfetexten nach HZM umbenannt... damit wieder im Rausch der Zeit unterwegs... °>sm_10.gif<°°##°°-°";
			changelog[28] = "_Version 3.9_ °-°- Der Bot kann nun auch Dinge tun... mit _°>/dobot TEXT|/dobot Text<°_ lässt man den Bot TEXT tun.°##°- Ab sofort kann man Zweitnicklisten anlegen (wenn HZM-Modul aktiv!). So fällt es den HZM und den CMs bei der Strafbemessung deutlich leichter...°##°_/nicklist {Nick}:add:Nick1,Nick2,Nick3,........NickN_ fügt die Nicks der Liste hinzu.°#°_/nicklist {NICK}:list_ zeigt alle eingetragenen Nicks.°#°Löschen aus der Liste geht analog zum Hinzufügen nur mit :del:°##°- °BB°_Ab sofort keine Dopplungen auf den Listen mehr möglich (Abfrage eingebaut!)_°r°°##°°-°";
			changelog[29] = "_Version 4.0_ °-°- Systemseitige Sonderfunktionsnachricht eingebaut°#°- Optimierungen bei den Listen°#°- unterschiedliche Anzeigemodi CM / VCM / HZM°#°- Eintragungen auf der Nickliste sind nun durch HZM zu prüfen und zu bestätigen. Bis zur Bestätigung stehen die durch CMs eingetragenen Nicks unter Vorbehalt mit dabei (extra aufgeführt!)°#°- Verknüpfung der °>/sul|/sul<° mit der °>/nicklist|/nicklist<° zur besseren Übersicht hergestellt.°#°- _HZM-App - Doku_ in Systemnachricht an alle HZM verlinkt zur Verfügung gestellt.°##°°-°";
			
			if(extver >= "4.1" || channel.toLowerCase() == DEVCHANNEL.toLowerCase()){
				changelog[30] = "_Version 4.1_ °-°- Automatisches Update der App bei neuer Version zu CMV-unüblichen Zeiten.°#°- Infonachricht im Demochannel beigefügt.°#°- Channel-Eigentümer kann den Link zur Doku nun ausblenden.°#°- HZM können die Begrüßungsnachricht nun auch ausblenden.°#°°BB°_beim Ausblenden der Nachrichten verbleibt ein INFO-Text, welcher die Möglichkeit bietet, die Nachrichten wieder 'wie üblich' zu aktivieren!°r°_°##°- Probleme bei der Begrüßung von Usern ohne eingetragene persönliche Begrüßung behoben.°#°- Einzelne weitere Bugs behoben°#°- HZM können nun selbst entscheiden welche Infonachrichten ihre CM zu sehen bekommen (einzeln aktivier- und deaktivierbar!).°r°°##°°-°";
			}
			
			if(extver >= "4.2" || channel.toLowerCase() == DEVCHANNEL.toLowerCase()){
				changelog[31] = "_Version 4.2_ °-°- Kleinere Korrekturen. U.A. zeigt die Nickliste nun auch Nicks der zu prüfenden Nicks für die Liste wenn die Hauptlistee leer sein sollte.°#°- /sul hat nun eine Prüfliste wie die bei der /nicklist bekommen. Somit können CM nun auch User auf die StressUserListe setzen, wobei die HZM entscheiden können/sollen, ob die User tatsächlich auf der richtigen (festen!) StressUserListe landen können/sollen.°#°- bei der /sul zusätzlich noch die Möglichkeit eingebaut, :add: anstatt :set: zu nutzen...°#°- Codefehler bei den Spielereien (/disco und /confetti) korrigiert°#°- Rundmail logischer gecodet (Generalreformkonzept!).°#°- Kartensystem eindeutiger gestaltet (Formatierung der (offiziellen) Ausgaben)°#°- Codeformatierung auf neue Definitionen angepasst.°#°- °>/role|/role<° revolutioniert (Codetechnisch)°##°°-°";
			}
			
			if(extver >= "4.3" || channel.toLowerCase() == DEVCHANNEL.toLowerCase()){
				changelog[32] = "_Version 4.3_ °-°- Ab sofort können auch Nicks mit Sonderzeichen jeglicher Facon in die Listen eingetragen werden.°#°- Einige weitere Bugs behoben.°#°- Zweitnicks des Programmierers für Rückfragen dem ChannelOwner per _°>/sofu sysarray|/sofu sysarray<°_ zur Verfügung gestellt.°#°- versehentliche Antworten an Bot werden nun den ChannelEigentümer übersandt soweit online. Zusätzlich wird ein INFO-Eintrag im Logger erzeugt.°#°- die °>/sofu sysarray|/sofu sysarray<° in soweit umgebaut, dass nun auf den jeweiligen Servern nur noch der Teil angezeigt wird, der tatsächlich korrekt ist (Serverspezifische Nicks!).°##°°-°";
			}
			
			if(extver >= "4.4" || channel.toLowerCase() == DEVCHANNEL.toLowerCase()){
				changelog[33] = "_Version 4.4_ °-°- Modulmanager hinzugefügt°#°-- UI für den Modulmanager hinzugefügt.°##°°-°";
			}

			
			
 		var pfad = KnuddelsServer.getFullImagePath('');
		var saschaus = '°>'+pfad+'sascha.gif<°';
				
					msg = "";
						for(i=0;i<changelog.length;i++){
						msg=msg + ' °#° '+ changelog[i] +' °#°'
						}
		
		user.sendPrivateMessage(msg);
		};
		
			var appPersistence = KnuddelsServer.getPersistence();
	var matchicofunc = appPersistence.getObject('matchicobeta', []);
	
//		if(matchicofunc == "on"){
		App.cmdmatchico = function(user){
			var appPersistence = KnuddelsServer.getPersistence();
			var isAdmin 		= user.isInTeam("Admin");
//			var matchicofunc	= appPersistence.getObject('matchicofunction', ['']);
/*			
			if(!user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
		}
*/

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;


		var appPersistence = KnuddelsServer.getPersistence();
		if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('sonderuser', []).indexOf(user.getNick()) < 0 && appPersistence.getObject('CMs', []).indexOf(user.getNick()) < 0 && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner() && !isAdmin){
			user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
			return;
			}
			
		var userAccess = KnuddelsServer.getUserAccess();
		var appPersistence = KnuddelsServer.getPersistence();
		
		if (userAccess.exists(user)){
				var ishzae = appPersistence.getObject('hzae', []);
//				ChanKey.push('offen');
				appPersistence.getObject('CMs', []);
					if (appPersistence.getObject('CMs', []).indexOf(user.getNick()) >= 0) {
						user.sendPrivateMessage('CM-Befehle:°-°°#°°>/sul|/sul<° => zeigt die aktuelle Stress-User-Liste.°#°°-°');
						return;
					}
				appPersistence.getObject('hzae', []);
					if (appPersistence.getObject('hzae', []).indexOf(user.getNick()) >= 0) {
						user.sendPrivateMessage('HZM-Dinge:°#°_°R°[Karten]°r°_ (HZM)§°#°°#°Auf der _°>/warn help|/warn help<°_ gibt es die genauen Informationen.°#°°#°°-°°#°_°R°[Zugangsbeschränkungsdinge]°r°_ (HZM)§°#°°#°_/role set:ROLLE:[Nick]_			=> Nick die ROLLE verleihen. Zum Beispiel um den Zutritt zum Channel zu gewähren (Channel im CM-Modus (s.o.))°#°Näheres hierzu auf der _°>/role help|/role help<°_°#°°#°°#°°#°_°R°[Stress-User-Liste]°r°_(Berechtigung in Klammer dahinter)§ °#°°#°_°>/sul|/sul<°_			=>	Stress-User-Liste aufrufen/anzeigen (CMs & HZM) °#°_/sul set:[Nick]_	=>	Nick auf die Stress-User-Liste setzen (HZM) °#°_/sul delete:[Nick]_	=>	Nick von der Stress-User-Liste entfernen. (HZM)');
//						return;
					}
/*					else if ( == "Closed") {
						user.sendPrivateMessage('Der _Channel_ ist aktuell °R°_abgeschlossen_§.');
						return;
					}
*/
				if(user.isChannelOwner()) {
					user.sendPrivateMessage('°#°_Channelbefehle für den Channelowner:_°#°°#°°#°_/restricttext TEXT_ => Channelsperrtext für den Channel-CM-Modus definieren.°#°°#°_°>/restrictiontext|/restrictiontext<°_ => zeigt den aktuell definierten Channelsperrtext für den im CM-Modus befindlichen Channel an. Ist keiner definiert, wird der vorgegebene Standardtext angezeigt.°#°°#°°#°_/closedtext TEXT_ => definiert den Anzeigetext bei Channelbetretversuch im Falle eines geschlossenen Channels.°#°°#°_°>/showclosedtext|/showclosedtext<°_ => zeigt den definierten Sperrtext. Ist keiner definiert, liefert dieser Befehl ein _leeres Array_ zurück. In diesem Falle ist der vorgegebene Standardtext aktiv (siehe _°>/chlog|/chlog<°_)°#°°#°_°>/schriftart|/schriftart<°_ ändert die Bot-Schriftart.°#°°#°_/greetings on/off_ schaltet die Nutzer-Begrüßung ein/aus.°-°');
				}
				user.sendPrivateMessage('°-°_Channelbefehle (Admins/ChannelOwner/AppManager):_°#°°#°_°>/opened|/opened<°_		=> Channel für jedermann öffnen (Standardeinstellung!)°#°_°>/usual|/usual<°_		=> Channel nur für explizit zugelassene Personen öffnen (CM-Modus für HZM und CMs) [Mehr unter \"Zugangsbeschränkungsdinge\"]°#°_°>/closed|/closed<°_		=> Channel abschließen. Dies hat zur Folge, dass _KEINER_ mehr den Channel betreten kann!°#°°#°_°>/chanstate|/chanstate<°_	=> Anfrage in welchem Status sich der Channel gerade befindet (Kontrolle damit man den Channel nicht verschlossen verlässt!)');
//				Bot.sendPublicMessage('°R°_*INFO*_ §Der °R°_Channel_§ ist nun °R°_für JEDERMANN geöffnet!_°r°');
				return;
		} else {
			user.sendPrivateMessage('Leider hast Du den Befehl falsch eingegeben. -- Keine Funktion!');
		}
		};
//	};
		
			//Blackliste
		App.cmdblacklist = function(user,params,func){

			var appPersistence = KnuddelsServer.getPersistence();
		
			var userAccess = KnuddelsServer.getUserAccess();
//			var appPersistence = KnuddelsServer.getPersistence();

		var srvID			= KnuddelsServer.getChatServerInfo().getServerId();
		var UserID			= KnuddelsServer.getUserAccess().getUserId(user.getNick());
	
		var userkennung		= UserID+'.'+srvID;

		
			if (userAccess.exists(user)){
				var pspl = params.split(':');
				var msg	 = "";
				
				if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelModerator() && !user.isAppManager() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isChannelOwner()){
					user.sendPrivateMessage('Du hast leider nicht die Berechtigung für diese Funktion.');
					return;
				}
				
				if(pspl[0] == "help"){
					user.sendPrivateMessage('Die Syntax für die BLACKLIST-Funktion lautet:°#°_/blacklist set:NICK_ um NICK dauerhaft aus dem Channel zu verbannen.°#°Mit _/blacklist show_ zeigst Du dauerhaft ausgesperrten Nutzer an.°#°Mit _/blacklist remark:NICK_ begnadigst Du NICK.');
					return;
				}
				
				if(pspl[0] == "set" && pspl[1] != undefined && pspl[1] != " "){
					if(appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0 && !user.isChannelModerator() && App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner()){
						user.sendPrivateMessage('Für diese Funktion musst Du _mindestens MCM_ oder App-Manager sein!');
						return;
						}
					
					var userAccess = KnuddelsServer.getUserAccess();
					
					var vaspl = pspl[1].split(',');
					
					for(i=0;i<vaspl.length;i++){
						msg=msg + vaspl[i]
					
						if(userAccess.exists(vaspl[i])){
							var users	= appPersistence.getObject('blacklisted', []);
							users.push(vaspl[i]);
							appPersistence.setObject('blacklisted', users);
							user.sendPrivateMessage('Ich habe '+vaspl[i]+' soeben eine _°R°dauerhafte Channelsperre°r°_ verpasst. °R°_°>Rückgängig|/blacklist remark:'+vaspl[i]+'<°_°r°');
						
						} else {
						user.sendPrivateMessage('Dieser User existiert nicht! -- Hast Du Dich vielleicht vertippt?');
						}
					}
					return;
				}
/*					else {						
					user.sendPrivateMessage('Ich kenne den Nutzer nicht.');
					return;
					}
*/
				if(pspl[0] == "show"){
/*					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelModerator() && !isAdmin){
						user.sendPrivateMessage('Für diese Funktion musst Du die HZM - Rolle haben.');
						return;
					}
*/					
					var bll = appPersistence.getObject('blacklisted', []);
				
					if(bll.length <= 0){
						user.sendPrivateMessage('Aktuell ist niemand dauerhaft ausgesperrt. °R°_°>Jemanden dauerhaft aussperren|/tf-overridesb /blacklist set:[James]<°_°r°');
						return;
					}
					
					var msg = 'Aktuell sind ausgesperrt: °#°';
				
					for(i=0;i<bll.length;i++){
						msg=msg + i+' | '+ bll[i] +' | °R°_°>Entfernen|/blacklist remark:'+ bll[i] +'<°_°r° °#°'
					}
					msg = msg+ '°#° °[000,175,225]°_°>Weiteren User dauerhaft aussperren|/tf-overridesb /blacklist set:[James]<°_°r°';
						user.sendPrivateMessage(msg);
						return;
				}
					
				if(pspl[0] == "remark" && pspl[1] != undefined && pspl[1] != " "){
					if(App.coDevs.indexOf(userkennung) == -1 && App.hiddenCoDev.indexOf(userkennung) == -1 && !user.isAppManager() && !user.isChannelOwner() && appPersistence.getObject('hzae', []).indexOf(user.getNick()) < 0){
						user.sendPrivateMessage('Begnadigen können nur der Channel-Eigentümer und der App-Manager.');
						return;
						}
						
					var bll = appPersistence.getObject('blacklisted', []);
				
					var vaspl = pspl[1].split(',');
					
					for(i=0;i<vaspl.length;i++){
						msg=msg + vaspl[i]
				
						if(bll.indexOf(vaspl[i]) == -1){
							user.sendPrivateMessage(vaspl[i] +' ist doch gar nicht ausgesperrt!)');
							return;
						}
					
						bll.splice(bll.indexOf(vaspl[i]), 1);
				
						appPersistence.setObject('blacklisted', bll);
				
						user.sendPrivateMessage('Ich habe _'+ vaspl[i] +'_ soeben _begnadigt_. °R°_°>Rückgängig|/blacklist set:'+ vaspl[i] +'<°_°r°');
					}
				} else {
					user.sendPrivateMessage('Du hast die Funktion falsch eingegeben. Die Syntax findest Du unter _°>/blacklist help|/blacklist help<°_');
				}
			}
		};
		
//	};
//	});